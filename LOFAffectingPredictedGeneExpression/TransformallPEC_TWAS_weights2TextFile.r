
mydir="/data/ruderferlab/resources/psychencode/PEC_TWAS_weights";
myFileList=dir(mydir, pattern=NULL,full.names=FALSE);
print(length(myFileList));
print(myFileList[1:5]);
OutDir="/data/ruderferlab/resources/psychencode/PEC_TWAS_weights_Text";
for(ii in seq(1,length(myFileList)))
{
  inputfileName=paste(mydir,"/",myFileList[ii],sep="");
print (inputfileName);
  load(inputfileName, ex<-new.env());
  ls.str(ex)
  ExList=as.list(ex); 
  TWAS_Weight=as.data.frame(ExList$wgt.matrix);
  #colnames(TWAS_Weight)=c("top1","blup","bslmm","lasso","enet");
  OutfileName= gsub("RDat","txt",myFileList[ii]);  
  OutFullFileName=paste(OutDir,"/", OutfileName,sep="");
  write.table(TWAS_Weight,OutFullFileName,sep="\t",row.names=TRUE, quote=FALSE);
}
