#Merge Psychencode based predicted gene expression data with RNASeq  actual gene expression data 
#!/usr/bin/perl -w
 use strict;
 use warnings;

#psychencode gene level correlation
#/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMC_ACC_top1_PsychencodeWeightGeneSpearmanCorr.txt
#/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMC_DLPFC_top1_PsychencodeWeightGeneSpearmanCorr.txt
#/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSHBCC_DLPFC_top1_PsychencodeWeightGeneSpearmanCorr.txt,
#/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMC_ACC_blup_PsychencodeWeightGeneSpearmanCorr.txt
#/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMC_DLPFC_blup_PsychencodeWeightGeneSpearmanCorr.txt  
#/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSHBCC_DLPFC_blup_PsychencodeWeightGeneSpearmanCorr.txt
 #Two tissue gene level correlation
 #/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/Brain13CorrelationBetweenActual_PredictedGeneExpression/WGSCMC_ACC_ACC_BA24_V8SingleTissueGeneSpearmanCorr.txt 
# /data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/Brain13CorrelationBetweenActual_PredictedGeneExpression/WGSCMC_DLPFC_PFC_BA9_V8SingleTissueGeneSpearmanCorr.txt 
# /data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/Brain13CorrelationBetweenActual_PredictedGeneExpression/WGSHBCC_DLPFC_PFC_BA9_V8SingleTissueGeneSpearmanCorr.txt



my @TissueColumnNum=(13,12,12);
my @TwoTissues=("ACC_BA24","PFC_BA9","PFC_BA9");
my @ActualGeneExpression=("CMC_ACC","CMC_DLPFC","HBCC_DLPFC");
my @TwoPsychencodeAlgorithms=("top1","blup");

my @PredictionMethods=("top1","blup","bslmm","lasso","enet");

 for(my $iT=0;$iT<3;$iT++)#3
{

    my $iLineNum=0;
   my %GeneID_SpearmanCorr=();
   my $SingleTissueCorrFile="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/Brain13CorrelationBetweenActual_PredictedGeneExpression/WGS$ActualGeneExpression[$iT]_$TwoTissues[$iT]_V8SingleTissueGeneSpearmanCorr.txt";
   open(INPUT,$SingleTissueCorrFile) || die "can’t open $SingleTissueCorrFile";
   while (<INPUT>)
   {
        chomp;
        if($iLineNum==0)
        { 
          $iLineNum++;
          next;
        }
        my @cols=split(/\s++/,$_);
        $GeneID_SpearmanCorr{$cols[0]}=$cols[1];    
     }
    close INPUT;
    for(my $iA=0;$iA<@PredictionMethods;$iA++)
    {
      $iLineNum=0;
      my $psychencodeCorrFile="/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGS$ActualGeneExpression[$iT]_$PredictionMethods[$iA]_PsychencodeWeightGeneSpearmanCorr.txt";
      open(INPUT,$psychencodeCorrFile) || die "can’t open $psychencodeCorrFile";
      while (<INPUT>)
      {
        chomp;
        if($iLineNum==0)
        { 
          $iLineNum++;
          next;
        }
        my @cols=split(/\s++/,$_);
        if(defined($GeneID_SpearmanCorr{$cols[0]}))
        {
         $GeneID_SpearmanCorr{$cols[0]}.="\t$cols[1]";
        }
      }
      close INPUT;
    }
  
    my $OutFile="/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGS$ActualGeneExpression[$iT]_GeneLevelCorrPsyCode_V8SingleTissue_CEUWithoutOutliers.txt";
    open OUT, ">$OutFile" or die "Can't open Output file:$OutFile!"; 
    print OUT "GeneID\tV8SignleTissue\tPsyCode_top1\tPsyCode_blup\tPsyCode_bslmm\tPsyCode_lasso\tPsyCode_enet\n";
    
   foreach my $GeneID (keys %GeneID_SpearmanCorr)
     { 
          my @CorrList=split(/\t/,$GeneID_SpearmanCorr{$GeneID});
          my $iSize=scalar(@CorrList);     
          if($iSize==6)
         { 
           print OUT "$GeneID\t$GeneID_SpearmanCorr{$GeneID}\n";
         }
      }  
   close OUT;
 }
