 #!/usr/in/perl -w
  use strict;
  use List::Util qw( min max );
  use warnings;
  use Data::Dumper;
  use lib "/home/hanl3/perl5/module/lib/site_perl/5.14.2";
  use Text::CSV_PP;
 

  #Based on the chromosome that each gene located, all files are classified.
       my $iNumberofFiles=0;
       my $dir ="/data/ruderferlab/resources/psychencode/PEC_TWAS_weights_Text";
       my %Chromosome_FileNameList=();#Based on the chromosome that each gene located, all files are classified.
        opendir(DIR, $dir) or die $!;
        while (my $file = readdir(DIR))
        {# Use a regular expression to ignore files beginning with a period
         next if ($file =~ m/^\./);
         my  $iLineNum=0;
          my $InputFile="$dir/$file";
          open(INPUT,$InputFile) || die "can’t open $InputFile";
          while (<INPUT>)
          {
            if($iLineNum==0)
            {
              $iLineNum++;
             next;
            }
           last if($iLineNum>1);
            my @cols=split(/\t/,$_);
            my @Chr_Position=split(/:/,$_);
            my $GeneID=$file;
            $GeneID =~ s/\.wgt\.txt//ig;
            if(!defined($Chromosome_FileNameList{$Chr_Position[0]}))
            {
              $Chromosome_FileNameList{$Chr_Position[0]}=$GeneID;
            }
           else
           {
              $Chromosome_FileNameList{$Chr_Position[0]}.=":$GeneID";
           }
           $iLineNum++;
         }
       close INPUT;
      $iNumberofFiles++;
     }
    closedir(DIR);
      print "Finishe reading the file: /data/ruderferlab/resources/psychencode/PEC_TWAS_weights_Text\n";
  

 
      my @Batches=("10073_B01_GRM_WGS_2016-02-25","11694_B01_GRM_WGS_2017-08-18","11154_B01_GRM_WGS_2016-03-17");
   for(my $iBatch=2;$iBatch<3;$iBatch++)
   {
    my $PredictedExpressionFile="/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/SKL_$Batches[$iBatch]PsychodeBestPredictionExpression.txt";
    open OUTPUT, ">$PredictedExpressionFile" or die "Can't open Output file:$PredictedExpressionFile";
    for(my $iChr=1;$iChr<=22;$iChr++)
     {
      print "Analysis are on Chr$iChr\n";
      my $Index_SNP=0;
      my  $iLineNum=0;
       my @AllIndIDs=();
       my %PESID_dosageIndex=();
      #Read dosage file and weight file, obtain predicted gene expression
      my @AllIndDosageArray=();
      my $WGSFile="/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/SKL_$Batches[$iBatch].recalibrated_SNVs_PassFilterWholeGenome.dosage";
      open(INPUT,$WGSFile) || die "can’t open $WGSFile";
      while (<INPUT>)
      {
         chomp;
         my @cols=split(/\t/,$_,6);
         if($iLineNum==0)
         {
            my @cols=split(/\t/,$_);
            splice(@cols,0,5);
            @AllIndIDs=@cols;     
            $iLineNum++;
            next;
         }
          next if($_ !~ /^$iChr\t/);         
          $PESID_dosageIndex{$cols[2]}=$Index_SNP;
          $Index_SNP++; 
         # last if($Index_SNP>1000); 
      }      
      close INPUT; 
     print "Finish reading dosage file:$WGSFile\n"; 
      my @AllGenesFiles_Chr=split(/:/,$Chromosome_FileNameList{$iChr});
      my %GeneID_TotalSNPs=();
      my %GeneID_EffectiveSNPs=();
      for(my $kk=0;$kk<@AllGenesFiles_Chr;$kk++)
      {
          my $iNumberofSNPs=0;
          my $NumberofEffectiveSNPs=0;
          my $ScoreFile="/data/ruderferlab/resources/psychencode/PEC_TWAS_weights_Score/$AllGenesFiles_Chr[$kk].wgt.score";
          open(INPUT,$ScoreFile) || die "can’t open $ScoreFile";
          while (<INPUT>)
          {
            chomp;
           my @cols=split(/\s++/,$_);
            if(defined($PESID_dosageIndex{$cols[0]}))
            {
             $NumberofEffectiveSNPs++;
            }
           $iNumberofSNPs++;
           }
           close INPUT;  
           $GeneID_EffectiveSNPs{$AllGenesFiles_Chr[$kk]}=$NumberofEffectiveSNPs;
           $GeneID_TotalSNPs{$AllGenesFiles_Chr[$kk]}=$iNumberofSNPs;
         }
         print "Finsh reading score file\n";

        my $NumberofInds=scalar(@AllIndIDs);#Number of individuals

        if($iChr==1)
        {
          print OUTPUT "Chr\tBatches\tNumberofInds\tGeneName\tNumberofSNPs\tNumberofTotalSNPs"; 
          for(my $iInd=0;$iInd<@AllIndIDs;$iInd++)
          {       
           print OUTPUT "\t$AllIndIDs[$iInd]";
          }       
          print OUTPUT "\n";
        }
         my $number_of_rows=@AllIndDosageArray;#Number of Rows
        # my $number_of_columns=@{$AllIndDosageArray[0] };#Number of columns
        # print "$number_of_rows\t$number_of_columns\t$Index_SNP\t$NumberofInds\n";
        #Estimate each gene *individual gene expression

        my @AllRelatedGeneList=(); #Gene list with profile;
        my %WGSID_GeneExpression=();
         for(my $kk=0;$kk<@AllGenesFiles_Chr;$kk++) 
         {  
           my $InputFile="/data/ruderferlab/resources/psychencode/PEC_TWAS_weights_Profile/$Batches[$iBatch]_$AllGenesFiles_Chr[$kk].txt.profile";
           if (-e $InputFile) 
           {
              my $iLineNum=0;
              push   @AllRelatedGeneList, $AllGenesFiles_Chr[$kk];  
              open(INPUT,$InputFile) || die "can’t open $InputFile"; 
              while (<INPUT>)
              {
               $_ =~ s/^\s+|\s+$//g;      
              if($iLineNum==0)
              {       
                $iLineNum++;
                next;   
              } 
               my @cols=split(/\s+/,$_);
               my $iSize=scalar(@cols);
               if($iSize>=7)
               { 
                print "Long Row $_\n";
               }
               if(!defined($WGSID_GeneExpression{$cols[1]}))
               {
                $WGSID_GeneExpression{$cols[1]}=$cols[5];
               }
               else
               {
                $WGSID_GeneExpression{$cols[1]}.="\t$cols[5]";
              }
           }
           close INPUT;
         }  
       }#end loop for @AllGenesFiles_Chr 
       print "Finish reading profile file\n";
          my @AllIndGeneExpresson=();     
          for(my $iInd=0;$iInd<@AllIndIDs;$iInd++)
           {
             if(!defined($WGSID_GeneExpression{$AllIndIDs[$iInd]}))
             {
                print "Undefined $AllIndIDs[$iInd]\n";
              } 
              my @EachIndGeneExpression=split(/\t/,$WGSID_GeneExpression{$AllIndIDs[$iInd]}); 
              push @AllIndGeneExpresson,[@EachIndGeneExpression];
            }
            for(my $iGene=0;$iGene<@AllRelatedGeneList;$iGene++)
           {
             print OUTPUT "$iChr\t$Batches[$iBatch]\t$NumberofInds\t$AllRelatedGeneList[$iGene]\t$GeneID_EffectiveSNPs{$AllRelatedGeneList[$iGene]}\t$GeneID_TotalSNPs{$AllRelatedGeneList[$iGene]}"; 
             for(my $iInd=0;$iInd<@AllIndIDs;$iInd++)
             { 
               print  OUTPUT "\t$AllIndGeneExpresson[$iInd][$iGene]";
              } 
              print OUTPUT "\n";
            }
        }#end loop for Chr==1..22 
       close OUTPUT;
  } 

