 #!/usr/in/perl -w
  use strict;
  use List::Util qw( min max );
  use warnings;
  use Data::Dumper;
  use lib "/home/hanl3/perl5/module/lib/site_perl/5.14.2";
  use Text::CSV_PP; 

  my $iSpecificBatch=$ARGV[0];#[0..2]
  my $iClass=$ARGV[1];#[0..49]




  my $iNumberofFiles=0;

#/home/hanl3/cmc/data/wgs/vcf/SKL_11694_B01_GRM_WGS_2017-08-18.recalibrated_SNVs_PassFilterWholeGenome
#/home/hanl3/cmc/data/wgs/vcf/SKL_11154_B01_GRM_WGS_2016-03-17.recalibrated_SNVs_PassFilterWholeGenome
#/home/hanl3/cmc/data/wgs/vcf/SKL_10073_B01_GRM_WGS_2016-02-25.recalibrated_SNVs_PassFilterWholeGenome

my @WGSBatches=("11694_B01_GRM_WGS_2017-08-18","11154_B01_GRM_WGS_2016-03-17","10073_B01_GRM_WGS_2016-02-25");
#plink --bfile /home/hanl3/cmc/data/wgs/vcf/SKL_10073_B01_GRM_WGS_2016-02-25.recalibrated_SNVs_PassFilterWholeGenome  -score  /data/ruderferlab/resources/psychencode/PEC_TWAS_weights_Score/ENSG00000028839.wgt.score 1 2 4 --out /data/ruderferlab/resources/psychencode/PEC_TWAS_weights_Score/SKL_10073_B01_GRM_WGS_2016-02-25_ENSG00000028839


 for(my $iBatch=$iSpecificBatch;$iBatch<($iSpecificBatch+1);$iBatch++)
{
  my $plinkBinaryFile="/home/hanl3/cmc/data/wgs/vcf/SKL_$WGSBatches[$iBatch].recalibrated_SNVs_PassFilterWholeGenome"; 
  #Based on the chromosome that each gene located, all files are classified.
   my $dir ="/data/ruderferlab/resources/psychencode/PEC_TWAS_weights_Score";
   opendir(DIR, $dir) or die $!;
   while (my $file = readdir(DIR)) {
        # Use a regular expression to ignore files beginning with a period
        next if ($file =~ m/^\./);
        $iNumberofFiles++; 
       my $iStartClass=$iClass*295+1;
       my $iEndClass=($iClass+1)*295;
      if($iNumberofFiles>=$iStartClass && $iNumberofFiles<=$iEndClass)
       {
        my  $InputFile="$dir/$file";
	my  $GeneID=$file; 
        #ENSG00000100129.wgt.score
        $GeneID=~ s/\.wgt\.score//ig;
        my $OutputFile="/data/ruderferlab/resources/psychencode/PEC_TWAS_weights_Profile/$WGSBatches[$iBatch]_$GeneID.txt";
        my $command= "plink --bfile $plinkBinaryFile  -score $InputFile 1 2 4 --out $OutputFile";  
        system($command);     
         print "Have Finished files:$iNumberofFiles\n";
       }
    }
    closedir(DIR);
}
