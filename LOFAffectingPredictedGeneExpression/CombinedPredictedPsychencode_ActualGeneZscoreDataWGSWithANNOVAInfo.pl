#Merge Psychencode based predicted gene expression data with RNASeq  actual gene expression data 
#!/usr/bin/perl -w
 use strict;
 use warnings;

# gene level spearman corr file
   #/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMC_ACC_best_PsychencodeWeightGeneSpearmanCorr.txt
   # /data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSHBCC_DLPFC_best_PsychencodeWeightGeneSpearmanCorr.txt
   # /data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMC_DLPFC_best_PsychencodeWeightGeneSpearmanCorr.txt
#Annotation file
   # /data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/SKL_10073_B01_GRM_WGS_2016-02-25LOFVarinatInfo_synonymousSNV.filter.RareGType.txt
   #/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/SKL_11154_B01_GRM_WGS_2016-03-17LOFVarinatInfo_synonymousSNV.filter.RareGType.txt
   #/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/SKL_11694_B01_GRM_WGS_2017-08-18LOFVarinatInfo_synonymousSNV.filter.RareGType.txt

#INPUT Files:
#      Standardized actual gene expression
#     /data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMC_ACC_best_PsychencodeWeightGeneActualExpressionZscore.txt
#     /data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMC_DLPFC_best_PsychencodeWeightGeneActualExpressionZscore.txt
#     /data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSHBCC_DLPFC_best_PsychencodeWeightGeneActualExpressionZscore.txt
#   Standardized predicted gene expression
#     /data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMC_ACC_best_PsychencodeWeightGenePredictedExpressionZScore.txt
#     /data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMC_DLPFC_best_PsychencodeWeightGenePredictedExpressionZScore.txt
#     /data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSHBCC_DLPFC_best_PsychencodeWeightGenePredictedExpressionZScore.txt


    my $iLineNum=0;
    my %WGSID_Dx=();
    my $CMCIDInfoFile="/data/ruderferlab/projects/cmc/files/CMC-WGSID_NoRNASeqDuplicateV2.map";# WGS.DataID[9], updated.dlpfcID[12]updated.accID[13]
    open(INPUT,$CMCIDInfoFile) || die "can’t open $CMCIDInfoFile";
    while (<INPUT>)
    {
        chomp;
        if($iLineNum==0)
        {
          $iLineNum++;
          next;
        }
        my @cols=split(/\s++/,$_);
        $WGSID_Dx{$cols[9]}=$cols[7];
    }
    close INPUT;
    my $size=keys %WGSID_Dx;


my @MergedGeneExpression=("WGSCMC_ACC","WGSCMC_DLPFC","WGSHBCC_DLPFC");

my @PredictedGeneExpessioniPsychencodeWeights=(["SKL_10073_B01_GRM_WGS_2016-02-25"],["SKL_10073_B01_GRM_WGS_2016-02-25"],["SKL_11694_B01_GRM_WGS_2017-08-18","SKL_11154_B01_GRM_WGS_2016-03-17"]);
 for(my $iT=0;$iT<3;$iT++)#3
{
   my $OutFile="/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/$MergedGeneExpression[$iT]_PsychencodeWeightZScore_AnnovarMergedCEUBestAlgorithmWithoutOutliers.txt";
   open OUT, ">$OutFile" or die "Can't open Output file:$OutFile!";
    my $NumberofSelectedInds=0;
    my $iLineNum=0;
    my %GenesWithAnnovar=();
    my %IndividualsWithAnnovar=();
    for(my $iTissueType=0;$iTissueType<@{$PredictedGeneExpessioniPsychencodeWeights[$iT]};$iTissueType++)
     {
             $iLineNum=0;
      #/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/SKL_10073_B01_GRM_WGS_2016-02-25LOFVarinatInfo_synonymousSNV.filter.RareGType.txt
            my $AnnovarInfoFile="/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/$PredictedGeneExpessioniPsychencodeWeights[$iT][$iTissueType]LOFVarinatInfo_synonymousSNV.filter.RareGType.txt";
#Chr     Pos_vcf Ref_vcf Alt_vcf Start_Annovar(5)   End_Annovar     Ref_Annovar     Alt_Annovar     Func_ref        Gene_ref(10)        ExonicFunc_ref  AAChange_ref    Func_ens        Gene_ens        CADD_phred(15)      gnomAD_genome_ALL       NumTotal        NumOther        NumMissing      fMissingRate(20)    fAlleleFreq     LineSize        WGSIDWithRareAllele   RareGtype(24)
            open(INPUT, $AnnovarInfoFile) || die "can’t open $AnnovarInfoFile";
            while (<INPUT>)
            {
               chomp;
                my @cols=split(/\t/,$_);
               if($iLineNum==0)
               {
                 $iLineNum++;
                 next;
               }
              $GenesWithAnnovar{$cols[13]}=1;#cols[13]->Gene_ens 
              my @AllIndList=split(/:/,$cols[22]);
              for(my $iInd=0;$iInd<@AllIndList;$iInd++)
              {
               $IndividualsWithAnnovar{$AllIndList[$iInd]}=1;#WGSIDWithRareAllele->cols[22];
              }
          }
          close INPUT;
       }
        my $IndsizeWithANNOVAR=keys %IndividualsWithAnnovar;
        my $GeneSizeWithAnnovar=keys %GenesWithAnnovar;
       print "IndWithAnnovarSize:$IndsizeWithANNOVAR\tGeneSizeWithAnnovar: $GeneSizeWithAnnovar\n";
#/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMC_ACC_best_PsychencodeWeightGeneSpearmanCorr.txt
            my %GeneID_SpearmanCorr=();
            $iLineNum=0;
            my $GeneLevelCorrFile="/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/$MergedGeneExpression[$iT]_best_PsychencodeWeightGeneSpearmanCorr.txt"; 
            open(INPUT, $GeneLevelCorrFile) || die "can’t open $GeneLevelCorrFile";
            while (<INPUT>)
            {       
               chomp;  
                my @cols=split(/\s++/,$_);
               if($iLineNum==0)
               {       
                 $iLineNum++;
                 next;   
               }
             if(defined($GenesWithAnnovar{$cols[0]}))
              {
                $GeneID_SpearmanCorr{$cols[0]}=$cols[1];
              }       
          }       
          close INPUT;
         my $GeneSizeWithSpearmanCorr=keys %GeneID_SpearmanCorr;
         print "GenesizeWithCorr:$GeneSizeWithSpearmanCorr\n";
# Standardized predicted gene expression
#     /data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMC_ACC_best_PsychencodeWeightGenePredictedExpressionZScore.txt
      my %GeneListInPredictedEx=();
      my %IndListInPredictedEx=();
      my @PredictedGeneExpression=();

            $iLineNum=0;
            my $PredictedGeneExpressionFile="/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/$MergedGeneExpression[$iT]_best_PsychencodeWeightGenePredictedExpressionZScore.txt";
            open(INPUT, $PredictedGeneExpressionFile) || die "can’t open $PredictedGeneExpressionFile";
            while (<INPUT>)
            {
               chomp;
                my @cols=split(/\s++/,$_);
               if($iLineNum==0)
               {
                 $iLineNum++;
                 splice(@cols,0,2);
                 for(my $ii=0;$ii<@cols;$ii++)
                 {
                  $GeneListInPredictedEx{$cols[$ii]}=$ii;
                  }
                 next;
               }
               $IndListInPredictedEx{$cols[0]}=$iLineNum-1;
               splice(@cols,0,2);                
               push @PredictedGeneExpression,[@cols];
              $iLineNum++;              
          }
          close INPUT;
# Standardized actual gene expression
#     /data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMC_ACC_best_PsychencodeWeightGeneActualExpressionZscore.txt
           my %GeneListInActualEx=();
           my %IndListInActualEx=();
           my @ActualGeneExpression=();
          
           $iLineNum=0;
           my $ActualGeneExpressionFile="/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/$MergedGeneExpression[$iT]_best_PsychencodeWeightGeneActualExpressionZscore.txt";
           open(INPUT, $ActualGeneExpressionFile) || die "can’t open $ActualGeneExpressionFile";
           while (<INPUT>)
            {
               chomp;
                my @cols=split(/\s++/,$_);
               if($iLineNum==0)
               {
                 $iLineNum++;
                 splice(@cols,0,2);
                 for(my $ii=0;$ii<@cols;$ii++)
                 {
                  $GeneListInActualEx{$cols[$ii]}=$ii;
                  }
                 next;
               }
               $IndListInActualEx{$cols[0]}=$iLineNum-1;
               splice(@cols,0,2);
               push @ActualGeneExpression,[@cols];
              $iLineNum++;
          }
          close INPUT;

       for(my $iTissueType=0;$iTissueType<@{$PredictedGeneExpessioniPsychencodeWeights[$iT]};$iTissueType++)
        {
            $iLineNum=0;
            my $AnnovarInfoFile="/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/$PredictedGeneExpessioniPsychencodeWeights[$iT][$iTissueType]LOFVarinatInfo_synonymousSNV.filter.RareGType.txt";
            open(INPUT, $AnnovarInfoFile) || die "can’t open $AnnovarInfoFile";
            while (<INPUT>)
            {
               chomp;
                my @cols=split(/\t/,$_);
               if($iLineNum==0)
               {
                 $iLineNum++;
                 if($iTissueType==0)
                 {
                 print OUT "$_\tActualExZScore\tPredictedZScore\tSpearmanCorr\tDx\n";
                 }
                 next;
               }
               my $GeneID=$cols[13];
              $GenesWithAnnovar{$cols[13]}=1;#cols[13]->Gene_ens
              my @AllIndList=split(/:/,$cols[22]);
              my @AllIndGTypeList=split(/:/,$cols[23]);
              for(my $iInd=0;$iInd<@AllIndList;$iInd++)
              {
                for(my $ii=0;$ii<22;$ii++)
                {
                 print OUT "$cols[$ii]\t";
                 }
                print OUT "$AllIndList[$iInd]\t$AllIndGTypeList[$iInd]";
                 my $WGSID=$AllIndList[$iInd];
                if(defined($GeneListInActualEx{$GeneID}) && defined($IndListInActualEx{$WGSID}))
                {
                  my $value="NA";
                  if($ActualGeneExpression[$IndListInActualEx{$WGSID}][$GeneListInActualEx{$GeneID}] ne "NA")
                  {
                     $value=sprintf("%.4f",$ActualGeneExpression[$IndListInActualEx{$WGSID}][$GeneListInActualEx{$GeneID}]);
                   }  
                print OUT  "\t$value";
                # print OUT  "\t$ActualGeneExpression[$IndListInActualEx{$WGSID}][$GeneListInActualEx{$GeneID}]";
                }
                else
                {
                print OUT "\tNA";
                }
                if(defined($GeneListInPredictedEx{$GeneID}) && defined($IndListInPredictedEx{$WGSID}))
                {
                 my $value="NA";
                 if($PredictedGeneExpression[$IndListInPredictedEx{$WGSID}][$GeneListInPredictedEx{$GeneID}] ne "NA")
                 {
                   $value=sprintf("%.4f",$PredictedGeneExpression[$IndListInPredictedEx{$WGSID}][$GeneListInPredictedEx{$GeneID}]);
                 }
                 print OUT "\t$value";
                } 
                else
                {  
                print OUT "\tNA";
                }
                if(defined($GeneID_SpearmanCorr{$GeneID}))
                {
                 my $value="NA";
                 if($GeneID_SpearmanCorr{$GeneID} ne "NA")
                 {
                   $value=sprintf("%.4f",$GeneID_SpearmanCorr{$GeneID});
                 }
                 print OUT "\t$value";
                }
                else
                {
                 print OUT "\tNA";
                }
               print OUT "\t$WGSID_Dx{$WGSID}\n";
              }
          }
          close INPUT;
        }
      close OUT;
      }
