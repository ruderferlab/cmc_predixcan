#Merge Psychencode based predicted gene expression data with RNASeq  actual gene expression data 
#!/usr/bin/perl -w
 use strict;
 use warnings;

  my %LOFVariantInfo=();#Select the loci with CADD or synomous variants 
  my $iLineNum=0;
  my $LOFVariantFile="/data/ruderferlab/projects/cmc/results/pseq_SKLWGS_Summary/allchroms.genome_annovar_exonicFun_LOFSubset_synonymousSNV.vcf";
  open(INPUT,$LOFVariantFile) || die "can’t open $LOFVariantFile";
  while (<INPUT>)
    {
      chomp;
      my @cols=split(/\t/,$_,3);
     $LOFVariantInfo{$cols[0]}{$cols[1]}=1;
     }
    close INPUT;


   my @OutlierWGSIndID=("MSSM-DNA-PFC-375","MSSM-DNA-PFC-269",
   "CMC-HBCC-DNA-ACC-6052","CMC-HBCC-DNA-ACC-5669","CMC-HBCC-DNA-ACC-4237","CMC-HBCC-DNA-ACC-5646",
    "CMC-HBCC-DNA-ACC-5777","CMC-HBCC-DNA-ACC-5682","CMC-HBCC-DNA-ACC-4284","CMC-HBCC-DNA-ACC-4137",
    "CMC-HBCC-DNA-ACC-6009","CMC-HBCC-DNA-ACC-6056","CMC-HBCC-DNA-ACC-4029","CMC-HBCC-DNA-ACC-4200",
    "CMC-HBCC-DNA-ACC-4074","CMC-HBCC-DNA-ACC-4204","CMC-HBCC-DNA-ACC-5654","CMC-HBCC-ACC-DNA-4235");

  my %hOutlierWGSID=();#Set outlier individuals
  for(my $ii=0;$ii<@OutlierWGSIndID;$ii++)
  {
    $hOutlierWGSID{$OutlierWGSIndID[$ii]}=1;
  }

#Selected CEU population and remove outlier individuals
   $iLineNum=0;
  my %CEUIndInfo=();#Make sure CEUs 
  my $InputFile="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/CMC_CAUSampleInformation.txt";;
  open(INPUT, $InputFile) || die "can’t open $InputFile";
  while (<INPUT>)
  {
        chomp;
        my @cols=split(/\s++/,$_,3);
        if($iLineNum==0)
        { 
          $iLineNum++;
          next;
        }
        next if(defined($hOutlierWGSID{$cols[1]}));#remove abnormal individuals
        $CEUIndInfo{$cols[1]}=1;#CEU WGSID 
        $iLineNum++;
    }
   close INPUT;
  my $Size=keys %CEUIndInfo;
  print "CEU Population Sample Size: $Size\n";

  #Combine all genes with Psychencode weight based predicted and actual gene expression
#/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMC_ACC_best_PsychencodeWeightGeneSpearmanCorr.txt  
#  /data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSHBCC_DLPFC_best_PsychencodeWeightGeneSpearmanCorr.txt
#/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMC_DLPFC_best_PsychencodeWeightGeneSpearmanCorr.txt

 my %CorrGeneList=();#Obtain gene list with spearman corr between predicted and actual expression
 my @GeneBasedSpearmanCorrList=("CMC_ACC","HBCC_DLPFC","CMC_DLPFC");
  for(my $iT=0;$iT<3;$iT++)
  {   
    my $iLineNum=0;
    my $GeneSpearmanCorrFile="/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGS$GeneBasedSpearmanCorrList[$iT]_best_PsychencodeWeightGeneSpearmanCorr.txt";
    open(INPUT,$GeneSpearmanCorrFile) || die "can’t open $GeneSpearmanCorrFile";
    while (<INPUT>)
    {
        chomp;
        if($iLineNum==0)
        {
          $iLineNum++;
          next;
        }
        my @cols=split(/\s++/,$_);
        $CorrGeneList{$cols[0]}=1;
     }
     close INPUT;    
   }
 my $GeneSize=keys %CorrGeneList;

  my %gnomad211_exomeVariantsFreqList=();#Obtain the updated gnomad211_exome frequency

     $iLineNum=0;
   my $gnomadFile="/home/hanl3/cmc/database/annovar/humandbOrg/hg19_gnomad211_exome.txt";
   open(INPUT,$gnomadFile) || die "can’t open $gnomadFile";
   while (<INPUT>)
    {  
       chomp;
       my @cols=split(/\t/,$_,7);
       next if($cols[3] eq "-" ||$cols[4] eq "-");#remove some variants with ref or alt allele are missing allele
       $cols[0]=~ s/^\s+|\s+$//g;
       $cols[1]=~ s/^\s+|\s+$//g;
       $cols[3]=~ s/^\s+|\s+$//g;
       $cols[4]=~ s/^\s+|\s+$//g;
       if(defined($LOFVariantInfo{$cols[0]}{$cols[1]}))
       { 
         $gnomad211_exomeVariantsFreqList{$cols[0]}{$cols[1]}{$cols[3]}{$cols[4]}=$cols[5];
         $iLineNum++;# last if($iLineNum>10);
       }
     }
    close INPUT;
     print "Finish reading file: $gnomadFile\n";
  
    $iLineNum=0;
    my %CADD_INDELs_Score=();#Construct the hash of CADD score 
    my $CADDScoreFile="/fs0/hanld/CADD/CADDv1.4_hg19_InDels_inclAnno_subset.tsv";
    open(INPUT,$CADDScoreFile) || die "can’t open $CADDScoreFile";
    while (<INPUT>)
    {  
       chomp;
       if($iLineNum<2)
       { 
         $iLineNum++;
         next;
       }
       my @cols=split(/\t/,$_);
       if(defined($LOFVariantInfo{$cols[0]}{$cols[1]}))
       { 
         $CADD_INDELs_Score{$cols[0]}{$cols[1]}{$cols[2]}{$cols[3]}=$cols[16];
        # print "$cols[0]\t$cols[1]\t$cols[2]\t$cols[3]\t$cols[16]\n";
         $iLineNum++;
        # last if($iLineNum>10);
       }
     }
    close INPUT;
    print "Finish reading file: $CADDScoreFile\n";


    print "Number of Genes with corr:$GeneSize\n";
#      /data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/SKL_10073_B01_GRM_WGS_2016-02-25LOFVarinatInfo_synonymousSNV.filter.txt
#    /data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/SKL_11694_B01_GRM_WGS_2017-08-18LOFVarinatInfo_synonymousSNV.filter.txt
#    /data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/SKL_11154_B01_GRM_WGS_2016-03-17LOFVarinatInfo_synonymousSNV.filter.txt   
    my @WGS_Batches=("SKL_10073_B01_GRM_WGS_2016-02-25","SKL_11694_B01_GRM_WGS_2017-08-18","SKL_11154_B01_GRM_WGS_2016-03-17");
   for(my $iBatch=0;$iBatch<3;$iBatch++)
   {
     my $OutFile="/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/$WGS_Batches[$iBatch]LOFVarinatInfo_synonymousSNV.filter.RareGType.txt";
    open OUT, ">$OutFile" or die "Can't open Output file:$OutFile!";

   my $iTotalSelectLoci=0;
   my $iLineNum=0;
   my %WGSRefLociListWithCorr=();
   my %EachPos_AnnovaInfo=();
   my $EachLosFunction_BatchFile="/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/$WGS_Batches[$iBatch]LOFVarinatInfo_synonymousSNV.filter.txt";
    my $iLineLength=0;
   open(INPUT,$EachLosFunction_BatchFile) || die "can’t open $EachLosFunction_BatchFile";
   while (<INPUT>)
   {
        chomp;
          my @cols=split(/\t/,$_);
        #next if($cols[10] eq "synonymous SNV" ||$cols[10] eq "stopgain");#This  line is for test program
        if($iLineNum==0) 
        {
          $iLineNum++;
         $iLineLength=scalar(@cols); 
         print OUT "$cols[0]";
          for(my $iCol=1;$iCol<=12;$iCol++) 
          {
             print OUT "\t$cols[$iCol]";
           }
           print OUT "\t$cols[13]";#Gene_ENS
           print OUT "\t$cols[53]"; #CADD; 
           print OUT "\t$cols[83]"; #Genom_AD
           print OUT "\t$cols[124]";
           print OUT "\t$cols[125]";
           print OUT "\t$cols[126]";
           print OUT "\t$cols[127]";
           print OUT "\t$cols[128]\tLineSize\tWGSIDWithRareAllele\tRareGtype\n";
           next;
        }
       next if($cols[10] eq  ".");#Remove non-explanation locus
       my $iLineSize=scalar(@cols);
        if( $iLineSize != $iLineLength)
       {
        print "$_\n";
        }
 
       # next if( !($cols[2] eq $cols[6] && $cols[3] eq $cols[7])); # make sure to satisfy the condition Ref_vcf= Ref_Annovar,  Alt_vcf= Alt_Annovar, there are some mistakes, no need make sure the  condition exist for shift
        
        my @GenesList_ENS=split(/;/,$cols[13]);
        for(my $iG=0;$iG<@GenesList_ENS;$iG++)
        {
          next if(!defined($CorrGeneList{$GenesList_ENS[$iG]}));#Remove non-selected gene list
          $WGSRefLociListWithCorr{$cols[0]}{$cols[1]}{$cols[2]}{$cols[3]}=1;
          my $AnnovaInfo="$cols[0]";#obtain CADD and genome_AD information
          for(my $iCol=1;$iCol<=12;$iCol++)
          {
             $AnnovaInfo.="\t$cols[$iCol]";
           }
           $AnnovaInfo.="\t$GenesList_ENS[$iG]";
          if($cols[83] eq "." && defined($gnomad211_exomeVariantsFreqList{$cols[0]}{$cols[1]}{$cols[3]}{$cols[4]}))
          {
           print "Genom_AD:\t $cols[83]\t$gnomad211_exomeVariantsFreqList{$cols[0]}{$cols[1]}{$cols[3]}{$cols[4]}\n";
          }
         if($cols[53] eq "." && defined($CADD_INDELs_Score{$cols[0]}{$cols[1]}{$cols[3]}{$cols[4]}))
          {       
           print "CADD Score:\t $cols[53]\t$CADD_INDELs_Score{$cols[0]}{$cols[1]}{$cols[3]}{$cols[4]}\n";
          }  
           $AnnovaInfo.="\t$cols[53]";#CADD;
           $AnnovaInfo.="\t$cols[83]";#Genom_AD
           $AnnovaInfo.="\t$cols[124]";
           $AnnovaInfo.="\t$cols[125]";
           $AnnovaInfo.="\t$cols[126]";
           $AnnovaInfo.="\t$cols[127]";
           $AnnovaInfo.="\t$cols[128]";
           $AnnovaInfo.="\t$iLineSize";
           $EachPos_AnnovaInfo{$cols[0]}{$cols[1]}{$cols[2]}{$cols[3]}{$GenesList_ENS[$iG]}=$AnnovaInfo; #Total Loci:73572,Total loci: 73565 missing 7 loci 
           #$EachPos_AnnovaInfo{$cols[0]}{$cols[1]}{$cols[2]}{$cols[3]}{$cols[4]}{$cols[5]}{$GenesList_ENS[$iG]}=$AnnovaInfo;
           $iTotalSelectLoci++;
       }     
    }
    print "Total Loci:$iTotalSelectLoci\n";

  my $iNum=0;
   foreach my $chr (sort keys %EachPos_AnnovaInfo)
    {
     foreach my $pos (keys %{ $EachPos_AnnovaInfo{$chr} })
     {
      foreach my $ref (keys %{$EachPos_AnnovaInfo{$chr}{$pos}})
      {
      foreach my $alt (keys %{$EachPos_AnnovaInfo{$chr}{$pos}{$ref}})
       { 
       foreach my $gene (keys %{$EachPos_AnnovaInfo{$chr}{$pos}{$ref}{$alt}} )
        {
         $iNum++;
        }
       }
      } 
     }
   }
   print "Total loci: $iNum\n";

     my @AllIndsWGSID=();
     my @AllIndsWGSIDIndex=();  
     my $WGSFileDir="/home/hanl3/cmc/data/wgs/vcf";
     $iLineNum=0;
     my $VCFFile="$WGSFileDir/$WGS_Batches[$iBatch].recalibrated_variants_PASS_WholeGenome.vcf";
     open(INPUT,$VCFFile) || die "can’t open $VCFFile";
     while (<INPUT>)
     { 
      chomp;
      if($_ !~ /^##/)
      { 
       if($_ =~ /^#/)
       {
          my  @cols=split(/\t/,$_);
          @AllIndsWGSID=@cols;
          for(my $ii=9;$ii<@cols;$ii++)
          {
            if(defined($CEUIndInfo{$cols[$ii]}))
            {
             push @AllIndsWGSIDIndex,$ii;
            }
          }
       }
      else
       {
        my @cols=split(/\t/,$_,10);
        my @RefAlleleList=split(/,/,$cols[3]);#Split refence allele and altnative allele for variant polymorphism
        my @AltAlleleList=split(/,/,$cols[4]);
        if(defined($WGSRefLociListWithCorr{$cols[0]}{$cols[1]}{$RefAlleleList[0]}{$AltAlleleList[0]}))
        {
         @cols=split(/\t/,$_);
          my $iAA=0;
          my $iAa=0;
          my $iaa=0;
          my @CommonIndIndex=();
          my @RareIndIndex=();
           my @IndWGSIDWithRareAlleles=();
           my @IndGTypesWithRareAlleles=();
          for(my $iCol=0;$iCol<@AllIndsWGSIDIndex;$iCol++)
          { 
            my $GtypeStr=substr($cols[$AllIndsWGSIDIndex[$iCol]],0,3);
            if($GtypeStr eq "0/0")
            { 
              $iAA++;
              push @CommonIndIndex,$AllIndsWGSIDIndex[$iCol];
            }
            elsif($GtypeStr eq "0/1")
            { 
              $iAa++;
              push @CommonIndIndex,$AllIndsWGSIDIndex[$iCol];
              push @RareIndIndex,$AllIndsWGSIDIndex[$iCol];
            }
            elsif($GtypeStr eq "1/1")
            { 
               push @RareIndIndex,$AllIndsWGSIDIndex[$iCol];
               $iaa++;
            }
           }#End loop for
           if(scalar(@CommonIndIndex)>scalar(@RareIndIndex))
           {
             for(my $iCol=0;$iCol<@RareIndIndex;$iCol++)
             {
                 my $GtypeStr=substr($cols[$RareIndIndex[$iCol]],0,3);
                 push @IndGTypesWithRareAlleles,$GtypeStr;
                 push @IndWGSIDWithRareAlleles,$AllIndsWGSID[$RareIndIndex[$iCol]];
              }                        
           }
          else
           {
              for(my $iCol=0;$iCol<@CommonIndIndex;$iCol++)
              {   
                 my $GtypeStr=substr($cols[$CommonIndIndex[$iCol]],0,3);
                 push @IndGTypesWithRareAlleles,$GtypeStr;
                 push @IndWGSIDWithRareAlleles,$AllIndsWGSID[$CommonIndIndex[$iCol]];
              }
            }
            if(scalar(@IndGTypesWithRareAlleles)>0)
            {
              my $GType=join(":", @IndGTypesWithRareAlleles);
              my $WGSIDWithRareAllele=join(":",@IndWGSIDWithRareAlleles);
              foreach my $gene (keys %{$EachPos_AnnovaInfo{$cols[0]}{$cols[1]}{$RefAlleleList[0]}{$AltAlleleList[0]}} )
              {
               print OUT "$EachPos_AnnovaInfo{$cols[0]}{$cols[1]}{$RefAlleleList[0]}{$AltAlleleList[0]}{$gene}\t$WGSIDWithRareAllele\t$GType\n";
              }
           } 
        }#defined loop or
       }#else
     }
   }
   close OUT;  
  my %AllIndsWithAnnotation=();
   
   $iLineNum=0;
   open(INPUT,$OutFile) || die "can’t open $OutFile";
   while (<INPUT>)
   {
        chomp;
        my @cols=split(/\t/,$_);
        if($iLineNum==0)
        {
           $iLineNum++;
           next;
        }
        my @AllIndIDs=split(/:/,$cols[22]); 
        for(my $iInd=0;$iInd<@AllIndIDs;$iInd++)
        {
         $AllIndsWithAnnotation{$AllIndIDs[$iInd]}=1;
        }
   }
   close INPUT;
   my $IndSize=keys %AllIndsWithAnnotation;
   print "$OutFile\n IndSize: $IndSize\n"; 
  
 }

