 #!/usr/in/perl -w
  use strict;
  use List::Util qw( min max );
  use warnings;
  use Data::Dumper;
  use lib "/home/hanl3/perl5/module/lib/site_perl/5.14.2";
  use Text::CSV_PP;
 

 my  $specificChr=$ARGV[0];;
  #Based on the chromosome that each gene located, all files are classified.
   my %Chromosome_FileNameList=();
   my $iNumberofFiles=0;
   my @Batches=("10073_B01_GRM_WGS_2016-02-25","11694_B01_GRM_WGS_2017-08-18","11154_B01_GRM_WGS_2016-03-17");
   my $dir ="/data/ruderferlab/resources/psychencode/PEC_TWAS_weights_Text";
   my %Chromosome_FileNames=();
   opendir(DIR, $dir) or die $!;
   while (my $file = readdir(DIR)) {
        # Use a regular expression to ignore files beginning with a period
        next if ($file =~ m/^\./);
        #print "$file\n";
        my  $iLineNum=0;
        my $InputFile="$dir/$file";
        open(INPUT,$InputFile) || die "can’t open $InputFile";
        while (<INPUT>)
        {
            if($iLineNum==0)
            {
              $iLineNum++;
             next;
            }
           last if($iLineNum>1);
           my @cols=split(/\t/,$_);
           my @Chr_Position=split(/:/,$_);
           next if($specificChr ne $specificChr);
           if(!defined($Chromosome_FileNameList{$Chr_Position[0]}))
           {
              $Chromosome_FileNameList{$Chr_Position[0]}=$file;
           }
           else
           {
              $Chromosome_FileNameList{$Chr_Position[0]}.=":$file";
           }
           $iLineNum++;
        }
       close INPUT;
   #    last if($iNumberofFiles>100);
      $iNumberofFiles++;
    }
    closedir(DIR);
 
   #foreach my $key (keys %Chromosome_FileNameList)
   #{
   #  print "$key\t$Chromosome_FileNameList{$key}\n";
   # }

   my @PredictedMethods=("top1","blup","bslmm","lasso","enet");
   for(my $iChr=$specificChr;$iChr<=$specificChr;$iChr++)
    {
     for(my $iBatch=0;$iBatch<3;$iBatch++)
     {
      my $Index_SNP=0;
      my  $iLineNum=0;
       my @AllIndIDs=();
       my %PESID_dosageIndex=();
       my %PESID_Position=(); 
      my @AllDosageArray=();
      #Read dosage file and weight file, obtain predicted gene expression
      my $PredictedExpressionFile="/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/SKL_$Batches[$iBatch]Chr$iChr.PECPredictionExpression.txt";
      open OUTPUT, ">$PredictedExpressionFile" or die "Can't open Output file:$PredictedExpressionFile";
      my @AllIndDosageArray=();
      my $WGSFile="/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/SKL_$Batches[$iBatch].recalibrated_SNVs_PassFilterWholeGenome.dosage";
   #   print "$WGSFile\n";
      open(INPUT,$WGSFile) || die "can’t open $WGSFile";
      while (<INPUT>)
      {
         chomp;
         if($iLineNum==0)
         {
            my @cols=split(/\t/,$_);
            splice(@cols,0,5);
            @AllIndIDs=@cols;     
            $iLineNum++;
            next;
         }
          next if($_ !~ /^$iChr\t/);         
          my @cols=split(/\t/,$_);
          $PESID_dosageIndex{$cols[2]}=$Index_SNP;
          $PESID_Position{$cols[2]}=$cols[1]; 
          splice(@cols,0,5);
          push @AllIndDosageArray,[@cols];
          $Index_SNP++;  
      }      
       close INPUT; 

       my $NumberofInds=scalar(@AllIndIDs);#Number of individuals
       print OUTPUT "Chr\tBatches\tNumberofInds\tGeneName\tNumberofSNPs\tNumberofTotalSNPs"; 
       for(my $iM=0;$iM<5;$iM++)
        {
          for(my $iInd=0;$iInd<@AllIndIDs;$iInd++)
          {       
           print OUTPUT "\t$PredictedMethods[$iM]_$AllIndIDs[$iInd]";
          }       
        }
        print OUTPUT "\n";

       my $number_of_rows=@AllIndDosageArray;#Number of Rows
       my $number_of_columns=@{$AllIndDosageArray[0] };#Number of columns
       print "$number_of_rows\t$number_of_columns\t$Index_SNP\t$NumberofInds\n";
      #Estimate each gene *individual gene expression
       my @AllGenesFiles_Chr=split(/:/,$Chromosome_FileNameList{$iChr});
      my $NumberofRepeated=5*$NumberofInds;  
       for(my $kk=0;$kk<@AllGenesFiles_Chr;$kk++) 
       {  
          my $iLineNum=0; 
          my $InputFile="/data/ruderferlab/resources/psychencode/PEC_TWAS_weights_Text/$AllGenesFiles_Chr[$kk]";  
          my $iRelatedSNPs=0;
          my @GeneExpressionAllInds=(0) x $NumberofRepeated;
         my $iTotalSNPs=0; 

         open(INPUT,$InputFile) || die "can’t open $InputFile"; 
          while (<INPUT>)
          {       
            if($iLineNum==0)
            {       
              $iLineNum++;
             next;   
            } 
             my @cols=split(/\t/,$_);
            $iTotalSNPs++;
            if(defined($PESID_dosageIndex{$cols[0]}))
            {
               $iRelatedSNPs++;
               my @dosage_Ind=@{$AllIndDosageArray[$PESID_dosageIndex{$cols[0]}]};
               for(my $iType=1;$iType<=5;$iType++)
               {
                  my $iTypeMinus1=$iType-1;
                  my $iStartPoint=$iTypeMinus1*$NumberofInds;
                  if($cols[$iType] ne "NA")
                  {
                    for(my $iInd=0;$iInd<$NumberofInds;$iInd++)
                    {
                     $GeneExpressionAllInds[$iStartPoint+$iInd]+=$cols[$iType]*$dosage_Ind[$iInd]; 
                    }
                  }
               }
            }
          }
         close INPUT;
         my $GeneName=$AllGenesFiles_Chr[$kk];
         $GeneName =~ s/.wgt.txt//ig;  #ENSG00000118298.wgt.txt
         print OUTPUT "$iChr\t$Batches[$iBatch]\t$NumberofInds\t$GeneName\t$iRelatedSNPs\t$iTotalSNPs"; 
         for(my $ii=0;$ii<@GeneExpressionAllInds;$ii++)
         { 
          print OUTPUT "\t$GeneExpressionAllInds[$ii]";
         } 
        print OUTPUT "\n";
       }
       close OUTPUT;
     } 
  } 

