 #!/usr/in/perl -w
  use strict;
  use List::Util qw( min max );
  use POSIX;
  use warnings;

  for(my $iBatch=0;$iBatch<3;$iBatch++)
   {
    my $ExcuteBatchFile="/gpfs23/data/ruderferlab/projects/cmc/scripts/LOFAffectingPredictedGeneExpression/ExecuteEstimateProfileCommandline$iBatch";
    open OUTPUT, ">$ExcuteBatchFile" or die "Can't open Output file:$ExcuteBatchFile!";
    for(my $iPart=0;$iPart<50;$iPart++)
    {
      print OUTPUT "perl /gpfs23/data/ruderferlab/projects/cmc/scripts/LOFAffectingPredictedGeneExpression/CMC_HBCCWGSPsychencodeWeightProfileProduce.pl $iBatch $iPart &\n";
    }
    close OUTPUT;
 }

