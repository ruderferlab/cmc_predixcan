#Merge Psychencode based predicted gene expression data with RNASeq  actual gene expression data 
#!/usr/bin/perl -w
 use strict;
 use warnings;

#matrix transpose
sub pivot {
    my @src = @_;

    my $max_col = 0;
    $max_col < $#$_ and $max_col = $#$_ for @src;
    my @dest;
    for my $col (0..$max_col) {
        push @dest, [map {$src[$_][$col] // ''} (0..$#src)];
    }
    return @dest;
}


 my @OutlierWGSIndID=("MSSM-DNA-PFC-375","MSSM-DNA-PFC-269",
  "CMC-HBCC-DNA-ACC-6052","CMC-HBCC-DNA-ACC-5669","CMC-HBCC-DNA-ACC-4237","CMC-HBCC-DNA-ACC-5646",
    "CMC-HBCC-DNA-ACC-5777","CMC-HBCC-DNA-ACC-5682","CMC-HBCC-DNA-ACC-4284","CMC-HBCC-DNA-ACC-4137",
    "CMC-HBCC-DNA-ACC-6009","CMC-HBCC-DNA-ACC-6056","CMC-HBCC-DNA-ACC-4029","CMC-HBCC-DNA-ACC-4200",
    "CMC-HBCC-DNA-ACC-4074","CMC-HBCC-DNA-ACC-4204","CMC-HBCC-DNA-ACC-5654","CMC-HBCC-ACC-DNA-4235");

  my %hOutlierWGSID=();
  for(my $ii=0;$ii<@OutlierWGSIndID;$ii++)
  {
    $hOutlierWGSID{$OutlierWGSIndID[$ii]}=1;
  }

#Selected CEU population
  my $iLineNum=0;
  my %CEUIndInfo=();#Make sure CEUs 
  my $InputFile="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/CMC_CAUSampleInformation.txt";;
  open(INPUT, $InputFile) || die "can’t open $InputFile";
  while (<INPUT>)
  {
        chomp;
       my @cols=split(/\s++/,$_,3);
        if($iLineNum==0)
        { 
          $iLineNum++;
          next;
        }
        $CEUIndInfo{$cols[1]}=1;#CEU WGSID 
        $iLineNum++;
    }
   close INPUT;
  my $Size=keys %CEUIndInfo;
  print "CEU Population Sample Size: $Size\n";


 #     /data/ruderferlab/projects/cmc/results/sv/expression/CMC_ACC_eqtlResidualExpression_WithoutSVbasedOutierIndividuals.tsv
 #    /data/ruderferlab/projects/cmc/results/sv/expression/CMC_DLPFC_eqtlResidualExpression_WithoutSVbasedOutierIndividuals.tsv
 #    /data/ruderferlab/projects/cmc/results/sv/expression/HBCC_DLPFC_eqtlResidualExpression_WithoutSVbasedOutierIndividuals.tsv
#predicted psychencode based gene expression
 #  /data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/SKL_10073_B01_GRM_WGS_2016-02-25.PECPredictionExpression.txt
 #  /data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/SKL_11694_B01_GRM_WGS_2017-08-18.PECPredictionExpression.txt
 #  /data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/SKL_11154_B01_GRM_WGS_2016-03-17.PECPredictionExpression.txt

my @TissueColumnNum=(13,12,12);
my @MergedGeneExpression=("WGSCMC_ACC","WGSCMC_DLPFC","WGSHBCC_DLPFC");
my @ActualGeneExpression=("CMC_ACC","CMC_DLPFC","HBCC_DLPFC");

my @PredictedGeneExpessioniPsychencodeWeights=(["SKL_10073_B01_GRM_WGS_2016-02-25"],["SKL_10073_B01_GRM_WGS_2016-02-25"],["SKL_11694_B01_GRM_WGS_2017-08-18","SKL_11154_B01_GRM_WGS_2016-03-17"]);
my @NumberofInds_Batch=([328],[328],[119,326]);


my $NumberofSNVsPercentageofTrainingDataThreshold=1;

 my @PrediXcanModelNumberofSVsColumnNum=(5,5,6);  
 my @NumberofBatchesinTissues=(1,1,2);
my @PredictionMethods=("top1","blup","bslmm","lasso","enet");

 for(my $iT=0;$iT<3;$iT++)#3
{
   my $OutFile="/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/$MergedGeneExpression[$iT]_MergeddActualPredicted_PsychencodeWeightEXP_CEUWithout.Outliers.txt";
   open OUT, ">$OutFile" or die "Can't open Output file:$OutFile!";
   my $NumberofSelectedInds=0;
   my $iLineNum=0;
   my %RNASeqID_WGSID=();
   my $CMCIDInfoFile="/data/ruderferlab/projects/cmc/files/CMC-WGSID_NoRNASeqDuplicateV2.map";# WGS.DataID[9], updated.dlpfcID[12]updated.accID[13]
   open(INPUT,$CMCIDInfoFile) || die "can’t open $CMCIDInfoFile";
   while (<INPUT>)
   {
        chomp;
        if($iLineNum==0) 
        {
          $iLineNum++; 
          next;
        }
        my @cols=split(/\s++/,$_);
       if(defined($CEUIndInfo{$cols[9]}) && !defined($hOutlierWGSID{$cols[9]}))#keep CEU individuals and remove WGS outlier individuals
       {
         if($cols[$TissueColumnNum[$iT]] ne "NA")
         {
           $NumberofSelectedInds++;
           $RNASeqID_WGSID{$cols[$TissueColumnNum[$iT]]}=$cols[9];
           $iLineNum++;    
          }
        }
    }
    close INPUT;
    my $size=keys %RNASeqID_WGSID;

   #record actual gene expression individual wgs ID and gene ID
    my %ShareddGeneIDList=();        
     my %ActualWGSIndID=();#Actual Individual ID for WGS data
     my %ActualGeneID=();#Actual gene id List from actual gene expression
     my $ActualGeneExpressionFile="/data/ruderferlab/projects/cmc/results/sv/expression/$ActualGeneExpression[$iT]"."_eqtlResidualExpression_WithoutSVbasedOutierIndividuals.tsv";
     $iLineNum=0;
      print "Actual gene expression: $ActualGeneExpressionFile\n";
     open(INPUT, $ActualGeneExpressionFile) || die "can’t open $ActualGeneExpressionFile";
     while (<INPUT>)
     { 
       chomp;
        my @cols=split(/\s++/,$_);
        if($iLineNum==0)
        {  
          for(my $ii=1;$ii<@cols;$ii++)#gene name list
          {
             $cols[$ii] =~ s/^\s+|\s+$//g;
            if(defined($RNASeqID_WGSID{$cols[$ii]}))
            {
               $ActualWGSIndID{$RNASeqID_WGSID{$cols[$ii]}}=1;
            }
           }#End loop for
           $iLineNum++;
          next;
         }
         $ActualGeneID{$cols[0]}=1;
   }
     close INPUT; 
   
    ##record predicted gene expression individual wgs ID and gene ID
         my %PredictedExpression_IndWGSID=(); 
         my %UnionPredictedGeneIDs=(); #unite the gene ID for predicted gene expression
         for(my $iTissueType=0;$iTissueType<@{$PredictedGeneExpessioniPsychencodeWeights[$iT]};$iTissueType++)
         {
            my $iStartInd=0;
            my $iEndInd=$NumberofInds_Batch[$iT][$iTissueType];
            $iLineNum=0;
            my $PredictedGeneExpressionFile="/gpfs23/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/$PredictedGeneExpessioniPsychencodeWeights[$iT][$iTissueType].PECPredictionExpression.txt";
            my $iTotalCutSize=$NumberofInds_Batch[$iT][$iTissueType]+7;
            open(INPUT, $PredictedGeneExpressionFile) || die "can’t open $PredictedGeneExpressionFile";
            while (<INPUT>)
            {
             chomp;
             if($iLineNum==0)
             {
               my @cols=split(/\s++/,$_,$iTotalCutSize); 
              pop(@cols);
               splice @cols,0,6;
               my $iSize=scalar(@cols);
               for(my $ii=0;$ii<@cols;$ii++)#gene name list
               {
                  $cols[$ii] =~ s/^\s+|\s+$//g;
                  my @IndIDList=split(/_/,$cols[$ii]);
                  $PredictedExpression_IndWGSID{$IndIDList[1]}=1;
               }
              $iLineNum++;
             }
            else
             {
              my @cols=split(/\s++/,$_,5);
              $UnionPredictedGeneIDs{$cols[3]}=1; 
             }
           }
          close INPUT;
      }
 
     #Find common individual wgs ID and gene ID between actual and predicted  gene expression
      my $iActualGene=keys %ActualGeneID;
      my $iPredictedGenes=keys %UnionPredictedGeneIDs;
      foreach my $gene (keys %UnionPredictedGeneIDs)
      {
        if(defined($ActualGeneID{$gene}))
        {
          $ShareddGeneIDList{$gene}=1;
         }
       }
      my %SharedIndIDList=();
      foreach my $IndID (keys %PredictedExpression_IndWGSID)
      {
        if(defined($ActualWGSIndID{$IndID}))
        {       
          $SharedIndIDList{$IndID}=1;
         }       
       } 
  
      my $iSharedGenes=keys %ShareddGeneIDList;
      my $iActualInds=keys %ActualWGSIndID;     
      my $iPredictedInds=keys %PredictedExpression_IndWGSID;
       my $iSharedInds=keys %SharedIndIDList;
     
      print "Number of Selected genes:$iActualGene,\t$iPredictedGenes\t$iSharedGenes\n";
      print "Number of Selected Individuals:$iActualInds,\t$iPredictedInds\t$iSharedInds\n";
 
      $iLineNum=0;
      #filter actual gene expression information based on shared individual ID and gene ID.
      my @SelectedIndID_ActualData=();#Overlapped Ind ID, Keep all common individual ID order
      my @SelectedGeneID_ActualData=();#overlapped gene ID
      my @SelectedIndIDIndex_ActualData=();#Overlapped Ind Index
      my @SelectedGeneExpression_ActualData=();#OVERLapped actual gene expression
      my %Actual_PredictedSelectedGenes=(); #Overlpapped gene 
      my %Actual_PredictedSelectedInds=(); #Overlapped Individuals
      my $iOverlapActual_PredictedGenes=0;
      open(INPUT, $ActualGeneExpressionFile) || die "can’t open $ActualGeneExpressionFile";
      while (<INPUT>)
      { 
        chomp;
        if($iLineNum==0)
        {
           my @cols=split(/\s++/,$_);
          for(my $ii=1;$ii<@cols;$ii++)#gene name list 
          {
             $cols[$ii] =~ s/^\s+|\s+$//g;  
            if(defined($RNASeqID_WGSID{$cols[$ii]}))
            {
              my $IndWGSID=$RNASeqID_WGSID{$cols[$ii]};
              if(defined($SharedIndIDList{$IndWGSID}))#filter by shared individuals
              {
               $Actual_PredictedSelectedInds{$RNASeqID_WGSID{$cols[$ii]}}=1; 
               push @SelectedIndID_ActualData,$RNASeqID_WGSID{$cols[$ii]};
               push @SelectedIndIDIndex_ActualData,$ii;#selected ind index in @cols  to be analyzed
              }
            }            
          }
           my $NumberofIndsInPredictedData=scalar(@cols)-1;
           my $iOverLappedInds=scalar(@SelectedIndID_ActualData); 
           print "Number of Inds from Actual expression data:$NumberofIndsInPredictedData\t$iOverLappedInds\n";
           $iLineNum++;
        }
        else
         {
            my @cols=split(/\s++/,$_,2);
            $cols[0] =~ s/^\s+|\s+$//g;  
            if(defined($ShareddGeneIDList{$cols[0]}))
            {
                push @SelectedGeneID_ActualData,$cols[0];#Actual Gene Expression Gene ID
                @cols=split(/\s++/,$_);
                my @EachGeneExpression=();
       #         push @EachGeneExpression,$cols[0];
      #          push @EachGeneExpression,"Actual";
               for(my $ii=0;$ii<@SelectedIndIDIndex_ActualData;$ii++)
               {
                push @EachGeneExpression,$cols[$SelectedIndIDIndex_ActualData[$ii]];
               }
               my $GeneBasedGTypeStr = join('\t',@EachGeneExpression); 
               $Actual_PredictedSelectedGenes{$cols[0]}=$GeneBasedGTypeStr;  
               push @SelectedGeneExpression_ActualData,[@EachGeneExpression];#gene expression matrix: row:gene,@SelectedGeneID_ActualData order, column:individual,@SelectedIndID_ActualData
               $iOverlapActual_PredictedGenes++;    
            }
          }
        $iLineNum++;
      }
      close INPUT;
      print "Number of Overlap Genes with Actual expression data :$iOverlapActual_PredictedGenes\n";

      my @GeneExpression_ActualDataTanspose=pivot(@SelectedGeneExpression_ActualData);  #Actual gene expression transpose

   #Filter predicted gene expression based on common individual ID and  gene IDs
      my @OverlappedGeneExpression_Predicted=();
      my @TotalGeneIDList_PredictedExp=();
      
      my @TotalIndList_Prediction=();
      my @TotalAlgorithms_Prediction=();
      for(my $iV=0;$iV<5;$iV++)#iV defined Five algorithms:("top1","blup","bslmm","lasso","enet")
      {
        my $iRowIndex=0;
        my @EachAlgorithm_PredictGeneExpression=();
        my @EachAlgorithm_IndID=();
         my %GeneTissue_Number=();  

        for(my $iTissueType=0;$iTissueType<@{$PredictedGeneExpessioniPsychencodeWeights[$iT]};$iTissueType++)
        {           
            my $iShareGeneNumber=0;
           my $iStartInd=$iV*$NumberofInds_Batch[$iT][$iTissueType];
           my $iEndInd=($iV+1)*$NumberofInds_Batch[$iT][$iTissueType];  
           $iLineNum=0;
           my @OverlappedGeneIndex=();#sort the order based on Actual gene order
           my @FilteredPredictedExpressionColumnList=();#Select columns based on available overlpaaped gene list
           my @OverlappedGeneID=();
           my $PredictedGeneExpressionFile="/gpfs23/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/$PredictedGeneExpessioniPsychencodeWeights[$iT][$iTissueType].PECPredictionExpression.txt";         
           my $iTotalCutSize=($iV+1)*$NumberofInds_Batch[$iT][$iTissueType]+6;
           if($iV!=4)          
           {
            $iTotalCutSize=($iV+1)*$NumberofInds_Batch[$iT][$iTissueType]+7;
           }
           
           my @EachAlogrithm_Tissue_PredictedExp=();#Each algorithm abd Tissue gene expression
           my @SelectedColumnsListinPredictedData=();
           open(INPUT, $PredictedGeneExpressionFile) || die "can’t open $PredictedGeneExpressionFile";
           while (<INPUT>)
           {  
            chomp;
            if($iLineNum==0)
             {
                my @cols=split(/\s++/,$_,$iTotalCutSize);
               if($iV!=4) 
               {
                pop(@cols);
               }
               splice @cols,0,($iStartInd+6);
               my $iSize=scalar(@cols);
               for(my $ii=0;$ii<@cols;$ii++)#gene name list
               {
                  $cols[$ii] =~ s/^\s+|\s+$//g;
                  my @IndIDList=split(/_/,$cols[$ii]); 
                  if(defined($SharedIndIDList{$IndIDList[1]}))#defined each column 
                  {                   
                    push @SelectedColumnsListinPredictedData,$ii; 
                    push @EachAlgorithm_IndID,$IndIDList[1];
                  }
                }
                $iLineNum++;
            }
           else
           { 
              my @cols=split(/\s++/,$_,5);
              if(defined($ShareddGeneIDList{$cols[3]}))
              {    
                $GeneTissue_Number{$cols[3]}=$iShareGeneNumber;
                $iShareGeneNumber++;   
                push @TotalGeneIDList_PredictedExp, $cols[3];
                my @EachGeneExpression=();#each gene expression
                @cols=split(/\s++/,$_,$iTotalCutSize);
                if($iV!=4)
                {
                 pop(@cols);
                }
                 splice @cols,0,($iStartInd+6);
                # push @EachGeneExpression,$cols[3];
                # push @EachGeneExpression,$PredictionMethods[$iV];
                 for(my $kk=0;$kk<@SelectedColumnsListinPredictedData;$kk++)
                 {
                    push @EachGeneExpression,$cols[$SelectedColumnsListinPredictedData[$kk]];
                 }
                  push  @EachAlogrithm_Tissue_PredictedExp,[@EachGeneExpression];
               }    
              #last;
              $iLineNum++;
            }
         }
          close INPUT;
         #based on actual gene ID, the predicted gene expression are sorted.
          my @EachAlogrithm_Tissue_PredictedExp_ShareGeneOrder=();
         for(my $kk=0;$kk<@SelectedGeneID_ActualData;$kk++)
         {
           my $index=$GeneTissue_Number{$SelectedGeneID_ActualData[$kk]};
       #     if($kk != $index)
       #    {     
       #        print "$kk\t$index\n"; 
       #    }
            push @EachAlogrithm_Tissue_PredictedExp_ShareGeneOrder,[@{$EachAlogrithm_Tissue_PredictedExp[$index]}];
          }
          my @EachAlogrithm_Tissue_PredictedExpTranspose=pivot(@EachAlogrithm_Tissue_PredictedExp_ShareGeneOrder); #Each tissue gene predicted  expression transpose
          push @EachAlgorithm_PredictGeneExpression,@EachAlogrithm_Tissue_PredictedExpTranspose;#Merge two tissues'data together
          my $iRowSize=scalar(@EachAlgorithm_PredictGeneExpression);
          print "TotalRows_inAlgorithmTissue:$iRowSize\n";
       }

         #Based on shared Ind WGSID, the prediction expression is sorted.
         my %EachAlgorithm_IndID_Index=();
         for(my $iInd=0;$iInd<@EachAlgorithm_IndID;$iInd++)
         {
           $EachAlgorithm_IndID_Index{$EachAlgorithm_IndID[$iInd]}=$iInd;
         }       
         my @EachAlogrithm_Tissue_PredictedExp_ShareIndOrder=(); 
         for(my $kk=0;$kk<@SelectedIndID_ActualData;$kk++)
         {
           push @TotalIndList_Prediction,$SelectedIndID_ActualData[$kk];
           push @TotalAlgorithms_Prediction,$PredictionMethods[$iV];
           my $index=$EachAlgorithm_IndID_Index{$SelectedIndID_ActualData[$kk]};
          # if($kk != $index)
          # {
          #   print "$kk\t$index\n";
          # }
          push @EachAlogrithm_Tissue_PredictedExp_ShareIndOrder,[@{$EachAlgorithm_PredictGeneExpression[$index]}];
         }
         push @OverlappedGeneExpression_Predicted,@EachAlogrithm_Tissue_PredictedExp_ShareIndOrder;
         my $iRowSize=scalar(@OverlappedGeneExpression_Predicted);
         print "TotalRows_inAlgorithm:$iRowSize\n";

     }

   my $iPredictedExpressionSize=scalar(@SelectedIndID_ActualData);
   print "iPredictedExpressionSize:$iPredictedExpressionSize\n";
   print  OUT "IndID\tDataType";
    for(my $ii=0;$ii<@SelectedGeneID_ActualData;$ii++)
    {
        print OUT "\t$SelectedGeneID_ActualData[$ii]";
    }
     print OUT "\n";
     for(my $ii=0; $ii<@GeneExpression_ActualDataTanspose;$ii++)
    {
      print OUT "$SelectedIndID_ActualData[$ii]\tActualData";
      for(my $jj=0;$jj<@{$GeneExpression_ActualDataTanspose[$ii]};$jj++)
      {
       print OUT "\t$GeneExpression_ActualDataTanspose[$ii][$jj]";
      }
      print OUT  "\n";
    }
   my $iAllPredictedInds=scalar(@TotalIndList_Prediction);
   print "Total Individuals:$iAllPredictedInds\n";
   for(my $ii=0;$ii<@OverlappedGeneExpression_Predicted;$ii++)
    {
      print OUT "$TotalIndList_Prediction[$ii]\t$TotalAlgorithms_Prediction[$ii]"; 
      for(my $jj=0;$jj<@{$OverlappedGeneExpression_Predicted[$ii]};$jj++)
      {
       print OUT "\t$OverlappedGeneExpression_Predicted[$ii][$jj]";
      }
      print OUT "\n";
     }
   close OUT;
}
