
library(dplyr)

Outputfile=paste("/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/PsychencodeWeightZScore_AnnovarRelationshipBoxplot_Variant4.pdf",sep="");
  pdf(file=Outputfile);
  par(mar=c(10,4,1.5,0.25),mfrow=c(2,2),oma=c(0,0,0,0));
   linewide=2;
   axixwide=0.5;
   tickwide=0.5;
   cex_size=0.62;
   leg_cex_size=0.8;
   axes=F;
   xaxs="s";
   cex_size3=0.65;


  DeaseTypes=c("Control","SCZ");

TissueTypes=c("CMC_ACC","CMC_DLPFC","HBCC_DLPFC");
#SingletonFreqThreshold=c(0.0015,0.0015,0.0042[119]0.0015[326]
#17th,21th column [NumTotal fAlleleFreq]
#awk 'BEGIN{FS="\t"}{print $17,$21}'  /data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSHBCC*_PsychencodeWeightZScore_AnnovarMergedCEUBestAlgorithmWithoutOutliers.txt|sort -n|uniq -c[NumTotal fAlleleFreq]
  for(iDx in seq(1,2))
  {
    AllTissueGeneExpression=NULL;

      #WGSMerged_PsychencodeWeightZScore_AnnovarMergedCEUBestAlgorithmWithoutOutliers.txt
       InFile=paste("/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSMerged_PsychencodeWeightZScore_AnnovarMergedCEUBestAlgorithmWithoutOutliers_PLIScore_Singleton.txt",sep="");
       ZScore_ANNOVAR=read.table(InFile,sep="\t",colClasses="character",na.strings = "NA", header=TRUE,fill=FALSE, strip.white=TRUE);
       ZScore_ANNOVAR=as.data.frame(ZScore_ANNOVAR); 
      #Chr     Pos_vcf Ref_vcf Alt_vcf Start_Annovar(5)   End_Annovar     Ref_Annovar     Alt_Annovar     Func_ref        Gene_ref(10)        ExonicFunc_ref  AAChange_ref    Func_ens        Gene_ens     CADD_phred(15)     gnomAD_genome_ALL       NumTotal        NumOther        NumMissing      fMissingRate(20)    fAlleleFreq     LineSize        WGSIDWithRareAllele     RareGtype   ActualExZScore(25)   PredictedZScore SpearmanCorr    Dx      VariantType     oe_LOF_gnomad( 30)  PLI_gnomad      EXAC_LoF_FDR 
     ZScore_ANNOVAR=ZScore_ANNOVAR[which(ZScore_ANNOVAR$Dx ==DeaseTypes[iDx]),];#Select one disease
     ZScore_ANNOVAR=ZScore_ANNOVAR[which(ZScore_ANNOVAR$VariantType !="NA"),];   
     ZScore_ANNOVAR=ZScore_ANNOVAR[which(ZScore_ANNOVAR$oe_LOF_gnomad !="NA"),];

#  ZScore_ANNOVAR=ZScore_ANNOVAR[which(ZScore_ANNOVAR$ExonicFunc_ref != "nonframeshift deletion"),];
 #   ZScore_ANNOVAR=ZScore_ANNOVAR[which(ZScore_ANNOVAR$ExonicFunc_ref != "nonframeshift insertion"),];
 #  ZScore_ANNOVAR=ZScore_ANNOVAR[which(ZScore_ANNOVAR$ExonicFunc_ref != "nonsynonymous SNV"),];
 #  ZScore_ANNOVAR=ZScore_ANNOVAR[which(ZScore_ANNOVAR$ExonicFunc_ref != "stoploss"),];
      ZScore_ANNOVARFull=ZScore_ANNOVAR[which(ZScore_ANNOVAR$ActualExZScore != "NA" & ZScore_ANNOVAR$PredictedZScore != "NA"),];
      # print(dim(ZScore_ANNOVAR_CADD));
   #   ZScore_ANNOVAR_synonymousSNV=ZScore_ANNOVARFull[which((ZScore_ANNOVARFull$CADD_phred == "." & ZScore_ANNOVARFull$ExonicFunc_ref == "synonymous SNV")),];
     # print(dim(ZScore_ANNOVAR_synonymousSNV));

      AllTissueGeneExpression=as.data.frame(ZScore_ANNOVAR);
      colnames(AllTissueGeneExpression)=colnames(ZScore_ANNOVAR);
  #    print(dim(TotalMergedSubset));
      AllTissueGeneExpression$CADD_phred[AllTissueGeneExpression$CADD_phred == "."] <- 0; 
      AllTissueGeneExpression$CADD_phred=as.numeric(AllTissueGeneExpression$CADD_phred);
  print(dim(AllTissueGeneExpression));
  AllTissueGeneExpression$ActualExZScore=as.numeric(AllTissueGeneExpression$ActualExZScore);
  AllTissueGeneExpression$PredictedZScore=as.numeric(AllTissueGeneExpression$PredictedZScore);
  AllTissueGeneExpression$SpearmanCorr=as.numeric(AllTissueGeneExpression$SpearmanCorr);
 

   boxplot(ActualExZScore~VariantType,data=AllTissueGeneExpression,names =c("Loss of function","Missense pathogenic","Missense benign","Synonymous control"),main=paste(DeaseTypes[iDx],"_Actual",sep=""),
        xlab=NULL, ylab="Z_Score",outline=FALSE,las=2);
        abline(h =0,lty=2,lwd=0.5,col=2);
    ActualZScore_lossofFun=AllTissueGeneExpression$ActualExZScore[AllTissueGeneExpression$VariantType=="loss-of-function"];
    ActualZScore_synonymous=AllTissueGeneExpression$ActualExZScore[AllTissueGeneExpression$VariantType=="synonymous"];
    ActualZScore_CADDLarger15=AllTissueGeneExpression$ActualExZScore[AllTissueGeneExpression$VariantType=="missense_CADDLarger15"];
    ActualZScore_CADDLess15=AllTissueGeneExpression$ActualExZScore[AllTissueGeneExpression$VariantType=="missense_CADDSmaller15"];
     NumofEachTypeVariants=c(length(ActualZScore_lossofFun),length(ActualZScore_CADDLarger15),length(ActualZScore_CADDLess15),length(ActualZScore_synonymous));;  
    print(NumofEachTypeVariants);
   ttestPvalue_ActualZScore_LoF_Ctr=t.test(ActualZScore_synonymous, ActualZScore_lossofFun)$p.value;
    
   print(ttestPvalue_ActualZScore_LoF_Ctr);
    ttestPvalue_ActualZScore_LoF_Ctr=round(t.test(ActualZScore_synonymous, ActualZScore_lossofFun)$p.value,5);
    ttestPvalue_ActualZScore_CADDLarger15_Ctr=round(t.test(ActualZScore_synonymous,ActualZScore_CADDLarger15)$p.value,3);
    ttestPvalue_ActualZScore_CADDLess15_Ctr=round(t.test(ActualZScore_synonymous,ActualZScore_CADDLess15)$p.value,3); 


    text(1, 2, paste0("p=",ttestPvalue_ActualZScore_LoF_Ctr));
    text(2, 2, paste0("p=",ttestPvalue_ActualZScore_CADDLarger15_Ctr));
     text(3,2, paste0("p=",ttestPvalue_ActualZScore_CADDLess15_Ctr));
 

  print(ttestPvalue_ActualZScore_LoF_Ctr);  
   print(ttestPvalue_ActualZScore_CADDLarger15_Ctr);
  print(ttestPvalue_ActualZScore_CADDLess15_Ctr);
  
   boxplot(PredictedZScore~VariantType,data=AllTissueGeneExpression,  names =c("Loss of function","Missense pathogenic","Missense benign","Synonymous control"),main=paste(DeaseTypes[iDx],"_Predicted",sep=""),
        xlab=NULL, ylab="Z_Score",outline=FALSE,las=2);
    abline(h =0,lty=2,lwd=0.5,col=2);

    PredictedZScore_lossofFun=AllTissueGeneExpression$PredictedZScore[AllTissueGeneExpression$VariantType=="loss-of-function"];
    PredictedZScore_synonymous=AllTissueGeneExpression$PredictedZScore[AllTissueGeneExpression$VariantType=="synonymous"];
    PredictedZScore_CADDLarger15=AllTissueGeneExpression$PredictedZScore[AllTissueGeneExpression$VariantType=="missense_CADDLarger15"];
    PredictedZScore_CADDLess15=AllTissueGeneExpression$PredictedZScore[AllTissueGeneExpression$VariantType=="missense_CADDSmaller15"];
     NumofEachTypeVariants=c(length(PredictedZScore_lossofFun),length(PredictedZScore_CADDLarger15),length(PredictedZScore_CADDLess15), length(PredictedZScore_synonymous));;  
    print(NumofEachTypeVariants);
      ttestPvalue_PredictedZScore_LoF_Ctr=t.test(PredictedZScore_synonymous, PredictedZScore_lossofFun)$p.value;
      print(ttestPvalue_PredictedZScore_LoF_Ctr);
    ttestPvalue_PredictedZScore_LoF_Ctr=round(t.test(PredictedZScore_synonymous, PredictedZScore_lossofFun)$p.value,5);
    ttestPvalue_PredictedZScore_CADDLarger15_Ctr=round(t.test(PredictedZScore_synonymous,PredictedZScore_CADDLarger15)$p.value,3);
    ttestPvalue_PredictedZScore_CADDLess15_Ctr=round(t.test(PredictedZScore_synonymous,PredictedZScore_CADDLess15)$p.value,3); 

    print(ttestPvalue_PredictedZScore_LoF_Ctr);  
   print(ttestPvalue_PredictedZScore_CADDLarger15_Ctr);
  print(ttestPvalue_PredictedZScore_CADDLess15_Ctr); 
       text(1, 2, paste0("p=",ttestPvalue_PredictedZScore_LoF_Ctr));
    text(2, 2, paste0("p=",ttestPvalue_PredictedZScore_CADDLarger15_Ctr));
     text(3,2, paste0("p=",ttestPvalue_PredictedZScore_CADDLess15_Ctr));
   
 

}  
 


 dev.off();
