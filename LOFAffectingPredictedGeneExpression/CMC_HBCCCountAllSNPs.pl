 #!/usr/in/perl -w
  use strict;
  use List::Util qw( min max );
  use warnings;
  use Data::Dumper;
  use lib "/home/hanl3/perl5/module/lib/site_perl/5.14.2";
  use Text::CSV_PP;
 

  #Based on the chromosome that each gene located, all files are classified.
   my %Chromosome_FileNameList=();
   my $iNumberofFiles=0;
   my @Batches=("10073_B01_GRM_WGS_2016-02-25","11694_B01_GRM_WGS_2017-08-18","11154_B01_GRM_WGS_2016-03-17");
   my $dir ="/data/ruderferlab/resources/psychencode/PEC_TWAS_weights_Text";
   my %AllSNPs=();
   opendir(DIR, $dir) or die $!;
   while (my $file = readdir(DIR)) {
        # Use a regular expression to ignore files beginning with a period
        next if ($file =~ m/^\./);
        #print "$file\n";
        my  $iLineNum=0;
        my $InputFile="$dir/$file";
        open(INPUT,$InputFile) || die "can’t open $InputFile";
        while (<INPUT>)
        {
            if($iLineNum==0)
            {
              $iLineNum++;
             next;
            }
           my @cols=split(/\t/,$_,2);
           my @PosList=split(/:/,$cols[0]);
           $AllSNPs{$PosList[0]}{$PosList[1]}=$cols[0];
           $iLineNum++;
        }
       close INPUT;
   #    last if($iNumberofFiles>100);
      $iNumberofFiles++;
    }
    closedir(DIR);
 
      my $PredictedExpressionFile="/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/SKL_$Batches[$iBatch]Chr$iChr.PECPredictionExpression.txt";
      open OUTPUT, ">$PredictedExpressionFile" or die "Can't open Output file:$PredictedExpressionFile";
   foreach my $chr (sort{$a<=> $b} keys %AllSNPs)
  {
     foreach my $pos (sort{$a<=> $b} keys %{$AllSNPs{$chr}})
     {
       print "$key\t$Chromosome_FileNameList{$key}\n";
     }
   }
    close OUTPUT;
   } 
  } 

