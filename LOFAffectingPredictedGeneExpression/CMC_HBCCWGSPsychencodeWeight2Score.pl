 #!/usr/in/perl -w
  use strict;
  use List::Util qw( min max );
  use warnings;
  use Data::Dumper;
  use lib "/home/hanl3/perl5/module/lib/site_perl/5.14.2";
  use Text::CSV_PP; 

  my $iNumberofFiles=0;
  #Based on the chromosome that each gene located, all files are classified.
   my $dir ="/data/ruderferlab/resources/psychencode/PEC_TWAS_weights";
   my %AllSNPs=();
   opendir(DIR, $dir) or die $!;
   while (my $file = readdir(DIR)) {
        # Use a regular expression to ignore files beginning with a period
        next if ($file =~ m/^\./);
        #print "$file\n";
        my  $iLineNum=0;
        my  $InputFile="$dir/$file";
	my  $OutFile=$file; 
        #ENSG00000100129.wgt.RDat
        $OutFile =~ s/\.RDat/.score/ig;
        my $OutputFile="/data/ruderferlab/resources/psychencode/PEC_TWAS_weights_Score/$OutFile";
        my $command= "Rscript /fs0/hanld/fusion_twas-master/utils/make_score.R $InputFile  >$OutputFile";  
        system($command);     
        $iNumberofFiles++;
       print "Have Finished files:$iNumberofFiles\n";
    }
    closedir(DIR);

