 #!/usr/in/perl -w
  use strict;
  use List::Util qw( min max );
  use warnings;
  use Data::Dumper;
  use lib "/home/hanl3/perl5/module/lib/site_perl/5.14.2";
  use Text::CSV_PP;
 
 #Convert genetype to dosage effect for same Ref Alt Allele order  
 sub ConvertDosageEffect{
    my ($OrginalGtypesRef,$fMeanDosage) = @_;
     my @OrginalGtypes=@{$OrginalGtypesRef};
    my @Doseage=(); 
    for(my $kk=9;$kk<@OrginalGtypes;$kk++)
     {
        if($OrginalGtypes[$kk] eq "0/0")
        {       
           push  @Doseage,0; 
         }       
        elsif($OrginalGtypes[$kk] eq "0/1")
        {       
            push  @Doseage,1; 
        }       
        elsif($OrginalGtypes[$kk] eq "1/1")
         {       
           push  @Doseage,2;
         }       
        elsif($OrginalGtypes[$kk] eq "./.")
        {       
            push  @Doseage,$fMeanDosage;                    
        }       
        else    
        {       
           push  @Doseage,$fMeanDosage;
        }  
     } 
    return @Doseage;
 }


#Convert genetype to dosage effect for revese Ref Alt Allele order
 sub ConvertDosageEffect_Reverse{
    my ($OrginalGtypesRef,$fMeanDosage) = @_;
     my @OrginalGtypes=@{$OrginalGtypesRef};
    my @Doseage=();
    for(my $kk=9;$kk<@OrginalGtypes;$kk++)
     {
        if($OrginalGtypes[$kk] eq "0/0")
        {           
           push  @Doseage,2;
         }          
        elsif($OrginalGtypes[$kk] eq "0/1")
        {            
            push  @Doseage,1;  
        }           
        elsif($OrginalGtypes[$kk] eq "1/1")
         {           
           push  @Doseage,0;
         }           
        elsif($OrginalGtypes[$kk] eq "./.")
        {             
            push  @Doseage,$fMeanDosage;
        }            
        else            
        {             
           push  @Doseage,$fMeanDosage;
        }
     } 
    return @Doseage;
 }

  my @Batches=("10073_B01_GRM_WGS_2016-02-25","11694_B01_GRM_WGS_2017-08-18","11154_B01_GRM_WGS_2016-03-17");
#    /home/hanl3/cmc/data/wgs/vcf/SKL_10073_B01_GRM_WGS_2016-02-25.recalibrated_SNVs_PassFilterWholeGenome.vcf
#    /home/hanl3/cmc/data/wgs/vcf/SKL_11154_B01_GRM_WGS_2016-03-17.recalibrated_SNVs_PassFilterWholeGenome.vcf  
#    /home/hanl3/cmc/data/wgs/vcf/SKL_11694_B01_GRM_WGS_2017-08-18.recalibrated_SNVs_PassFilterWholeGenome.vcf  
#Read all allele information  
   my %SNPInfo=();
      my  $iLineNum=0;
    my $ChrType=0;
     my  $SNPPosFile="/data/ruderferlab/resources/psychencode/hg19_SNP_Information_Table_with_Alleles.txt";
    #PEC_id	Rsid	chr	position	REF	ALT
    #1:927741	rs6665587	chr1	927741	G	A
     open(INPUT,$SNPPosFile) || die "can’t open $SNPPosFile";
     while (<INPUT>)
     {
        chomp;
         if($iLineNum == 0)
         {
           $iLineNum++;
           next;
         }
           my @cols=split(/\t/,$_);
           $cols[2] =~ s/chr//ig;
           $SNPInfo{$cols[2]}{$cols[3]}{0}=$cols[4];#col[0]->PEC_id,col[1]->Rsid,col[2]->chr,col[3]->position,col[4]->REF,col[5]->ALT
           $SNPInfo{$cols[2]}{$cols[3]}{1}=$cols[5];
           $SNPInfo{$cols[2]}{$cols[3]}{2}=$cols[0];
           $SNPInfo{$cols[2]}{$cols[3]}{3}=$cols[1];         
           if($ChrType != $cols[2])
           {
              print "$iLineNum\t$cols[2]\n";
            }
           $ChrType=$cols[2]; 
         $iLineNum++;
      }
   close INPUT;
  print "Finish reading file: $SNPPosFile\n"; 
   my %ComplementaryAlleles=();
   $ComplementaryAlleles{"A"}="T";
   $ComplementaryAlleles{"T"}="A";
   $ComplementaryAlleles{"C"}="G";
   $ComplementaryAlleles{"G"}="C";
   for(my $iBatch=0;$iBatch<3;$iBatch++)
   {
      my  $iLineNum=0;
      my @AllIndwgsID=();
      my @DosageEffect=();
      my $RefAlleleOrder=0;
      #Read wgs file and convert them into dosage 
      my $WGSDosageFile="/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/SKL_$Batches[$iBatch].recalibrated_SNVs_PassFilterWholeGenome.dosage";
      open OUTPUT, ">$WGSDosageFile" or die "Can't open Output file:$WGSDosageFile";
      my $WGSFile="/home/hanl3/cmc/data/wgs/vcf/SKL_$Batches[$iBatch].recalibrated_SNVs_PassFilterWholeGenome.vcf";
      open(INPUT,$WGSFile) || die "can’t open $WGSFile";
      while (<INPUT>)
      {
        chomp;
        next if($_ =~/^##/);
        if($_ =~/^#/)
        {
            my @cols=split(/\t/,$_);
            splice(@cols,0,9);  
            @AllIndwgsID=@cols;
            print OUTPUT "Chr\tPos\tpsy_ID\trsid\tRefAlleleOrder";
            for(my $kk=0;$kk<@cols;$kk++)
            {
              print OUTPUT  "\t$cols[$kk]";
            } 
            print OUTPUT  "\n";
            next;
        }
        else
        {
           my @cols=split(/\t/,$_,8);
           if(defined($SNPInfo{$cols[0]}{$cols[1]}{0}) && $cols[6] eq "PASS")
           {
               my $iAA=0;
               my $iAa=0;
               my $iaa=0;
               my $iMissing=0;
               my $iOther=0;
               @cols=split(/\t/,$_);
               $RefAlleleOrder=0;  
               for(my $kk=9;$kk<@cols;$kk++)
               {
                  if($cols[$kk] eq "0/0")
                  { 
                    $iAA++;
                   }
                   elsif($cols[$kk] eq "0/1")
                   { 
                     $iAa++;
                   }
                   elsif($cols[$kk] eq "1/1")
                   { 
                     $iaa++;
                    }
                   elsif($cols[$kk] eq "./.")
                   { 
                      $iMissing++;
                    }
                    else
                    { 
                      $iOther++;
                    }
               }
               
                my $iTotalGtypes=$iAA+$iAa+$iaa;
                my $fMissingRate=sprintf("%.4f",$iMissing/($iTotalGtypes+$iMissing+$iOther));
                next if($fMissingRate>=0.2);
                my $fMean=sprintf("%.4f",($iAa+2*$iaa)/$iTotalGtypes);             
                my @AltAlleleList=split(/,/,$cols[4]);
                my @TrainingSampleAlleles=split(/,/,$SNPInfo{$cols[0]}{$cols[1]}{1});#$TrainingSampleAlleles[0]
                if($cols[3] eq $SNPInfo{$cols[0]}{$cols[1]}{0} && $AltAlleleList[0] eq $TrainingSampleAlleles[0])#Complete Same
                {
                   $RefAlleleOrder=1;
                   @DosageEffect=ConvertDosageEffect(\@cols,$fMean);
                }
                elsif($cols[3] eq $TrainingSampleAlleles[0]  && $AltAlleleList[0] eq $SNPInfo{$cols[0]}{$cols[1]}{0})#revers
                {
                   $RefAlleleOrder=2;
                   $fMean=sprintf("%.4f",($iAa+2*$iAA)/$iTotalGtypes);
                   @DosageEffect=ConvertDosageEffect_Reverse(\@cols,$fMean);
                }
                elsif($cols[3] eq $ComplementaryAlleles{$SNPInfo{$cols[0]}{$cols[1]}{0}} && $AltAlleleList[0] eq $ComplementaryAlleles{$TrainingSampleAlleles[0]})#Complete complementary
                {
                    $RefAlleleOrder=3;
                    @DosageEffect=ConvertDosageEffect(\@cols,$fMean);
                }
                elsif($cols[3] eq $ComplementaryAlleles{$TrainingSampleAlleles[0]} && $AltAlleleList[0] eq $ComplementaryAlleles{$SNPInfo{$cols[0]}{$cols[1]}{0}})#reverse complementary
                {
                    $RefAlleleOrder=4;
                    $fMean=sprintf("%.4f",($iAa+2*$iAA)/$iTotalGtypes);
                    @DosageEffect=ConvertDosageEffect_Reverse(\@cols,$fMean);
                }
               # print "$cols[0]\t$cols[1]\t$cols[2]\t$cols[3]\t$AltAlleleList[0]\t$SNPInfo{$cols[0]}{$cols[1]}{1}\t$SNPInfo{$cols[0]}{$cols[1]}{0}\n";
               next if($RefAlleleOrder==0);
               print OUTPUT "$cols[0]\t$cols[1]\t$SNPInfo{$cols[0]}{$cols[1]}{2}\t$SNPInfo{$cols[0]}{$cols[1]}{3}\t$RefAlleleOrder";
               for(my $kk=0;$kk<@DosageEffect;$kk++)
               {
                print OUTPUT "\t$DosageEffect[$kk]";
                }
               print OUTPUT "\n";
               $iLineNum++;
              #last if($iLineNum>20);
           } 
         }
      }
     close INPUT; 
     close OUTPUT;  
   }

