
library(ggplot2)
library(gridExtra)
 library(grid)
library(lattice)
  library(ggplot2);
  library(devtools);
 library(magrittr);

#    /data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMC_ACC_MergeddActualPredicted_PsychencodeWeightEXP_CEUWithout_BestAlgorithm.Outliers.txt
#    /data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMC_DLPFC_MergeddActualPredicted_PsychencodeWeightEXP_CEUWithout_BestAlgorithm.Outliers.txt
#    /data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSHBCC_DLPFC_MergeddActualPredicted_PsychencodeWeightEXP_CEUWithout_BestAlgorithm.Outliers.txt

#      635 WGSHBCC_DLPFC_ActualPredicted_MergedExpression_Percentage0.txt

 Outputfile="/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/GeneLevelActual_PsychencodeWeightPredictedGeneExpressionSpearmanCorrWGSBrain2TissuesHistogram_BestAlgorithm.pdf";
  pdf(file=Outputfile,pointsize=16,width=16, height=8);
  par(mar=c(4,4,1.0,0.25),mfrow=c(3,2),oma=c(0,0,0,0));

   DataSampleSize=c(210,253,138);#All individuals 
   ActualTissues=c("WGSCMC_ACC","WGSCMC_DLPFC","WGSHBCC_DLPFC");

 #CEUSampleIDInfoFile="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/CMC_CAUSampleInformation.txt";
# CEUSampleIDMatrix=read.table(CEUSampleIDInfoFile, sep="\t",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE);#Obtain column names from File By check.names  
# CEUWGSID=CEUSampleIDMatrix[,2];

 ThreeTissues=c("ACC_BA24","PFC_BA9","PFC_BA9");
 MultiplyAlgorithms=c("best");

 myPlot<-list();
for(iActualT in seq(1,3))
{
   iExpressionFile=paste("/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/",ActualTissues[iActualT],"_","MergeddActualPredicted_PsychencodeWeightEXP_CEUWithout_BestAlgorithm.Outliers.txt",sep="");
     GeneExpression=read.table(iExpressionFile, sep="\t",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE);#Obtain column names from File By check.names   
       GeneIDColNames=colnames(GeneExpression);
#      GeneExpression=GeneExpression[which(GeneExpression$IndID %in% CEUWGSID),]; 
     GeneExpressionDim=dim(GeneExpression);
     print(GeneExpressionDim);
     iSplitSize=GeneExpressionDim[1]/2;
     print(iSplitSize);
     ActualGeneExpression=GeneExpression[1:iSplitSize,]; #select Actual and predicted gene expression
     ActualGeneExpression[,3:GeneExpressionDim[2]] <- sapply(ActualGeneExpression[,3:GeneExpressionDim[2]], as.numeric);
     ActualGeneExpressionCpy=ActualGeneExpression;

      ActualGeneExpressionValueDim=dim(ActualGeneExpressionCpy);;
     ActualGeneExpressionAllValuesSD=scale(ActualGeneExpression[,3:GeneExpressionDim[2]]);# standardize the orginal matrix
      print(ActualGeneExpressionAllValuesSD[c(1:5),c(1:5)]);
      for(iCol in seq(3,GeneExpressionDim[2]))
      {
         ActualGeneExpressionCpy[,iCol]=ActualGeneExpressionAllValuesSD[,iCol-2];
       }
        ActualGeneExpressionCpy=as.data.frame(ActualGeneExpressionCpy);      
        ActualExpressionFile=paste("/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/",ActualTissues[iActualT],"_",MultiplyAlgorithms[1],"_PsychencodeWeightGeneActualExpressionZscore.txt",sep="");
       write.table(ActualGeneExpressionCpy, file=ActualExpressionFile, append = FALSE, quote =FALSE, sep = "\t", eol = "\n",col.names=TRUE,row.names=FALSE);

 
        PredictedGeneExpression=GeneExpression[c((1+iSplitSize):GeneExpressionDim[1]),];
        PredictedGeneExpression[,3:GeneExpressionDim[2]] <- sapply(PredictedGeneExpression[,3:GeneExpressionDim[2]], as.numeric);
        PredictedGeneExpressionCpy= PredictedGeneExpression;
    
       PredictedGeneExpressionDim=dim(PredictedGeneExpression);
      PredictedGeneExpressionAllValuesSD=scale(PredictedGeneExpression[,3:PredictedGeneExpressionDim[2]]);# standardize the orginal matrix
      print( PredictedGeneExpressionAllValuesSD[c(1:5),c(1:5)]);
      for(iCol in seq(3,GeneExpressionDim[2]))
      {
          PredictedGeneExpressionCpy[,iCol]= PredictedGeneExpressionAllValuesSD[,iCol-2];
       }
       PredictedGeneExpressionCpy=as.data.frame(PredictedGeneExpressionCpy);
        PredictedExpressionFile=paste("/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/",ActualTissues[iActualT],"_",MultiplyAlgorithms[1],"_PsychencodeWeightGenePredictedExpressionZScore.txt",sep="");
       write.table(PredictedGeneExpressionCpy, file=PredictedExpressionFile, append = FALSE, quote =FALSE, sep = "\t", eol = "\n",col.names=TRUE,row.names=FALSE);


        EachBatchGeneBasedSpearmanCorr=NULL;
        GeneID=NULL;
        for(jj in seq(3,GeneExpressionDim[2]))
        {
          uniquePredict<- unique(PredictedGeneExpression[,jj]);#find some genes with all predicted gene expression equal to zero
          uniqueActual<- unique(ActualGeneExpression[,jj]);#find some genes with all predicted gene expression equal to zero 
          if(length(uniquePredict)!=1 && length(uniqueActual)!=1)
          {
            SpearmanCorr=cor(ActualGeneExpression[,jj],PredictedGeneExpression[,jj],"pairwise.complete.obs",method="spearman");
            EachBatchGeneBasedSpearmanCorr=c(EachBatchGeneBasedSpearmanCorr,SpearmanCorr);
            GeneID=c(GeneID,GeneIDColNames[jj]);
          }
       }   
         EachBatchGeneBasedSpearmanCorr=data.frame("GeneID"=GeneID,"SpearmanCorr"=EachBatchGeneBasedSpearmanCorr);#Construct a data frame based on two vectors
         EachBatchGeneBasedSpearmanCorr=EachBatchGeneBasedSpearmanCorr[complete.cases(EachBatchGeneBasedSpearmanCorr),];#Remove rows with all or some NAs (missing values) in data.fr
         EachBatchGeneBasedSpearmanCorr[,2]=as.numeric(EachBatchGeneBasedSpearmanCorr[,2]);
        max1=round(max(abs(EachBatchGeneBasedSpearmanCorr[,2])),3);
        mean1=round(mean(EachBatchGeneBasedSpearmanCorr[,2]),3);
        median1=round(median(EachBatchGeneBasedSpearmanCorr[,2]),3);
        sd1=round(sd(EachBatchGeneBasedSpearmanCorr[,2]),3);
       size=length(EachBatchGeneBasedSpearmanCorr[,2]);
      print(max1);
      print (mean1); 
     print (median1);

      myPlot[[iActualT]]<-ggplot(data=EachBatchGeneBasedSpearmanCorr,aes(SpearmanCorr))+ geom_histogram(binwidth=0.01, colour="black", fill="white") + ggtitle(paste(ActualTissues[iActualT],"_",MultiplyAlgorithms[1],"Psycode_Mean",mean1,"_Median",median1,"_sd",sd1,"_size",size,sep=""))+ xlab("Spearman Corr")+ coord_cartesian(xlim=c(-1,1)) + scale_x_continuous(breaks=seq(-1,1,0.1));
       print(dim(EachBatchGeneBasedSpearmanCorr));
      CorrelationFile=paste("/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/",ActualTissues[iActualT],"_",MultiplyAlgorithms[1],"_PsychencodeWeightGeneSpearmanCorr.txt",sep="");
       write.table(EachBatchGeneBasedSpearmanCorr, file=CorrelationFile, append = FALSE, quote =FALSE, sep = "\t", eol = "\n",col.names=TRUE,row.names=FALSE);


  }
   myPlot[[4]]<-ggplot() + theme_void();

    pushViewport(viewport(layout=grid.layout(2,2)))#defined how to arrange the plots, there are 30 
   vplayout<-function(x,y){viewport(layout.pos.row=x,layout.pos.col=y)}
   iNum=1;
   for(i in myPlot)
   {  
     print(iNum);
    if(iNum %% 2 == 1)
    {   
        print(myPlot[[iNum]],vp=vplayout((iNum-1)%/%2+1,1));
    }
    else
    {  
       print(myPlot[[iNum]],vp=vplayout((iNum-1)%/%2+1,2));
    }

   iNum=iNum+1;
 }

dev.off();

