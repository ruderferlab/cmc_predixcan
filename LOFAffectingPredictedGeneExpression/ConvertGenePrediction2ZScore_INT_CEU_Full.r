#Convert imputed WGS based prediction into corresponing ZSCORE and int zscore

 library(magrittr);
 library(RNOmni);
 library(dplyr)
 library(stringr)


BatchList=c("SKL_10073_B01_GRM_WGS_2016-02-25","SKL_11154_B01_GRM_WGS_2016-03-17","SKL_11694_B01_GRM_WGS_2017-08-18");
ConsiseBatchNamelist=c("SKL_10073","SKL_11154","SKL_11694");
DataList=c("Full","CEU");
   Outliers=c("MSSM-DNA-PFC-375","MSSM-DNA-PFC-269","CMC-HBCC-DNA-ACC-6052","CMC-HBCC-DNA-ACC-5669","CMC-HBCC-DNA-ACC-4237","CMC-HBCC-DNA-ACC-5646",
    "CMC-HBCC-DNA-ACC-5777","CMC-HBCC-DNA-ACC-5682","CMC-HBCC-DNA-ACC-4284","CMC-HBCC-DNA-ACC-4137",
    "CMC-HBCC-DNA-ACC-6009","CMC-HBCC-DNA-ACC-6056","CMC-HBCC-DNA-ACC-4029","CMC-HBCC-DNA-ACC-4200",
    "CMC-HBCC-DNA-ACC-4074","CMC-HBCC-DNA-ACC-4204","CMC-HBCC-DNA-ACC-5654","CMC-HBCC-ACC-DNA-4235");
  CEUSampleIDInfoFile="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/CMC_CAUSampleInformation.txt";
  CEUSampleIDMatrix=read.table(CEUSampleIDInfoFile, sep="\t",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE);#Obtain column names from File By check.names  
  CEUWGSID=CEUSampleIDMatrix[,2];
  print(length(CEUWGSID));

  for(iBatch in seq(1,3))#2
  {

#     /data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSImputationSKL_10073_B01_GRM_WGS_2016-02-25PsychodeBestPredictionExpression.txt
#     /data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSImputationSKL_11154_B01_GRM_WGS_2016-03-17PsychodeBestPredictionExpression.txt
#     /data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSImputationSKL_11694_B01_GRM_WGS_2017-08-18PsychodeBestPredictionExpression.txt

     iPredictedExpressionFile=paste("/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSImputation",BatchList[iBatch],"PsychodeBestPredictionExpression.txt",sep="");
     PredictedGeneExpression=read.table(iPredictedExpressionFile, sep="\t",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE);#Obtain column names from File By check.names   
    print(iPredictedExpressionFile); 
   PredictedGeneExpression =as.data.frame(PredictedGeneExpression);
    GeneIDColNames=colnames(PredictedGeneExpression);
     GeneIDColNames=str_replace_all(GeneIDColNames, "[.]", "-");#replace "." into "-"
    colnames(PredictedGeneExpression)=GeneIDColNames;

  #  GeneIDColNames=as.vector(GeneIDColNames);
    print(length(GeneIDColNames));
    OverlapID=intersect(GeneIDColNames,Outliers);
    print(OverlapID);
    print(length(OverlapID));
  #  print(GeneIDColNames);
     print(dim(PredictedGeneExpression));
     NormalPredictedGeneExpression=PredictedGeneExpression[,!(names(PredictedGeneExpression)%in%Outliers)]; 
     print(dim(NormalPredictedGeneExpression));
    GeneIDRowMatrix=NormalPredictedGeneExpression[c(1:7)];#Obtain Gene Name column information
    GeneIDRowMatrixColNames=colnames(GeneIDRowMatrix);
    NormalPredictedGeneExpression=NormalPredictedGeneExpression[-1:-7];#Remove Gene Name column information
      for(iCEU in seq(1,2))
     {
       if(iCEU==1)
       {
         GeneExpression=NormalPredictedGeneExpression;
       }
       else
       {
         GeneExpression=NormalPredictedGeneExpression[,(names(NormalPredictedGeneExpression)%in%CEUWGSID)];
        }
      GeneExpressionNames=colnames(GeneExpression);

      GeneExpressionDim=dim(GeneExpression);
     print(GeneExpressionDim);    
     GeneExpression[,1:GeneExpressionDim[2]] <- sapply(GeneExpression[,1:GeneExpressionDim[2]], as.numeric);
     GeneExpressionINT= t(apply(GeneExpression, 1, rankNorm));# The result from the apply() had to be transposed using t() to get the same layout as the input matrix A  
     GeneExpressionZScore=t(apply(GeneExpression, 1,scale));
     print(GeneExpressionINT[c(1:2),c(1:5)]);
     print( "GeneExpressionZScore");
     print(GeneExpressionZScore[c(1:2),c(1:5)]);
     GeneExpressionINTMerged=as.data.frame(cbind(GeneIDRowMatrix, GeneExpressionINT));
     colnames(GeneExpressionINTMerged)=c(GeneIDRowMatrixColNames,GeneExpressionNames);;
     print(dim(GeneExpressionINTMerged)); 
     PredictedExpressionFile=paste("/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/CMC_HBCC_PredictedExpression_INT_ZScore/PsychEncodeWeightBased",ConsiseBatchNamelist[iBatch],"_",DataList[iCEU],"_GenePredictedExINTZScore_ImputedWGS.txt",sep="");   
     write.table(GeneExpressionINTMerged, file=PredictedExpressionFile, append = FALSE, quote =FALSE, sep = "\t", eol = "\n",col.names=TRUE,row.names=FALSE);
     GeneExpressionIZScoreMerged=as.data.frame(cbind(GeneIDRowMatrix,GeneExpressionZScore)); 
     colnames(GeneExpressionIZScoreMerged)=c(GeneIDRowMatrixColNames,GeneExpressionNames); 
     PredictedExpressionFile=paste("/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/CMC_HBCC_PredictedExpression_INT_ZScore/PsychEncodeWeightBased",ConsiseBatchNamelist[iBatch],"_",DataList[iCEU],"_GenePredictedExNormalZScore_ImputedWGS.txt",sep="");     
     write.table(GeneExpressionIZScoreMerged, file=PredictedExpressionFile, append = FALSE, quote =FALSE, sep = "\t", eol = "\n",col.names=TRUE,row.names=FALSE);
   }
} 



