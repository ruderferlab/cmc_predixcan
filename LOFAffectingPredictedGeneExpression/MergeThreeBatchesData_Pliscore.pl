 #!/usr/in/perl -w
  use strict;
  use List::Util qw( min max );
  use warnings;
  use Data::Dumper;
  use lib "/home/hanl3/perl5/module/lib/site_perl/5.14.2";
  use Text::CSV_PP;

   my %EnsembleST_SG=();
   my $iLineNum=0;
   
   my $EnsembleIDFile="/home/hanl3/cmc/database/EnsembleGeneID.txt";
      open(INPUT,$EnsembleIDFile) || die "can’t open $EnsembleIDFile";
        while (<INPUT>)
        {
           chomp;
             my @cols=split(/\t/,$_);
           if($iLineNum==0)
           {
             $iLineNum++;
              next;
           }
          $EnsembleST_SG{$cols[1]}=$cols[12];
        }
      close INPUT;


     my %Gnomad_LOF=();#EnsembleID_oePLI_PLI#oe_lof[col6],oe_lof[col21]
     my %GeneID_ENSG=();
       $iLineNum=0;
      my $gnomadFile="/home/hanl3/cmc/files/constraint.txt";
    #gene	transcript	canonical	obs_lof	exp_lof(5)	oe_lof	oe_lof_lower	oe_lof_upper	obs_mis	exp_mis(10)	oe_mis	oe_mis_lower	oe_mis_upper	obs_syn	exp_syn(15)	oe_syn	oe_syn_lower	oe_syn_upper	lof_z	mis_z(20)	syn_z	pLI[22]	pRec	pNull	gene_issues
      open(INPUT,$gnomadFile) || die "can’t open $gnomadFile";
        while (<INPUT>)
        {
            chomp;
            my @cols=split(/\s++/,$_);
            if($iLineNum==0)
            {
             $iLineNum++;
             print "$cols[0]\t$cols[1]\t$cols[2]\tENSGID\t$cols[5]\t$cols[21]\n";
              next;
            }
           next if($cols[2] ne "true");
           if(defined($EnsembleST_SG{$cols[1]}))#cols[1]->transcript, cols[5]->oe_lof,cols[21]->pLI
           {
              my $Ensemble_SG=$EnsembleST_SG{$cols[1]};
              $GeneID_ENSG{$cols[0]}=$Ensemble_SG;
              my $OE_LOF="NA";
              my $Pliscore="NA";
              if($cols[5] ne "NA")
              {
                $OE_LOF=sprintf("%.6f",$cols[5]);
              }
              if($cols[21] ne "NA")
              {
               $Pliscore=sprintf("%.10f",$cols[21]);
              }
              $Gnomad_LOF{$Ensemble_SG}="$OE_LOF\t$Pliscore";
           }
          $iLineNum++;
          if($iLineNum<20)
          {
           print "$cols[0]\t$cols[1]\t$cols[2]\t$EnsembleST_SG{$cols[1]}\t$cols[5]\t$cols[21]\n";
          }
       }  
      close INPUT;

     my %EnsembleSG_PLIFDR=();
    $iLineNum=0;
     my $PliFDRFile="/home/hanl3/cmc/database/GenicIntolerance_v3_12Mar16.txt";
     # GENE    ALL_0.01%       %ALL_0.01%      ALL_0.1%        %ALL_0.1%(5)       ALL_1%  %ALL_1% PP2_ALL_0.1%    %PP2_ALL_0.1%   EA_0.1%(10)  %EA_0.1%        EA_1%   %EA_1%  AA_0.1% %AA_0.1%(15)        AA_1%   %AA_1%  OEratio %OEratio        %ExAC_0.1%popn(20)  %ExAC_0.05%popn %ExAC_0.01%  OEratio-percentile[ExAC] LoF-FDR[ExAC]
       open(INPUT,$PliFDRFile) || die "can’t open $PliFDRFile";
        while (<INPUT>)
        {  
           chomp;
           my @cols=split(/\s++/,$_);
           if($iLineNum==0)
           { 
             print "$cols[0]\t$cols[23]\n";
             $iLineNum++;
              next;
            }
           if(defined($GeneID_ENSG{$cols[0]}))
           {
              my $Ensemble_SG=$GeneID_ENSG{$cols[0]};
               my $EXAC_PLI_FDR="NA";  
              if($cols[23] ne "N/A")
              { 
                $EXAC_PLI_FDR=sprintf("%.8f",$cols[23]);
              }
               $EnsembleSG_PLIFDR{$Ensemble_SG}=$EXAC_PLI_FDR;
           }
           $iLineNum++;   
          if($iLineNum<20)
          {
            my $size=scalar(@cols);
            print "$cols[0]\t$cols[23]\t$size\n";
          }
       }
      close INPUT;



     my $size=keys %EnsembleSG_PLIFDR;
     my $size2=keys %Gnomad_LOF;
    print "gene size:\t$size\t$size2\n";     


#/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSMerged_PsychencodeWeightZScore_AnnovarMergedCEUBestAlgorithmWithoutOutliers.txt
     my %EachVariantFreq=();
      my $MergedAnnotatedInfoFile="/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSMerged_PsychencodeWeightZScore_AnnovarMergedCEUBestAlgorithmWithoutOutliers_PLIScore.txt";
      open OUTPUT, ">$MergedAnnotatedInfoFile" or die "Can't open Output file:$MergedAnnotatedInfoFile";
      $iLineNum=0;
      my $GeneAnnovarFile="/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSMerged_PsychencodeWeightZScore_AnnovarMergedCEUBestAlgorithmWithoutOutliers.txt";
      open(INPUT,$GeneAnnovarFile) || die "can’t open $GeneAnnovarFile";
        while (<INPUT>)
        {       
            chomp;  
            my @cols=split(/\t/,$_);
            if($iLineNum==0)
            { 
            print OUTPUT "$_\toe_LOF_gnomad\tPLI_gnomad\tEXAC_LoF_FDR\n";      
              $iLineNum++;
              next;   
            } 
           if(!defined($EachVariantFreq{$cols[0]}{$cols[1]}{$cols[6]}{$cols[7]}))
           {      
             $EachVariantFreq{$cols[0]}{$cols[1]}{$cols[6]}{$cols[7]}=1;
            }
           else
            {
             $EachVariantFreq{$cols[0]}{$cols[1]}{$cols[6]}{$cols[7]}++;
            }

           if(defined($Gnomad_LOF{$cols[13]}))
           {
            print OUTPUT  "$_\t$Gnomad_LOF{$cols[13]}";
           }   
           else
           {   
           print OUTPUT "$_\tNA\tNA";
           }      

           if(defined($EnsembleSG_PLIFDR{$cols[13]}))
           {
            print OUTPUT  "\t$EnsembleSG_PLIFDR{$cols[13]}\n";
           }
           else
           {
           print OUTPUT "\tNA\n";
           }
         }
        close INPUT;
     close OUTPUT;
 
      my $MergedAnnotatedSingletonFile="/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSMerged_PsychencodeWeightZScore_AnnovarMergedCEUBestAlgorithmWithoutOutliers_PLIScore_Singleton.txt";
      open OUTPUT, ">$MergedAnnotatedSingletonFile" or die "Can't open Output file:$MergedAnnotatedSingletonFile";
      $iLineNum=0;
      $GeneAnnovarFile="/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSMerged_PsychencodeWeightZScore_AnnovarMergedCEUBestAlgorithmWithoutOutliers.txt"; 
      open(INPUT,$GeneAnnovarFile) || die "can’t open $GeneAnnovarFile";
        while (<INPUT>)
        {   
            chomp;
            my @cols=split(/\t/,$_);
            if($iLineNum==0)
            {
              print OUTPUT "$_\tVariantType\toe_LOF_gnomad\tPLI_gnomad\tEXAC_LoF_FDR\n";
               print "$cols[10]\t$cols[14]\t$cols[23]\n";
              $iLineNum++;
              next;
            }
           #	Ref_Annovar[6]	Alt_Annovar[7] ExonicFunc_ref[10],CADD_phred[14]
           next if($EachVariantFreq{$cols[0]}{$cols[1]}{$cols[6]}{$cols[7]}>=2);#Chr	Pos_vcf	Ref_vcf	Alt_vcf
           next if($cols[23] ne "0/1");#cols[23]==RareGtype
            print OUTPUT  "$_";
           if($cols[10] eq "frameshift deletion"|| $cols[10] eq "frameshift insertion" ||$cols[10] eq "stopgain")
           {
            print OUTPUT "\tloss-of-function";
           }
           elsif($cols[10] eq "synonymous SNV")
           {
            print OUTPUT "\tsynonymous";
           }
           elsif($cols[14] ne "."&& $cols[14]>15)
           {
             print OUTPUT "\tmissense_CADDLarger15";
           }
           elsif($cols[14] ne "."&& $cols[14]<=15)
           {
             print OUTPUT "\tmissense_CADDSmaller15";
           }
           else
           {
             print OUTPUT "\tNA";
           }
          if(defined($Gnomad_LOF{$cols[13]}))
           {
            print OUTPUT  "\t$Gnomad_LOF{$cols[13]}";
           }
           else
           {
           print OUTPUT "\tNA\tNA";
           }

           if(defined($EnsembleSG_PLIFDR{$cols[13]}))
           {
            print OUTPUT  "\t$EnsembleSG_PLIFDR{$cols[13]}\n";
           }   
           else
           {
           print OUTPUT "\tNA\n";
           }
           if($iLineNum<10)
           {
             print "$cols[10]\t$cols[14]\t$cols[23]\n";
            }
           $iLineNum++;
         }
        close INPUT;
     close OUTPUT;

   
