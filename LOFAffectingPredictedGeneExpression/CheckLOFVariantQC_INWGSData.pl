 #!/usr/in/perl -w
 use strict;
 use List::Util qw( min max );
 use warnings;
 use Data::Dumper;
 use Getopt::Long;
 use Fcntl qw/ :flock /;# Import LOCK_* constants
 use lib "/home/hanl3/perl5/module/lib/site_perl/5.14.2";
 use Text::CSV_PP;


 my $FileOrder=$ARGV[0];
 my $FileOrder1=$FileOrder+1;
my %LOFVariantInfo=();
  my $iLineNum=0;
  my $LOFVariantFile="/data/ruderferlab/projects/cmc/results/pseq_SKLWGS_Summary/allchroms.genome_annovar_exonicFun_LOFSubset.vcf";
    open(INPUT,$LOFVariantFile) || die "can’t open $LOFVariantFile";
    while (<INPUT>)
    {
      chomp;
      my @cols=split(/\t/,$_,19);
      splice(@cols,18);
     my $string=join("\t",@cols);
     $LOFVariantInfo{$cols[0]}{$cols[1]}=$string;
     }
    close INPUT;
  my @Batches=("10073_B01_GRM_WGS_2016-02-25","11694_B01_GRM_WGS_2017-08-18","11154_B01_GRM_WGS_2016-03-17");

my $WGSFileDir="/home/hanl3/cmc/data/wgs/vcf";
 for(my $iBatch=$FileOrder;$iBatch<$FileOrder1;$iBatch++)
  {
   my $LOF_WGS_MergedFile="/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/SKL_$Batches[$iBatch]LOFVarinatInfo.txt";
   open OUTPUT, ">$LOF_WGS_MergedFile" or die "Can't open Output file:$LOF_WGS_MergedFile";

    $iLineNum=0;
  my $VCFFile="$WGSFileDir/SKL_$Batches[$iBatch].recalibrated_variants_PASS_WholeGenome.vcf";
  print "$Batches[$iBatch]\n";
    open(INPUT,$VCFFile) || die "can’t open $VCFFile";
    while (<INPUT>)
    {
      chomp;
     if($_ !~ /^##/)
     {
       if($_ =~ /^#/)
       { 
        my  @cols=split(/\t/,$_,9);
        my $str1="Chr";
        my $str2="Pos_vcf";
        print OUTPUT  "$LOFVariantInfo{$str1}{$str2}\tREF\tALT\tQUAL\tFILTER\tINFO\n";
       }
      else
       {
       my @cols=split(/\t/,$_,3);
   
      if(defined($LOFVariantInfo{$cols[0]}{$cols[1]}))
      {
       @cols=split(/\t/,$_,9);
       if($cols[6] eq "PASS")
       {
        print OUTPUT "$LOFVariantInfo{$cols[0]}{$cols[1]}\t$cols[3]\t$cols[4]\t$cols[5]\t$cols[6]\t$cols[7]\n";
       }

        $iLineNum++;
      }
      }
    }
    }
    close INPUT;

   close OUTPUT;
}
