 #!/usr/in/perl -w
 use strict;
 use List::Util qw( min max );
 use warnings;
 use Data::Dumper;
 use lib "/home/hanl3/perl5/module/lib/site_perl/5.14.2";
 use Text::CSV_PP;

my %LOFVariantInfo=();
  my $iLineNum=0;
  my $LOFVariantFile="/data/ruderferlab/projects/cmc/results/pseq_SKLWGS_Summary/allchroms.genome_annovar_exonicFun_LOFSubset_synonymousSNV.vcf";
    open(INPUT,$LOFVariantFile) || die "can’t open $LOFVariantFile";
    while (<INPUT>)
    {
      chomp;
      my @cols=split(/\t/,$_,19);
      splice(@cols,18);
     my $string=join("\t",@cols);
     $LOFVariantInfo{$cols[0]}{$cols[1]}=$string;
     }
    close INPUT;

  print "Finish reading file: $LOFVariantFile\n";
   my %gnomad211_exomeVariantsFreqList=();
     $iLineNum=0;
   my $gnomadFile="/home/hanl3/cmc/database/annovar/humandbOrg/hg19_gnomad211_exome.txt";
   open(INPUT,$gnomadFile) || die "can’t open $gnomadFile";
   while (<INPUT>)
    { 
       chomp;
       my @cols=split(/\t/,$_,7);
       next if($cols[3] eq "-" ||$cols[4] eq "-");#remove some variants with ref or alt allele are missing allele 
       $cols[0]=~ s/^\s+|\s+$//g;
       $cols[1]=~ s/^\s+|\s+$//g;
       $cols[3]=~ s/^\s+|\s+$//g;
       $cols[4]=~ s/^\s+|\s+$//g;
       if(defined($LOFVariantInfo{$cols[0]}{$cols[1]}))
       {
         $gnomad211_exomeVariantsFreqList{$cols[0]}{$cols[1]}{$cols[3]}{$cols[4]}=$cols[5];
         $iLineNum++;# last if($iLineNum>10);
       }
     }
    close INPUT;
     print "Finish reading file: $gnomadFile\n";

   my %CADD_INDELs_Score=();
     $iLineNum=0;
  
   my $CADDScoreFile="/fs0/hanld/CADD/CADDv1.4_hg19_InDels_inclAnno_subset.tsv";
   open(INPUT,$CADDScoreFile) || die "can’t open $CADDScoreFile";
   while (<INPUT>)
    {   
       chomp;
       if($iLineNum<2)
       {
         $iLineNum++;
         next;
       }
       my @cols=split(/\t/,$_);
       if(defined($LOFVariantInfo{$cols[0]}{$cols[1]}))
       {   
         $CADD_INDELs_Score{$cols[0]}{$cols[1]}{$cols[2]}{$cols[3]}=$cols[16];
        # print "$cols[0]\t$cols[1]\t$cols[2]\t$cols[3]\t$cols[16]\n";
         $iLineNum++;
        # last if($iLineNum>10);
       }   
     }   
    close INPUT;
    print "Finish reading file: $CADDScoreFile\n";

   my @Batches=("10073_B01_GRM_WGS_2016-02-25","11694_B01_GRM_WGS_2017-08-18","11154_B01_GRM_WGS_2016-03-17");
   my $iMissingCadd=0;
   my $iCadd=0;
   my $iTotalSelectedVariants=0;
 
    for(my $iBatch=0;$iBatch<3;$iBatch++)
    {
      my $LOF_WGS_MergedFile="/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/SKL_$Batches[$iBatch]LOFVarinatInfo_synonymousSNV.filter.txt";
      open OUTPUT, ">$LOF_WGS_MergedFile" or die "Can't open Output file:$LOF_WGS_MergedFile";
      $iLineNum=0;
      #SKL_10073_B01_GRM_WGS_2016-02-25LOFVarinatInfo.txt  SKL_11154_B01_GRM_WGS_2016-03-17LOFVarinatInfo.txt  SKL_11694_B01_GRM_WGS_2017-08-18LOFVarinatInfo.txt
      my $VCFFile="/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/SKL_$Batches[$iBatch]LOFVarinatInfo_synonymousSNV.txt";
     print "$Batches[$iBatch]\n";
     open(INPUT,$VCFFile) || die "can’t open $VCFFile";
     while (<INPUT>)
     {
      chomp;
       if($iLineNum == 0)
       { 
        print OUTPUT "$_\n";
        $iLineNum++;
       }
      else
       {

       my @cols=split(/\t/,$_);
       my $BoolSNP=0;#Judge if the variant is SNP or INDEL
      #     print "$cols[2]\t$cols[3]\t$cols[83]\t$cols[19]\n";
       if(length($cols[2])==1 && length($cols[3])==1)
       {
         $BoolSNP=1;
       }#CADD_phred (col 54) gnomAD_genome_ALL(84),col120(RefAllele_batch),col121(AltAllele_Batch),col122(QUAL),col124( each variant info),col128(fMissingRate),col129(fAlleleFreq)
    
       #Indel CADD filter(phred score >15)
       next if( $BoolSNP==0 && !defined($CADD_INDELs_Score{$cols[0]}{$cols[1]}{$cols[2]}{$cols[3]}));# keep CADD<15 || $CADD_INDELs_Score{$cols[0]}{$cols[1]}{$cols[2]}{$cols[3]}<15));
       if(defined($CADD_INDELs_Score{$cols[0]}{$cols[1]}{$cols[2]}{$cols[3]}))
       {
         $cols[53]=$CADD_INDELs_Score{$cols[0]}{$cols[1]}{$cols[2]}{$cols[3]};
       }
    #  next if ($cols[53] ne ".");
      # {
       #  $iMissingCadd++;
       # }
       #if($cols[53] eq ".")
       # {
       #  $iCadd++;
       # }

       #next if ($cols[53] ne "." && $cols[53]<15);#filter `CADD_Phred<15
       next  if($cols[127]>=0.05);#missing rate less than 0.05
       next  if($cols[128]==0 ||$cols[128]==1);
       next  if($cols[128]>=0.01 && $cols[128]<=0.99);#each batch based rare allele frequency less than 1%;
      # next  if($cols[83] ne "." &&  $cols[83]>=0.01);  #gnomAD_genome_ALL<0.01 
      # gnomAD211_genome filter prob>0.01   
       if(defined($gnomad211_exomeVariantsFreqList{$cols[0]}{$cols[1]}{$cols[2]}{$cols[3]}))
       {
          if($gnomad211_exomeVariantsFreqList{$cols[0]}{$cols[1]}{$cols[2]}{$cols[3]}>=0.01)
          {
              print "$gnomad211_exomeVariantsFreqList{$cols[0]}{$cols[1]}{$cols[2]}{$cols[3]}\n";
              next;
          }
         else
          {
            $cols[83]=$gnomad211_exomeVariantsFreqList{$cols[0]}{$cols[1]}{$cols[2]}{$cols[3]};
           }
       }
       my @AllAltAlleleList=split(/,/,$cols[120]);
      if($cols[2] eq $cols[119] && $cols[3] eq $AllAltAlleleList[0] && $cols[121]>=30) #QUAL,All batchs vs single batch, make sure that ref1=ref2,Alt1=Alt2
      {
        my $iFilter=0;

          my @QCmeasurements=split(/;/,$cols[123]);#-SNP filter criteria:DP<10,QD<2.0,SOR > 3.0,FS > 60.0, MQ < 40.0, MQRankSum < -12.5, ReadPosRankSum < -8.0
                                                  #-INDEL filter criteria:DP<10,,QD<2.0,FS>200.0,ReadPosRankSum<-20.0
          my $NumberofElements=scalar(@QCmeasurements);

           for(my $ii=0;$ii<@QCmeasurements;$ii++)
           {
         #    print "$ii,$QCmeasurements[$ii],\n";
           }
           my @DP_Index = grep{$QCmeasurements[$_] =~ /^DP=/} (0 ..$#QCmeasurements); #Filter DP<10;DP has been checked
           if(scalar(@DP_Index) != 0)
           {
              $QCmeasurements[$DP_Index[0]] =~ s/^DP=//ig;
              if($QCmeasurements[$DP_Index[0]]<10)
              {
                 $iFilter++;
                print "DP=$QCmeasurements[$DP_Index[0]]\n";
              }
           } 
           my @QD_Index = grep{$QCmeasurements[$_] =~ /^QD=/} (0 ..$#QCmeasurements);#QD Filter:QD<2, has been checked
           if(scalar(@QD_Index) !=0) 
           {   
             $QCmeasurements[$QD_Index[0]] =~ s/^QD=//ig;
             if($QCmeasurements[$QD_Index[0]]<2)
             {
                $iFilter++;
               print "QD=$QCmeasurements[$QD_Index[0]]\n";
              }
           }   
           my @FS_Index = grep{$QCmeasurements[$_] =~ /^FS=/} (0 ..$#QCmeasurements);#has been checked
           if(scalar(@FS_Index) != 0)
           {
             $QCmeasurements[$FS_Index[0]] =~ s/^FS=//ig;
             if($BoolSNP==1)
             {
              if($QCmeasurements[$FS_Index[0]]>60)
              {
                 $iFilter++;
                 print "SNP\tFS=$QCmeasurements[$FS_Index[0]]\n";
               }
             }
             else
             {
                 if($QCmeasurements[$FS_Index[0]]>200)
                 {
                   $iFilter++;
                   print "INDEL\tFS=$QCmeasurements[$FS_Index[0]]\n";
                 }
              }
           }
            my @RPRS_Index= grep{$QCmeasurements[$_] =~ /^ReadPosRankSum=/} (0 ..$#QCmeasurements);#ReadPosRankSum has been checked
           if(scalar(@RPRS_Index) !=0)
           {
             $QCmeasurements[$RPRS_Index[0]] =~ s/^ReadPosRankSum=//ig;
             if($BoolSNP==1)
             {   
                if($QCmeasurements[$RPRS_Index[0]]<-8.0)
                {   
                 $iFilter++;
                 print "ReadPosRankSum=$QCmeasurements[$RPRS_Index[0]]\n";
                }   
             }   
             else
             {   
                 if($QCmeasurements[$RPRS_Index[0]]<-20.0)
                 {   
                   $iFilter++;
                   print "ReadPosRankSum=$QCmeasurements[$RPRS_Index[0]]\n";
                 } 
              } 
           }

           my @SOR_Index = grep{$QCmeasurements[$_] =~ /^SOR=/} (0 ..$#QCmeasurements);#SOR>3
           if( scalar(@SOR_Index)!= 0 )
           {
             $QCmeasurements[$SOR_Index[0]] =~ s/^SOR=//ig;
             if($BoolSNP==1 && $QCmeasurements[$SOR_Index[0]]>3)
             {
                $iFilter++; 
              print "SOR=$QCmeasurements[$SOR_Index[0]]\n";
             }
           } 
           my @MQ_Index = grep{$QCmeasurements[$_] =~ /^MQ=/} (0 ..$#QCmeasurements);#MQ has been checked
           if( scalar(@MQ_Index)!= 0 )
           {
             $QCmeasurements[$MQ_Index[0]] =~ s/^MQ=//ig;
             if($BoolSNP==1 && $QCmeasurements[$MQ_Index[0]]<40)
             { 
                $iFilter++;
              print "MQ=$QCmeasurements[$MQ_Index[0]]\n";
             }
           }       
           my @MQRankSum_Index = grep{$QCmeasurements[$_] =~ /^MQRankSum=/} (0 ..$#QCmeasurements);
           if( scalar(@MQRankSum_Index)!= 0 )
           {
             $QCmeasurements[$MQRankSum_Index[0]] =~ s/^MQRankSum=//ig;#MQRankSum has been checked
             if($BoolSNP==1 && $QCmeasurements[$MQRankSum_Index[0]]<-12.5)
             { 
                $iFilter++;
               print "MQRankSum=$QCmeasurements[$MQRankSum_Index[0]]\n";
             }
           }
           if($iFilter==0)
           {
              print OUTPUT "$cols[0]";
             for(my $ii=1;$ii<@cols;$ii++)
              {
                print OUTPUT "\t$cols[$ii]";
              }
              print OUTPUT "\n";
              $iTotalSelectedVariants++;
           }
           $iLineNum++;
        }
      }
    }
    close INPUT;

   close  OUTPUT;
}
 print "$iTotalSelectedVariants\t$iMissingCadd\t$iCadd\n";
