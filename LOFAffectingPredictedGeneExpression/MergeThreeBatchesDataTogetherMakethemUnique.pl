 #!/usr/in/perl -w
  use strict;
  use List::Util qw( min max );
  use warnings;
  use Data::Dumper;
  use lib "/home/hanl3/perl5/module/lib/site_perl/5.14.2";
  use Text::CSV_PP;

#     /data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMC_ACC_PsychencodeWeightZScore_AnnovarMergedCEUBestAlgorithmWithoutOutliers.txt
#    /data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSHBCC_DLPFC_PsychencodeWeightZScore_AnnovarMergedCEUBestAlgorithmWithoutOutliers.txt
#    /data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMC_DLPFC_PsychencodeWeightZScore_AnnovarMergedCEUBestAlgorithmWithoutOutliers.txt 
 my @TissueArray=("HBCC_DLPFC","CMC_DLPFC","CMC_ACC");
  my %EachIndSNP_Info=();
  my %EachLineSize=();
  my $Headingline=""; 
  my %AllIndsWGS=();
  for(my $iT=0;$iT<@TissueArray;$iT++)
   {
      my %AllIndsIDs=();
        my %AllIndsIDs1=(); 
        my $iLineNum=0;
        my $InputFile="/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGS$TissueArray[$iT]_PsychencodeWeightZScore_AnnovarMergedCEUBestAlgorithmWithoutOutliers.txt";
        open(INPUT,$InputFile) || die "can’t open $InputFile";
        while (<INPUT>)
        {   
            chomp;  
             my @cols=split(/\t/,$_);
             my $Size=scalar(@cols); 
             $EachLineSize{$Size}=1;
             if($iLineNum==0)
             {
               print "$cols[0]\t$cols[1]\t$cols[13]\t$cols[22]\t$cols[26]\n"; 
               if($iT==0)
               {
                $Headingline=$_;
               }
               $iLineNum++;
                next;
           }
            $cols[0] =~ s/^\s+|\s+$//g;
            $cols[1] =~ s/^\s+|\s+$//g;
            $cols[13] =~ s/^\s+|\s+$//g;
            $cols[22] =~ s/^\s+|\s+$//g; 
            $AllIndsIDs1{$cols[22]}=1;
            $AllIndsWGS{$cols[22]}=1;
            if(!defined($EachIndSNP_Info{$cols[0]}{$cols[1]}{$cols[13]}{$cols[22]}))
            {
               $AllIndsIDs{$cols[22]}=1;
               $EachIndSNP_Info{$cols[0]}{$cols[1]}{$cols[13]}{$cols[22]}=$_;   
            #   print"$EachIndSNP_Info{$cols[0]}{$cols[1]}{$cols[13]}{$cols[22]}\n"; 
            }
            else
            {
               my @PreviousSNPInfo=split(/\t/,$EachIndSNP_Info{$cols[0]}{$cols[1]}{$cols[13]}{$cols[22]});
               # print "$TissueArray[$iT]:$cols[0]\t$cols[1]\t$cols[13]\t$cols[22]\t$PreviousSNPInfo[26]\n";  
               if($PreviousSNPInfo[26] ne "NA" && $cols[26] ne "NA" &&  $PreviousSNPInfo[26]<$cols[26]) #select higher correlated predicted expression
               {
                 $AllIndsIDs{$cols[22]}=1;
                 $EachIndSNP_Info{$cols[0]}{$cols[1]}{$cols[13]}{$cols[22]}=$_;
               }
               elsif($cols[26] ne "NA" && $PreviousSNPInfo[26] eq "NA")
               {
                 $AllIndsIDs{$cols[22]}=1;
                # print "$PreviousSNPInfo[26]\t$cols[26]\n"; 
                $EachIndSNP_Info{$cols[0]}{$cols[1]}{$cols[13]}{$cols[22]}=$_;
               }
             }
           $iLineNum++;
        }
       close INPUT;
       my $IndSize=keys %AllIndsIDs;
       my $IndSize1=keys %AllIndsIDs1;
      print "IndSize= $IndSize\t$IndSize1\n ";
  }
    my $LineSize=keys %EachLineSize;
    my $IndSize=keys %AllIndsWGS;
    print "line size: $LineSize\t$IndSize\n";

    my $MergedAnnotatedInfoFile="/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSMerged_PsychencodeWeightZScore_AnnovarMergedCEUBestAlgorithmWithoutOutliers.txt";
     open OUTPUT, ">$MergedAnnotatedInfoFile" or die "Can't open Output file:$MergedAnnotatedInfoFile";
     print OUTPUT "$Headingline\n";
     foreach my $chr (sort keys %EachIndSNP_Info)
     {
       foreach my $pos (sort{$a<=> $b} keys %{$EachIndSNP_Info{$chr}})
       {
         foreach my  $gene (  keys %{ $EachIndSNP_Info{$chr}{$pos} })
         {   
           foreach my $ind ( keys %{ $EachIndSNP_Info{$chr}{$pos}{$gene} }) 
           {
             print OUTPUT "$EachIndSNP_Info{$chr}{$pos}{$gene}{$ind}\n";
          }
        }
      }
   }
  close OUTPUT;

