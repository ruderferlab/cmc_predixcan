#This plot is used to compare prediction accuracy among different algorithms
#!/usr/local/R/3.2.0/x86_64/intel14/nonet/bin/Rscript  --vanilla
library(dplyr);
library(data.table)

mydir="/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression";
PredictedExpresionResultComparisonFile=paste(mydir,"/psychencode_PredictionBestAlgorithmUsedSNPsPropHstgram_Scatterplot.pdf",sep="");
pdf(file=PredictedExpresionResultComparisonFile,paper='A4',pointsize=16);

  par(mar=c(5,4,1.5,0.25),mfrow=c(2,1),oma=c(0,0,0,0));
  linewide=2;
  axixwide=0.5;
    tickwide=0.5;
   cex_size=0.62;
   leg_cex_size=0.8;
   axes=F;
   xaxs="s";
   cex_size3=0.65;

WGSBatches=c("10073_B01_GRM_WGS_2016-02-25","11694_B01_GRM_WGS_2017-08-18","11154_B01_GRM_WGS_2016-03-17");
NumberofInds=c(328,119,326);

#  /data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/SKL_10073_B01_GRM_WGS_2016-02-25PsychodeBestPredictionExpression.txt 
#  /data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/SKL_11694_B01_GRM_WGS_2017-08-18PsychodeBestPredictionExpression.txt  
#  /data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/SKL_11154_B01_GRM_WGS_2016-03-17PsychodeBestPredictionExpression.txt

Methods=c("best");
 
for(iB in seq(1,3))
{
     PredictedExpressionFile=paste(mydir,"/SKL_",WGSBatches[iB],"PsychodeBestPredictionExpression.txt",sep="");
     PredictedExpressionMatrix=fread(PredictedExpressionFile, sep="\t",colClasses="character",na.strings = "NA", header=TRUE,fill=FALSE, strip.white=TRUE);
     PredictedExpressionMatrix=as.data.frame(PredictedExpressionMatrix);
     PredictedExpressionMatrix_Dim=dim(PredictedExpressionMatrix); 
     print(PredictedExpressionMatrix_Dim);
     for(iC in seq(5,PredictedExpressionMatrix_Dim[[2]]))
     {
        PredictedExpressionMatrix[,iC] <- as.numeric(PredictedExpressionMatrix[,iC]);
     }
 
    plot(PredictedExpressionMatrix[,6],PredictedExpressionMatrix[,5], main=WGSBatches[iB],ylab="Number of Actual SNPs", xlab="Number of Total SNPs",cex.main=0.6,cex=0.25,col=1,pch=21);
    print(summary(PredictedExpressionMatrix[,5]));
     print(summary(PredictedExpressionMatrix[,6]));
     PredictedExpressionMatrix$Poportion=PredictedExpressionMatrix$NumberofSNPs/PredictedExpressionMatrix$NumberofTotalSNPs;
     print(summary(PredictedExpressionMatrix$Poportion));

    hist(PredictedExpressionMatrix[,6], main="Training Data",xlab="Number of All SNPs for Each Gene",cex.main=0.6);
    hist(PredictedExpressionMatrix[,5], main=WGSBatches[iB],xlab="Number of Actual SNPs for Each Gene",cex.main=0.6);
   hist(PredictedExpressionMatrix$Poportion, main=WGSBatches[iB],xlab="Proportion of Used SNPs for Gene Prediction",cex.main=0.6);     
}



 dev.off();


