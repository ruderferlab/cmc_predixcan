
library(ggplot2)
library(gridExtra)
 library(grid)
library(lattice)
  library(ggplot2);
  library(devtools);
 library(magrittr);

#   2377 /data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMC_ACC_GeneLevelCorrPsyCode_V8SingleTissue_CEUWithoutOutliers.txt
#   3126 /data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMC_DLPFC_GeneLevelCorrPsyCode_V8SingleTissue_CEUWithoutOutliers.txt
#   3045 /data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSHBCC_DLPFC_GeneLevelCorrPsyCode_V8SingleTissue_CEUWithoutOutliers.txt


 Outputfile="/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/GeneLevelSingTissueV8_PsychencodeSpearmanCorrWGSBrain2TissuesScatterPlot_BestAlgorithm.pdf";
pdf(file=Outputfile,pointsize=16,width=12, height=10);#for best algorithm
 
#  pdf(file=Outputfile,pointsize=16,width=12, height=30);#Fol all algorithm
  par(mar=c(4,4,1.0,0.25),mfrow=c(3,2),oma=c(0,0,0,0));

   DataSampleSize=c(210,253,138);#All individuals 
   ActualTissues=c("WGSCMC_ACC","WGSCMC_DLPFC","WGSHBCC_DLPFC");

# CEUWGSID=CEUSampleIDMatrix[,2];

 ThreeTissues=c("ACC_BA24","PFC_BA9","PFC_BA9");
 MultiplyAlgorithms=c("top1","blup","bslmm","lasso","enet");

 myPlot<-list();
for(iActualT in seq(1,3))
{
   GeneLevelCorrFile=paste("/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/",ActualTissues[iActualT],"_","GeneLevelCorrPsyCode_V8SingleTissue_CEUWithoutOutliers.txt",sep="");
     GeneLevelCorrMatrix=read.table(GeneLevelCorrFile, sep="\t",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE);#Obtain column names from File By check.names   
      GeneLevelCorrMatrixColNames=colnames(GeneLevelCorrMatrix);
     GeneLevelCorrDim=dim(GeneLevelCorrMatrixColNames);
      for(kk in seq(2,8))
      {
       GeneLevelCorrMatrix[,kk]=as.numeric(GeneLevelCorrMatrix[,kk]);
      }  
 
     #for(iM in seq(1,5))     
     for(iM in seq(6,6)) 
    {
        PearsonCorr=signif(cor(GeneLevelCorrMatrix[,2],GeneLevelCorrMatrix[,2+iM],"pairwise.complete.obs",method="pearson"),digits = 3);
        SubsetMatrix=GeneLevelCorrMatrix[,c(1,2,(iM+2))];
        SubsetMatrix=as.data.frame(SubsetMatrix);
        colnames(SubsetMatrix)=c("GeneID","V8SingleTissue","Psycode"); 
        SubsetMatrix[,2]=as.numeric(SubsetMatrix[,2]);
        SubsetMatrix[,3]=as.numeric(SubsetMatrix[,3]);
        GeneLevelCorrDim=dim(SubsetMatrix);  
      size=GeneLevelCorrDim[1];
         print (size);
        yLabel=paste("Psycode_",MultiplyAlgorithms[iM],"SpearmanCorr",sep="");
        myPlot[[iActualT]]<-ggplot(data=SubsetMatrix,aes(x=V8SingleTissue,y=Psycode))+ geom_abline(intercept=0,slope=1,colour="red",size=0.8)+geom_point(size=1, shape=21) + ggtitle(paste(ActualTissues[iActualT],"_size",size,"_pCorr",PearsonCorr,sep=""))+ xlab("V8SingleTissue SpearmanCorr")+ylab(yLabel)+ coord_cartesian(xlim=c(-1,1)) + scale_x_continuous(breaks=seq(-1,1,0.1));#only for best algorithm

        #myPlot[[(iActualT-1)*5+iM]]<-ggplot(data=SubsetMatrix,aes(x=V8SingleTissue,y=Psycode))+ geom_abline(intercept=0,slope=1,colour="red",size=0.8)+geom_point(size=1, shape=21) + ggtitle(paste(ActualTissues[iActualT],"_size",size,"_pCorr",PearsonCorr,sep=""))+ xlab("V8SingleTissue SpearmanCorr")+ylab(yLabel)+ coord_cartesian(xlim=c(-1,1)) + scale_x_continuous(breaks=seq(-1,1,0.1));#for all algorithms

    }

  }

#   myPlot[[16]]<-ggplot() + theme_void();#For all algorithms

 myPlot[[4]]<-ggplot() + theme_void(); #just for best algorithm

  pushViewport(viewport(layout=grid.layout(2,2)))#defined how to arrange the plots, there are 4 for best algorithms
#    pushViewport(viewport(layout=grid.layout(8,2)))#defined how to arrange the plots, there are 30 
   vplayout<-function(x,y){viewport(layout.pos.row=x,layout.pos.col=y)}
   iNum=1;
   for(i in myPlot)
   {  
     print(iNum);
    if(iNum %% 2 == 1)
    {   
        print(myPlot[[iNum]],vp=vplayout((iNum-1)%/%2+1,1));
    }
    else
    {  
       print(myPlot[[iNum]],vp=vplayout((iNum-1)%/%2+1,2));
    }

   iNum=iNum+1;
 }



