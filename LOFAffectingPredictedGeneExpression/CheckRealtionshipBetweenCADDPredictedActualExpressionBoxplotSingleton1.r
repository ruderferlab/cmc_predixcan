
library(dplyr)

Outputfile=paste("/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/PsychencodeWeightZScore_AnnovarRelationshipBoxplot_Variant3.pdf",sep="");
  pdf(file=Outputfile);
  par(mar=c(5,4,1.5,0.25),mfrow=c(2,2),oma=c(0,0,0,0));
   linewide=2;
   axixwide=0.5;
   tickwide=0.5;
   cex_size=0.62;
   leg_cex_size=0.8;
   axes=F;
   xaxs="s";
   cex_size3=0.65;


  DeaseTypes=c("Control","SCZ");

TissueTypes=c("CMC_ACC","CMC_DLPFC","HBCC_DLPFC");
#SingletonFreqThreshold=c(0.0015,0.0015,0.0042[119]0.0015[326]
#17th,21th column [NumTotal fAlleleFreq]
#awk 'BEGIN{FS="\t"}{print $17,$21}'  /data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGSHBCC*_PsychencodeWeightZScore_AnnovarMergedCEUBestAlgorithmWithoutOutliers.txt|sort -n|uniq -c[NumTotal fAlleleFreq]
  for(iDx in seq(1,2))
  {
    AllTissueGeneExpression=NULL;

   for(iT in seq(1,3))
  {
       InFile=paste("/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/WGS",TissueTypes[iT],"_PsychencodeWeightZScore_AnnovarMergedCEUBestAlgorithmWithoutOutliers.txt",sep="");
       ZScore_ANNOVAR=read.table(InFile,sep="\t",colClasses="character",na.strings = "NA", header=TRUE,fill=FALSE, strip.white=TRUE);
       ZScore_ANNOVAR=as.data.frame(ZScore_ANNOVAR); 
      #Chr     Pos_vcf Ref_vcf Alt_vcf Start_Annovar(5)   End_Annovar     Ref_Annovar     Alt_Annovar     Func_ref        Gene_ref(10)        ExonicFunc_ref  AAChange_ref    Func_ens        Gene_ens        CADD_phred(15)      gnomAD_genome_ALL       NumTotal        NumOther        NumMissing      fMissingRate(20)    fAlleleFreq     LineSize        WGSIDWithRareAllele   RareGtype       ActualExZScore(25)  PredictedZScore SpearmanCorr 
     ZScore_ANNOVAR=ZScore_ANNOVAR[which(ZScore_ANNOVAR$Dx ==DeaseTypes[iDx]),];#Select one disease
#   1653 nonframeshift deletion
#    548 nonframeshift insertion
# 147425 nonsynonymous SNV
#   110 stoploss
#  ZScore_ANNOVAR=ZScore_ANNOVAR[which(ZScore_ANNOVAR$ExonicFunc_ref != "nonframeshift deletion"),];
 #   ZScore_ANNOVAR=ZScore_ANNOVAR[which(ZScore_ANNOVAR$ExonicFunc_ref != "nonframeshift insertion"),];
 #  ZScore_ANNOVAR=ZScore_ANNOVAR[which(ZScore_ANNOVAR$ExonicFunc_ref != "nonsynonymous SNV"),];
 #  ZScore_ANNOVAR=ZScore_ANNOVAR[which(ZScore_ANNOVAR$ExonicFunc_ref != "stoploss"),];

     ZScore_ANNOVAR$NumTotal=as.numeric(ZScore_ANNOVAR$NumTotal);
      ZScore_ANNOVAR$fAlleleFreq=as.numeric(ZScore_ANNOVAR$fAlleleFreq);
        ZScore_ANNOVAR$Singleton=ZScore_ANNOVAR$fAlleleFreq;
        ZScore_ANNOVAR$Singleton=0;
        ZScore_ANNOVAR$Singleton[ZScore_ANNOVAR$fAlleleFreq<=(0.5/ZScore_ANNOVAR$NumTotal)]=1;

       ZScore_ANNOVAR=ZScore_ANNOVAR[which(ZScore_ANNOVAR$Singleton==1),];#Select Rareallele
      ZScore_ANNOVARFull=ZScore_ANNOVAR[which(ZScore_ANNOVAR$ActualExZScore != "NA" & ZScore_ANNOVAR$PredictedZScore != "NA"),];
      ZScore_ANNOVARFull=ZScore_ANNOVARFull[which(!(ZScore_ANNOVARFull$CADD_phred != "." & ZScore_ANNOVARFull$ExonicFunc_ref == "synonymous SNV")),]; 
     # print(dim(ZScore_ANNOVARFull));
       ZScore_ANNOVAR_CADD=ZScore_ANNOVARFull[which(!(ZScore_ANNOVARFull$CADD_phred == "." & ZScore_ANNOVARFull$ExonicFunc_ref == "synonymous SNV")),];
       ZScore_ANNOVAR_CADD=ZScore_ANNOVAR_CADD[which(ZScore_ANNOVAR_CADD$CADD_phred != "."),];
       print(dim(ZScore_ANNOVAR_CADD));
       ZScore_ANNOVAR_synonymousSNV=ZScore_ANNOVARFull[which((ZScore_ANNOVARFull$CADD_phred == "." & ZScore_ANNOVARFull$ExonicFunc_ref == "synonymous SNV")),];
     # print(dim(ZScore_ANNOVAR_synonymousSNV));

  ZScore_ANNOVAR_synonymousSNVSubset=ZScore_ANNOVAR_synonymousSNV; #Full data[sample(nrow(ZScore_ANNOVAR_synonymousSNV), dim(ZScore_ANNOVAR_CADD)[[1]]), ];
#       ZScore_ANNOVAR_synonymousSNVSubset=ZScore_ANNOVAR_synonymousSNV[sample(nrow(ZScore_ANNOVAR_synonymousSNV), dim(ZScore_ANNOVAR_CADD)[[1]]), ];
     #print(dim(ZScore_ANNOVAR_synonymousSNVSubset));
      TotalMergedSubset<- rbind(ZScore_ANNOVAR_CADD,ZScore_ANNOVAR_synonymousSNVSubset);
      TotalMergedSubset=as.data.frame(TotalMergedSubset);
      colnames(TotalMergedSubset)=colnames(ZScore_ANNOVARFull);
  #    print(dim(TotalMergedSubset));
      TotalMergedSubset$CADD_phred[TotalMergedSubset$CADD_phred == "."] <- 0; 
      TotalMergedSubset$CADD_phred=as.numeric(TotalMergedSubset$CADD_phred);
      TotalMergedSubset$VariantClass= TotalMergedSubset$ExonicFunc_ref;
     TotalMergedSubset$VariantClass[TotalMergedSubset$ExonicFunc_ref != "synonymous SNV" & TotalMergedSubset$CADD_phred>15]="Pathogenic";
      TotalMergedSubset$VariantClass[TotalMergedSubset$ExonicFunc_ref != "synonymous SNV" & TotalMergedSubset$CADD_phred<=15]="Benign";
      TotalMergedSubset$VariantClass[TotalMergedSubset$ExonicFunc_ref == "synonymous SNV"]="Control";


#      TotalMergedSubset$VariantClass[TotalMergedSubset$ExonicFunc_ref != "synonymous SNV" & TotalMergedSubset$ExonicFunc_ref != "nonframeshift deletion" & TotalMergedSubset$ExonicFunc_ref != "nonframeshift insertion" & TotalMergedSubset$CADD_phred>15]="Pathogenic";
#      TotalMergedSubset$VariantClass[TotalMergedSubset$ExonicFunc_ref != "synonymous SNV" & TotalMergedSubset$ExonicFunc_ref != "nonframeshift deletion" & TotalMergedSubset$ExonicFunc_ref != "nonframeshift insertion"  & TotalMergedSubset$CADD_phred<=15]="Benign";
#      TotalMergedSubset$VariantClass[TotalMergedSubset$ExonicFunc_ref == "synonymous SNV" | TotalMergedSubset$ExonicFunc_ref == "nonframeshift deletion"| TotalMergedSubset$ExonicFunc_ref == "nonframeshift insertion"]="Control";      
      if(iT==1)
      {
        AllTissueGeneExpression=TotalMergedSubset;
       }
       else
      {
        AllTissueGeneExpression=rbind(AllTissueGeneExpression,TotalMergedSubset);
      }
  }
  AllTissueGeneExpression=as.data.frame(AllTissueGeneExpression);
 # print(colnames(AllTissueGeneExpression));
 # print(dim(AllTissueGeneExpression));
  AllTissueGeneExpression$ActualExZScore=as.numeric(AllTissueGeneExpression$ActualExZScore);
  AllTissueGeneExpression$PredictedZScore=as.numeric(AllTissueGeneExpression$PredictedZScore);
  AllTissueGeneExpression$SpearmanCorr=as.numeric(AllTissueGeneExpression$SpearmanCorr);
  boxplot(ActualExZScore~VariantClass,data=AllTissueGeneExpression, main=paste(DeaseTypes[iDx],"_Actual",sep=""),
        xlab=NULL, ylab="Z_Score",outline=FALSE);
        abline(h =0,lty=2,lwd=0.5,col=2);
    ActualZScore_Benign=AllTissueGeneExpression$ActualExZScore[AllTissueGeneExpression$VariantClass=="Benign"];
    ActualZScore_Control=AllTissueGeneExpression$ActualExZScore[AllTissueGeneExpression$VariantClass=="Control"];
   ActualZScore_Pathogenic=AllTissueGeneExpression$ActualExZScore[AllTissueGeneExpression$VariantClass=="Pathogenic"];
   
  ttestPvalue_ActualZScore_Path_Control=round(t.test(ActualZScore_Control,ActualZScore_Pathogenic)$p.value,3);
   ttestPvalue_ActualZScore_Benign_Control=round(t.test(ActualZScore_Control,ActualZScore_Benign)$p.value,3); 
    ttestPvalue_ActualZScore_Benign_Path=round(t.test(ActualZScore_Pathogenic,ActualZScore_Benign)$p.value,3);
      text(1.5, 2, paste0("p_BC=",ttestPvalue_ActualZScore_Benign_Control));
    text(2.5, 2, paste0("p_CP=",ttestPvalue_ActualZScore_Path_Control));
   text(2, -2, paste0("p_BP=",ttestPvalue_ActualZScore_Benign_Path));
 

  print(ttestPvalue_ActualZScore_Path_Control);  
   print(ttestPvalue_ActualZScore_Benign_Control);
  print(ttestPvalue_ActualZScore_Benign_Path);

  #boxplot(PredictedZScore~VariantClass,data=AllTissueGeneExpression, main=paste(DeaseTypes[iDx],"_Predicted(include all)",sep=""),
  #      xlab=NULL, ylab="Z_Score",outline=FALSE);
  #      abline(h =0,lty=2,lwd=0.5,col=2);#names=c("","","")
  

#  boxplot(SpearmanCorr~VariantClass,data=AllTissueGeneExpression, main=DeaseTypes[iDx],
 #       xlab=NULL, ylab="Corr",outline=FALSE);
 #       abline(h =0,lty=2,lwd=0.5,col=2);
        #abline(h =1.5,lty=2,lwd=0.3,col=2);
   AllTissueGeneExpressionHighCorr= AllTissueGeneExpression[which(abs(AllTissueGeneExpression$SpearmanCorr)>0.1),];
 
   boxplot(PredictedZScore~VariantClass,data=AllTissueGeneExpressionHighCorr, main=paste(DeaseTypes[iDx],"_Predicted(|r|>0.1)",sep=""),
        xlab=NULL, ylab="Z_Score",outline=FALSE);
    abline(h =0,lty=2,lwd=0.5,col=2);

    PredictedZScore_Benign=AllTissueGeneExpression$PredictedZScore[AllTissueGeneExpression$VariantClass=="Benign"];  
    PredictedZScore_Control=AllTissueGeneExpression$PredictedZScore[AllTissueGeneExpression$VariantClass=="Control"];
    PredictedZScore_Pathogenic=AllTissueGeneExpression$PredictedZScore[AllTissueGeneExpression$VariantClass=="Pathogenic"];
  EachPartLength=c(length(ActualZScore_Benign),length(ActualZScore_Control),length(ActualZScore_Pathogenic),length(PredictedZScore_Benign),length(PredictedZScore_Control),length(PredictedZScore_Pathogenic));
   print(EachPartLength);
   ttestPvalue_PredictedZScore_Path_Control=round(t.test(PredictedZScore_Control,PredictedZScore_Pathogenic)$p.value,3);
   ttestPvalue_PredictedZScore_Benign_Control=round(t.test(PredictedZScore_Control,PredictedZScore_Benign)$p.value,3);
    ttestPvalue_PredictedZScore_Benign_Path=round(t.test(PredictedZScore_Pathogenic,PredictedZScore_Benign)$p.value,3);
   print(ttestPvalue_PredictedZScore_Path_Control);  
   print(ttestPvalue_PredictedZScore_Benign_Control);
  print(ttestPvalue_PredictedZScore_Benign_Path);  
     text(1.5, 2, paste0("p_BC=",ttestPvalue_PredictedZScore_Benign_Control));
    text(2.5, 2, paste0("p_CP=",ttestPvalue_PredictedZScore_Path_Control));
   text(2, -2, paste0("p_BP=",ttestPvalue_PredictedZScore_Benign_Path));
}  
 


 dev.off();
