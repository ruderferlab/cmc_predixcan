#!/usr/bin/perl -w
 use strict;

 use lib "/home/hanl3/perl5/module/lib/site_perl/5.14.2";
 use Text::CSV_PP;
  use Tie::File; 
 use POSIX qw(WNOHANG);
 use IO::Uncompress::Gunzip qw($GunzipError);
 use warnings;
 use Scalar::Util qw(looks_like_number);

 my $iSpecificBatch=$ARGV[0];
 # /data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8AllSNVsInfoforPrediction.txt

  my %AllUsedSNVsInfo=();
  my %AllUsedSNVsPosRange=();
    my $iLineNumber=0;#Cll candiate  llleles 
    my $V8SNVInfoFile="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8AllSNVsInfoforPrediction.txt";
    open(INPUT, $V8SNVInfoFile) || die "can’t open $V8SNVInfoFile";
    while (<INPUT>)
    {
        chomp;
        if($iLineNumber==0)
        {
          $iLineNumber++;
          next;
        }
        my @cols=split(/\s+/,$_);
 #      last if($cols[1]!=1);
        $AllUsedSNVsInfo{$cols[6]}="$cols[4]\t$cols[5]"; #ref_allele[col3]eff_allele[col4]     
        $iLineNumber++;
     #   last if($iLineNumber>100);  
   }
     close INPUT;

my $NumberofAllUsedSNVs=keys %AllUsedSNVsInfo;

 print "Number of all used snvs: $NumberofAllUsedSNVs\n";

my @BatchList=("SKL_10073_B01_GRM_WGS_2016-02-25","SKL_11154_B01_GRM_WGS_2016-03-17","SKL_11694_B01_GRM_WGS_2017-08-18");
#     /home/hanl3/cmc/data/wgs/vcf/SKL_10073_B01_GRM_WGS_2016-02-25.recalibrated_variants_PASS_WholeGenome.vcf.gz[328 samples]
#          /home/hanl3/cmc/data/wgs/vcf/SKL_11154_B01_GRM_WGS_2016-03-17.recalibrated_variants_PASS_WholeGenome.vcf.gz[326 samples]
#               /home/hanl3/cmc/data/wgs/vcf/SKL_11694_B01_GRM_WGS_2017-08-18.recalibrated_variants_PASS_WholeGenome.vcf.gz[119 samples]
#

  my %ComplementaryAlleles=();
  $ComplementaryAlleles{"A"}="T";
  $ComplementaryAlleles{"T"}="A";
  $ComplementaryAlleles{"C"}="G";
  $ComplementaryAlleles{"G"}="C";
  my @BatchNameList=("SKL10073","SKL11154","SKL11694");
 my @WGSBatches=("SKL_10073_B01_GRM_WGS_2016-02-25","SKL_11154_B01_GRM_WGS_2016-03-17","SKL_11694_B01_GRM_WGS_2017-08-18");
 for(my $iBatch=$iSpecificBatch;$iBatch<$iSpecificBatch+1;$iBatch++)
 {

   my $ChrPositionFilter=0;
    my $ChrPositionFilter1=0;
   my %UniqLociPos=();
    my $iLineNumber=0;
    my $iLineNumber0=0;
  my $iLineNumber1=0;
   my $iLineNumber2=0;
     my $iLineNumber3=0; 
   my $iLineNumber4=0;
    my @SelectedIndInOneBatch=();
    my @SelectedIndIndexInOneBatch=();
       my $V8DifferentModelShareWGSFile0="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/$BatchNameList[$iBatch]ShareV8MultiplyModel.CommonSNVs_ImputedWGS_FullGenotype.vcf";
    open OUT0, ">$V8DifferentModelShareWGSFile0" or die "Can't open Output file:$V8DifferentModelShareWGSFile0!";

    my $V8DifferentModelShareWGSFile1="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/$BatchNameList[$iBatch]ShareV8MultiplyModel.CommonSNVs_ImputedWGS_Haplotype1.vcf";
    open OUT1, ">$V8DifferentModelShareWGSFile1" or die "Can't open Output file:$V8DifferentModelShareWGSFile1!";

    my $V8DifferentModelShareWGSFile2="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/$BatchNameList[$iBatch]ShareV8MultiplyModel.CommonSNVs_ImputedWGS_Haplotype2.vcf";
    open OUT2, ">$V8DifferentModelShareWGSFile2" or die "Can't open Output file:$V8DifferentModelShareWGSFile2!";
    my $WGSFile="/home/hanl3/cmc/data/wgs/vcf/$BatchNameList[$iBatch]ImputedSNVs_1000GPV3Rsq03.dose.vcf.gz";#SKL10073ImputedSNVs_1000GPV3Rsq03.dose.vcf.gz SKL10073ImputedSNVs_1000GPV3Rsq03.dose.vcf.gz 
   print "$WGSFile\n";
  open (INPUT, "gunzip -c $WGSFile|") or die "gunzip  $WGSFile: $!";
 # open(INPUT,$WGSFile) || die "can’t open $WGSFile";
    while (<INPUT>)
   {
       chomp;
   #    next if($_ =~ /^X|Y|MT/);
       if($_ =~ /^##/)
       {
         next if($_ =~ /^##GATKCommandLine/);
         next if($_ =~ /^##GVCFBlock/);
         next if($_ =~/^##contig=<ID=GL/);
         next if($_ =~/^##contig=<ID=X/);
         print OUT0"$_\n";
         print OUT1"$_\n";
         print OUT2"$_\n";
        next;
      }
      elsif($_ =~ /^#/)
      {
        print OUT0"##Full genotypes\n";
        print OUT1"##1st haplotype\n";
        print OUT2"##2nd haplotype\n";
        print OUT0"$_\n";
        print OUT1"$_\n";
        print OUT2"$_\n";
        next;
       }
       my @cols=split(/\s+/,$_,6);
#       last if($cols[0]>2);
       my @ChrPosAlleles=split(/:/,$cols[2]);
       my $ChrPos="$ChrPosAlleles[0]:$ChrPosAlleles[1]";
       if(defined($AllUsedSNVsInfo{$ChrPos}))
       {
          my @TwoAlleles_Model=split(/\t/,$AllUsedSNVsInfo{$ChrPos});
          @cols=split(/\s+/,$_);
          $cols[8]="GT";
          for(my $iCol=9;$iCol<@cols;$iCol++)
          {
             $cols[$iCol]=substr($cols[$iCol],0,3);
           }
          my $iOutput=0;
  
          if($TwoAlleles_Model[0] eq $cols[3] && $TwoAlleles_Model[1] eq $cols[4])
          {
           $iLineNumber0++;
             $iOutput=1;
          }  
          elsif($TwoAlleles_Model[0] eq $cols[4] && $TwoAlleles_Model[1] eq $cols[3])
          {
            $iLineNumber1++;
            $iOutput=1;
             my $sTemp=$cols[3];
             $cols[3]=$cols[4];
             $cols[4]=$sTemp;
             for(my $iCol=9;$iCol<@cols;$iCol++)
             {
               if($cols[$iCol] eq "0|0")
               {
                 $cols[$iCol]="1|1";
               }
               elsif($cols[$iCol] eq "1|1")
               {
                  $cols[$iCol]="0|0";
               }
             }

          }
          elsif(($TwoAlleles_Model[0] eq $cols[3] && $TwoAlleles_Model[1] ne $cols[4])||($TwoAlleles_Model[0] ne $cols[3] && $TwoAlleles_Model[1] eq $cols[4]))
          {
            next;
          }
           elsif(($TwoAlleles_Model[0] eq $cols[4] && $TwoAlleles_Model[1] ne $cols[3]) || ($TwoAlleles_Model[0] ne $cols[4] && $TwoAlleles_Model[1] eq $cols[3]))
          {
            next;
          }
          elsif(defined($ComplementaryAlleles{$cols[3]})&& defined($ComplementaryAlleles{$cols[4]}))
          {
             if($TwoAlleles_Model[0] eq $ComplementaryAlleles{$cols[3]} && $TwoAlleles_Model[1] eq $ComplementaryAlleles{$cols[4]})
            {
              $iLineNumber3++;
               $iOutput=1;
               $cols[3]=$ComplementaryAlleles{$cols[3]};
                $cols[4]=$ComplementaryAlleles{$cols[4]};
             }
             elsif($TwoAlleles_Model[0] eq $ComplementaryAlleles{$cols[4]} && $TwoAlleles_Model[1] eq $ComplementaryAlleles{$cols[3]})
            {
              $iLineNumber4++;
               $iOutput=1;
               my $sTemp=$ComplementaryAlleles{$cols[3]};
               $cols[3]=$ComplementaryAlleles{$cols[4]};
               $cols[4]=$sTemp;
               for(my $iCol=9;$iCol<@cols;$iCol++)
               {
                 if($cols[$iCol] eq "0|0")
                 {
                   $cols[$iCol]="1|1";
                 }
                 elsif($cols[$iCol] eq "1|1")
                 {
                  $cols[$iCol]="0|0";
                }
               }
             }  
          }         
          else
          {
           print "$ChrPos\t$AllUsedSNVsInfo{$ChrPos}:$cols[3],$cols[4]\n";
           }
           #if($iLineNumber%100==0)
          # {
           # print "$cols[2]\t$iLineNumber\n";
          #  }
          $cols[2]=$ChrPos;
          my @haplotype1=@cols;
           my @haplotype2=@cols;
            s/\|1/\|0/ for @haplotype1;# convert |1 to |0
            s/1\|/0\|/ for @haplotype2; # convert 1| to 0|
           s/1\|0/0\|1/ for @cols; #convert 1|0 to 0|1
           s/1\|0/0\|1/ for @haplotype1; #convert 1|0 to 0|1
           s/1\|0/0\|1/ for @haplotype2; #convert 1|0 to 0|1  
        if($iOutput==1)
           {
              if(!defined($UniqLociPos{$ChrPos}))
             {
              $UniqLociPos{$ChrPos}=1;
              print OUT0 "$cols[0]";
              for(my $iCol=1;$iCol<@cols;$iCol++)
              {
                print OUT0 "\t$cols[$iCol]";
              }
              print OUT0 "\n";
              print OUT1 "$haplotype1[0]";
              for(my $iCol=1;$iCol<@haplotype1;$iCol++)
              {
                print OUT1 "\t$haplotype1[$iCol]";
              }
              print OUT1 "\n";
              print OUT2 "$haplotype2[0]";
              for(my $iCol=1;$iCol<@haplotype2;$iCol++)
              { 
                print OUT2 "\t$haplotype2[$iCol]";
              }
              print OUT2 "\n";           
            $iLineNumber++;
          }      
       }#end iOutput
     }
  }
   close INPUT;
   print "$iLineNumber\t$iLineNumber0\t$iLineNumber1\t$iLineNumber3\t$iLineNumber4\n";
   close OUT0;
   close OUT1;
   close OUT2;
}

