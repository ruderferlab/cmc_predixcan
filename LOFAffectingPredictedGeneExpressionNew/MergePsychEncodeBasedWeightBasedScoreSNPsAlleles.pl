#!/usr/bin/perl -w
 use strict;
  use 5.012;
 use lib "/home/hanl3/perl5/module/lib/site_perl/5.14.2";
 use Text::CSV_PP;
  use Tie::File; 
 use POSIX qw(WNOHANG);
 use IO::Uncompress::Gunzip qw($GunzipError);
 use warnings;
use Scalar::Util qw(looks_like_number);

 my $OutZFile="/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/PsychEncodeWeightBasedAllEffectAlleleList.txt";
 open OUTZ, ">$OutZFile" or die "Can't open Output file:$OutZFile!"; 

           my $iNumberofFile=0;
            my %ChrPos_RefAltAlleles=();
           #/data/ruderferlab/resources/psychencode/PEC_TWAS_weights_Score/ENSG00000167103.wgt.score 
           my $mydir="/data/ruderferlab/resources/psychencode/PEC_TWAS_weights_Score";
           opendir(DIR, $mydir) || die "Can't open $mydir: $!";
           #9:130183486     C       G       -2.36e-04 
            while (my $scoreFile=readdir (DIR)) {
           next if ($scoreFile =~ m/^\./);
           if ($scoreFile =~ m/\.wgt\.score$/ && $scoreFile =~ m/^ENSG/)
           {
              my $InputFile="$mydir/$scoreFile"; 
              open(INPUT,$InputFile) || die "can’t open $InputFile";
              while (<INPUT>)
              {
                chomp;
                my @cols=split(/\s+/,$_);
                $ChrPos_RefAltAlleles{$cols[0]}="$cols[2]\t$cols[1]";
              }
             close INPUT;     
              $iNumberofFile++;
           #  last if($iNumberofFile>10); 
           }       
          }
        closedir (DIR);
        my $iLineNumber=0;
     my $NumberofSNVs=keys %ChrPos_RefAltAlleles;
     print "NumberofSNVs:\t$NumberofSNVs\n";

      my $InputFile="/data/ruderferlab/resources/psychencode/hg19_SNP_Information_Table_with_Alleles.txt";

                open(INPUT,$InputFile) || die "can’t open $InputFile";
              while (<INPUT>)
              { 
                chomp;
                if($iLineNumber==0)
                {
#PEC_id  Rsid    chr     position        REF     ALT
                print OUTZ  "PEC_id\tRsid\tchr\tposition\tRef\tEffect\n";    
                 $iLineNumber++;
                  next;
                }
                my @cols=split(/\s+/,$_);
                if(defined($ChrPos_RefAltAlleles{$cols[0]}))
                {
                   print OUTZ "$cols[0]\t$cols[1]\t$cols[2]\t$cols[3]\t$ChrPos_RefAltAlleles{$cols[0]}\n";
                 }
               }
             close INPUT;
             close OUTZ;  
