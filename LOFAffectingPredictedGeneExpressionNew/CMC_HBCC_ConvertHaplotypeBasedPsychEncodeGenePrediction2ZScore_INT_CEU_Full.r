#Convert imputed WGS based prediction into corresponing ZSCORE and int zscore

 library(magrittr);
 library(RNOmni);
 library(dplyr)
 library(stringr)


BatchList=c("SKL_10073_B01_GRM_WGS_2016-02-25","SKL_11154_B01_GRM_WGS_2016-03-17","SKL_11694_B01_GRM_WGS_2017-08-18");
ConsiseBatchNamelist=c("SKL_10073","SKL_11154","SKL_11694");
DataTypes=c("FullGenotype_Imputed","Original");
   DuplicateOutliers=c("MSSM-DNA-PFC-375","MSSM-DNA-PFC-269","CMC-HBCC-DNA-ACC-6052","CMC-HBCC-DNA-ACC-5669","CMC-HBCC-DNA-ACC-4237","CMC-HBCC-DNA-ACC-5646",
    "CMC-HBCC-DNA-ACC-5777","CMC-HBCC-DNA-ACC-5682","CMC-HBCC-DNA-ACC-4284","CMC-HBCC-DNA-ACC-4137",
    "CMC-HBCC-DNA-ACC-6009","CMC-HBCC-DNA-ACC-6056","CMC-HBCC-DNA-ACC-4029","CMC-HBCC-DNA-ACC-4200",
    "CMC-HBCC-DNA-ACC-4074","CMC-HBCC-DNA-ACC-4204","CMC-HBCC-DNA-ACC-5654","CMC-HBCC-ACC-DNA-4235");
 MissingOutlierCEUs=c("MSSM-DNA-PFC-153","Penn-DNA-PFC-033","Pitt-DNA-PFC-1324","Pitt-DNA-PFC-1524","Pitt-DNA-PFC-970","Pitt-DNA-BP-PFC-789","Pitt-DNA-BP-PFC-840","Pitt-DNA-PFC-1088","MSSM-DNA-PFC-56");


  Haplotypes=c("Haplotype1","Haplotype2"); 
  AllSampleIDInfoFile="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/WGSData_VariantSummary_InferredAncestry.txt";
  AllSampleIDMatrix=read.table(AllSampleIDInfoFile, sep="\t",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE);#Obtain column names from File By check.names  
  InferCEUSampleSubset=AllSampleIDMatrix[which(AllSampleIDMatrix$InferredAncestry=="CEU"),]
  CEUWGSID=InferCEUSampleSubset$WGSID;
  print(length(CEUWGSID));
  print(CEUWGSID[5:5]);

  for(iBatch in seq(1,3))#3
  {
    MergedPredictedGeneExpression=NULL;
    TotalGeneExpression=NULL;
    for(iType in seq(1,2))#2
    {
   #file1:Chr	WGSBatch	PredictionMethods	BrainsTissues	Source	GeneID	NumberofLociInWeightModel	NumberofLociInActualModel
   #file2: Chr	WGSBatch	PredictionMethods	BrainsTissues	Source	GeneID	NumberofLociInWeightModel	NumberofLociInActualModel
     PredictedExpressionFile=paste("/home/hanl3/cmc/scratch/Predicted_ActualGeneExpressionMerged/PsychEncodePredictionEx/PredictedValues/",ConsiseBatchNamelist[iBatch],"PsychEncode_PredictedGeneExpression_",Haplotypes[iType],"_Imputed.txt.gz",sep="");
#     if(iType==2)
 #    {
 #   PredictedExpressionFile=paste("/home/hanl3/cmc/scratch/Predicted_ActualGeneExpressionMerged/PsychEncodePredictionEx/PredictedValues/",ConsiseBatchNamelist[iBatch],"PsychEncode_PredictedGeneExpression_",Haplotypes[iType],"_Imputed1.txt.gz",sep="");
#      }
     con<-file(PredictedExpressionFile,'rt');
     PredictedGeneExpression=read.table(con, sep="\t",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE,row.names=NULL);#Obtain column names from File By check.names   
      print(PredictedExpressionFile); 
     PredictedGeneExpression =as.data.frame(PredictedGeneExpression);
     GeneIDColNames=colnames(PredictedGeneExpression);
     GeneIDColNames=str_replace_all(GeneIDColNames, "[.]", "-");#replace "." into "-"
     colnames(PredictedGeneExpression)=GeneIDColNames; 
     GeneIDColNames=as.vector(GeneIDColNames);
     print(length(GeneIDColNames));
     OverlapID=intersect(GeneIDColNames,DuplicateOutliers);
   #  print(GeneIDColNames[c(1:10)]);
    #  print(length(OverlapID));
     #print(dim(PredictedGeneExpression));
     GeneIDRowMatrix=PredictedGeneExpression[,c(1:8)];#Obtain Gene Name column information
     GeneIDRowMatrixColNames=colnames(GeneIDRowMatrix);
      print("Name1");
        print(GeneIDRowMatrixColNames);
       PredictedGeneExpression=PredictedGeneExpression[,(names(PredictedGeneExpression)%in%CEUWGSID)];#Keep CEU samples
       #print(dim(PredictedGeneExpression));
       NormalPredictedGeneExpression=PredictedGeneExpression[,!(names(PredictedGeneExpression)%in%DuplicateOutliers)];#Remove duplicate outlier individuals
       print(dim(NormalPredictedGeneExpression));
       PredictedGeneExpressionWithoutGeneID=NormalPredictedGeneExpression[,!(names(NormalPredictedGeneExpression)%in%MissingOutlierCEUs)];#Remove Individuals with missing rate >=0.01 
       print(dim(PredictedGeneExpressionWithoutGeneID));
       PredictedGeneExpressionWithoutGeneID_Dim=dim(PredictedGeneExpressionWithoutGeneID);
       GeneExpressionWithoutGeneIDNames=colnames(PredictedGeneExpressionWithoutGeneID);
          print("Name2");
         print(GeneExpressionWithoutGeneIDNames[1:10]);
        GeneExpressionWithID=as.data.frame(cbind(GeneIDRowMatrix,PredictedGeneExpressionWithoutGeneID));
        colnames(GeneExpressionWithID)=c(GeneIDRowMatrixColNames,GeneExpressionWithoutGeneIDNames);
        print(colnames(GeneExpressionWithID)[1:10]);#1] "Chr"               "WGSBatch"          "PredictionMethods" "BrainsTissues"     "GeneID"  
        if(iType==1)
        {
           HyploType1PredictedGeneExpression=GeneExpressionWithID;
        }
        else#Chr     WGSBatch        PredictionMethods       BrainsTissues   Source  GeneID  NumberofLociInWeightModel       NumberofLociInActualModel
        {
            MergedPredictedGeneExpression=inner_join(x= HyploType1PredictedGeneExpression,y=GeneExpressionWithID,by =c("Chr","WGSBatch","PredictionMethods","BrainsTissues","GeneID"),suffixes = c(".x",".y"));
            print(dim(MergedPredictedGeneExpression));
         }
          close(con);
         MergedPredictedGeneExpression=as.data.frame(MergedPredictedGeneExpression);# Source NumberofLociInWeightModel NumberofLociInActualModel
         MergedPredictedGeneExpression_Subset=MergedPredictedGeneExpression[, !(colnames(MergedPredictedGeneExpression) %in% c("Source.x","NumberofLociInWeightModel.x","NumberofLociInActualModel.x","Source.y","NumberofLociInWeightModel.y","NumberofLociInActualModel.y"))];
         print(colnames(MergedPredictedGeneExpression_Subset)[1:10]);
       }
       print(dim(MergedPredictedGeneExpression_Subset));

        GeneIDRowMatrix=MergedPredictedGeneExpression_Subset[,c(1:5)];#Obtain Gene Name column information
        GeneIDRowMatrixColNames=colnames(GeneIDRowMatrix);
        TotalGeneExpression=MergedPredictedGeneExpression_Subset[,!(colnames(MergedPredictedGeneExpression_Subset) %in% c("Chr","WGSBatch","PredictionMethods","BrainsTissues","GeneID"))];
        print("Combined Matrix");
        print(dim(TotalGeneExpression));
        FullColNames=colnames(TotalGeneExpression);
        GeneExpressionDim=dim(TotalGeneExpression);
        print(TotalGeneExpression[c(1:5),c(1:10)]);

         TotalGeneExpression[,1:GeneExpressionDim[2]] <- sapply(TotalGeneExpression[,1:GeneExpressionDim[2]], as.numeric);
        TotalGeneExpressionINT= t(apply(TotalGeneExpression, 1, rankNorm));# The result from the apply() had to be transposed using t() to get the same layout as the input matrix A  
         TotalGeneExpressionZScore=t(apply(TotalGeneExpression, 1,scale));
         TotalGeneExpressionINTDim=dim(TotalGeneExpressionINT);
         HalfColumn=TotalGeneExpressionINTDim[2]/2;
         print(HalfColumn);

         A1Names=FullColNames[1:HalfColumn];
         A2Names=FullColNames[(HalfColumn+1):TotalGeneExpressionINTDim[2]];
         A1Names=gsub(".x", "",A1Names);
         A2Names=gsub(".y", "",A2Names);
         iUnequal=0;
         for(iNum in seq(1:HalfColumn))
         {
           if(A1Names[iNum] !=A2Names[iNum])
           {
             iUnequal= iUnequal+1;
           }
         }
         print(iUnequal);
         for(iType in seq(1,2))
         {
           StartPos=(iType-1)*HalfColumn;
           EndPos=iType*HalfColumn;
           GeneExpressionINT=TotalGeneExpressionINT[,c((StartPos+1):EndPos)];
           GeneExpressionZScore=TotalGeneExpressionZScore[,c((StartPos+1):EndPos)];
            GeneExpressionINTMerged=as.data.frame(cbind(GeneIDRowMatrix, GeneExpressionINT));#INT score merged
           colnames(GeneExpressionINTMerged)=c(GeneIDRowMatrixColNames,A1Names);;
           print(dim(GeneExpressionINTMerged));
           PredictedExpressionFile_INT=paste("/home/hanl3/cmc/scratch/Predicted_ActualGeneExpressionMerged/PsychEncodePredictionEx/PredictedValues_Zscore/PsychEncodeWeightBased",ConsiseBatchNamelist[iBatch],"_",Haplotypes[iType],"_GenePredictedExCEUBasedINTZScore.txt",sep="");
          print(PredictedExpressionFile_INT);
           write.table(GeneExpressionINTMerged, file=PredictedExpressionFile_INT, append = FALSE, quote =FALSE, sep = "\t", eol = "\n",col.names=TRUE,row.names=FALSE);

           GeneExpressionIZScoreMerged=as.data.frame(cbind(GeneIDRowMatrix,GeneExpressionZScore)); #Standard score merged
            colnames(GeneExpressionIZScoreMerged)=c(GeneIDRowMatrixColNames,A1Names);
            PredictedExpressionFile_Standard=paste("/home/hanl3/cmc/scratch/Predicted_ActualGeneExpressionMerged/PsychEncodePredictionEx/PredictedValues_Zscore/PsychEncodeWeightBased",ConsiseBatchNamelist[iBatch],"_",Haplotypes[iType],"_GenePredictedExCEUBasedStandardZScore.txt",sep="");
           write.table(GeneExpressionIZScoreMerged, file=PredictedExpressionFile_Standard,append = FALSE, quote =FALSE, sep = "\t", eol = "\n",col.names=TRUE,row.names=FALSE);
      }
  } 



