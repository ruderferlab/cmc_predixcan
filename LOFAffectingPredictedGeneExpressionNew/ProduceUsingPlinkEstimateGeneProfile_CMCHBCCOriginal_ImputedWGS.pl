#!/usr/bin/perl -w
 use strict;
  use 5.012;
 use lib "/home/hanl3/perl5/module/lib/site_perl/5.14.2";
 use Text::CSV_PP;
  use Tie::File; 
 use POSIX qw(WNOHANG);
 use IO::Uncompress::Gunzip qw($GunzipError);
 use warnings;
use Scalar::Util qw(looks_like_number);

# UTMOST, XT_Scan,PrediXcan two brain tissues' preciction model which based on GRch38 assembly
# /data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/gtex_v8_st_Brain_Anterior_cingulate_cortex_BA24.txt
#       gtex_v8_st_Brain_Frontal_Cortex_BA9.txt  gtex_v8_ut_Brain_Anterior_cingulate_cortex_BA24.txt
#       gtex_v8_ut_Brain_Frontal_Cortex_BA9.txt  gtex_v8_xt_Brain_Anterior_cingulate_cortex_BA24.txt  gtex_v8_xt_Brain_Frontal_Cortex_BA9.txt
#/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs
#SKL_10073ShareV8MultiplyModel.CommonSNVs_PassWGS
#SKL_11154ShareV8MultiplyModel.CommonSNVs_PassWGS
#SKL_11694ShareV8MultiplyModel.CommonSNVs_PassWGS


  my @WGSBatches=("SKL_10073","SKL_11154","SKL_11694");
  my @PredictionMethods=("st","ut","xt");
  my @BrainsTissues=("Brain_Frontal_Cortex_BA9","Brain_Anterior_cingulate_cortex_BA24");
  my @BrainsTissuesNames=("DLPFC_BA9","ACC_BA24");


  my $CMC_HBCC_GTExV8PredictExpressionCommandlineFile="/gpfs23/data/ruderferlab/projects/cmc/scripts/LOFAffectingPredictedGeneExpressionNew/CMC_HBCC_GTEXV8PredictGeneExpressionCommandline.sh";
  open OUT, "> $CMC_HBCC_GTExV8PredictExpressionCommandlineFile" or die "Can't open Output file: $CMC_HBCC_GTExV8PredictExpressionCommandlineFile!";
 
  for(my $iBatch=0;$iBatch<@WGSBatches;$iBatch++)
  {
   for(my $iM=0;$iM<@PredictionMethods;$iM++)
    {
      for(my $iT=0;$iT<@BrainsTissues;$iT++)
      {
         my $iNumberofFile=1;
          my $mydir="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTExV8_Weight/$PredictionMethods[$iM]_$BrainsTissuesNames[$iT]";
          opendir(DIR, $mydir) || die "Can't open $mydir: $!";
           while (my $weightFile=readdir (DIR)) {
           next if ($weightFile =~ m/^\./);
           my $geneID=$weightFile;
           $geneID =~ s/\.wgt\.score//ig;
           my $scoreWeightFile="$mydir/$weightFile";
           my $plinkFilePrefix="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/$WGSBatches[$iBatch]ShareV8MultiplyModel.CommonSNVs_PassWGS";
           my $outfilePrefix="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTExV8_Profile/$PredictionMethods[$iM]_$BrainsTissuesNames[$iT]/$WGSBatches[$iBatch]_OriginalWGS_$geneID";          
          #print "$mydir/$weightFile\n";
          #print "$plinkFilePrefix\n";
          #print "$outfilePrefix\n";
          my $cmd="plink --bfile $plinkFilePrefix --score $scoreWeightFile 1 3 4 sum  double-dosage --out $outfilePrefix";#system($cmd); 
         print OUT "$cmd\n";
          $iNumberofFile++;
      #    last if($iNumberofFile>2);
         }
          closedir (DIR);       
      }
    }
 }

   @WGSBatches=("SKL10073","SKL11154","SKL11694");
  my @GenoTypes=("FullGenotype","Haplotype1","Haplotype2");


 for(my $iType=0;$iType<@GenoTypes;$iType++)
 {
    for(my $iBatch=0;$iBatch<@WGSBatches;$iBatch++)
    {
      for(my $iM=0;$iM<@PredictionMethods;$iM++)
       {
         for(my $iT=0;$iT<@BrainsTissues;$iT++)
         {
          my $iNumberofFile=1;
          my $mydir="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTExV8_Weight/$PredictionMethods[$iM]_$BrainsTissuesNames[$iT]";
          opendir(DIR, $mydir) || die "Can't open $mydir: $!";
           while (my $weightFile=readdir (DIR)) {
           next if ($weightFile =~ m/^\./);
           my $geneID=$weightFile;
           $geneID =~ s/\.wgt\.score//ig;
           my $scoreWeightFile="$mydir/$weightFile";
           my $plinkFilePrefix="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/$WGSBatches[$iBatch]ShareV8MultiplyModel.CommonSNVs_ImputedWGS_$GenoTypes[$iType]";
           my $outfilePrefix="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTExV8_Profile/$PredictionMethods[$iM]_$BrainsTissuesNames[$iT]/$WGSBatches[$iBatch]_$GenoTypes[$iType]_$geneID";
           my $cmd="plink --bfile $plinkFilePrefix --score $scoreWeightFile 1 3 4 sum  double-dosage --out $outfilePrefix";# system($cmd);
           print OUT "$cmd\n";
           $iNumberofFile++;
         }
         closedir (DIR);
     }
   }
  }
 }
 close  OUT;
