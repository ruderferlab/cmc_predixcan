#!/usr/bin/perl -w
 use strict;

 use lib "/home/hanl3/perl5/module/lib/site_perl/5.14.2";
 use Text::CSV_PP;
  use Tie::File; 
 use POSIX qw(WNOHANG);
 use IO::Uncompress::Gunzip qw($GunzipError);
 use warnings;
 use Scalar::Util qw(looks_like_number);


my $iSpecificBatch=$ARGV[0];
# /data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8AllSNVsInfoforPrediction.txt

my @NumberofInds=(337,335,128);
my @BatchList=("SKL_10073_B01_GRM_WGS_2016-02-25","SKL_11154_B01_GRM_WGS_2016-03-17","SKL_11694_B01_GRM_WGS_2017-08-18");
#     /home/hanl3/cmc/data/wgs/vcf/SKL_10073_B01_GRM_WGS_2016-02-25.recalibrated_variants_PASS_WholeGenome.vcf.gz[328 samples]
#          /home/hanl3/cmc/data/wgs/vcf/SKL_11154_B01_GRM_WGS_2016-03-17.recalibrated_variants_PASS_WholeGenome.vcf.gz[326 samples]
#               /home/hanl3/cmc/data/wgs/vcf/SKL_11694_B01_GRM_WGS_2017-08-18.recalibrated_variants_PASS_WholeGenome.vcf.gz[119 samples]
#
my @iAllowableMissingRate=(3,3,1);

  my %ComplementaryAlleles=();
  $ComplementaryAlleles{"A"}="T";
  $ComplementaryAlleles{"T"}="A";
  $ComplementaryAlleles{"C"}="G";
  $ComplementaryAlleles{"G"}="C";
  my @BatchNameList=("SKL_10073","SKL_11154","SKL_11694");for(my $iBatch=$iSpecificBatch;$iBatch<$iSpecificBatch+1;$iBatch++)
 {
     my @NumberofIndels=(0) x $NumberofInds[$iBatch];#Indels
     my @NumberofSNVs=(0) x $NumberofInds[$iBatch];#SNVs
     my @NumberofABs_SNV=(0) x $NumberofInds[$iBatch];#Heterozygote
      my @NumberofABs_INDEL=(0) x $NumberofInds[$iBatch];#Heterozygote
     my @NumberofSingletons_SNV=(0) x $NumberofInds[$iBatch];#Singletons
     my @NumberofSingletons_INDEL=(0) x $NumberofInds[$iBatch];#Singletons
     my @Numberofdoubletons_SNV=(0) x $NumberofInds[$iBatch];#Doubletons 
       my @Numberofdoubletons_INDEL=(0) x $NumberofInds[$iBatch];#Doubletons
    my @SelectedIndInOneBatch=();
    my @SelectedIndIndexInOneBatch=();
    my $V8DifferentModelShareWGSFile="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/$BatchNameList[$iBatch]IndividualBasedVariansSummary.txt";
    open OUT, ">$V8DifferentModelShareWGSFile" or die "Can't open Output file:$V8DifferentModelShareWGSFile!";
  
   my $WGSFile="/home/hanl3/cmc/data/wgs/vcf/$BatchList[$iBatch].recalibrated_variants_PASS_WholeGenome.vcf.gz";#snp150_37.txt.gz"; and   snp150.txt.gz
   open (INPUT, "gunzip -c $WGSFile|") or die "gunzip  $WGSFile: $!";;
  while (<INPUT>)
  {
     chomp;
      next if($_ =~ /^X|Y|MT/);
     if($_ =~ /^##/)
     {
      next if($_ =~ /^##GATKCommandLine/);
      next if($_ =~ /^##GVCFBlock/);
      next if($_ =~/^##contig=<ID=GL/); 
      next if($_ =~/^##contig=<ID=X/);
       next;
     }
      elsif($_ =~ /^#/)    
     {
        print OUT"$_\n";
        next;
        my @cols=split(/\s+/,$_);
        for(my $ii=9;$ii<@cols;$ii++)
        {
           push @SelectedIndInOneBatch,$cols[$ii];
            push @SelectedIndIndexInOneBatch,$ii;
        }
       my $iLength=scalar(@SelectedIndIndexInOneBatch);
       print "Number ofInds:\t$iLength\n";
     }
    else
     {  
          my @cols=split(/\s+/,$_,8);
          my @iAlleles=split(/,/,$cols[4]);#for the situations such as CTTT,CTTTT,C
          next  if(scalar(@iAlleles)>=2);#Remove alternative alleles number larger than 2
           @cols=split(/\s+/,$_);
           my $iMissingInd=0;
           my $iAa=0;
           my $iaa=0;
           my $iAA=0;   
           for(my $iInd=9;$iInd<@cols;$iInd++)  
           {
               $cols[$iInd]=substr($cols[$iInd],0,3);
               if($cols[$iInd] =~/^\./)
               {
                 $iMissingInd++;
               }
               elsif($cols[$iInd] =~/0\/0/)
               {
                $iAA++;
               }
               elsif($cols[$iInd] =~/1\/1/)
               {
                $iaa++;
               }
               elsif($cols[$iInd] =~/0\/1/ ||$cols[$iInd] =~/1\/0/)
               {
                $iAa++;
               }
           }
           next if($iMissingInd>$iAllowableMissingRate[$iBatch]);          
          # my $hwe_Pvalue=snphwe($iAa,$iaa,$iAA);  
          # print "$iAA\t$iaa\t$iAa\t Pvalue:$hwe_Pvalue\n";  
           # next if($hwe_Pvalue<1e-6);
            if(length($cols[3])==1 && length($cols[4])==1)
            {
              if(($iaa+$iAa)!=1 && ($iAA+$iAa)!=1)
              {
                for(my $iCol=9;$iCol<@cols;$iCol++)
                {
                   if($cols[$iCol] ne "./.")
                  {
                   $NumberofSNVs[$iCol]++;
                  }
                  if($cols[$iCol] eq "0/1" || $cols[$iCol] eq "1/0")
                  {
                   $NumberofABs_SNV[$iCol]++;
                  }
                }#for loop end 
               }
              elsif(($iaa+$iAa)==1) 
               {
                 for(my $iCol=9;$iCol<@cols;$iCol++)
                 {
                   if($cols[$iCol] eq "0/1" ||$cols[$iCol] eq "1/0" )
                  {
                     $NumberofSingletons_SNV[$iCol]++;
                  }
                  elsif($cols[$iCol] eq "1/1")
                  {
                    $Numberofdoubletons_SNV[$iCol]++;
                  }
                 } 
               }
              else
              {
               for(my $iCol=9;$iCol<@cols;$iCol++)
                {
                 if($cols[$iCol] eq "0/1" ||$cols[$iCol] eq "1/0" )
                 {
                   $NumberofSingletons_SNV[$iCol]++;
                 }
                 elsif($cols[$iCol] eq "0/0")
                 {
                   $Numberofdoubletons_SNV[$iCol]++;
                 }
               }#for loop
              }#end else
            }# end id
           elsif(length($cols[3])!=1 || length($cols[4])!=1)
            {
               if(($iaa+$iAa)!=1 && ($iAA+$iAa)!=1)
               {
                for(my $iCol=9;$iCol<@cols;$iCol++)
                {
                  if($cols[$iCol] ne "./.")
                  {
                   $NumberofIndels[$iCol]++;
                  }
                  if($cols[$iCol] eq "0/1" || $cols[$iCol] eq "1/0")
                  {
                    $NumberofABs_INDEL[$iCol]++;
                  }
                 }#for loop end 
               }
               elsif(($iaa+$iAa)==1)
               {
                 for(my $iCol=9;$iCol<@cols;$iCol++)
                 {  
                   if($cols[$iCol] eq "0/1" ||$cols[$iCol] eq "1/0" )
                   {
                     $NumberofSingletons_INDEL[$iCol]++;
                   }
                   elsif($cols[$iCol] eq "1/1")
                   {
                     $Numberofdoubletons_INDEL[$iCol]++;
                    }
                 }
               }
               else
               {
                 for(my $iCol=9;$iCol<@cols;$iCol++)
                 {
                  if($cols[$iCol] eq "0/1" ||$cols[$iCol] eq "1/0" )
                  {
                    $NumberofSingletons_INDEL[$iCol]++;
                  }
                  elsif($cols[$iCol] eq "0/0")
                  {
                    $Numberofdoubletons_INDEL[$iCol]++;
                  }
                }#loop for
               }#loop else
            }#loop elsif                 
         }# loop else 
     }
   close INPUT;
   print OUT "Indels";
   for(my $ii=1;$ii<@NumberofIndels;$ii++)
   {
     print OUT "\t$NumberofIndels[$ii]";
    }
   print OUT "\n";
     print OUT "SNVs";
   for(my $ii=1;$ii<@NumberofSNVs;$ii++)
   {
     print OUT "\t$NumberofSNVs[$ii]";
    }
   print OUT "\n";
   print OUT "Hyterrozyogotes_SNV";
   for(my $ii=1;$ii<@NumberofABs_SNV;$ii++)
   {
     print OUT "\t$NumberofABs_SNV[$ii]";
    }
   print OUT "\n";
   print OUT "Hyterrozyogotes_INDEL";
   for(my $ii=1;$ii<@NumberofABs_INDEL;$ii++)
   {
     print OUT "\t$NumberofABs_INDEL[$ii]";
    }
   print OUT "\n";

   print OUT "Singleton_SNV";
   for(my $ii=1;$ii<@NumberofSingletons_SNV;$ii++)
   {
     print OUT "\t$NumberofSingletons_SNV[$ii]";
    }
   print OUT "\n";
   print OUT "Doubleton_SNV";
   for(my $ii=1;$ii<@Numberofdoubletons_SNV;$ii++)
   {
     print OUT "\t$Numberofdoubletons_SNV[$ii]";
    }
   print OUT  "\n";
   print OUT "Singleton_INDEL";
   for(my $ii=1;$ii<@NumberofSingletons_INDEL;$ii++)
   {
     print OUT "\t$NumberofSingletons_INDEL[$ii]";
    }
   print OUT "\n";
   print OUT "Doubleton_INDEL";
   for(my $ii=1;$ii<@Numberofdoubletons_INDEL;$ii++)
   {
     print OUT "\t$Numberofdoubletons_INDEL[$ii]";
    }
   print OUT  "\n";
   close OUT;
}



