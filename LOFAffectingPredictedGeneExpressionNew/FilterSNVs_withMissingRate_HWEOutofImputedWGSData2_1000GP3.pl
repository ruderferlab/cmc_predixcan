#!/usr/bin/perl -w
 use strict;

 use lib "/home/hanl3/perl5/module/lib/site_perl/5.14.2";
 use Text::CSV_PP;
  use Tie::File; 
 use POSIX qw(WNOHANG);
 use IO::Uncompress::Gunzip qw($GunzipError);
 use warnings;
 use Scalar::Util qw(looks_like_number);



my $iSpecificBatch=$ARGV[0];
# /data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8AllSNVsInfoforPrediction.txt

  my %AllUsedSNVsInfo=();
  my %AllUsedSNVsPosRange=();
    my $iLineNumber=0;#Cll candiate  llleles 
    my $V8SNVInfoFile="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8AllSNVsInfoforPrediction.txt";
    open(INPUT, $V8SNVInfoFile) || die "can’t open $V8SNVInfoFile";
    while (<INPUT>)
    {
        chomp;
        if($iLineNumber==0)
        {
          $iLineNumber++;
          next;
        }
        my @cols=split(/\s+/,$_);
#       last if($cols[1]!=1);
        $AllUsedSNVsInfo{$cols[6]}="$cols[4]\t$cols[5]"; #ref_allele[col3]eff_allele[col4]     
        $iLineNumber++;
     #   last if($iLineNumber>100);  
   }
     close INPUT;

my $NumberofAllUsedSNVs=keys %AllUsedSNVsInfo;

 print "Number of all used snvs: $NumberofAllUsedSNVs\n";

my @BatchList=("SKL_10073_B01_GRM_WGS_2016-02-25","SKL_11154_B01_GRM_WGS_2016-03-17","SKL_11694_B01_GRM_WGS_2017-08-18");
#     /home/hanl3/cmc/data/wgs/vcf/SKL_10073_B01_GRM_WGS_2016-02-25.recalibrated_variants_PASS_WholeGenome.vcf.gz[328 samples]
#          /home/hanl3/cmc/data/wgs/vcf/SKL_11154_B01_GRM_WGS_2016-03-17.recalibrated_variants_PASS_WholeGenome.vcf.gz[326 samples]
#               /home/hanl3/cmc/data/wgs/vcf/SKL_11694_B01_GRM_WGS_2017-08-18.recalibrated_variants_PASS_WholeGenome.vcf.gz[119 samples]
#

  my %ComplementaryAlleles=();
  $ComplementaryAlleles{"A"}="T";
  $ComplementaryAlleles{"T"}="A";
  $ComplementaryAlleles{"C"}="G";
  $ComplementaryAlleles{"G"}="C";
  my @BatchNameList=("SKL10073","SKL11154","SKL11694");
 my @WGSBatches=("SKL_10073_B01_GRM_WGS_2016-02-25","SKL_11154_B01_GRM_WGS_2016-03-17","SKL_11694_B01_GRM_WGS_2017-08-18");
 for(my $iBatch=$iSpecificBatch;$iBatch<$iSpecificBatch+1;$iBatch++)
 {

  my $iUsedSNVs=0;
   my $ChrPositionFilter=0;
    my $ChrPositionFilter1=0;
   my %UniqLociPos=();
    my $iLineNumber=0;
    my $iLineNumber0=0;
   my $iLineNumber00=0;
   my $iLineNumber1=0;
   my $iLineNumber2=0;
     my $iLineNumber3=0; 
   my $iLineNumber4=0;
    my $iLineNumber01=0;
    my $iLineNumber02=0; 
    my $iUniqLineNumber=0;
    my @SelectedIndInOneBatch=();
    my @SelectedIndIndexInOneBatch=();
   #my $WGSFile="/home/hanl3/cmc/scratch/SV_Expression/MichiganImputation_PhaseV2/SKL11694/chr1.dose.vcf.gz";#SKL10073ImputedSNVs_1000GPV3Rsq03.vcf 
   my $WGSFile="/home/hanl3/cmc/scratch/SV_Expression/MichiganImputation_1000GPV3/$BatchNameList[$iBatch]ImputedSNVs_1000GPV3Rsq03.vcf";#snp150_37.txt.gz"; and   snp150.txt.gz
  open(INPUT, $WGSFile) || die "can’t open $WGSFile"; 
# open (INPUT, "gunzip -c $WGSFile|") or die "gunzip  $WGSFile: $!";
    while (<INPUT>)
   {
       chomp;
       my @cols=split(/\s+/,$_,6);
       if($_ =~ m/^#/) 
       {
         next;      
       }
       last if($cols[0]>2);
       my @ChrPosAlleles=split(/:/,$cols[2]);
      my $ChrPos="$ChrPosAlleles[0]:$ChrPosAlleles[1]";
       if(defined($AllUsedSNVsInfo{$ChrPos}))
       {
         if($iLineNumber00<10)
         {
           print "$cols[2]\t";
         } 

          $iUsedSNVs++;
           my @TwoAlleles_Model=split(/\t/,$AllUsedSNVsInfo{$ChrPos});
           my $iOut=0;
          if($TwoAlleles_Model[0] eq $cols[3] && $TwoAlleles_Model[1] eq $cols[4])
          {
           $iOut=1;
           $iLineNumber0++;
          }  
          elsif($TwoAlleles_Model[0] eq $cols[4] && $TwoAlleles_Model[1] eq $cols[3])
          {
             $iOut=1;
            $iLineNumber1++;
          }
          elsif(($TwoAlleles_Model[0] eq $cols[3] && $TwoAlleles_Model[1] ne $cols[4])||($TwoAlleles_Model[0] ne $cols[3] && $TwoAlleles_Model[1] eq $cols[4]))
          {
            $iLineNumber01++; 
           next;
          }
           elsif(($TwoAlleles_Model[0] eq $cols[4] && $TwoAlleles_Model[1] ne $cols[3]) || ($TwoAlleles_Model[0] ne $cols[4] && $TwoAlleles_Model[1] eq $cols[3]))
          {
            $iLineNumber02++;
            next;
          }
          elsif(defined($ComplementaryAlleles{$cols[3]})&& defined($ComplementaryAlleles{$cols[4]}))
          {         
            if($TwoAlleles_Model[0] eq $ComplementaryAlleles{$cols[3]} && $TwoAlleles_Model[1] eq $ComplementaryAlleles{$cols[4]})
            {
             $iOut=1;
            $iLineNumber3++;
            }
            elsif($TwoAlleles_Model[0] eq $ComplementaryAlleles{$cols[4]} && $TwoAlleles_Model[1] eq $ComplementaryAlleles{$cols[3]})
            {
              $iOut=1;
             $iLineNumber4++;
            }           
          }
          else
          {
           print "$ChrPos;$AllUsedSNVsInfo{$ChrPos};$cols[3];$cols[4]\n";
          }
          if($iOut==1)
          { 
           $iLineNumber++;
            if(!defined($UniqLociPos{$ChrPos}))
            {
             $UniqLociPos{$ChrPos}=1;
              $iUniqLineNumber++;
            }
          }
         $iLineNumber00++;
       }      
   }
  close INPUT;
  print "$NumberofAllUsedSNVs\t$iUsedSNVs\t$iUniqLineNumber\t$iLineNumber01\t$iLineNumber02\t$iLineNumber\t$iLineNumber0\t$iLineNumber1\t$iLineNumber3\t$iLineNumber4\n";
}



