#Convert imputed WGS based prediction into corresponing ZSCORE and int zscore
library(ggplot2)
 library(magrittr);
 library(RNOmni);
 library(dplyr)
 library(stringr)

#Imputed st ut,xt psyencode,standard
#Original st ut,xt psyencode,standard

#Impute and original

 mydir="/home/hanl3/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMCHBCC_DLPFC_GeneLevelSpearmanCorrforActualPredictedExpression/";
  Outputfile=paste(mydir,"/CMC_HBCCDifferentMethodBasedCorrComparisonScatterPlot_UTMOSTImputed_WGS_NumberofGenes.pdf",sep="");
   pdf(Outputfile, 6, 6);
   par(mar=c(4,4,1.5,0.25), mgp=c(2.5,0.5,0), mfrow=c(1,1),oma=c(0.5,0.5,0.5,0.5)); #For UTMOST
   # par(mar=c(5,7,1.5,0.25), mgp=c(3,1,0), mfrow=c(1,1),oma=c(0.5,0.5,0.5,0.5));
 
 DataTypes=c("FullGenotype","Original");
    WholeDataTypes1=c("GTExV8FullGenotype_Imputed_st","GTExV8FullGenotype_Imputed_ut","GTExV8FullGenotype_Imputed_xt","PsychEncodeFullGenotype_Imputed_INT","PsychEncodeFullGenotype_Imputed_Standard");
    MethodTypes1=c("Imputed_GTExV8_st","Imputed_GTExV8_ut","Imputed_GTExV8_xt","Imputed_PsychEncode_INT","Imputed_PsychEncode_Standard");

    WholeDataTypes2=c("GTExV8Original_st","GTExV8Original_ut","GTExV8Original_xt","PsychEncodeOriginal_INT","PsychEncodeOriginal_Standard");
    MethodTypes2=c("Original_GTExV8_st","Original_GTExV8_ut","Original_GTExV8_xt","Original_PsychEncode_INT","Original_PsychEncode_Standard");
      ii=2;
        CorrFile1=paste(mydir,WholeDataTypes1[ii],"_GeneLevelSpearmanCorr.txt",sep="");
        CorrMatrix1=read.table(CorrFile1, sep="",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE,row.names=NULL);#Obtain column names from File By check.names   
        CorrMatrix1=as.data.frame(CorrMatrix1);
        print(dim(CorrMatrix1));
        WGS_NumberofGenes=dim(CorrMatrix1)[[1]];
        CorrFile2=paste(mydir,WholeDataTypes2[ii],"_GeneLevelSpearmanCorr.txt",sep="");
        CorrMatrix2=read.table(CorrFile2, sep="",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE,row.names=NULL);#Obtain column names from File By check.names   
        CorrMatrix2=as.data.frame(CorrMatrix2);
          Imputed_NumberofGenes=dim(CorrMatrix2)[[1]];
        print(dim(CorrMatrix2));
        NumberofGenes <- data.frame(DataType=c("WGS", "Imputed"),
                NumGenes=c(WGS_NumberofGenes,Imputed_NumberofGenes));

     NumberofGenes[,2]=as.numeric(NumberofGenes[,2]);
      print(NumberofGenes);
      P<-ggplot(data=NumberofGenes,aes(x=DataType,y=NumGenes))+geom_bar(stat="identity", width=0.5)+theme(axis.title.y = element_blank(),axis.text.y = element_text(size=20),axis.title.x =element_text(size=16))+ ylab("Number of predicted genes")+ coord_flip();
      print(P);
      ggsave(Outputfile);
      dev.off();
   
       Outputfile=paste(mydir,"/CMC_HBCCDifferentMethodBasedCorrComparisonScatterPlot_UTMOSTImputed_WGS.pdf",sep="");
        MergedCorr=inner_join(x=CorrMatrix1,y=CorrMatrix2,by =c("GeneID"),suffixes = c(".x",".y"));
         MergedCorr=as.data.frame(MergedCorr);
        colnames(MergedCorr)=c("GeneID","Corr1","Corr2");
        MergedCorr[,2]=as.numeric(MergedCorr[,2]);
        MergedCorr[,3]=as.numeric(MergedCorr[,3]);
        MergedCorr_Dim=dim(MergedCorr);
        corr=signif(cor(MergedCorr[,2],MergedCorr[,3],use="complete.obs"),3);
        P1<-ggplot(MergedCorr, aes(x=Corr1,y=Corr2)) + geom_point()+ geom_abline(intercept =0, slope =1,color="red", linetype="dashed", size=0.5)+scale_x_continuous(breaks=seq(-0.7,0.9,0.2),limits=c(-0.7,0.9))+
        scale_y_continuous(breaks=seq(-0.7,0.9,0.2),limits=c(-0.7,0.9))+ annotate(geom="text", x=-0.55, y=0.85, label=paste("r= ",corr,"\nn= ",MergedCorr_Dim[[1]],sep=""),color="red",size=5)+xlab("Imputation based corr (GTEx V8 UTMOST)")+ylab("WGS based corr (GTEx V8 UTMOST)");
        print(P1);
        ggsave(Outputfile);
        dev.off();

       # plot(MergedCorr$Corr1,MergedCorr$Corr2, main=NULL,ylab="WGS based Corr", xlab="Imputed based Corr",frame=FALSE, xaxt="n",yaxt="n");
       # axis(1,seq(-0.7,0.9,0.2),seq(-0.7,0.95,0.2),cex.axis=1);
       # axis(2,seq(-0.7,0.9,0.2),seq(-0.7,0.95,0.2),cex.axis=1);
       # abline(a = 0, b = 1, col = 2,lty=2);
       #label=paste("r= ",corr,"\nn= ",MergedCorr_Dim[1],sep="");
     #   text(-0.5,0.7,label,cex=1.4);

