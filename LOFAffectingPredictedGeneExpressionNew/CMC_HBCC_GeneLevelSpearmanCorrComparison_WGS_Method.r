#Convert imputed WGS based prediction into corresponing ZSCORE and int zscore
 library(ggplot2)
 library(magrittr);
 library(RNOmni);
 library(dplyr)
 library(stringr)

#Input file list
#/home/hanl3/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMCHBCC_DLPFC_GeneLevelSpearmanCorrforActualPredictedExpression/GTExV8FullGenotype_Imputed_st_GeneLevelSpearmanCorr.txt
#/home/hanl3/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMCHBCC_DLPFC_GeneLevelSpearmanCorrforActualPredictedExpression/GTExV8FullGenotype_Imputed_ut_GeneLevelSpearmanCorr.txt
#/home/hanl3/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMCHBCC_DLPFC_GeneLevelSpearmanCorrforActualPredictedExpression/GTExV8FullGenotype_Imputed_xt_GeneLevelSpearmanCorr.txt
#/home/hanl3/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMCHBCC_DLPFC_GeneLevelSpearmanCorrforActualPredictedExpression/GTExV8Original_st_GeneLevelSpearmanCorr.txt
#/home/hanl3/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMCHBCC_DLPFC_GeneLevelSpearmanCorrforActualPredictedExpression/GTExV8Original_ut_GeneLevelSpearmanCorr.txt
#/home/hanl3/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMCHBCC_DLPFC_GeneLevelSpearmanCorrforActualPredictedExpression/GTExV8Original_xt_GeneLevelSpearmanCorr.txt
#/home/hanl3/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMCHBCC_DLPFC_GeneLevelSpearmanCorrforActualPredictedExpression/PsychEncodeFullGenotype_Imputed_INT_GeneLevelSpearmanCorr.txt
#/home/hanl3/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMCHBCC_DLPFC_GeneLevelSpearmanCorrforActualPredictedExpression/PsychEncodeFullGenotype_Imputed_Standard_GeneLevelSpearmanCorr.txt
#/home/hanl3/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMCHBCC_DLPFC_GeneLevelSpearmanCorrforActualPredictedExpression/PsychEncodeOriginal_INT_GeneLevelSpearmanCorr.txt
#/home/hanl3/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMCHBCC_DLPFC_GeneLevelSpearmanCorrforActualPredictedExpression/PsychEncodeOriginal_Standard_GeneLevelSpearmanCorr.txt


#Imputed st ut,xt psyencode,standard
#Original st ut,xt psyencode,standard


#Impute and original

 mydir="/home/hanl3/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMCHBCC_DLPFC_GeneLevelSpearmanCorrforActualPredictedExpression/";
  Outputfile=paste(mydir,"/CMC_HBCCDifferentMethodBasedCorrComparisonScatterPlot_NumberofGenes_Methods.pdf",sep="");
   pdf(Outputfile, 6, 6);
   par(mar=c(4,4,1.5,0.25), mgp=c(2.5,0.5,0), mfrow=c(1,1),oma=c(0.5,0.5,0.5,0.5)); #For UTMOST

 DataTypes=c("FullGenotype","Original");


   WholeDataTypes=NULL;
   MethodTypes=NULL;
    WholeDataTypes=c("GTExV8Original_st","GTExV8Original_ut","GTExV8Original_xt","PsychEncodeOriginal_INT","PsychEncodeOriginal_Standard");
    MethodTypes=c("Original_GTExV8_st","Original_GTExV8_ut","Original_GTExV8_xt","Original_PsychEncode_INT","Original_PsychEncode_Standard");

       CorrFile1=paste(mydir,WholeDataTypes[1],"_GeneLevelSpearmanCorr.txt",sep="");
        CorrMatrix1=read.table(CorrFile1, sep="",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE,row.names=NULL);#Obtain column names from File By check.names   
        CorrMatrix1=as.data.frame(CorrMatrix1);
         PrediXcan_NumberofGenes=dim(CorrMatrix1)[[1]];
        CorrFile2=paste(mydir,WholeDataTypes[2],"_GeneLevelSpearmanCorr.txt",sep="");
        CorrMatrix2=read.table(CorrFile2, sep="",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE,row.names=NULL);#Obtain column names from File By check.names   
        CorrMatrix2=as.data.frame(CorrMatrix2);
        Utmost_NumberofGenes=dim(CorrMatrix2)[[1]];
        CorrFile3=paste(mydir,WholeDataTypes[4],"_GeneLevelSpearmanCorr.txt",sep="");
        CorrMatrix3=read.table(CorrFile3, sep="",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE,row.names=NULL);#Obtain column names from File By check.names   
        CorrMatrix3=as.data.frame(CorrMatrix3);
        TWAS_NumberofGenes=dim(CorrMatrix3)[[1]];
        NumberofGenes <- data.frame(DataType=c("GTExV8_PrediXcan", "GTExV8_UTMOST","PsychEncode_TWAS"),
        NumGenes=c(PrediXcan_NumberofGenes,Utmost_NumberofGenes,TWAS_NumberofGenes));
       NumberofGenes[,2]=as.numeric(NumberofGenes[,2]);
       print(NumberofGenes);
       P<-ggplot(data=NumberofGenes,aes(x=DataType,y=NumGenes))+geom_bar(stat="identity", width=0.5)+theme(axis.title.y = element_blank(),axis.text.y = element_text(size=20),axis.title.x =element_text(size=16))+ ylab("Number of predicted genes")+ coord_flip();
       print(P);
       ggsave(Outputfile);
       dev.off();
       

      mydir="/home/hanl3/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMCHBCC_DLPFC_GeneLevelSpearmanCorrforActualPredictedExpression/";
       Outputfile1=paste(mydir,"/CMC_HBCCDifferentMethodBasedCorrComparisonScatterPlot_PrediXcan_UTMOST.pdf",sep="");
       pdf(Outputfile1, 6, 6);
       par(mar=c(4,4,1.5,0.25), mgp=c(2.5,0.5,0), mfrow=c(1,1),oma=c(0.5,0.5,0.5,0.5)); #For UTMOST
       CorrFile1=paste(mydir,WholeDataTypes[1],"_GeneLevelSpearmanCorr.txt",sep="");
       CorrMatrix1=read.table(CorrFile1,sep="",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE,row.names=NULL);#Obtain column names from File By check.names   
       CorrMatrix1=as.data.frame(CorrMatrix1);
       CorrFile2=paste(mydir,WholeDataTypes[2],"_GeneLevelSpearmanCorr.txt",sep="");
       CorrMatrix2=read.table(CorrFile2, sep="",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE,row.names=NULL);#Obtain column names from File By check.names   
       CorrMatrix2=as.data.frame(CorrMatrix2);
       MergedCorr=inner_join(x=CorrMatrix1,y=CorrMatrix2,by =c("GeneID"),suffixes = c(".x",".y"));
       MergedCorr=as.data.frame(MergedCorr);
       colnames(MergedCorr)=c("GeneID","GTExV8_PrediXcan","GTExV8_UTMOST");
       MergedCorr[,2]=as.numeric(MergedCorr[,2]);
       MergedCorr[,3]=as.numeric(MergedCorr[,3]);
       MergedCorr_Dim=dim(MergedCorr);       
       print(MergedCorr_Dim);
      print(MergedCorr[c(1:5),]);
       corr=signif(cor(MergedCorr[,2],MergedCorr[,3],use="complete.obs"),3);
       print (corr);
       P1<-ggplot(MergedCorr, aes(x=GTExV8_PrediXcan,y=GTExV8_UTMOST)) + geom_point()+ geom_abline(intercept =0, slope =1,color="red", linetype="dashed", size=0.5)+scale_x_continuous(breaks=seq(-0.7,0.9,0.2),limits=c(-0.7,0.9))+
            scale_y_continuous(breaks=seq(-0.7,0.9,0.2),limits=c(-0.7,0.9))+ annotate(geom="text", x=-0.55, y=0.85, label=paste("r= ",corr,"\nn= ",MergedCorr_Dim[[1]],sep=""),color="red",size=5)+xlab("WGS based corr (GTEx V8 PrediXcan)")+ylab("WGS based corr (GTEx V8 UTMOST)");
       print(P1);
       ggsave(Outputfile1);
        dev.off();  

       Outputfile2=paste(mydir,"/CMC_HBCCDifferentMethodBasedCorrComparisonScatterPlot_UTMOST_TWAS.pdf",sep="");
       pdf(Outputfile2, 6, 6);
       par(mar=c(4,4,1.5,0.25), mgp=c(2.5,0.5,0), mfrow=c(1,1),oma=c(0.5,0.5,0.5,0.5)); #For UTMOST
       CorrFile1=paste(mydir,WholeDataTypes[2],"_GeneLevelSpearmanCorr.txt",sep="");
       CorrMatrix1=read.table(CorrFile1,sep="",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE,row.names=NULL);#Obtain column names from File By check.names   
       CorrMatrix1=as.data.frame(CorrMatrix1);
       CorrFile2=paste(mydir,WholeDataTypes[4],"_GeneLevelSpearmanCorr.txt",sep="");
       CorrMatrix2=read.table(CorrFile2, sep="",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE,row.names=NULL);#Obtain column names from File By check.names   
       CorrMatrix2=as.data.frame(CorrMatrix2);
       MergedCorr=inner_join(x=CorrMatrix1,y=CorrMatrix2,by =c("GeneID"),suffixes = c(".x",".y"));
       MergedCorr=as.data.frame(MergedCorr);
       colnames(MergedCorr)=c("GeneID","GTExV8_UTMOST","PsychEncode_TWAS");
       MergedCorr[,2]=as.numeric(MergedCorr[,2]);
        MergedCorr[,3]=as.numeric(MergedCorr[,3]);
       MergedCorr_Dim=dim(MergedCorr);
       corr=signif(cor(MergedCorr[,2],MergedCorr[,3],use="complete.obs"),3);
      print(corr);
       P2<-ggplot(MergedCorr, aes(x=GTExV8_UTMOST,y=PsychEncode_TWAS)) + geom_point()+ geom_abline(intercept =0, slope =1,color="red", linetype="dashed", size=0.5)+scale_x_continuous(breaks=seq(-0.7,0.9,0.2),limits=c(-0.7,0.9))+
            scale_y_continuous(breaks=seq(-0.7,0.9,0.2),limits=c(-0.7,0.9))+ annotate(geom="text", x=-0.55, y=0.85, label=paste("r= ",corr,"\nn= ",MergedCorr_Dim[[1]],sep=""),color="red",size=5)+xlab("WGS based corr (GTEx V8 UTMOST)")+ylab("WGS based corr (PsychEncode TWAS)");
       print(P2);
       ggsave(Outputfile2);
       dev.off();

