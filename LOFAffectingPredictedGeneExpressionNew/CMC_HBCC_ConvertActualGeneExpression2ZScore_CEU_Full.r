#Convert imputed WGS based prediction into corresponing ZSCORE and int zscore

 library(magrittr);
 library(RNOmni);
 library(dplyr)
 library(stringr)


  Batch_Tissue=c("CMC_ACC","CMC_DLPFC","HBCC_DLPFC");
  DuplicateOutliers=c("MSSM-DNA-PFC-375","MSSM-DNA-PFC-269","CMC-HBCC-DNA-ACC-6052","CMC-HBCC-DNA-ACC-5669","CMC-HBCC-DNA-ACC-4237","CMC-HBCC-DNA-ACC-5646",
    "CMC-HBCC-DNA-ACC-5777","CMC-HBCC-DNA-ACC-5682","CMC-HBCC-DNA-ACC-4284","CMC-HBCC-DNA-ACC-4137",
    "CMC-HBCC-DNA-ACC-6009","CMC-HBCC-DNA-ACC-6056","CMC-HBCC-DNA-ACC-4029","CMC-HBCC-DNA-ACC-4200",
    "CMC-HBCC-DNA-ACC-4074","CMC-HBCC-DNA-ACC-4204","CMC-HBCC-DNA-ACC-5654","CMC-HBCC-ACC-DNA-4235");
 MissingOutlierCEUs=c("MSSM-DNA-PFC-153","Penn-DNA-PFC-033","Pitt-DNA-PFC-1324","Pitt-DNA-PFC-1524","Pitt-DNA-PFC-970","Pitt-DNA-BP-PFC-789","Pitt-DNA-BP-PFC-840","Pitt-DNA-PFC-1088","MSSM-DNA-PFC-56");

#     18,842 /data/ruderferlab/projects/cmc/results/sv/expression/CMC_ACC_eqtlResidualExpression_WithoutSVbasedOutierIndividuals.tsv [261 samples]
#     18,842 /data/ruderferlab/projects/cmc/results/sv/expression/CMC_DLPFC_eqtlResidualExpression_WithoutSVbasedOutierIndividuals.tsv [312 samples]
#     17,251 /data/ruderferlab/projects/cmc/results/sv/expression/HBCC_DLPFC_eqtlResidualExpression_WithoutSVbasedOutierIndividuals.tsv [317 samples]

  AllSampleIDInfoFile="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/WGSData_VariantSummary_InferredAncestry.txt";
  AllSampleIDMatrix=read.table(AllSampleIDInfoFile, sep="\t",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE);#Obtain column names from File By check.names  
  InferCEUSampleSubset=AllSampleIDMatrix[which(AllSampleIDMatrix$InferredAncestry=="CEU"),]
  CEUWGSID=InferCEUSampleSubset$WGSID;
  print(length(CEUWGSID));
  print(CEUWGSID[1:5]);
#SKL_10073GTEXV8_PredictedGeneExpressionst_ACC_BA24_FullGenotype_Imputed.txt

  for(iBT in seq(1,3))#3
  {
      ActualExpressionFile=paste("/data/ruderferlab/projects/cmc/results/sv/expression/",Batch_Tissue[iBT],"_eqtlResidualExpression_WithoutSVbasedOutierIndividuals.tsv",sep="");
      iFileSize=file.info(ActualExpressionFile)$size;
      if(iFileSize==0)
       {
         print ("ActualExpressionFile\n"); 
         next;
       }
      ActualGeneExpression=read.table(ActualExpressionFile, sep="\t",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,nrows=-1,fill=FALSE,strip.white=TRUE,row.names=NULL);#Obtain column names from File By check.names   
      print(ActualExpressionFile); 
      ActualGeneExpression =as.data.frame(ActualGeneExpression);
      GeneIDColNames=colnames(ActualGeneExpression);
      GeneIDColNames=str_replace_all(GeneIDColNames, "[.]", "-");#replace "." into "-"
      colnames(ActualGeneExpression)=GeneIDColNames; 
      GeneIDColNames=as.vector(GeneIDColNames);
      OverlapID=intersect(GeneIDColNames,DuplicateOutliers);
      GeneIDRowMatrix=ActualGeneExpression[,1];#Obtain Gene Name column information
      GeneIDRowMatrix=as.data.frame(GeneIDRowMatrix);
      colnames(GeneIDRowMatrix)="GeneID";
      GeneIDRowMatrixColNames=colnames(GeneIDRowMatrix);
      print(GeneIDRowMatrixColNames);
     print(dim(ActualGeneExpression));
       ActualGeneExpression=ActualGeneExpression[,(names(ActualGeneExpression)%in%CEUWGSID)];#Use CEUID to filter columns  
      print(dim(ActualGeneExpression));
      NormalActualGeneExpression=ActualGeneExpression[,!(names(ActualGeneExpression)%in%DuplicateOutliers)]; #Use duplicate overlapped ID to filter
      print(dim(NormalActualGeneExpression));
      GeneExpression=NormalActualGeneExpression[,!(names(NormalActualGeneExpression)%in%MissingOutlierCEUs)];#Use Missing outlier to filter
      GeneExpressionNames=colnames(GeneExpression);

      GeneExpressionDim=dim(GeneExpression);
     GeneExpression[,1:GeneExpressionDim[2]] <- sapply(GeneExpression[,1:GeneExpressionDim[2]], as.numeric);

     GeneExpressionZScore=t(apply(GeneExpression, 1,scale));
     GeneExpressionIZScoreMerged=as.data.frame(cbind(GeneIDRowMatrix,GeneExpressionZScore)); 
     colnames(GeneExpressionIZScoreMerged)=c(GeneIDRowMatrixColNames,GeneExpressionNames); 
     ActualExpressionFile=paste("/home/hanl3/cmc/scratch/Predicted_ActualGeneExpressionMerged/CMC_HBCCActualGeneExpression/",Batch_Tissue[iBT],"_ActualGeneExInferredCEUBasedStandardZScore.txt",sep="");     
     write.table(GeneExpressionIZScoreMerged, file=ActualExpressionFile, append = FALSE, quote =FALSE, sep = "\t", eol = "\n",col.names=TRUE,row.names=FALSE);
 }

