#Convert imputed WGS based prediction into corresponing ZSCORE and int zscore

 library(magrittr);
 library(RNOmni);
 library(dplyr)
 library(stringr)


BatchList=c("SKL_10073_B01_GRM_WGS_2016-02-25","SKL_11154_B01_GRM_WGS_2016-03-17","SKL_11694_B01_GRM_WGS_2017-08-18");
ConsiseBatchNamelist=c("SKL_10073","SKL_11154","SKL_11694");
DataTypes=c("FullGenotype_Imputed","Original");
   DuplicateOutliers=c("MSSM-DNA-PFC-375","MSSM-DNA-PFC-269","CMC-HBCC-DNA-ACC-6052","CMC-HBCC-DNA-ACC-5669","CMC-HBCC-DNA-ACC-4237","CMC-HBCC-DNA-ACC-5646",
    "CMC-HBCC-DNA-ACC-5777","CMC-HBCC-DNA-ACC-5682","CMC-HBCC-DNA-ACC-4284","CMC-HBCC-DNA-ACC-4137",
    "CMC-HBCC-DNA-ACC-6009","CMC-HBCC-DNA-ACC-6056","CMC-HBCC-DNA-ACC-4029","CMC-HBCC-DNA-ACC-4200",
    "CMC-HBCC-DNA-ACC-4074","CMC-HBCC-DNA-ACC-4204","CMC-HBCC-DNA-ACC-5654","CMC-HBCC-ACC-DNA-4235");
 MissingOutlierCEUs=c("MSSM-DNA-PFC-153","Penn-DNA-PFC-033","Pitt-DNA-PFC-1324","Pitt-DNA-PFC-1524","Pitt-DNA-PFC-970","Pitt-DNA-BP-PFC-789","Pitt-DNA-BP-PFC-840","Pitt-DNA-PFC-1088","MSSM-DNA-PFC-56");

#MSSM-DNA-PFC-153	SKL_10073	0.0041	0.0100	CEU
#Penn-DNA-PFC-033	SKL_10073	0.0041	0.0101	CEU
#Pitt-DNA-PFC-1324	SKL_10073	0.0045	0.0101	CEU
#Pitt-DNA-PFC-1524	SKL_10073	0.0043	0.0101	CEU
#Pitt-DNA-PFC-970	SKL_10073	0.0040	0.0101	CEU
#Pitt-DNA-BP-PFC-789	SKL_10073	0.0044	0.0102	CEU
#Pitt-DNA-BP-PFC-840	SKL_10073	0.0041	0.0102	CEU
#Pitt-DNA-PFC-1088	SKL_10073	0.0043	0.0102	CEU
#MSSM-DNA-PFC-56	SKL_10073	0.0043	0.0103	CEU


  
  AllSampleIDInfoFile="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/WGSData_VariantSummary_InferredAncestry.txt";
  AllSampleIDMatrix=read.table(AllSampleIDInfoFile, sep="\t",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE);#Obtain column names from File By check.names  
  InferCEUSampleSubset=AllSampleIDMatrix[which(AllSampleIDMatrix$InferredAncestry=="CEU"),]
  CEUWGSID=InferCEUSampleSubset$WGSID;
  print(length(CEUWGSID));
  print(CEUWGSID[1:5]);
  Tissues=c("DLPFC_BA9","ACC_BA24");
  PredictionMethods=c("st","ut","xt");
#SKL_10075GTEXV8_PredictedGeneExpressionst_ACC_BA24_FullGenotype_Imputed.txt
  Haplotypes=c("Haplotype1","Haplotype2");

  for(iBatch in seq(1,3))#3
  {
   for(iType in seq(1,2))#2
   {
    for(iMethod in seq(1,3))
    {
    for(iTissue in seq(1,2))
    {
  
       MergedPredictedGeneExpression=NULL;
      TotalGeneExpression=NULL;
       for(iType in seq(1,2))
       {
         PredictedExpressionFile=paste("/home/hanl3/cmc/scratch/Predicted_ActualGeneExpressionMerged/GTEXV8_Prediction/PredictedValues/",ConsiseBatchNamelist[iBatch],"GTEXV8_PredictedGeneExpression",PredictionMethods[iMethod],"_",Tissues[iTissue],"_",Haplotypes[iType],"_Imputed.txt.gz",sep="");
         con<-file(PredictedExpressionFile,'rt');
         PredictedGeneExpression=read.table(con, sep="",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE,row.names=NULL);#Obtain column names from File By check.names   
         # PredictedGeneExpression=read.table(PredictedExpressionFile, sep="",check.names=TRUE,nrows=-1, colClasses="character",na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE,row.names=NULL);#Using space as splitter to solve the problem
         print(PredictedExpressionFile); 
         PredictedGeneExpression =as.data.frame(PredictedGeneExpression);
         print(dim(PredictedGeneExpression));
      #   PredictedGeneExpression=PredictedGeneExpression[,-c(1)];#Remove 1st column
         GeneIDColNames=colnames(PredictedGeneExpression);
         GeneIDColNames=str_replace_all(GeneIDColNames, "[.]", "-");#replace "." into "-"
         colnames(PredictedGeneExpression)=GeneIDColNames;
          print(dim(PredictedGeneExpression)); 
         GeneIDColNames=as.vector(GeneIDColNames);
         print(length(GeneIDColNames));
         OverlapID=intersect(GeneIDColNames,DuplicateOutliers);
         print(dim(PredictedGeneExpression));
         GeneIDRowMatrix=PredictedGeneExpression[,c(1:8)];#Obtain Gene Name column information
         GeneIDRowMatrixColNames=colnames(GeneIDRowMatrix);

          PredictedGeneExpression=PredictedGeneExpression[,(names(PredictedGeneExpression)%in%CEUWGSID)]; 
         print(dim(PredictedGeneExpression));
         NormalPredictedGeneExpression=PredictedGeneExpression[,!(names(PredictedGeneExpression)%in%DuplicateOutliers)]; 
         print(dim(NormalPredictedGeneExpression));
         PredictedGeneExpressionWithoutGeneID=NormalPredictedGeneExpression[,!(names(NormalPredictedGeneExpression)%in%MissingOutlierCEUs)];
         print("prediction expression");
          print(dim(PredictedGeneExpressionWithoutGeneID));
      
          GeneExpressionWithoutGeneIDNames=colnames(PredictedGeneExpressionWithoutGeneID);
          GeneExpressionWithID=as.data.frame(cbind(GeneIDRowMatrix,PredictedGeneExpressionWithoutGeneID));
          colnames(GeneExpressionWithID)=c(GeneIDRowMatrixColNames,GeneExpressionWithoutGeneIDNames);
     
         if(iType==1)
         {
           HyploType1PredictedGeneExpression=GeneExpressionWithID;
         }
         else#Chr     WGSBatch        PredictionMethods       BrainsTissues   Source  GeneID  NumberofLociInWeightModel       NumberofLociInActualModel
         { 
             MergedPredictedGeneExpression=inner_join(x= HyploType1PredictedGeneExpression,y=GeneExpressionWithID,by =c("Chr","WGSBatch","PredictionMethods","BrainsTissues","GeneID"),suffixes = c(".x",".y"));
           #  MergedPredictedGeneExpression=inner_join(x= HyploType1PredictedGeneExpression,y=GeneExpressionWithID,by =c("Chr","WGSBatch","PredictionMethods","BrainsTissues","GeneID"),suffixes = c("",".y"));        
          #  MergedPredictedGeneExpression=merge(x= HyploType1PredictedGeneExpression,y=GeneExpressionWithID,by =c("Chr","WGSBatch","PredictionMethods","BrainsTissues","GeneID"),suffixes = c(".x",".y"));
            print(dim(MergedPredictedGeneExpression));
         }
          close(con);
         MergedPredictedGeneExpression=as.data.frame(MergedPredictedGeneExpression);
         MergedPredictedGeneExpression_Subset=MergedPredictedGeneExpression[, !(colnames(MergedPredictedGeneExpression) %in% c("Source.x","NumberofLociInWeightModel.x","NumberofLociInActualModel.x","Source.y","NumberofLociInWeightModel.y","NumberofLociInActualModel.y"))]
       } 
      print(dim(MergedPredictedGeneExpression_Subset));
      GeneIDRowMatrix=MergedPredictedGeneExpression_Subset[,c(1:5)];#Obtain Gene Name column information
      GeneIDRowMatrixColNames=colnames(GeneIDRowMatrix);
      TotalGeneExpression=MergedPredictedGeneExpression_Subset[,!(colnames(MergedPredictedGeneExpression_Subset) %in% c("Chr","WGSBatch","PredictionMethods","BrainsTissues","GeneID"))];
       print("Combined Matrix");
      print(dim(TotalGeneExpression));
       FullColNames=colnames(TotalGeneExpression);
       GeneExpressionDim=dim(TotalGeneExpression);
         TotalGeneExpression[,1:GeneExpressionDim[2]] <- sapply(TotalGeneExpression[,1:GeneExpressionDim[2]], as.numeric);
         TotalGeneExpressionINT= t(apply(TotalGeneExpression, 1, rankNorm));# The result from the apply() had to be transposed using t() to get the same layout as the input matrix A  
         TotalGeneExpressionZScore=t(apply(TotalGeneExpression, 1,scale));
         TotalGeneExpressionINTDim=dim(TotalGeneExpressionINT);
         HalfColumn=TotalGeneExpressionINTDim[2]/2;
         print(HalfColumn);

         A1Names=FullColNames[1:HalfColumn];
         A2Names=FullColNames[(HalfColumn+1):TotalGeneExpressionINTDim[2]];
         A1Names=gsub(".x", "",A1Names);
          A2Names=gsub(".y", "",A2Names);
         iUnequal=0;
         for(iNum in seq(1:HalfColumn))
         {
           if(A1Names[iNum] !=A2Names[iNum])
           {
             iUnequal= iUnequal+1;
           }
         }
         print(iUnequal);
         for(iType in seq(1,2))
         {
           StartPos=(iType-1)*HalfColumn;
           EndPos=iType*HalfColumn;
           GeneExpressionINT=TotalGeneExpressionINT[,c((StartPos+1):EndPos)];
           GeneExpressionZScore=TotalGeneExpressionZScore[,c((StartPos+1):EndPos)];
            GeneExpressionINTMerged=as.data.frame(cbind(GeneIDRowMatrix, GeneExpressionINT));#INT score merged
           colnames(GeneExpressionINTMerged)=c(GeneIDRowMatrixColNames,A1Names);;
           print(GeneExpressionINTMerged[c(1:2),c(1:20)]);   
           print(dim(GeneExpressionINTMerged));
           PredictedExpressionFile=paste("/home/hanl3/cmc/scratch/Predicted_ActualGeneExpressionMerged/GTEXV8_Prediction/PredictedValues_Zscore/GTExV8Based",ConsiseBatchNamelist[iBatch],PredictionMethods[iMethod],"_",Tissues[iTissue],"_",Haplotypes[iType],"_GenePredictedExCEUBasedINTZScore.txt",sep="");
           write.table(GeneExpressionINTMerged, file=PredictedExpressionFile, append = FALSE, quote =FALSE, sep = "\t", eol = "\n",col.names=TRUE,row.names=FALSE);

           GeneExpressionIZScoreMerged=as.data.frame(cbind(GeneIDRowMatrix,GeneExpressionZScore)); #Standard score merged
            colnames(GeneExpressionIZScoreMerged)=c(GeneIDRowMatrixColNames,A1Names);
            PredictedExpressionFile=paste("/home/hanl3/cmc/scratch/Predicted_ActualGeneExpressionMerged/GTEXV8_Prediction/PredictedValues_Zscore/GTExV8Based",ConsiseBatchNamelist[iBatch],PredictionMethods[iMethod],"_",Tissues[iTissue],"_",Haplotypes[iType],"_GenePredictedExCEUBasedREGZScore.txt",sep="");
           write.table(GeneExpressionIZScoreMerged, file=PredictedExpressionFile, append = FALSE, quote =FALSE, sep = "\t", eol = "\n",col.names=TRUE,row.names=FALSE);
        }
     }
   } 
  }
}

