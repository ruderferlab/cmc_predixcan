#!/usr/bin/perl -w
 use strict;
  use 5.012;
 use lib "/home/hanl3/perl5/module/lib/site_perl/5.14.2";
 use Text::CSV_PP;
  use Tie::File; 
 use POSIX qw(WNOHANG);
 use warnings;
use Scalar::Util qw(looks_like_number);

# UTMOST, XT_Scan,PrediXcan two brain tissues' preciction model which based on GRch38 assembly
# /data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/gtex_v8_st_Brain_Anterior_cingulate_cortex_BA24.txt
#       gtex_v8_st_Brain_Frontal_Cortex_BA9.txt  gtex_v8_ut_Brain_Anterior_cingulate_cortex_BA24.txt
#       gtex_v8_ut_Brain_Frontal_Cortex_BA9.txt  gtex_v8_xt_Brain_Anterior_cingulate_cortex_BA24.txt  gtex_v8_xt_Brain_Frontal_Cortex_BA9.txt
#/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs
#SKL_10073ShareV8MultiplyModel.CommonSNVs_PassWGS
#SKL_11154ShareV8MultiplyModel.CommonSNVs_PassWGS
#SKL_11694ShareV8MultiplyModel.CommonSNVs_PassWGS


  my @WGSBatches=("SKL_10073","SKL_11154","SKL_11694");
  my @PredictionMethods=("st","ut","xt");
  my @BrainsTissues=("Brain_Frontal_Cortex_BA9","Brain_Anterior_cingulate_cortex_BA24");
  my @BrainsTissuesNames=("DLPFC_BA9","ACC_BA24");

 my %Chr_GeneID=();#Chr_ geneid
 my %AllUsedSNVsInfo=(); 

my $iSpecificBatch=$ARGV[0];#0,1,2
my $iSpecificMethod=$ARGV[1];#0,1,2
my $iSpecificTussue=$ARGV[2];#0,1

#Obtain Gene list
#/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTExV8_Weight/*_*/$gene.wgt.score
 
 for(my $iBatch=$iSpecificBatch;$iBatch<$iSpecificBatch+1;$iBatch++)
  {
     for(my $iM=$iSpecificMethod;$iM<$iSpecificMethod+1;$iM++)
     {
       for(my $iT=$iSpecificTussue;$iT<$iSpecificTussue+1;$iT++)
       {
            my $OutZFile1="/home/hanl3/cmc/scratch/Predicted_ActualGeneExpressionMerged/GTEXV8_Prediction/PredictedValues/$WGSBatches[$iBatch]GTEXV8_UnPredictedGeneExpression$PredictionMethods[$iM]_$BrainsTissuesNames[$iT]_Original.txt";
            open OUTZ1, ">$OutZFile1" or die "Can't open Output file:$OutZFile1!";
             my $OutZFile="/home/hanl3/cmc/scratch/Predicted_ActualGeneExpressionMerged/GTEXV8_Prediction/PredictedValues/$WGSBatches[$iBatch]GTEXV8_PredictedGeneExpression$PredictionMethods[$iM]_$BrainsTissuesNames[$iT]_Original.txt";
            open OUTZ, ">$OutZFile" or die "Can't open Output file:$OutZFile!";   
            my $iNumberofFile=0;
            my %Chr_GeneID=();
            my %GeneID_NumberofLociInModel=();
            my $mydir="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTExV8_Weight/$PredictionMethods[$iM]_$BrainsTissuesNames[$iT]";
            opendir(DIR, $mydir) || die "Can't open $mydir: $!";
            while (my $weightFile=readdir (DIR)) {
            next if ($weightFile =~ m/^\./);
            my $geneID=$weightFile;
            $geneID =~ s/\.wgt\.score//ig;
            my $scoreWeightFile="$mydir/$weightFile";
           # print "$mydir/$weightFile\n";
           my $iLineNumber=0;
            my $ScoreFile="$mydir/$weightFile";
            open(INPUT,$ScoreFile) || die "can’t open $ScoreFile";
            while (<INPUT>)
            {
              chomp;
              if($iLineNumber==0)
              {
               my @cols=split(/\t/,$_,2);
               my @ChrPos=split(/:/,$cols[0]);
               if(!defined($Chr_GeneID{$ChrPos[0]}))
               {
                 $Chr_GeneID{$ChrPos[0]}=$geneID;
               } 
              else
               {
                 $Chr_GeneID{$ChrPos[0]}.="\t$geneID";
               }
             }
              $iLineNumber++; 
            }         
           close INPUT;
           $GeneID_NumberofLociInModel{$geneID}=$iLineNumber;
           $iNumberofFile++;
         #   last if($iNumberofFile>10);
         }
         closedir (DIR);
        my %GeneID_AllInfo=();
         my $NumberofGenes=keys %GeneID_NumberofLociInModel;
        print "Number of Genes:$NumberofGenes\n";
            $iNumberofFile=0;         
            my @AllIndIDList=();           
           $mydir="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTExV8_Profile/$PredictionMethods[$iM]_$BrainsTissuesNames[$iT]";
           opendir(DIR, $mydir) || die "Can't open $mydir: $!";
           while (my $profileFile=readdir (DIR)) {
           next if ($profileFile =~ m/^\./);
           if ($profileFile =~ m/profile$/ && $profileFile =~ m/^$WGSBatches[$iBatch]_OriginalWGS_/)
           {
               my $GeneIDFull=$profileFile;
               $GeneIDFull =~ s/.profile//ig;#SKL_11694_OriginalWGS_ENSG00000283538
               $GeneIDFull=~ s/^$WGSBatches[$iBatch]\_OriginalWGS\_//ig;              
              next if(!defined($GeneID_NumberofLociInModel{$GeneIDFull}));
             my $iLineNumber=0; 
             my $iEffectiveNumber=0;
             my $iDependentLociNumber=-1;
             my @OneGeneProfile=();
             my $profileFileName="$mydir/$profileFile";
          #   print "$profileFileName\n";
             open(INPUT,$profileFileName) || die "can’t open $profileFileName";
             while (<INPUT>)
             {
               chomp;
               if($iLineNumber==0)
               {
                 $iLineNumber++;
                 next;
               }
               $_ =~ s/^\s+|\s+$//g;#Remove space around the string 
                my @cols=split(/\s+/,$_);
               if($iEffectiveNumber==0)
               {
               $iDependentLociNumber=$cols[3]/2;
              #  print "$iDependentLociNumber\n";
               }
              if($iNumberofFile==0)
              {
               push  @AllIndIDList,$cols[1];
               push @OneGeneProfile,$cols[5];
              }
              else
              {
                 push @OneGeneProfile,$cols[5];
                if($AllIndIDList[$iEffectiveNumber] ne $cols[1] )
                 { 
                  print "Ind don't match: $iNumberofFile, $AllIndIDList[$iEffectiveNumber],$cols[1]";
                 } 
               }
               my $NumberofColumns=scalar(@cols);
               $iEffectiveNumber++;
             }
            close INPUT;     
              my $OneGeneProfileStr=join("\t",@OneGeneProfile);
              $GeneID_AllInfo{$GeneIDFull}="$iDependentLociNumber\t$OneGeneProfileStr";
            #  print "$GeneIDFull\t$GeneID_AllInfo{$GeneIDFull}\n";
              $iNumberofFile++;
          #   last if($iNumberofFile>10); 
           }       
          }
        closedir (DIR);

       print OUTZ "Chr\tWGSBatch\tPredictionMethods\tBrainsTissues\tSource\tGeneID\tNumberofLociInWeightModel\tNumberofLociInActualModel";
       for(my $iInd=0;$iInd<@AllIndIDList;$iInd++)
       {
         print OUTZ "\t$AllIndIDList[$iInd]";
       }  
       print OUTZ "\n";
       for(my $iChr=1;$iChr<=22;$iChr++)
       {
         if(defined($Chr_GeneID{$iChr}))
         {
            my @GeneID=split(/\t/,$Chr_GeneID{$iChr});
            for(my $iG=0;$iG<@GeneID;$iG++)
            {
              if(defined($GeneID_NumberofLociInModel{$GeneID[$iG]}) && defined($GeneID_AllInfo{$GeneID[$iG]}))
              { 
                print OUTZ "$iChr\t$WGSBatches[$iBatch]\t$PredictionMethods[$iM]\t$BrainsTissuesNames[$iT]\tOriginalGtype\t$GeneID[$iG]\t$GeneID_NumberofLociInModel{$GeneID[$iG]}\t$GeneID_AllInfo{$GeneID[$iG]}\n";
              }
              else
              {
                print  OUTZ1 "$iChr\t$WGSBatches[$iBatch]\t$PredictionMethods[$iM]\t$BrainsTissuesNames[$iT]\tOriginalGtype\t$GeneID[$iG]\t$GeneID_NumberofLociInModel{$GeneID[$iG]}\n";    
              }
            }
         }
       }
      close OUTZ;
      close OUTZ1;
    }
  }
}
exit 0;

