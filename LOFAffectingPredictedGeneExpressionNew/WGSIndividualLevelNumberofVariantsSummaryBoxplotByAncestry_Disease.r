#! /usr/local/R/3.2.0/x86_64/intel14/nonet/bin/Rscript  --vanilla
library("dplyr");
library("tidyverse");

view = 2;
  remove_outliers <- function(x, na.rm = TRUE, ...) {
  qnt <- quantile(x, probs=c(.25, .75), na.rm = na.rm, ...)
   H <- 3 * IQR(x, na.rm = na.rm)
   y <- x
   Lowerbound=qnt[1] - H;
   UpperBound=qnt[2] + H;
   Bound=c(Lowerbound[[1]],UpperBound[[1]]);
   return(Bound);
   #y[x < (qnt[1] - H)] <- NA
  # y[x > (qnt[2] + H)] <- NA
  # y
  }

   Outliers=c("MSSM-DNA-PFC-375","MSSM-DNA-PFC-269","CMC-HBCC-DNA-ACC-6052","CMC-HBCC-DNA-ACC-5669","CMC-HBCC-DNA-ACC-4237","CMC-HBCC-DNA-ACC-5646",
    "CMC-HBCC-DNA-ACC-5777","CMC-HBCC-DNA-ACC-5682","CMC-HBCC-DNA-ACC-4284","CMC-HBCC-DNA-ACC-4137",
    "CMC-HBCC-DNA-ACC-6009","CMC-HBCC-DNA-ACC-6056","CMC-HBCC-DNA-ACC-4029","CMC-HBCC-DNA-ACC-4200",
    "CMC-HBCC-DNA-ACC-4074","CMC-HBCC-DNA-ACC-4204","CMC-HBCC-DNA-ACC-5654","CMC-HBCC-ACC-DNA-4235");
#Input file:
# /data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/WGSData_VariantSummary_Ancestry.txt

  myDir="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/";
  Batches=c("SKL_10073","SKL_11694","SKL_11154");

    Outputfile=paste(myDir,"CMC_HBCCIndividualLevelWGSBasedVariants_Summaryboxplot.pdf",sep="");
    pdf(Outputfile, 7, 9);
   # jpeg(Outputfile, width = 480, height =700, units = "px", pointsize = 12);
    par(mar=c(5,7,1.5,0.25), mgp=c(3,1,0), mfrow=c(3,2),oma=c(0,0,0,0));
    linewide=2;
    axixwide=0.5;
    tickwide=0.5;
   cex_size=0.62;
   leg_cex_size=0.8;
   axes=F;
   xaxs="s";
   cex_size1=0.65;
  

   #WGSID   Sex     Batch   NumbofHyterrozyogotes_SNV       NumHyterrozyogotes_INDEL        Disease Ancestry  PC1     PC2,SNV_Missing     INDEL_Missing
    WGSVariantSummaryFile=paste(myDir,"WGSData_VariantSummary_Ancestry.txt",sep="");
    WGSVariantSummaryMatrix=read.table(WGSVariantSummaryFile, sep="\t",colClasses="character",na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE);
   WGSVariantSummaryMatrix=as.data.frame(WGSVariantSummaryMatrix);
    WGSVariantSummaryMatrix[,4]=as.numeric(WGSVariantSummaryMatrix[,4]);
    WGSVariantSummaryMatrix[,5]=as.numeric(WGSVariantSummaryMatrix[,5]);
    WGSVariantSummaryMatrix[,8]=as.numeric(WGSVariantSummaryMatrix[,8]);
    WGSVariantSummaryMatrix[,9]=as.numeric(WGSVariantSummaryMatrix[,9]);  
    WGSVariantSummaryMatrix[,10]=as.numeric(WGSVariantSummaryMatrix[,10]);
    WGSVariantSummaryMatrix[,11]=as.numeric(WGSVariantSummaryMatrix[,11]);

   WGSVariantSummaryMatrix$AncestryValue=NA;
   WGSVariantSummaryMatrix$AncestryValue[WGSVariantSummaryMatrix$Ancestry=="African-American"]=1;
   WGSVariantSummaryMatrix$AncestryValue[WGSVariantSummaryMatrix$Ancestry=="Asian"]=2;
   WGSVariantSummaryMatrix$AncestryValue[WGSVariantSummaryMatrix$Ancestry=="Caucasian"]=3;
   WGSVariantSummaryMatrix$AncestryValue[WGSVariantSummaryMatrix$Ancestry=="Hispanic"]=4;
   WGSVariantSummaryMatrix$AncestryValue[WGSVariantSummaryMatrix$Ancestry=="(Multiracial)"]=5;
   WGSVariantSummaryMatrix$AncestryValue=as.numeric(WGSVariantSummaryMatrix$AncestryValue); 

 #   WGSVariantSummaryMatrix=WGSVariantSummaryMatrix[!(WGSVariantSummaryMatrix$WGSID%in%Outliers),];#Remove outliers judged by Ancestry.
    boxplot(NumbofHyterrozyogotes_SNV~Batch, data=WGSVariantSummaryMatrix, notch=FALSE,main=NULL,ylab="Heterozygous SNVs",cex.lab=0.8, xlab=NULL); 
    boxplot(NumHyterrozyogotes_INDEL~Batch, data=WGSVariantSummaryMatrix, notch=FALSE,main=NULL,ylab="Heterozygous INDELs",cex.lab=0.8, xlab=NULL);
   
    boxplot(SNV_Missing~Batch, data=WGSVariantSummaryMatrix, notch=FALSE,main=NULL,ylab="Missing Rate of SNVs",cex.lab=0.8, xlab=NULL);
        abline(h=0.0043, col="blue");
       abline(h=0.0044, col="blue");
      abline(h=0.045, col="red");

    boxplot(INDEL_Missing~Batch, data=WGSVariantSummaryMatrix, notch=FALSE,main=NULL,ylab="Missing Rate of INDELs",cex.lab=0.8, xlab=NULL);
     abline(h=0.0101, col="blue");
       abline(h=0.0102, col="blue");
      abline(h=0.0103, col="red");

     boxplot(NumbofHyterrozyogotes_SNV~Sex, data=WGSVariantSummaryMatrix, notch=FALSE,main=NULL,cex.lab=0.8, ylab="Heterozygous SNVs",xlab=NULL);
    boxplot(NumHyterrozyogotes_INDEL~Sex, data=WGSVariantSummaryMatrix, notch=FALSE,main=NULL,cex.lab=0.8,ylab="Heterozygous INDELs",xlab=NULL);

     boxplot(NumbofHyterrozyogotes_SNV~Disease, data=WGSVariantSummaryMatrix, notch=FALSE,main=NULL,cex.lab=0.8,ylab="Heterozygous SNVs",xlab=NULL);
    boxplot(NumHyterrozyogotes_INDEL~Disease, data=WGSVariantSummaryMatrix, notch=FALSE,main=NULL,cex.lab=0.8,ylab="Heterozygous INDELs",xlab=NULL);
    print(WGSVariantSummaryMatrix$NumHyterrozyogotes_INDEL[1:10]);

     boxplot(NumbofHyterrozyogotes_SNV~Ancestry, data=WGSVariantSummaryMatrix, notch=FALSE,main=NULL,cex.lab=0.8,cex.axis=0.8,ylab="Heterozygous SNVs",xlab=NULL,las=2);
    boxplot(NumHyterrozyogotes_INDEL~Ancestry, data=WGSVariantSummaryMatrix, notch=FALSE,main=NULL,cex.lab=0.8,cex.axis=0.8, ylab="Heterozygous INDELs",xlab=NULL,las=2);

     plot(WGSVariantSummaryMatrix$PC1,WGSVariantSummaryMatrix$NumbofHyterrozyogotes_SNV,main=NULL,cex.lab=0.8,ylab="Heterozygous SNVs", xlab="PC1", pch=19);
     plot(WGSVariantSummaryMatrix$PC1,WGSVariantSummaryMatrix$NumHyterrozyogotes_INDEL, main=NULL,cex.lab=0.8,ylab="Heterozygous INDELs", xlab="PC1", pch=19);
     
       plot(WGSVariantSummaryMatrix$PC2,WGSVariantSummaryMatrix$NumbofHyterrozyogotes_SNV,main=NULL,cex.lab=0.8,ylab="Heterozygous SNVs", xlab="PC2", pch=19);
       plot(WGSVariantSummaryMatrix$PC2,WGSVariantSummaryMatrix$NumHyterrozyogotes_INDEL, main=NULL,cex.lab=0.8,ylab="Heterozygous INDELs", xlab="PC2", pch=19);

    dev.off();
