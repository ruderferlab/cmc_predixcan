#Convert imputed WGS based prediction into corresponing ZSCORE and int zscore

 library(magrittr);
 library(RNOmni);
 library(dplyr)
 library(stringr)

#Input file list
#/home/hanl3/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMCHBCC_DLPFC_GeneLevelSpearmanCorrforActualPredictedExpression/GTExV8FullGenotype_Imputed_st_GeneLevelSpearmanCorr.txt
#/home/hanl3/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMCHBCC_DLPFC_GeneLevelSpearmanCorrforActualPredictedExpression/GTExV8FullGenotype_Imputed_ut_GeneLevelSpearmanCorr.txt
#/home/hanl3/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMCHBCC_DLPFC_GeneLevelSpearmanCorrforActualPredictedExpression/GTExV8FullGenotype_Imputed_xt_GeneLevelSpearmanCorr.txt
#/home/hanl3/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMCHBCC_DLPFC_GeneLevelSpearmanCorrforActualPredictedExpression/GTExV8Original_st_GeneLevelSpearmanCorr.txt
#/home/hanl3/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMCHBCC_DLPFC_GeneLevelSpearmanCorrforActualPredictedExpression/GTExV8Original_ut_GeneLevelSpearmanCorr.txt
#/home/hanl3/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMCHBCC_DLPFC_GeneLevelSpearmanCorrforActualPredictedExpression/GTExV8Original_xt_GeneLevelSpearmanCorr.txt
#/home/hanl3/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMCHBCC_DLPFC_GeneLevelSpearmanCorrforActualPredictedExpression/PsychEncodeFullGenotype_Imputed_INT_GeneLevelSpearmanCorr.txt
#/home/hanl3/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMCHBCC_DLPFC_GeneLevelSpearmanCorrforActualPredictedExpression/PsychEncodeFullGenotype_Imputed_Standard_GeneLevelSpearmanCorr.txt
#/home/hanl3/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMCHBCC_DLPFC_GeneLevelSpearmanCorrforActualPredictedExpression/PsychEncodeOriginal_INT_GeneLevelSpearmanCorr.txt
#/home/hanl3/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMCHBCC_DLPFC_GeneLevelSpearmanCorrforActualPredictedExpression/PsychEncodeOriginal_Standard_GeneLevelSpearmanCorr.txt


#Imputed st ut,xt psyencode,standard
#Original st ut,xt psyencode,standard


#Impute and original

 mydir="/home/hanl3/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMCHBCC_DLPFC_GeneLevelSpearmanCorrforActualPredictedExpression/";
  Outputfile=paste(mydir,"/CMC_HBCCDifferentMethodBasedCorrComparisonScatterPlot_XTSCANvsTWAS.pdf",sep="");
   pdf(Outputfile, 10, 10);
    par(mar=c(5,7,1.5,0.25), mgp=c(3,1,0), mfrow=c(1,1),oma=c(0,0,0,0));
    linewide=2;
    axixwide=0.5;
    tickwide=0.5;
   cex_size=0.62;
   leg_cex_size=0.8;
   axes=F;
   xaxs="s";
   cex_size1=0.65;

 
 DataTypes=c("FullGenotype","Original");
   for(iType in seq(2,2))
   {
    WholeDataTypes=NULL;
   MethodTypes=NULL;
   if(iType==1) 
   {
     WholeDataTypes=c("GTExV8FullGenotype_Imputed_st","GTExV8FullGenotype_Imputed_ut","GTExV8FullGenotype_Imputed_xt","PsychEncodeFullGenotype_Imputed_INT","PsychEncodeFullGenotype_Imputed_Standard");
     MethodTypes=c("Imputed_GTExV8_st","Imputed_GTExV8_ut","Imputed_GTExV8_xt","Imputed_PsychEncode_INT","Imputed_PsychEncode_Standard");
   }   
   else
   {
     WholeDataTypes=c("GTExV8Original_st","GTExV8Original_ut","GTExV8Original_xt","PsychEncodeOriginal_INT","PsychEncodeOriginal_Standard");
    MethodTypes=c("Original_GTExV8_st","Original_GTExV8_ut","Original_GTExV8_xt","Original_PsychEncode_INT","Original_PsychEncode_Standard");
   }
    for(ii in seq(3,3))#2 utmost,3, xt-scan
    {
      for(jj in seq(5,5))#[(ii+1,5)]
      {
        CorrFile1=paste(mydir,WholeDataTypes[ii],"_GeneLevelSpearmanCorr.txt",sep="");
        CorrMatrix1=read.table(CorrFile1, sep="",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE,row.names=NULL);#Obtain column names from File By check.names   
        CorrMatrix1=as.data.frame(CorrMatrix1);
        CorrFile2=paste(mydir,WholeDataTypes[jj],"_GeneLevelSpearmanCorr.txt",sep="");
        CorrMatrix2=read.table(CorrFile2, sep="",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE,row.names=NULL);#Obtain column names from File By check.names   
        CorrMatrix2=as.data.frame(CorrMatrix2);
        MergedCorr=inner_join(x=CorrMatrix1,y=CorrMatrix2,by =c("GeneID"),suffixes = c(".x",".y"));
        MergedCorr=as.data.frame(MergedCorr);
        colnames(MergedCorr)=c("GeneID","Corr1","Corr2");
        MergedCorr[,2]=as.numeric(MergedCorr[,2]);
        MergedCorr[,3]=as.numeric(MergedCorr[,3]);
         MergedCorr$Difference=MergedCorr[,3]-MergedCorr[,2];
       a=MergedCorr$Difference[MergedCorr$Difference>0.5];
        print(a); 
        MergedCorr_Dim=dim(MergedCorr);
         corr=signif(cor(MergedCorr[,2],MergedCorr[,3],use="complete.obs"),4);; 
        plot(MergedCorr$Corr1, MergedCorr$Corr2, main=paste("Number of Shared Genes",MergedCorr_Dim[1],"_Corr",corr,sep=""),xlab="XTSCAN GTExV8 WGS based Spearman Corr ",cex.main=1.4,ylab="TWAS PsychENCODE WGS based Spearman Corr", pch=19,cex.lab=1.4);#Which is used for XT_SCAN VS TWAS
     #   plot(MergedCorr$Corr1, MergedCorr$Corr2, main=paste("Number of Shared Genes",MergedCorr_Dim[1],"_Corr",corr,sep=""),xlab="UTMOST GTExV8 WGS based Spearman Corr ",cex.main=1.4,ylab="TWAS PsychENCODE WGS based Spearman Corr", pch=19,cex.lab=1.4);#which is used for UTMOST vs TWAS
        abline(a=0,b=1,lty=2,col=4);     
       }
     }
   }


b=0;
if(b)
{
    WholeDataTypes1=c("GTExV8FullGenotype_Imputed_st","GTExV8FullGenotype_Imputed_ut","GTExV8FullGenotype_Imputed_xt","PsychEncodeFullGenotype_Imputed_INT","PsychEncodeFullGenotype_Imputed_Standard");
    MethodTypes1=c("Imputed_GTExV8_st","Imputed_GTExV8_ut","Imputed_GTExV8_xt","Imputed_PsychEncode_INT","Imputed_PsychEncode_Standard");

    WholeDataTypes2=c("GTExV8Original_st","GTExV8Original_ut","GTExV8Original_xt","PsychEncodeOriginal_INT","PsychEncodeOriginal_Standard");
    MethodTypes2=c("Original_GTExV8_st","Original_GTExV8_ut","Original_GTExV8_xt","Original_PsychEncode_INT","Original_PsychEncode_Standard");
     for(ii in seq(2,2))
      {
        CorrFile1=paste(mydir,WholeDataTypes1[ii],"_GeneLevelSpearmanCorr.txt",sep="");
        CorrMatrix1=read.table(CorrFile1, sep="",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE,row.names=NULL);#Obtain column names from File By check.names   
        CorrMatrix1=as.data.frame(CorrMatrix1);
        CorrFile2=paste(mydir,WholeDataTypes2[ii],"_GeneLevelSpearmanCorr.txt",sep="");
        CorrMatrix2=read.table(CorrFile2, sep="",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE,row.names=NULL);#Obtain column names from File By check.names   
        CorrMatrix2=as.data.frame(CorrMatrix2);
        MergedCorr=inner_join(x=CorrMatrix1,y=CorrMatrix2,by =c("GeneID"),suffixes = c(".x",".y"));
        MergedCorr=as.data.frame(MergedCorr);
        colnames(MergedCorr)=c("GeneID","Corr1","Corr2");
        MergedCorr[,2]=as.numeric(MergedCorr[,2]);
        MergedCorr[,3]=as.numeric(MergedCorr[,3]);
        MergedCorr_Dim=dim(MergedCorr);
        corr=signif(cor(MergedCorr[,2],MergedCorr[,3],use="complete.obs"),4);;
       plot(MergedCorr$Corr1, MergedCorr$Corr2, main=paste("Number of Schared Gnees",MergedCorr_Dim[1],"_Corr",corr,sep=""),cex.main=1.4,ylab="UTMOST GTExV8 WGS Data based Spearman Corr", xlab="UTMOST GTExV8 Imputed Data based Spearman Corr", pch=19,cex.lab=1.4);

    #   plot(MergedCorr$Corr1, MergedCorr$Corr2, main=paste("Size",MergedCorr_Dim[1],"_Corr",corr,sep=""),xlab=MethodTypes1[ii], ylab=MethodTypes2[ii], pch=19,cex.lab=0.8);
        abline(a=0,b=1,lty=2,col=4);
      }
}
  dev.off(); 
