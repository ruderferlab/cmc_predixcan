#Convert imputed WGS based prediction into corresponing ZSCORE and int zscore

 library(magrittr);
 library(RNOmni);
 library(dplyr)
 library(stringr)


BatchList=c("SKL_10073_B01_GRM_WGS_2016-02-25","SKL_11154_B01_GRM_WGS_2016-03-17","SKL_11694_B01_GRM_WGS_2017-08-18");
ConsiseBatchNamelist=c("SKL_10073","SKL_11154","SKL_11694");
DataTypes=c("FullGenotype_Imputed","Original");
   DuplicateOutliers=c("MSSM-DNA-PFC-375","MSSM-DNA-PFC-269","CMC-HBCC-DNA-ACC-6052","CMC-HBCC-DNA-ACC-5669","CMC-HBCC-DNA-ACC-4237","CMC-HBCC-DNA-ACC-5646",
    "CMC-HBCC-DNA-ACC-5777","CMC-HBCC-DNA-ACC-5682","CMC-HBCC-DNA-ACC-4284","CMC-HBCC-DNA-ACC-4137",
    "CMC-HBCC-DNA-ACC-6009","CMC-HBCC-DNA-ACC-6056","CMC-HBCC-DNA-ACC-4029","CMC-HBCC-DNA-ACC-4200",
    "CMC-HBCC-DNA-ACC-4074","CMC-HBCC-DNA-ACC-4204","CMC-HBCC-DNA-ACC-5654","CMC-HBCC-ACC-DNA-4235");
 MissingOutlierCEUs=c("MSSM-DNA-PFC-153","Penn-DNA-PFC-033","Pitt-DNA-PFC-1324","Pitt-DNA-PFC-1524","Pitt-DNA-PFC-970","Pitt-DNA-BP-PFC-789","Pitt-DNA-BP-PFC-840","Pitt-DNA-PFC-1088","MSSM-DNA-PFC-56");

#MSSM-DNA-PFC-153	SKL_10073	0.0041	0.0100	CEU
#Penn-DNA-PFC-033	SKL_10073	0.0041	0.0101	CEU
#Pitt-DNA-PFC-1324	SKL_10073	0.0045	0.0101	CEU
#Pitt-DNA-PFC-1524	SKL_10073	0.0043	0.0101	CEU
#Pitt-DNA-PFC-970	SKL_10073	0.0040	0.0101	CEU
#Pitt-DNA-BP-PFC-789	SKL_10073	0.0044	0.0102	CEU
#Pitt-DNA-BP-PFC-840	SKL_10073	0.0041	0.0102	CEU
#Pitt-DNA-PFC-1088	SKL_10073	0.0043	0.0102	CEU
#MSSM-DNA-PFC-56	SKL_10073	0.0043	0.0103	CEU


  
  AllSampleIDInfoFile="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/WGSData_VariantSummary_InferredAncestry.txt";
  AllSampleIDMatrix=read.table(AllSampleIDInfoFile, sep="\t",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE);#Obtain column names from File By check.names  
  InferCEUSampleSubset=AllSampleIDMatrix[which(AllSampleIDMatrix$InferredAncestry=="CEU"),]
  CEUWGSID=InferCEUSampleSubset$WGSID;
  print(length(CEUWGSID));
  print(CEUWGSID[1:5]);
  Tissues=c("DLPFC_BA9","ACC_BA24");
  PredictionMethods=c("st","ut","xt");
#SKL_10073GTEXV8_PredictedGeneExpressionst_ACC_BA24_FullGenotype_Imputed.txt

  for(iBatch in seq(1,3))#3
  {
   for(iType in seq(1,2))#2
   {
    for(iMethod in seq(1,3))
    {
    for(iTissue in seq(1,2))
    {
  
      PredictedExpressionFile=paste("/home/hanl3/cmc/scratch/Predicted_ActualGeneExpressionMerged/GTEXV8_Prediction/PredictedValues/",ConsiseBatchNamelist[iBatch],"GTEXV8_PredictedGeneExpression",PredictionMethods[iMethod],"_",Tissues[iTissue],"_",DataTypes[iType],".txt.gz",sep="");
      con<-file(PredictedExpressionFile,'rt');
       PredictedGeneExpression=read.table(con, sep="",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE,row.names=NULL);#Obtain column names from File By check.names   
      print(PredictedExpressionFile); 
      PredictedGeneExpression =as.data.frame(PredictedGeneExpression);
      GeneIDColNames=colnames(PredictedGeneExpression);
      GeneIDColNames=str_replace_all(GeneIDColNames, "[.]", "-");#replace "." into "-"
      colnames(PredictedGeneExpression)=GeneIDColNames; 
      GeneIDColNames=as.vector(GeneIDColNames);
      print(GeneIDColNames[c(1:10)]);
      OverlapID=intersect(GeneIDColNames,DuplicateOutliers);
      print(length(OverlapID));
      print(dim(PredictedGeneExpression));
      GeneIDRowMatrix=PredictedGeneExpression[,c(1:8)];#Obtain Gene Name column information
      GeneIDRowMatrixColNames=colnames(GeneIDRowMatrix);
      print(GeneIDRowMatrixColNames);
      PredictedGeneExpression=PredictedGeneExpression[,(names(PredictedGeneExpression)%in%CEUWGSID)]; 
      print(dim(PredictedGeneExpression));
      NormalPredictedGeneExpression=PredictedGeneExpression[,!(names(PredictedGeneExpression)%in%DuplicateOutliers)]; 
      print(dim(NormalPredictedGeneExpression));
      NormalPredictedGeneExpression=NormalPredictedGeneExpression[,!(names(NormalPredictedGeneExpression)%in%MissingOutlierCEUs)];
      print(dim(NormalPredictedGeneExpression));
      GeneExpression=NormalPredictedGeneExpression;
      GeneExpressionNames=colnames(NormalPredictedGeneExpression);

      GeneExpressionDim=dim(GeneExpression);
     GeneExpression[,1:GeneExpressionDim[2]] <- sapply(GeneExpression[,1:GeneExpressionDim[2]], as.numeric);

      GeneExpressionINT= t(apply(GeneExpression, 1, rankNorm));# The result from the apply() had to be transposed using t() to get the same layout as the input matrix A  
     GeneExpressionZScore=t(apply(GeneExpression, 1,scale));
     GeneExpressionINTMerged=as.data.frame(cbind(GeneIDRowMatrix, GeneExpressionINT));
      colnames(GeneExpressionINTMerged)=c(GeneIDRowMatrixColNames,GeneExpressionNames);;
      print(dim(GeneExpressionINTMerged));#ConsiseBatchNamelist[iBatch],"GTEXV8_PredictedGeneExpression",PredictionMethods[iMethod],"_",Tissues[iTissue],"_",DataTypes[iType],".txt",sep=""); 
     PredictedExpressionFile=paste("/home/hanl3/cmc/scratch/Predicted_ActualGeneExpressionMerged/GTEXV8_Prediction/PredictedValues_Zscore/GTExV8Based",ConsiseBatchNamelist[iBatch],PredictionMethods[iMethod],"_",Tissues[iTissue],"_",DataTypes[iType],"_GenePredictedExCEUBasedINTZScore.txt",sep="");   
      write.table(GeneExpressionINTMerged, file=PredictedExpressionFile, append = FALSE, quote =FALSE, sep = "\t", eol = "\n",col.names=TRUE,row.names=FALSE);
       GeneExpressionIZScoreMerged=as.data.frame(cbind(GeneIDRowMatrix,GeneExpressionZScore)); 
       colnames(GeneExpressionIZScoreMerged)=c(GeneIDRowMatrixColNames,GeneExpressionNames); 
      PredictedExpressionFile=paste("/home/hanl3/cmc/scratch/Predicted_ActualGeneExpressionMerged/GTEXV8_Prediction/PredictedValues_Zscore/GTExV8Based",ConsiseBatchNamelist[iBatch],PredictionMethods[iMethod],"_",Tissues[iTissue],"_",DataTypes[iType],"_GenePredictedExCEUBasedREGZScore.txt",sep="");     
      write.table(GeneExpressionIZScoreMerged, file=PredictedExpressionFile, append = FALSE, quote =FALSE, sep = "\t", eol = "\n",col.names=TRUE,row.names=FALSE);
      close(con);
     }
   } 
  }
}

