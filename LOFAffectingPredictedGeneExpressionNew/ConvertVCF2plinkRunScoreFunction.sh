#!/bin/bash
#This is for original WGS data

 InputFileDir="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs";


declare -a batches=("SKL10073" "SKL11154" "SKL11694")
declare -a types=("FullGenotype" "Haplotype1" "Haplotype2")
declare -a Index=(1 2 3)
#This is for imputation WGS data
#/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/SKL10073ShareV8MultiplyModel.CommonSNVs_ImputedWGS_FullGenotype.vcf
for (( i =0; i <3; i++ ))
do 
   for entry in "${batches[@]}"
   do
     echo "$InputFileDir/$entry"
     sFullInputPath="$InputFileDir/${entry}ShareV8MultiplyModel.CommonSNVs_ImputedWGS_${types[$i]}_${Index[$i]}";
     sFullInputPath1="${sFullInputPath}.vcf"
      gzip "$sFullInputPath1"
      sFullInputPath2="${sFullInputPath}.vcf.gz";
      plink --vcf "$sFullInputPath2"   --set-missing-var-ids @:#  --make-bed --out "$sFullInputPath"
    done 
done     
