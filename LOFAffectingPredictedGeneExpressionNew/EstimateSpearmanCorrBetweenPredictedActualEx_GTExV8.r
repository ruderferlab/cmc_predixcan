
 library(ggplot2)
 library(gridExtra)
 library(grid)
 library(lattice)
 library(ggplot2);
 library(devtools);
 library(magrittr);

#EstimateSpearmanCorrBetweenPredictedActualEx_GTExV8.r
#1581 /home/hanl3/cmc/scratch/Predicted_ActualGeneExpressionMerged/CMC_HBCCActualPredictedMergedGeneExpression/GTExV8FullGenotype_Imputed_MergeddActualPredictedExpression.txt
#1581 /home/hanl3/cmc/scratch/Predicted_ActualGeneExpressionMerged/CMC_HBCCActualPredictedMergedGeneExpression/GTExV8Original_MergeddActualPredictedExpression.txt
#1186 /home/hanl3/cmc/scratch/Predicted_ActualGeneExpressionMerged/CMC_HBCCActualPredictedMergedGeneExpression/PsychEncodeFullGenotype_Imputed_MergeddActualPredictedExpression.txt,actual,INT,Standard
#1186 /home/hanl3/cmc/scratch/Predicted_ActualGeneExpressionMerged/CMC_HBCCActualPredictedMergedGeneExpression/PsychEncodeOriginal_MergeddActualPredictedExpression.txt,actual,INT,Standard


#  635 WGSHBCC_DLPFC_ActualPredicted_MergedExpression_Percentage0.txt
  mydir="/home/hanl3/cmc/scratch/Predicted_ActualGeneExpressionMerged/CMC_HBCCActualPredictedMergedGeneExpression";
  myDistDir="/home/hanl3/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMCHBCC_DLPFC_GeneLevelSpearmanCorrforActualPredictedExpression";
 
  Outputfile=paste(myDistDir,"/GeneLevelActual_GTExV8PsychEncodePredictedGeneExpressionSpearmanCorrWGSHistogram.pdf",sep="");
  pdf(file=Outputfile,pointsize=16,width=16, height=24);
  par(mar=c(4,4,1.0,0.25),mfrow=c(3,2),oma=c(0,0,0,0));

  CEUSampleIDInfoFile="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/CMC_CAUSampleInformation.txt";
  CEUSampleIDMatrix=read.table(CEUSampleIDInfoFile, sep="\t",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE);#Obtain column names from File By check.names  
  CEUWGSID=CEUSampleIDMatrix[,2];

  PsychEncode_GTExV8Types=c("GTExV8FullGenotype_Imputed","GTExV8Original","PsychEncodeFullGenotype_Imputed","PsychEncodeOriginal");
  EachPartSize=395;
  SplitPartList=c(3,3,2,2); 

  myPlot<-list();
   iPlotNum=1;
  for(iType in seq(1,4))
   {
       iExpressionFile=paste(mydir,"/",PsychEncode_GTExV8Types[iType],"_MergeddActualPredictedExpression.txt",sep="");
       GeneExpression=read.table(iExpressionFile, sep="\t",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE);#Obtain column names from File By check.names   
       GeneIDColNames=colnames(GeneExpression);
       GeneID=GeneIDColNames[-1:-4];#remove 1-4th element;
       print(dim(GeneExpression));
     # GeneExpression=GeneExpression[which(GeneExpression$IndID %in% CEUWGSID),]; 
       print(iExpressionFile);
       print(dim(GeneExpression));   
       GeneExpressionDim=dim(GeneExpression);
     ActualGeneExpression=GeneExpression[1:EachPartSize,]; #select Actual and predicted gene expression
     ActualGeneExpression[,5:GeneExpressionDim[2]] <- sapply(ActualGeneExpression[,5:GeneExpressionDim[2]], as.numeric);
   
     ActualGeneExpressionValueDim=dim(ActualGeneExpression);
     print(ActualGeneExpressionValueDim);
     if(iType==1 | iType==2)
      {
       PredictedMethods=c("st","ut","xt");
      }
     else
      {
        PredictedMethods=c("Standard","INT");
      }
   
     for(iPart in seq(1,SplitPartList[iType]))
     {     
         iPartAdd1=iPart+1;
         PredictedGeneExpression=GeneExpression[c((1+iPart*EachPartSize):(iPartAdd1*EachPartSize)),];
         PredictedGeneExpression[,5:GeneExpressionDim[2]] <- sapply(PredictedGeneExpression[,5:GeneExpressionDim[2]], as.numeric);
         print(dim(PredictedGeneExpression)); 
         EachBatchGeneBasedSpearmanCorr=rep(NA,GeneExpressionDim[2]-4);
         for(jj in seq(5,GeneExpressionDim[2]))
         {
           if(length(unique(ActualGeneExpression[,jj]))!=1 & length(unique(PredictedGeneExpression[,jj]))!=1  & sd(ActualGeneExpression[,jj],na.rm =TRUE)!=0  & sd(PredictedGeneExpression[,jj],na.rm =TRUE)!=0)
           {
            EachBatchGeneBasedSpearmanCorr[jj-4]=cor(ActualGeneExpression[,jj],PredictedGeneExpression[,jj],"pairwise.complete.obs",method="spearman");
           }
         }
        EachBatchGeneBasedSpearmanCorr_NA=EachBatchGeneBasedSpearmanCorr;
         EachBatchGeneBasedSpearmanCorr <- EachBatchGeneBasedSpearmanCorr[!is.na(EachBatchGeneBasedSpearmanCorr)];
         EachBatchGeneBasedSpearmanCorr_Larger01=length(EachBatchGeneBasedSpearmanCorr[abs(EachBatchGeneBasedSpearmanCorr)>=0.1]);
        print(EachBatchGeneBasedSpearmanCorr_Larger01);
         mean1=round(mean(EachBatchGeneBasedSpearmanCorr),3);
        median1=round(median(EachBatchGeneBasedSpearmanCorr),3);
        sd1=round(sd(EachBatchGeneBasedSpearmanCorr),3);
        size=length(EachBatchGeneBasedSpearmanCorr);
        EachBatchGeneBasedSpearmanCorr=as.data.frame(EachBatchGeneBasedSpearmanCorr);
        colnames(EachBatchGeneBasedSpearmanCorr)=c("SpearmanCorr");
        myPlot[[iPlotNum]]<-ggplot(data=EachBatchGeneBasedSpearmanCorr,aes(SpearmanCorr))+ geom_histogram(binwidth=0.01, colour="black", fill="white") + ggtitle(paste(PsychEncode_GTExV8Types[iType],"_",PredictedMethods[iPart],"_Mean",mean1,"_Median",median1,"_sd",sd1,"_size",size,sep=""))+ xlab("Spearman Corr")+ coord_cartesian(xlim=c(-1,1)) + scale_x_continuous(breaks=seq(-1,1,0.1))+theme(plot.title = element_text(size = 12));
        iPlotNum=iPlotNum+1;
#Construct a new data frame      
      EachBatchGeneBasedSpearmanCorr=data.frame("GeneID"=GeneID,"SpearmanCorr"=EachBatchGeneBasedSpearmanCorr_NA);#Construct a data frame based on two vectors
      EachBatchGeneBasedSpearmanCorr=EachBatchGeneBasedSpearmanCorr[which(EachBatchGeneBasedSpearmanCorr$SpearmanCorr!="NA"),];  
     
       EachBatchGeneBasedSpearmanCorr=EachBatchGeneBasedSpearmanCorr[complete.cases(EachBatchGeneBasedSpearmanCorr),];#Remove rows with all or some NAs (missing values) in data.frame
       print(dim(EachBatchGeneBasedSpearmanCorr));
       CorrelationFile=paste(myDistDir,"/",PsychEncode_GTExV8Types[iType],"_",PredictedMethods[iPart],"_GeneLevelSpearmanCorr.txt",sep="");
       write.table(EachBatchGeneBasedSpearmanCorr, file=CorrelationFile, append = FALSE, quote =FALSE, sep = "\t", eol = "\n",col.names=TRUE,row.names=FALSE);
     }#end for iPart
   }#end for iType
    pushViewport(viewport(layout=grid.layout(12,2)))#defined how to arrange the plots, there are 30 
   vplayout<-function(x,y){viewport(layout.pos.row=x,layout.pos.col=y)}
   iNum=1;
   for(i in myPlot)
   {  
     print(iNum);
    if(iNum %% 2 == 1)
    {   
        print(myPlot[[iNum]],vp=vplayout((iNum-1)%/%2+1,1));
    }
    else
    {  
       print(myPlot[[iNum]],vp=vplayout((iNum-1)%/%2+1,2));
    }
   iNum=iNum+1;
  }

dev.off();

