#!/bin/bash
#SBATCH --mail-user=lide.han@vumc.org
#SBATCH --mail-type=FAIL
#SBATCH --ntasks=1
#SBATCH --time=3:00:00
#SBATCH --mem=6G
#SBATCH --array=1-482
#SBATCH --output=tmp_array_job_slurm_%A_%a.out
#SBATCH --account=vgi

echo "SLURM_JOBID: " $SLURM_JOBID
echo "SLURM_ARRAY_TASK_ID: " $SLURM_ARRAY_TASK_ID
echo "SLURM_ARRAY_JOB_ID: " $SLURM_ARRAY_JOB_ID

# prog
awk 'int((NR-1)/2000)+1=='$SLURM_ARRAY_TASK_ID /gpfs23/data/ruderferlab/projects/cmc/scripts/LOFAffectingPredictedGeneExpressionNew/CMC_HBCC_GTEXV8PredictGeneExpressionCommandline.sh|sh
#962004:482
