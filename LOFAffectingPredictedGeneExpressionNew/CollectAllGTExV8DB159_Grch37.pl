#!/usr/bin/perl -w
 use strict;

 use lib "/home/hanl3/perl5/module/lib/site_perl/5.14.2";
 use Text::CSV_PP;
  use Tie::File; 
 use POSIX qw(WNOHANG);
 use IO::Uncompress::Gunzip qw($GunzipError);
 use warnings;
use Scalar::Util qw(looks_like_number);

# UTMOST, XT_Scan,PrediXcan two brain tissues' preciction model which based on GRch38 assembly
# /data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/gtex_v8_st_Brain_Anterior_cingulate_cortex_BA24.txt
#       gtex_v8_st_Brain_Frontal_Cortex_BA9.txt  gtex_v8_ut_Brain_Anterior_cingulate_cortex_BA24.txt
#       gtex_v8_ut_Brain_Frontal_Cortex_BA9.txt  gtex_v8_xt_Brain_Anterior_cingulate_cortex_BA24.txt  gtex_v8_xt_Brain_Frontal_Cortex_BA9.txt

  my @PredictionMethods=("st","ut","xt");
  my @BrainsTissues=("Brain_Frontal_Cortex_BA9","Brain_Anterior_cingulate_cortex_BA24");
  my @BrainsTissuesNames=("DLPFC_BA9","ACC_BA24");

 my %Chr_GeneID=();#Chr_ geneid
 my %AllUsedSNVsInfo=(); 
  for(my $iM=0;$iM<@PredictionMethods;$iM++)
 {
   for(my $iT=0;$iT<@BrainsTissues;$iT++)
   {
      my $iLineNumber=0;
      my $PredictionFile="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/gtex_v8_$PredictionMethods[$iM]_$BrainsTissues[$iT].txt";
      open(INPUT, $PredictionFile) || die "can’t open $PredictionFile";
      while (<INPUT>)
      { 
        chomp;
        if($iLineNumber==0)
        {
          $iLineNumber++;
          next;
        }
        my @cols=split(/\s+/,$_);
       $AllUsedSNVsInfo{$cols[0]}="$cols[3]\t$cols[4]"; #ref_allele[col3]eff_allele[col4]     
       # last if($iLineNumber>1000);
       $iLineNumber++;
     }
     close INPUT;
  }
}

  my %rsid_position=();
   my %rsid_FullpositionChr=();
  my %rsid_FullpositionStart=();
  my %rsid_FullpositionEnd=();
  my %Chr_rsid=();
  my $NumberofSNVs=keys%AllUsedSNVsInfo;
  print "NumberofSNVs:\t$NumberofSNVs\n";
  my $iNumberofSNVs_Model=0;#liftover by RSID
   my $NumberofLines=0;

   my $SNPPosFile_db150="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/snp150_hg19.txt.gz";
  open (INPUT, "gunzip -c $SNPPosFile_db150|") or die "gunzip  $SNPPosFile_db150: $!";;
  while (<INPUT>)
  {
      my @cols=split(/\s+/,$_);
      $cols[0]=~ s/chr//g;
       next if(!looks_like_number($cols[0]));#judge if the chromosome is a number.
       if(defined($AllUsedSNVsInfo{$cols[1]}))
      {
       $rsid_position{$cols[1]}="$cols[0]:$cols[2]";#chr[cols[0]] position[cols[1]], $cols[3]->rsid
      }
   }
   close INPUT;
   $NumberofSNVs=keys%rsid_position;
   print "NumberofSNVswithRSID:\t$NumberofSNVs\n";

  my $SNPPosFile="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/db150_37_CommonSNVs.ucsc_subset.txt.gz";
  open (INPUT, "gunzip -c $SNPPosFile|") or die "gunzip  $SNPPosFile: $!";;
  while (<INPUT>)
  {
      my @cols=split(/\s+/,$_);
       if($NumberofLines==0)
        {
          $NumberofLines++;
          next;
        }

       $cols[0]=~ s/chr//g;
       next if(!looks_like_number($cols[0]));#judge if the chromosome is a number.
       if(defined($AllUsedSNVsInfo{$cols[3]}))
      {
      $rsid_FullpositionChr{$cols[3]}="$cols[0]";
       $rsid_FullpositionStart{$cols[3]}="$cols[1]";#chr[cols[0]] position[cols[1]], $cols[3]->rsid
       $rsid_FullpositionEnd{$cols[3]}="$cols[2]";

       $iNumberofSNVs_Model++;
       if(!defined($Chr_rsid{$cols[0]}))
       {
        $Chr_rsid{$cols[0]}="$cols[3]";
       }
       else
       {
        $Chr_rsid{$cols[0]}.="\t$cols[3]";
       }
      }
   }
   close INPUT;
   $NumberofSNVs=keys%rsid_FullpositionStart;
   print "NumberofSNVswithRSID:\t$NumberofSNVs\n";   
  

   my $AllSNVsInfoFile="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8AllSNVsInfoforPrediction.txt";
   open OUT, ">$AllSNVsInfoFile" or die "Can't open Output file:$AllSNVsInfoFile!";  
   print OUT "RSID\tchrom\tStart\tEnd\tReference\tEffect\tChrPos_h37\n";
   foreach my $chr (sort {$a <=> $b} keys %Chr_rsid)
   {
      my @AllSNVrsid=split(/\t/,$Chr_rsid{$chr});
      for(my $iID=0;$iID<@AllSNVrsid;$iID++)
      {
        print OUT "$AllSNVrsid[$iID]\t$rsid_FullpositionChr{$AllSNVrsid[$iID]}\t$rsid_FullpositionStart{$AllSNVrsid[$iID]}\t$rsid_FullpositionEnd{$AllSNVrsid[$iID]}\t$AllUsedSNVsInfo{$AllSNVrsid[$iID]}\t$rsid_position{$AllSNVrsid[$iID]}\n";
      }
    }
   close OUT;


#st_ACC_BA24  st_DLPFC_BA9  ut_ACC_BA24  ut_DLPFC_BA9  xt_ACC_BA24  xt_DLPFC_BA9
  for(my $iM=0;$iM<@PredictionMethods;$iM++)
  {
   for(my $iT=0;$iT<@BrainsTissues;$iT++)
   {
      my $iLineNumber=0;
     my %Gene_weight=();
      my $PredictionFile="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/gtex_v8_$PredictionMethods[$iM]_$BrainsTissues[$iT].txt";
      open(INPUT, $PredictionFile) || die "can’t open $PredictionFile";
#rsid    gene    weight  ref_allele      eff_allele
#rs10800398      ENSG00000000457 0.0285292171757506      T       C
#Outputformat
#:4:89248461      T       C        0.01367
      while (<INPUT>)
      {       
         chomp;  
         if($iLineNumber==0)
         {
           $iLineNumber++;
           next;
         }
        my @cols=split(/\s+/,$_);
        if(!defined($Gene_weight{$cols[1]}))
        {
          if(defined($rsid_position{$cols[0]}))
          {
           $Gene_weight{$cols[1]}="$rsid_position{$cols[0]}\t$cols[3]\t$cols[4]\t$cols[2]";#position[col0,refAllele[cols3,EffectAlllele[col4],weight cols2
          }
        }
        else
        {   
          if(defined($rsid_position{$cols[0]}))
          {
           $Gene_weight{$cols[1]}.="\t$rsid_position{$cols[0]}\t$cols[3]\t$cols[4]\t$cols[2]";
          }
        }
       # last if($iLineNumber>100);
         $iLineNumber++;
      }
      close INPUT;
      foreach my $gene ( keys %Gene_weight)
       {
           my @WeightList=split(/\t/,$Gene_weight{$gene});
           my @Chr_Pos=split(/:/,$WeightList[0]);          
           if(!defined($Chr_GeneID{$Chr_Pos[0]}))
           {
             $Chr_GeneID{$Chr_Pos[0]}="$gene";
            }
            else
            {
              $Chr_GeneID{$Chr_Pos[0]}.="\t$gene";
             }           
            my $NumberofSNVs=scalar(@WeightList)/4;
        my %ChrPos=();
          my $ScoreFile="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTExV8_Weight/$PredictionMethods[$iM]_$BrainsTissuesNames[$iT]/$gene.wgt.score";#revise it 0919/2019
           open OUT, ">$ScoreFile" or die "Can't open Output file:$ScoreFile!";

           my %EachPos_Count=();
           my $iDuplicate=1;
           my @AllIndex=();
           for(my $iSNV=0;$iSNV<$NumberofSNVs;$iSNV++)
           {
             my $iStartIndex=$iSNV*4;
             if(!defined($EachPos_Count{$WeightList[$iStartIndex]}))
             {
              push @AllIndex,$WeightList[$iStartIndex];
              $EachPos_Count{$WeightList[$iStartIndex]}="$WeightList[$iStartIndex]\t$WeightList[$iStartIndex+1]\t$WeightList[$iStartIndex+2]\t$WeightList[$iStartIndex+3]";
             }
             else
              {
                $EachPos_Count{$WeightList[$iStartIndex]}.="\t$WeightList[$iStartIndex]\t$WeightList[$iStartIndex+1]\t$WeightList[$iStartIndex+2]\t$WeightList[$iStartIndex+3]"; 
                $iDuplicate++;
              }
           }           
              
          if($iDuplicate==1)
           {
             for(my $iSNV=0;$iSNV<$NumberofSNVs;$iSNV++)
             {
              my $iStartIndex=$iSNV*4;
               print OUT "$WeightList[$iStartIndex]\t$WeightList[$iStartIndex+1]\t$WeightList[$iStartIndex+2]\t$WeightList[$iStartIndex+3]\n";
             }
           }
           else
           {
              for(my $iSNV=0;$iSNV<@AllIndex;$iSNV++)
              {
                my @EachSNVInfo=split(/\t/,$EachPos_Count{$AllIndex[$iSNV]});
                if(scalar(@EachSNVInfo)==4)
                {
                 print OUT "$EachSNVInfo[0]\t$EachSNVInfo[1]\t$EachSNVInfo[2]\t$EachSNVInfo[3]\n";
                }
                else
                {
                 my $iNumberofSNVs=scalar(@EachSNVInfo)/4;
                 my $fSum=0;
                  for(my $ii=0;$ii<$iNumberofSNVs;$ii++)
                  {
                   $fSum+=$EachSNVInfo[$ii*4+3];
                  }
                  print OUT "$EachSNVInfo[0]\t$EachSNVInfo[1]\t$EachSNVInfo[2]\t$fSum\n";
                }
             }
           }
           close OUT;

        }
     }
 } 

   my $AllGeneChrInfoFile="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8AllGenesChrInfoforPrediction.txt";
   open OUT, ">$AllGeneChrInfoFile" or die "Can't open Output file:$AllGeneChrInfoFile!";
   foreach my $chr (sort {$a<=>$b}  keys %Chr_GeneID)
   {
        print OUT "$chr\t$Chr_GeneID{$chr}\n";
    }
   close OUT;

