#!/usr/bin/perl -w
 use strict;
  use 5.012;
 use lib "/home/hanl3/perl5/module/lib/site_perl/5.14.2";
 use Text::CSV_PP;
  use Tie::File; 
 use POSIX qw(WNOHANG);
 use IO::Uncompress::Gunzip qw($GunzipError);
 use warnings;
use Scalar::Util qw(looks_like_number);

  my @WGSBatches=("SKL10073","SKL11154","SKL11694");
   my @WGSBatchNames=("SKL_10073","SKL_11154","SKL_11694");
 
 my %Chr_GeneID=();#Chr_ geneid
 my %AllUsedSNVsInfo=(); 

my $iSpecificBatch=$ARGV[0];#0,1,2
my $iSpecificGType=$ARGV[1];#0,1,2

#Obtain Gene list
#/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTExV8_Weight/*_*/$gene.wgt.score

   my @GenoTypes=("FullGenotype","Haplotype1","Haplotype2");
 

  for(my $iBatch=$iSpecificBatch;$iBatch<$iSpecificBatch+1;$iBatch++)
   {
     for(my $iType=$iSpecificGType;$iType<$iSpecificGType+1;$iType++)
     {  
           my $OutZFile="/home/hanl3/cmc/scratch/Predicted_ActualGeneExpressionMerged/PsychEncodePredictionEx/PredictedValues/$WGSBatchNames[$iBatch]PsychEncode_PredictedGeneExpression_$GenoTypes[$iType]_Imputed1.txt";
            open OUTZ, ">$OutZFile" or die "Can't open Output file:$OutZFile!";   
            my $iNumberofFile=0;
            my %Chr_GeneID=();
            my %GeneID_NumberofLociInModel=();
            my $mydir="/data/ruderferlab/resources/psychencode/PEC_TWAS_weights_Score";
            opendir(DIR, $mydir) || die "Can't open $mydir: $!";
            while (my $weightFile=readdir (DIR)) {
            next if ($weightFile =~ m/^\./);
            my $geneID=$weightFile;
            $geneID =~ s/\.wgt\.score//ig;
            my $scoreWeightFile="$mydir/$weightFile";
           # print "$mydir/$weightFile\n";
           my $iLineNumber=0;
            my $ScoreFile="$mydir/$weightFile";
            open(INPUT,$ScoreFile) || die "can’t open $ScoreFile";
            while (<INPUT>)
            {
              chomp;
              if($iLineNumber==0)
              {
               my @cols=split(/\t/,$_,2);
               my @ChrPos=split(/:/,$cols[0]);
               if(!defined($Chr_GeneID{$ChrPos[0]}))
               {
                 $Chr_GeneID{$ChrPos[0]}=$geneID;
               } 
              else
               {
                 $Chr_GeneID{$ChrPos[0]}.="\t$geneID";
               }
             }
              $iLineNumber++; 
            }         
           close INPUT;
           $GeneID_NumberofLociInModel{$geneID}=$iLineNumber;
           $iNumberofFile++;
          # last if($iNumberofFile>10);
         }
         closedir (DIR);
            my %GeneID_AllInfo=();
            my $NumberofGenes=keys %GeneID_NumberofLociInModel;
            print "Number of Genes:$NumberofGenes\n";
            $iNumberofFile=0;         
            my @AllIndIDList=();
             $mydir="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/CMC_HBCC_PsychEncode_Profile";           
            opendir(DIR, $mydir) || die "Can't open $mydir: $!";
            while (my $profileFile=readdir (DIR)) {
            next if ($profileFile =~ m/^\./);
            next if($profileFile !~ m/profile$/);            
           #/$PredictionMethods[$iM]_$BrainsTissuesNames[$iT]/$WGSBatches[$iBatch]_$GenoTypes[$iType]_$geneID
           if ($profileFile =~ m/profile$/ && $profileFile =~ m/^$WGSBatches[$iBatch]\_$GenoTypes[$iType]\_/)
           {
               my $GeneIDFull=$profileFile;
               $GeneIDFull =~ s/.profile//ig;#SKL_11694_OriginalWGS_ENSG00000283538
               $GeneIDFull=~ s/^$WGSBatches[$iBatch]\_$GenoTypes[$iType]\_//ig;              
             next if(!defined($GeneID_NumberofLociInModel{$GeneIDFull}));
             my $iLineNumber=0; 
             my $iEffectiveNumber=0;
             my $iDependentLociNumber=-1;
             my @OneGeneProfile=();
             my $profileFileName="$mydir/$profileFile";
            #  print "$profileFileName\n";
             open(INPUT,$profileFileName) || die "can’t open $profileFileName";
             while (<INPUT>)
             {
               chomp;
               if($iLineNumber==0)
               {
                 $iLineNumber++;
                 next;
               }
             $_ =~ s/^\s+|\s+$//g;#Remove space around the string      
           my @cols=split(/\s+/,$_);
  
             my $iLength=scalar(@cols); 
             if($iEffectiveNumber==0)
               {
               $iDependentLociNumber=$cols[3]/2;
               }
              if($iNumberofFile==0)
              {
               push  @AllIndIDList,$cols[1];
               push @OneGeneProfile,$cols[5];
              }
              else
              {
                 push @OneGeneProfile,$cols[5];
               if(!defined($AllIndIDList[$iEffectiveNumber])) 
               {
                 print  OUTZ1  "Abnormal $GeneIDFull,@AllIndIDList\n";
                 print  OUTZ1  "Abnormal $iEffectiveNumber\n";
              #  last;
                }
   
               if($AllIndIDList[$iEffectiveNumber] ne $cols[1] )
                 { 
                  print "Ind don't match: $iNumberofFile, $AllIndIDList[$iEffectiveNumber],$cols[1]\n";
                 } 
               }
               my $NumberofColumns=scalar(@cols);
               $iEffectiveNumber++;
             }
            close INPUT;     
              my $OneGeneProfileStr=join("\t",@OneGeneProfile);
              $GeneID_AllInfo{$GeneIDFull}="$iDependentLociNumber\t$OneGeneProfileStr";
          #    print "$GeneIDFull\t$GeneID_AllInfo{$GeneIDFull}\n";
              $iNumberofFile++;
           #  last if($iNumberofFile>10); 
           }       
          }
        closedir (DIR);

       print OUTZ "Chr\tWGSBatch\tPredictionMethods\tBrainsTissues\tSource\tGeneID\tNumberofLociInWeightModel\tNumberofLociInActualModel";
       for(my $iInd=0;$iInd<@AllIndIDList;$iInd++)
       {
         print OUTZ "\t$AllIndIDList[$iInd]";
       }  
       print OUTZ "\n";
       for(my $iChr=1;$iChr<=22;$iChr++)
       {
         if(defined($Chr_GeneID{$iChr}))
         {
            my @GeneID=split(/\t/,$Chr_GeneID{$iChr});
            for(my $iG=0;$iG<@GeneID;$iG++)
            {
              if(defined($GeneID_NumberofLociInModel{$GeneID[$iG]}) && defined($GeneID_AllInfo{$GeneID[$iG]}))
              { 
                print OUTZ "$iChr\t$WGSBatches[$iBatch]\tNA\tNA\tImputed$GenoTypes[$iType]\t$GeneID[$iG]\t$GeneID_NumberofLociInModel{$GeneID[$iG]}\t$GeneID_AllInfo{$GeneID[$iG]}\n";
              }
              else
              {
                #print  OUTZ1 "$iChr\t$WGSBatches[$iBatch]\tNA\tNA\tImputed$GenoTypes[$iType]\t$GeneID[$iG]\t$GeneID_NumberofLociInModel{$GeneID[$iG]}\n";    
              }
            }
         }
       }
      close OUTZ;
      #close OUTZ1;
  }
}
exit 0;

