#!/usr/bin/perl -w
use Scalar::Util qw(looks_like_number);
 use strict;

 use lib "/home/hanl3/perl5/module/lib/site_perl/5.14.2";
 use Text::CSV_PP;
  use Tie::File; 
 use POSIX qw(WNOHANG);
 use IO::Uncompress::Gunzip qw($GunzipError);
 use warnings;
 use Scalar::Util qw(looks_like_number);

#       perl -e 'require "snphwe.pl"; print(snphwe(@ARGV))' 57 14 50
#
#    Where the three numbers at the end are the observed counts of the three 
#    genotypes: first the heterozygote count, then one of the homozygote genotype 
#    counts, and finally the other homozygote genotype count, in that order.  
#
#    The example above, which would be for 57 Aa, 14 aa, and 50 AA, should print 
#    the resulting P-value, which in this case is 0.842279756570793, to the 
#    standard output.
#
# Note:
#    Code for the alternate P-value calculation based on p_hi/p_lo that was 
#    included in the Wigginton, et al. C and R implementations (but was 
#    disabled) has been included here, but has not been tested.  It is 
#    therefore commented out.  If you wish to make use of this code, please 
#    verify it functions as desired.


 sub snphwe {
    my $obs_hets = shift;
    my $obs_hom1 = shift;
    my $obs_hom2 = shift;

    if($obs_hom1 < 0 || $obs_hom2 < 0 || $obs_hets <0) {
	return(-1);
    }
    # rare homozygotes
       my $obs_homr;
    # common homozygotes
    my $obs_homc;
    if($obs_hom1 < $obs_hom2) {
	$obs_homr = $obs_hom1;
	$obs_homc = $obs_hom2;
    } else {
	$obs_homr = $obs_hom2;
	$obs_homc = $obs_hom1;
    }
   # number of rare allele copies
    my $rare_copies = 2 * $obs_homr + $obs_hets;
  #total number of genotypes
    my $genotypes = $obs_homr + $obs_homc + $obs_hets;

    if($genotypes <= 0) {
	return(-1);
    }
 #  Initialize probability array
    my @het_probs;
    for(my $i=0; $i<=$rare_copies; $i++) {
	$het_probs[$i] = 0.0;
    }
   #start at midpoint
   my $mid = int($rare_copies * (2 * $genotypes - $rare_copies) / (2 * $genotypes));
  # check to ensure that midpoint and rare alleles have same parity
  if(($rare_copies & 1) ^ ($mid & 1)) {
	$mid++;
    }
    
    my $curr_hets = $mid;
    my $curr_homr = ($rare_copies - $mid) / 2;
    my $curr_homc = $genotypes - $curr_hets - $curr_homr;

    $het_probs[$mid] = 1.0;
    my $sum = $het_probs[$mid];
    for($curr_hets = $mid; $curr_hets > 1; $curr_hets -= 2) {
	$het_probs[$curr_hets - 2] = $het_probs[$curr_hets] * $curr_hets * ($curr_hets - 1.0) / (4.0 * ($curr_homr + 1.0) * ($curr_homc + 1.0));
	$sum += $het_probs[$curr_hets - 2]; 
      #2 fewer heterozygotes for next iteration -> add one rare, one common homozygote
	$curr_homr++;
	$curr_homc++;
    }

    $curr_hets = $mid;
    $curr_homr = ($rare_copies - $mid) / 2;
    $curr_homc = $genotypes - $curr_hets - $curr_homr;
    for($curr_hets = $mid; $curr_hets <= $rare_copies - 2; $curr_hets += 2) {
	$het_probs[$curr_hets + 2] = $het_probs[$curr_hets] * 4.0 * $curr_homr * $curr_homc / (($curr_hets + 2.0) * ($curr_hets + 1.0));
	$sum += $het_probs[$curr_hets + 2];

     # add 2 heterozygotes for next iteration -> subtract one rare, one common homozygote
	$curr_homr--;
	$curr_homc--;
    }

    for(my $i=0; $i<=$rare_copies; $i++) {
	$het_probs[$i] /= $sum;
    }

 #  Initialise P-value 
    my $p_hwe = 0.0;

    # P-value calculation for p_hwe
    for(my $i = 0; $i <= $rare_copies; $i++) {
	if($het_probs[$i] > $het_probs[$obs_hets]) {
	    next;
	}
	$p_hwe += $het_probs[$i];
    }
    
    if($p_hwe > 1) {
	$p_hwe = 1.0;
    }

    return($p_hwe);
}


sub detectAlleleSimilarity {
   my ($WGSAllele1,$WGSAllele2,$RefAllele1,$RefAllele2,$hComplementryAlleles)= (@_);
   my %ComplementAlleles=%{$hComplementryAlleles};
   my $bSimilarity=0;
   if($WGSAllele1 eq $RefAllele1 &&  $WGSAllele2 eq $RefAllele2)
   {
     $bSimilarity=1;
   }
   elsif($WGSAllele1 eq $RefAllele2 &&  $WGSAllele2 eq $RefAllele1)
   {
     $bSimilarity=1;
   }
   elsif($WGSAllele1 eq $ComplementAlleles{$RefAllele1} &&  $WGSAllele2 eq $ComplementAlleles{$RefAllele2})
   {
     $bSimilarity=1;
   }
   elsif($WGSAllele1 eq $ComplementAlleles{$RefAllele2} &&  $WGSAllele2 eq $ComplementAlleles{$RefAllele1})
   {
     $bSimilarity=1;
   }
  return $bSimilarity;
}

#my $iSpecificBatch=$ARGV[0];
# /data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8AllSNVsInfoforPrediction.txt

#/home/hanl3/cmc/files/HRC.r1-1.GRCh37.wgs.mac5.sites.vcf.gz

#  /data/ruderferlab/projects/sweden/data/geno/sweden_merged_Eli-s234.vcf.gz
#  /data/ruderferlab/projects/sweden/data/geno/sweden_merged_Eli-sw56.vcf.gz
#  /data/ruderferlab/projects/sweden/data/geno/sweden_merged_Eli-swe1.vcf.gz

 my @ArrayBatches=("Eli-s234","Eli-sw56","Eli-swe1");
for(my $iChr=1;$iChr<=22;$iChr++)
{

 my %G1000PhaseV3_Loci=();
  my $G1000phaseV3File="/fs0/1000_Phase3/ALL.chr1-22.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.bim";
  open(INPUT, $G1000phaseV3File) || die "can’t open $G1000phaseV3File";
  while (<INPUT>)
  {
     chomp;
     if($_ =~ m/^#/)
     {
       next;
     }
     my @cols=split(/\t/,$_);
     last if($cols[0]>$iChr);
     next if($cols[0]<$iChr);
     next if(length($cols[4])!=1||length($cols[5])!=1);    
    # last if(looks_like_number($cols[0]) && $cols[0]>$iChr);
   #  last if(!looks_like_number($cols[0]));
   #  next if(looks_like_number($cols[0]) && $cols[0]<$iChr); 
     $G1000PhaseV3_Loci{"$cols[0]:$cols[3]"}="$cols[4]\t$cols[5]";
   }
   close INPUT; 
   my $NumberofLoci=keys %G1000PhaseV3_Loci;
   print "NumberofLoci:$NumberofLoci\n"; 

  my %ComplementaryAlleles=();
  $ComplementaryAlleles{"A"}="T";
 $ComplementaryAlleles{"T"}="A";
$ComplementaryAlleles{"C"}="G";
$ComplementaryAlleles{"G"}="C";

 for(my $iBatch=0;$iBatch<@ArrayBatches;$iBatch++)
 # for(my $iBatch=0;$iBatch<1;$iBatch++) 
  {
    my $iLineNumber=0;
    my $iLineNumber0=0; 
    my $iLineNumber1=0;
    my $iLineNumber3=0;
    my $iLineNumber4=0;
     my $NumberofInds=0;
      my $ChrBasedVCFFile="/data/ruderferlab/projects/cmc/scratch/sweden/MichiganImputaionInputdata/$ArrayBatches[$iBatch]/$ArrayBatches[$iBatch]_Chr$iChr.vcf";
      open OUT, ">$ChrBasedVCFFile" or die "Can't open Output file:$ChrBasedVCFFile!";
      print "$ChrBasedVCFFile\n";
      my $ArrayBasedVCFFile="/data/ruderferlab/projects/sweden/data/geno/sweden_merged_$ArrayBatches[$iBatch].vcf.gz";#snp150_37.txt.gz";
      open (INPUT, "gunzip -c $ArrayBasedVCFFile|") or die "gunzip  $ArrayBasedVCFFile: $!";;
     while (<INPUT>)
     {  
       chomp;
        if($_ =~ m/^##/)
       {
         print OUT "$_\n";
         next;
        }
        if($_ =~ m/^#/)
       { 
         my @cols=split(/\s+/,$_);
         $NumberofInds=scalar(@cols)-9;
        print "NumberofInds:\t $NumberofInds\n";  
       print OUT "$_\n";
         next;
       }
       my @cols=split(/\s+/,$_,10);
       last if($cols[0]>$iChr);
       next if($cols[0]<$iChr);
       next if(!defined($G1000PhaseV3_Loci{"$cols[0]:$cols[1]"}));
       my  @GTypes=split(/\s+/,$cols[9]);   
       my @HRCGtypes=split("\t",$G1000PhaseV3_Loci{"$cols[0]:$cols[1]"});     
       my $iAlleleSimilarity=detectAlleleSimilarity($cols[3],$cols[4],$HRCGtypes[0],$HRCGtypes[1],\%ComplementaryAlleles); 
       next if($iAlleleSimilarity==0);#Delete the Loci with different alleles from the HRC panel.
      #  print "$cols[3],$cols[4],$HRCGtypes[0],$HRCGtypes[1]\t$iAlleleSimilarity \n";
     
       my %hGTypes=();
       $hGTypes{$_}++ for @GTypes;
       for my $Genotype (sort keys %hGTypes) {
     #    print "$Genotype -> $hGTypes{$Genotype}\n";
        }
       my $fMissingRate=0;
       my $iMissGenotype=0;
      if(defined($hGTypes{"./."}))
      {
       $iMissGenotype=$hGTypes{"./."};
        $fMissingRate=$iMissGenotype/$NumberofInds;
      }
     #print "$iMissGenotype\t$NumberofInds\t$fMissingRate\n";
      next if($fMissingRate>0.1);#Remove the SNVs with missing rate>0.1
      
      # print "$iMissGenotype\t$NumberofInds\t$fMissingRate,$NumberofInds\n";
       my $iAa=0;
       my $iaa=0;
       my $iAA=0;      
       if(defined($hGTypes{"0/1"}))
       {
         $iAa=$hGTypes{"0/1"};
       }
       if(defined($hGTypes{"1/1"}))
       {       
         $iaa=$hGTypes{"1/1"};
       }   
       if(defined($hGTypes{"0/0"}))
       {       
         $iAA=$hGTypes{"0/0"};
       }         
       my $hwe_Pvalue=snphwe($iAa, $iaa, $iAA);
       #print "$iAa, $iaa, $iAA,$hwe_Pvalue\n"; 
       next if($hwe_Pvalue<1e-6);#Remove SNVs without following HWE 
      
        if($HRCGtypes[0] eq $cols[3] && $HRCGtypes[1]  eq $cols[4])
          {
             $iLineNumber0++;
          }    
          elsif($HRCGtypes[1] eq $cols[4] && $HRCGtypes[0]  eq $cols[3])
          {
            $iLineNumber1++;
             my $sTemp=$cols[3];
             $cols[3]=$cols[4];
             $cols[4]=$sTemp;
             for(my $iCol=0;$iCol<@GTypes;$iCol++)
             {       
               if($GTypes[$iCol] eq "0/0")
               {       
                 $GTypes[$iCol]="1/1";
               }       
               elsif($GTypes[$iCol] eq "1/1")
               {       
                  $GTypes[$iCol]="0/0";
               }       
             }  
          }
          elsif($HRCGtypes[0] eq $ComplementaryAlleles{$cols[3]} && $HRCGtypes[1]  eq $ComplementaryAlleles{$cols[4]})
          {
            $iLineNumber3++;
            $cols[3]=$ComplementaryAlleles{$cols[3]};
            $cols[4]=$ComplementaryAlleles{$cols[4]};
          }
          elsif($HRCGtypes[0]  eq $ComplementaryAlleles{$cols[4]} && $HRCGtypes[1]  eq $ComplementaryAlleles{$cols[3]})
          {
            $iLineNumber4++;
             my $sTemp=$ComplementaryAlleles{$cols[3]};
            $cols[3]=$ComplementaryAlleles{$cols[4]};
             $cols[4]=$sTemp;
             for(my $iCol=0;$iCol<@GTypes;$iCol++)
             {
               if($GTypes[$iCol] eq "0/0")
               {         
                 $GTypes[$iCol]="1/1";
               }         
               elsif($GTypes[$iCol] eq "1/1")
               {         
                  $GTypes[$iCol]="0/0";
               }
             }
          }
        my $GTypeString=join("\t",@GTypes);
        for(my $iCol=0;$iCol<9;$iCol++)
        {
         print OUT "$cols[$iCol]\t";
        }
       print OUT  "$GTypeString\n";
 #     last if($iLineNumber>20);
       $iLineNumber++;
      }
     print "Chr$iChr\t $iLineNumber0\t$iLineNumber1\t$iLineNumber3\t$iLineNumber4\n";
       close INPUT;
       close OUT; 
     }
 }
 
