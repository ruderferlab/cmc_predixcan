#!/usr/bin/perl -w
 use strict;

 use lib "/home/hanl3/perl5/module/lib/site_perl/5.14.2";
 use Text::CSV_PP;
  use Tie::File; 
 use POSIX qw(WNOHANG);
 use IO::Uncompress::Gunzip qw($GunzipError);
 use warnings;
 use Scalar::Util qw(looks_like_number);



my $iSpecificBatch=$ARGV[0];
# /data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8AllSNVsInfoforPrediction.txt

  my %AllUsedSNVsInfo=();
  my %AllUsedSNVsPosRange=();
    my $iLineNumber=0;#Cll candiate  llleles 
    my $V8SNVInfoFile="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8AllSNVsInfoforPrediction.txt";
    open(INPUT, $V8SNVInfoFile) || die "can’t open $V8SNVInfoFile";
    while (<INPUT>)
    {
        chomp;
        if($iLineNumber==0)
        {
          $iLineNumber++;
          next;
        }
        my @cols=split(/\s+/,$_);
#       last if($cols[1]!=1);
        $AllUsedSNVsInfo{$cols[6]}="$cols[4]\t$cols[5]"; #ref_allele[col3]eff_allele[col4]     
        $iLineNumber++;
     #   last if($iLineNumber>100);  
   }
     close INPUT;

my $NumberofAllUsedSNVs=keys %AllUsedSNVsInfo;

 my %OverlappedSNVs=();
 print "Number of all used snvs: $NumberofAllUsedSNVs\n";
  my $WGSFile="/home/hanl3/cmc/files/HRC.r1-1.GRCh37.wgs.mac5.sites.vcf.gz";

   open (INPUT, "gunzip -c $WGSFile|") or die "gunzip  $WGSFile: $!";
    while (<INPUT>)
   {
       chomp;
       my @cols=split(/\s+/,$_,3);
       if($_ =~ m/^#/) 
       {
         next;
       }
      if(defined($AllUsedSNVsInfo{"$cols[0]:$cols[1]"}))
      {
       $OverlappedSNVs{"$cols[0]:$cols[1]"}=1;
      }
    }
  close INPUT;
  my $NumberofOverlappedSNVs=keys %OverlappedSNVs;
 print "OverlappedSNVs\t$NumberofOverlappedSNVs\n";
