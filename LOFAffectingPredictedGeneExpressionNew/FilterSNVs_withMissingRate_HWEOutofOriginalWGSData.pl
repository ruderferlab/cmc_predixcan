#!/usr/bin/perl -w
 use strict;

 use lib "/home/hanl3/perl5/module/lib/site_perl/5.14.2";
 use Text::CSV_PP;
  use Tie::File; 
 use POSIX qw(WNOHANG);
 use IO::Uncompress::Gunzip qw($GunzipError);
 use warnings;
 use Scalar::Util qw(looks_like_number);

#       perl -e 'require "snphwe.pl"; print(snphwe(@ARGV))' 57 14 50
#
#    Where the three numbers at the end are the observed counts of the three 
#    genotypes: first the heterozygote count, then one of the homozygote genotype 
#    counts, and finally the other homozygote genotype count, in that order.  
#
#    The example above, which would be for 57 Aa, 14 aa, and 50 AA, should print 
#    the resulting P-value, which in this case is 0.842279756570793, to the 
#    standard output.
#
# Note:
#    Code for the alternate P-value calculation based on p_hi/p_lo that was 
#    included in the Wigginton, et al. C and R implementations (but was 
#    disabled) has been included here, but has not been tested.  It is 
#    therefore commented out.  If you wish to make use of this code, please 
#    verify it functions as desired.


 sub snphwe {
    my $obs_hets = shift;
    my $obs_hom1 = shift;
    my $obs_hom2 = shift;

    if($obs_hom1 < 0 || $obs_hom2 < 0 || $obs_hets <0) {
	return(-1);
    }
    # rare homozygotes
       my $obs_homr;
    # common homozygotes
    my $obs_homc;
    if($obs_hom1 < $obs_hom2) {
	$obs_homr = $obs_hom1;
	$obs_homc = $obs_hom2;
    } else {
	$obs_homr = $obs_hom2;
	$obs_homc = $obs_hom1;
    }
   # number of rare allele copies
    my $rare_copies = 2 * $obs_homr + $obs_hets;
  #total number of genotypes
    my $genotypes = $obs_homr + $obs_homc + $obs_hets;

    if($genotypes <= 0) {
	return(-1);
    }
 #  Initialize probability array
    my @het_probs;
    for(my $i=0; $i<=$rare_copies; $i++) {
	$het_probs[$i] = 0.0;
    }
   #start at midpoint
   my $mid = int($rare_copies * (2 * $genotypes - $rare_copies) / (2 * $genotypes));
  # check to ensure that midpoint and rare alleles have same parity
  if(($rare_copies & 1) ^ ($mid & 1)) {
	$mid++;
    }
    
    my $curr_hets = $mid;
    my $curr_homr = ($rare_copies - $mid) / 2;
    my $curr_homc = $genotypes - $curr_hets - $curr_homr;

    $het_probs[$mid] = 1.0;
    my $sum = $het_probs[$mid];
    for($curr_hets = $mid; $curr_hets > 1; $curr_hets -= 2) {
	$het_probs[$curr_hets - 2] = $het_probs[$curr_hets] * $curr_hets * ($curr_hets - 1.0) / (4.0 * ($curr_homr + 1.0) * ($curr_homc + 1.0));
	$sum += $het_probs[$curr_hets - 2]; 
      #2 fewer heterozygotes for next iteration -> add one rare, one common homozygote
	$curr_homr++;
	$curr_homc++;
    }

    $curr_hets = $mid;
    $curr_homr = ($rare_copies - $mid) / 2;
    $curr_homc = $genotypes - $curr_hets - $curr_homr;
    for($curr_hets = $mid; $curr_hets <= $rare_copies - 2; $curr_hets += 2) {
	$het_probs[$curr_hets + 2] = $het_probs[$curr_hets] * 4.0 * $curr_homr * $curr_homc / (($curr_hets + 2.0) * ($curr_hets + 1.0));
	$sum += $het_probs[$curr_hets + 2];

     # add 2 heterozygotes for next iteration -> subtract one rare, one common homozygote
	$curr_homr--;
	$curr_homc--;
    }

    for(my $i=0; $i<=$rare_copies; $i++) {
	$het_probs[$i] /= $sum;
    }

 #  Initialise P-value 
    my $p_hwe = 0.0;

    # P-value calculation for p_hwe
    for(my $i = 0; $i <= $rare_copies; $i++) {
	if($het_probs[$i] > $het_probs[$obs_hets]) {
	    next;
	}
	$p_hwe += $het_probs[$i];
    }
    
    if($p_hwe > 1) {
	$p_hwe = 1.0;
    }

    return($p_hwe);
}


my $iSpecificBatch=$ARGV[0];
# /data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8AllSNVsInfoforPrediction.txt

  my %AllUsedSNVsInfo=();
    my $iLineNumber=0;#Cll candiate  llleles 
    my $V8SNVInfoFile="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8AllSNVsInfoforPrediction.txt";
    open(INPUT, $V8SNVInfoFile) || die "can’t open $V8SNVInfoFile";
    while (<INPUT>)
    {
        chomp;
        if($iLineNumber==0)
        {
          $iLineNumber++;
          next;
        }
        my @cols=split(/\s+/,$_);
        $AllUsedSNVsInfo{$cols[6]}="$cols[4]\t$cols[5]"; #ref_allele[col3]eff_allele[col4]     
        $iLineNumber++;
     #   last if($iLineNumber>100);  
   }
     close INPUT;


my @BatchList=("SKL_10073_B01_GRM_WGS_2016-02-25","SKL_11154_B01_GRM_WGS_2016-03-17","SKL_11694_B01_GRM_WGS_2017-08-18");
#     /home/hanl3/cmc/data/wgs/vcf/SKL_10073_B01_GRM_WGS_2016-02-25.recalibrated_variants_PASS_WholeGenome.vcf.gz[328 samples]
#          /home/hanl3/cmc/data/wgs/vcf/SKL_11154_B01_GRM_WGS_2016-03-17.recalibrated_variants_PASS_WholeGenome.vcf.gz[326 samples]
#               /home/hanl3/cmc/data/wgs/vcf/SKL_11694_B01_GRM_WGS_2017-08-18.recalibrated_variants_PASS_WholeGenome.vcf.gz[119 samples]
#
my @iAllowableMissingRate=(3,3,1);

  my %ComplementaryAlleles=();
  $ComplementaryAlleles{"A"}="T";
  $ComplementaryAlleles{"T"}="A";
  $ComplementaryAlleles{"C"}="G";
  $ComplementaryAlleles{"G"}="C";
  my @BatchNameList=("SKL_10073","SKL_11154","SKL_11694");

 for(my $iBatch=$iSpecificBatch;$iBatch<$iSpecificBatch+1;$iBatch++)
 {
    my $iLineNumber=0;
    my @SelectedIndInOneBatch=();
    my @SelectedIndIndexInOneBatch=();
    my $V8DifferentModelShareWGSFile="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/$BatchNameList[$iBatch]ShareV8MultiplyModel.CommonSNVs_PassWGS.vcf";
    open OUT, ">$V8DifferentModelShareWGSFile" or die "Can't open Output file:$V8DifferentModelShareWGSFile!";
  
   my $WGSFile="/home/hanl3/cmc/data/wgs/vcf/$BatchList[$iBatch].recalibrated_variants_PASS_WholeGenome.vcf.gz";#snp150_37.txt.gz"; and   snp150.txt.gz
   open (INPUT, "gunzip -c $WGSFile|") or die "gunzip  $WGSFile: $!";;
  while (<INPUT>)
  {
     chomp;
      next if($_ =~ /^X|Y|MT/);
     if($_ =~ /^##/)
     {
      next if($_ =~ /^##GATKCommandLine/);
      next if($_ =~ /^##GVCFBlock/);
      next if($_ =~/^##contig=<ID=GL/); 
      next if($_ =~/^##contig=<ID=X/);
        print OUT"$_\n";
       next;
     }
      elsif($_ =~ /^#/)    
     {
        print OUT"$_\n";
        next;
     }
    else
     {  
           my @cols=split(/\s+/,$_,8);
           my $ChrPos="$cols[0]:$cols[1]";
           next if(!defined($AllUsedSNVsInfo{$ChrPos})); #define chr:position
           my @AltAlleles=split(/,/,$cols[4]);#for the situations such as CTTT,CTTTT,C
           my @CandidateAlleles=split(/\t/,$AllUsedSNVsInfo{$ChrPos});
           next if(scalar(@AltAlleles)>=3);          

           my $iSituation=1;
          if(scalar(@AltAlleles)==1)
          {
            if( ($cols[3] eq $CandidateAlleles[0] && $AltAlleles[0] ne $CandidateAlleles[1])||($AltAlleles[0] eq $CandidateAlleles[0] && $cols[3] ne $CandidateAlleles[1]))
             {
             print "directly remove: $cols[3]:$AltAlleles[0];_$CandidateAlleles[0].$CandidateAlleles[1]\n";
             next;
            }
          }
          if(scalar(@AltAlleles)==2)
          {
             if( ($cols[3] eq $CandidateAlleles[0] && $AltAlleles[1] eq $CandidateAlleles[1])||($AltAlleles[1] eq $CandidateAlleles[0] && $cols[3] eq $CandidateAlleles[1]))
             {
               $iSituation=2;
             }         
             my $iMatch1=0;
             my $iMatch2=0;        
             if( ($cols[3] eq $CandidateAlleles[0] && $AltAlleles[0] ne $CandidateAlleles[1])||($AltAlleles[0] eq $CandidateAlleles[0] && $cols[3] ne $CandidateAlleles[1]))
             {
              $iMatch1=1;
              }
             if( ($cols[3] eq $CandidateAlleles[0] && $AltAlleles[1] ne $CandidateAlleles[1])||($AltAlleles[1] eq $CandidateAlleles[0] && $cols[3] ne $CandidateAlleles[1]))
             {
              $iMatch2=1;
              }     
              if($iMatch1==1 && $iMatch2==1)
              { 
               print "directly remove: $cols[3]:$AltAlleles[1];_$CandidateAlleles[0].$CandidateAlleles[1]\n";
                next;
               }           
          }
           @cols=split(/\s+/,$_); 
           $cols[2]=$ChrPos;
           $cols[8]="GT"; 
           my $iMissingInd=0;
           my $iAa=0;
           my $iaa=0;
           my $iAA=0;   
           for(my $iInd=9;$iInd<@cols;$iInd++)  
           {
               $cols[$iInd]=substr($cols[$iInd],0,3);
           }
           if($iSituation==1) 
           {
             $cols[4]=$AltAlleles[0];
           }  
           if($iSituation==2)
           {
              #for(my $iInd=0;$iInd<@cols;$iInd++)
             # {
             #   print "$cols[$iInd]\t";
             #  }
             # print"\n";
              s/1\/1/.\/./ for @cols; #convert 1|0 to 0|1
              s/0\/1/.\/./ for @cols; #convert 1|0 to 0|1
              s/0\/2/0\/1/ for @cols; #convert 1|0 to 0|1
              s/2\/2/1\/1/ for @cols; #convert 1|0 to 0|1
              $cols[4]=$AltAlleles[1];
            #  for(my $iInd=0;$iInd<@cols;$iInd++)
            #  {
            #    print "$cols[$iInd]\t";
            #   }
            # print "\n";
          }

           for(my $iInd=9;$iInd<@cols;$iInd++)
           {
               if($cols[$iInd] =~/^\./)
               {
                 $iMissingInd++;
               }
               elsif($cols[$iInd] =~/0\/0/)
               {
                $iAA++;
               }
               elsif($cols[$iInd] =~/1\/1/)
               {
                $iaa++;
               }
               elsif($cols[$iInd] =~/0\/1/ ||$cols[$iInd] =~/1\/0/)
               {
                $iAa++;
               }
           }
           next if($iMissingInd>$iAllowableMissingRate[$iBatch]);          
           my $hwe_Pvalue=snphwe($iAa,$iaa,$iAA);  
          # print "$iAA\t$iaa\t$iAa\t Pvalue:$hwe_Pvalue\n";  
           next if($hwe_Pvalue<1e-6);
           next if(($iaa+$iAa)==1||($iAA+$iAa)==1);#Remove singleton or doubleton
           #   print "$cols[5]:$cols[4];_$CandidateAlleles[0].$CandidateAlleles[1]\n";
           my $iOutput=0;
           if($cols[3] eq $CandidateAlleles[0] && $cols[4] eq $CandidateAlleles[1])
           {
             $iOutput=1;
           }
           elsif($cols[3] eq $CandidateAlleles[1] && $cols[4]  eq $CandidateAlleles[0])
           { 
              $iOutput=1;
             my $sTemp=$cols[3];
             $cols[3]=$cols[4];
             $cols[4]=$sTemp;
             for(my $iCol=9;$iCol<@cols;$iCol++)
             {
               if($cols[$iCol] eq "0/0")
               {
                $cols[$iCol]="1/1";
               }
               elsif($cols[$iCol] eq "1/1")
               {
                  $cols[$iCol]="0/0";
               }
             }

             # print "changed:$cols[3]:$AltAlleles[0]\n";
           }
           elsif(defined($ComplementaryAlleles{$cols[3]}) && defined($ComplementaryAlleles{$cols[4]})) 
           {
             if($ComplementaryAlleles{$cols[3]} eq $CandidateAlleles[0] && $ComplementaryAlleles{$cols[4]} eq $CandidateAlleles[1])
             { 
               $iOutput=1;
               $cols[3]=$ComplementaryAlleles{$cols[3]};
               $cols[4]=$ComplementaryAlleles{$cols[4]};
             }
             elsif($ComplementaryAlleles{$cols[4]} eq $CandidateAlleles[0] && $ComplementaryAlleles{$cols[3]} eq $CandidateAlleles[1])      
             {
            # print "changed 0:$cols[3]:$AltAlleles[0],$CandidateAlleles[0],$CandidateAlleles[1]\n";
               $iOutput=1;
                my $sTemp=$ComplementaryAlleles{$cols[3]};
                $cols[3]=$ComplementaryAlleles{$cols[4]};
                $cols[4]=$sTemp;
                for(my $iCol=9;$iCol<@cols;$iCol++)
                {
                  if($cols[$iCol] eq "0/0")
                  {
                      $cols[$iCol]="1/1";
                  }
                  elsif($cols[$iCol] eq "1/1")
                  {
                    $cols[$iCol]="0/0";
                  }
               }#for loop end

             }#elsif end Complementar
           } #elsif end
            else
            {
                $iLineNumber++;
              print "Double removed:$cols[3]:$cols[4];$CandidateAlleles[0].$CandidateAlleles[1]\n";
             }
           if($iOutput==1)
           {
              print OUT "$cols[0]";
              for(my $iCol=1;$iCol<@cols;$iCol++)
              {  
                print OUT "\t$cols[$iCol]";
              }  
              print OUT "\n";
               $iLineNumber++;
              if($iLineNumber%1000==0)
              {
               print "$ChrPos\t$iLineNumber\n";
              }
            
           }
          # last if($cols[0]>2);
          # last if($iLineNumber>1002);
         #last;
     } 
   }
}



