 #!/usr/b\nin/perl -w
 use strict;
use List::Util qw( min max ); 
use Scalar::Util qw(looks_like_number);
#use Statistics::Basic qw(:all); 
use warnings;

#/data/ruderferlab/projects/cmc/files/CMC-WGSID_NoRNASeqDuplicateV2.map

#/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/SKL_10073IndividualBasedVariansSummaryMissingness.txt
#/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/SKL_11154IndividualBasedVariansSummaryMissingness.txt
#/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/SKL_11694IndividualBasedVariansSummaryMissingness.txt
#   /data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/SKL_10073IndividualBasedVariansSummary.txt
#   /data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/SKL_11694IndividualBasedVariansSummary.txt
#   /data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/SKL_11154IndividualBasedVariansSummary.txt 


#matrix transpose
sub pivot {
    my @src = @_;

    my $max_col = 0;
    $max_col < $#$_ and $max_col = $#$_ for @src;

    my @dest;
    for my $col (0..$max_col) {
        push @dest, [map {$src[$_][$col] // ''} (0..$#src)];
    }

    return @dest;
}

  my $iLineNum=0;
  my %WGSID_Disease=();
  my  %WGSID_Ancestry=();  
  my  %WGSID_Sex=();  
  my @WGSIDs=();
  my  $correctIDMapFile="/data/ruderferlab/projects/cmc/files/CMC-WGSID_NoRNASeqDuplicateV2.map";#sample ID files
  open(INPUT,$correctIDMapFile) || die "can’t open $correctIDMapFile";
  while (<INPUT>)
  {   
     chomp;
     if($iLineNum==0)
     { 
       $iLineNum++;
       next;
      }
      my @cols=split(/\s++/,$_);
      push @WGSIDs,$cols[9];
      $WGSID_Sex{$cols[9]}=$cols[4];
      $WGSID_Disease{$cols[9]}=$cols[7];
      $WGSID_Ancestry{$cols[9]}=$cols[5];
  }
  close INPUT;

   my %WGSID_EigenValue=();
   my $PCA_EigenValueFile="/data/ruderferlab/projects/cmc/results/qc/pca/mergeSKL10073_11154_11694.eigenvec";
   open(INPUT,$PCA_EigenValueFile) || die "can’t open $PCA_EigenValueFile";
   while (<INPUT>)
   {   
      chomp;
      my @cols=split(/\s++/,$_);
      my @IndNameList=split(/\_/,$cols[1]);
      $WGSID_EigenValue{$IndNameList[1]}="$cols[2]\t$cols[3]";
   }
   close INPUT;

 
  my   $size1=keys %WGSID_Disease;
  print "Size:\t $size1\n";
 my @WGSBatches=("SKL_10073","SKL_11694","SKL_11154");

 my %WGSID_Batch=();
 my %WGSID_Indels=();
 my %WGSID_SNVs=(); 
 my %WGSID_MissedIndels=();
 my %WGSID_MissedSNVs=(); 

 my %WGSID_Hyterrozyogotes_SNV=();
 my %WGSID_Hyterrozyogotes_INDEL=();

  for(my $iBatch=0;$iBatch<@WGSBatches;$iBatch++)
 {
     my @AllIndSummaryInfo=();
     my $InputFile="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/$WGSBatches[$iBatch]IndividualBasedVariansSummary.txt";;
     open(INPUT, $InputFile) || die "can’t open $InputFile";
     while (<INPUT>)
     {
         chomp;
         my @cols=split(/\s++/,$_);
         if($cols[0]=~/^\#CHROM|Hyterrozyogotes|Indels|SNVs/)
         {
            print "$cols[0]\t$cols[8]\t$cols[9]\n";
            splice(@cols,1,8);
            print "$cols[0]\t$cols[1]\n";
            push @AllIndSummaryInfo,[@cols];
         }
      }
     close INPUT;
     for(my $ii=0;$ii<@AllIndSummaryInfo;$ii++)
     {
       for(my $jj=1;$jj<@{$AllIndSummaryInfo[0]};$jj++)
       {     
         $WGSID_Batch{$AllIndSummaryInfo[0][$jj]}=$WGSBatches[$iBatch];
         $WGSID_Indels{$AllIndSummaryInfo[0][$jj]}=$AllIndSummaryInfo[1][$jj];
         $WGSID_SNVs{$AllIndSummaryInfo[0][$jj]}=$AllIndSummaryInfo[2][$jj];
         $WGSID_Hyterrozyogotes_SNV{$AllIndSummaryInfo[0][$jj]}=$AllIndSummaryInfo[3][$jj];
         $WGSID_Hyterrozyogotes_INDEL{$AllIndSummaryInfo[0][$jj]}=$AllIndSummaryInfo[4][$jj];
       }
     }
       @AllIndSummaryInfo=();
     my $InputFile="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/$WGSBatches[$iBatch]IndividualBasedVariansSummaryMissingness.txt";;
     open(INPUT, $InputFile) || die "can’t open $InputFile";
     while (<INPUT>)
     {   
         chomp;
         my @cols=split(/\s++/,$_);
         if($cols[0]=~/^\#CHROM|MissingSNVs|MissingINDELs/)
         {  
            print "$cols[0]\t$cols[8]\t$cols[9]\n";
            splice(@cols,1,8);
            print "$cols[0]\t$cols[1]\n";   
            push @AllIndSummaryInfo,[@cols];
         }
      }   
     close INPUT;   
     for(my $ii=0;$ii<@AllIndSummaryInfo;$ii++)
     { 
       for(my $jj=1;$jj<@{$AllIndSummaryInfo[0]};$jj++)
       { 
         $WGSID_MissedIndels{$AllIndSummaryInfo[0][$jj]}=$AllIndSummaryInfo[2][$jj];
         $WGSID_MissedSNVs{$AllIndSummaryInfo[0][$jj]}=$AllIndSummaryInfo[1][$jj];  
       }
     }  



 }

 my $OutFile="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/WGSData_VariantSummary_Ancestry.txt";
 open OUT, ">$OutFile" or die "Can't open Output file:$OutFile!";
 print OUT "WGSID\tSex\tBatch\tNumbofHyterrozyogotes_SNV\tNumHyterrozyogotes_INDEL\tDisease\tAncestry\tPC1\tPC2\tSNV_Missing\tINDEL_Missing\n";
 for(my $iInd=0;$iInd<@WGSIDs;$iInd++)
 {
     my   $fSNVs_Missed=sprintf("%0.4f",$WGSID_MissedSNVs{$WGSIDs[$iInd]}/($WGSID_MissedSNVs{$WGSIDs[$iInd]}+$WGSID_SNVs{$WGSIDs[$iInd]}));
     my $fINDELs_Missed=sprintf("%0.4f",$WGSID_MissedIndels{$WGSIDs[$iInd]}/($WGSID_MissedIndels{$WGSIDs[$iInd]}+$WGSID_Indels{$WGSIDs[$iInd]}));

   print OUT "$WGSIDs[$iInd]\t$WGSID_Sex{$WGSIDs[$iInd]}\t$WGSID_Batch{$WGSIDs[$iInd]}\t$WGSID_Hyterrozyogotes_SNV{$WGSIDs[$iInd]}\t$WGSID_Hyterrozyogotes_INDEL{$WGSIDs[$iInd]}\t$WGSID_Disease{$WGSIDs[$iInd]}\t$WGSID_Ancestry{$WGSIDs[$iInd]}\t$WGSID_EigenValue{$WGSIDs[$iInd]}\t$fSNVs_Missed\t$fINDELs_Missed\n";
  }
 close OUT;
