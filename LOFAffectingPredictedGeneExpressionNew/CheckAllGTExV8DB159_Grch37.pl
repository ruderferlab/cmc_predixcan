#!/usr/bin/perl -w
 use strict;

 use lib "/home/hanl3/perl5/module/lib/site_perl/5.14.2";
 use Text::CSV_PP;
  use Tie::File; 
 use POSIX qw(WNOHANG);
 use IO::Uncompress::Gunzip qw($GunzipError);
 use warnings;
use Scalar::Util qw(looks_like_number);

# UTMOST, XT_Scan,PrediXcan two brain tissues' preciction model which based on GRch38 assembly
# /data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/gtex_v8_st_Brain_Anterior_cingulate_cortex_BA24.txt
#       gtex_v8_st_Brain_Frontal_Cortex_BA9.txt  gtex_v8_ut_Brain_Anterior_cingulate_cortex_BA24.txt
#       gtex_v8_ut_Brain_Frontal_Cortex_BA9.txt  gtex_v8_xt_Brain_Anterior_cingulate_cortex_BA24.txt  gtex_v8_xt_Brain_Frontal_Cortex_BA9.txt

  my @PredictionMethods=("st","ut","xt");
  my @BrainsTissues=("Brain_Frontal_Cortex_BA9","Brain_Anterior_cingulate_cortex_BA24");
  my @BrainsTissuesNames=("DLPFC_BA9","ACC_BA24");

 my %Chr_GeneID=();#Chr_ geneid
 my %AllUsedSNVsInfo=(); 
  for(my $iM=0;$iM<@PredictionMethods;$iM++)
 {
   for(my $iT=0;$iT<@BrainsTissues;$iT++)
   {
      my $iLineNumber=0;
      my $PredictionFile="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/gtex_v8_$PredictionMethods[$iM]_$BrainsTissues[$iT].txt";
      open(INPUT, $PredictionFile) || die "can’t open $PredictionFile";
      while (<INPUT>)
      { 
        chomp;
        if($iLineNumber==0)
        {
          $iLineNumber++;
          next;
        }
        my @cols=split(/\s+/,$_);
        $cols[3] =~ s/^\s+|\s+$//g;
        $cols[4] =~ s/^\s+|\s+$//g;
        $AllUsedSNVsInfo{$cols[0]}="$cols[3]:$cols[4]"; # rsid cols0, ref_allele[col3]eff_allele[col4]     
       # last if($iLineNumber>2000);
       $iLineNumber++;
     }
     close INPUT;
  }
}

  my %ChrPos_rsid=();
 my %ChrPos_2ndPos=();
   my %ComplementaryAlleles=();
   $ComplementaryAlleles{"A"}="T";
   $ComplementaryAlleles{"T"}="A";
   $ComplementaryAlleles{"C"}="G";
   $ComplementaryAlleles{"G"}="C";
   $ComplementaryAlleles{"-"}="-";
  my $NumberofSNVs=keys%AllUsedSNVsInfo;
  print "NumberofSNVs:\t$NumberofSNVs\n";
  my $iNumberofSNVs_Model=0;


 # liftover by position 
 my $SNPPosFile="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/hglft_genome_Dan_Cchr37.bed.gz";#snp150_37.txt.gz"; and   snp150.txt.gz #chr1    rs367896724     10177
    open (INPUT, "gunzip -c $SNPPosFile|") or die "gunzip  $SNPPosFile: $!";;
  while (<INPUT>)
  {
       my @cols=split(/\s+/,$_);
       $cols[0]=~ s/chr//g;
       next if(!looks_like_number($cols[0]));#judge if the chromosome is a number.
       if(defined($AllUsedSNVsInfo{$cols[3]}))
       {
          my $ChrPos="$cols[0]:$cols[1]";
         $ChrPos_rsid{$ChrPos}=$cols[3];
         $ChrPos_2ndPos{$ChrPos}=$cols[2];
          #$rsid_position{$cols[3]}="$cols[0]:$cols[1]";#chr[cols[0]] position[cols[1]], $cols[3]->rsid
         $iNumberofSNVs_Model++;
      #   last if($iNumberofSNVs_Model>1000);
       }
     }
   close INPUT;
   $NumberofSNVs=keys%ChrPos_rsid;
   print "NumberofSNVswithRSID:\t$NumberofSNVs\n";



   my $OutputFile="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/db150_37_LiftoverByPos_LociInfo.txt";
    open OUT, ">$OutputFile" or die "Can't open Output file:$OutputFile!";
   my $iLineNumber=0;#Consider completely match, reverse match, complementary match
    my $db150_37SubsetFile="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/db150_37_CommonSNVs.ucsc_subset.txt.gz";
    open (INPUT, "gunzip -c $db150_37SubsetFile |") or die "gunzip $db150_37SubsetFile: $!";;
    while (<INPUT>)
    {
       chomp;
      if($iLineNumber==0)
      {
         $iLineNumber++;
         print OUT "$_\trsid_Model\t2ndPos_Model\tTwoAlleles_Model\n";
         next;
      }
       my @cols=split(/\s+/,$_,3);
       next if(scalar(@cols)==0);
       $cols[0] =~ s/chr//g;
       next if(!looks_like_number($cols[0]));#judge if the chromosome is a number.
       my $ChrPos="$cols[0]:$cols[1]";
       next if(!defined($ChrPos_rsid{$ChrPos}));
        @cols=split(/\s+/,$_);
        $cols[0]=~ s/chr//g; 
       my @Alleles_GRch37=split(/\//,$cols[8]);      
       my @Alleles_PredictedModel=split(/:/,$AllUsedSNVsInfo{$ChrPos_rsid{$ChrPos}});
       if($Alleles_GRch37[0] eq "-")#This for allele missing situation
        {
         my $Allelelength1_Model=length($Alleles_PredictedModel[0]);
         my $Allelelength2_Model=length($Alleles_PredictedModel[1]);
         if($Allelelength1_Model>$Allelelength2_Model)
         { 
           my $postfix= substr($Alleles_PredictedModel[0],0,$Allelelength2_Model);
           if($postfix eq $Alleles_PredictedModel[1])
           {  
              substr($Alleles_PredictedModel[0],0,$Allelelength2_Model,"");
              $Alleles_PredictedModel[1]="-";
           }
         }
         else
         { 
           my $postfix= substr($Alleles_PredictedModel[1],0,$Allelelength1_Model);
           if($postfix eq $Alleles_PredictedModel[0])
           {  
              substr($Alleles_PredictedModel[1],0,$Allelelength1_Model,"");
              $Alleles_PredictedModel[0]="-";
           }
         }
      }
         my $iMatchAllels=0;
       for(my $ii=0;$ii<@Alleles_PredictedModel;$ii++)
       {
          for(my $jj=0;$jj<@Alleles_GRch37;$jj++)
          {
           if($Alleles_GRch37[$jj] eq $Alleles_PredictedModel[$ii])
           {
              $iMatchAllels++;
            }
          }
        }
        my $iComplementaryMatch=0;
        if($Alleles_GRch37[0] ne "-")
        {
          if($iMatchAllels==1 && scalar(@Alleles_GRch37)==2)
        {
         # print "one match: $AllUsedSNVsInfo{$ChrPos_rsid{$ChrPos}}:$cols[8]\n";
           next;
        }
        elsif($iMatchAllels==0 || $iMatchAllels==1 && scalar(@Alleles_GRch37)>2)
       {      
         if(!defined($ComplementaryAlleles{$Alleles_PredictedModel[0]})||!defined($ComplementaryAlleles{$Alleles_PredictedModel[1]}))
         {
         # print "No match: $AllUsedSNVsInfo{$ChrPos_rsid{$ChrPos}}:$cols[8]\n";
           next;
         }
         else
         {
           my @ComplementaryAlleles_Model=($ComplementaryAlleles{$Alleles_PredictedModel[0]},$ComplementaryAlleles{$Alleles_PredictedModel[1]});
           for(my $ii=0;$ii<@ComplementaryAlleles_Model;$ii++)
           {       
               for(my $jj=0;$jj<@Alleles_GRch37;$jj++)
               {       
                 if($Alleles_GRch37[$jj] eq $ComplementaryAlleles_Model[$ii])
                  {       
                    $iComplementaryMatch++;
                  }       
                }       
            }   
          }
          if($iComplementaryMatch==1||$iComplementaryMatch==0)
           {       
          #  print "no complementary match: $AllUsedSNVsInfo{$ChrPos_rsid{$ChrPos}}:$cols[8]\n";
            next;   
           }
          }  
        }
        else
         {
          if($iMatchAllels==1)
          {
           my $Allele_Reverse=reverse $Alleles_GRch37[1];
           my $AlleleLength=length($Alleles_GRch37[1]);
           my @ArrayAllele_Reverse = split //, $Allele_Reverse;
           for(my $ii=0;$ii<@ArrayAllele_Reverse;$ii++)
           {
              $ArrayAllele_Reverse[$ii]=$ComplementaryAlleles{$ArrayAllele_Reverse[$ii]};
           }
           $Alleles_GRch37[1]= join('', @ArrayAllele_Reverse);
           for(my $ii=0;$ii<@Alleles_PredictedModel;$ii++)
           {
               for(my $jj=0;$jj<@Alleles_GRch37;$jj++)
               {
                 if($Alleles_GRch37[$jj] eq $Alleles_PredictedModel[$ii])
                  {
                    $iComplementaryMatch++;
                  }
                }
            }
          }
         }

     #   print "$cols[8];$AllUsedSNVsInfo{$ChrPos_rsid{$ChrPos}};$iMatchAllels;$iComplementaryMatch;\n";
        if($iMatchAllels==2 || $iComplementaryMatch==2)
        {
         print OUT "$_\t$ChrPos_rsid{$ChrPos}\t$ChrPos_2ndPos{$ChrPos}\t$AllUsedSNVsInfo{$ChrPos_rsid{$ChrPos}}\n";
         $iLineNumber++;
        }
     #   last if($iLineNumber>20);
     } 
   close INPUT;
   close OUT;


  my $NumberofMatchRSID=0;
  my  $iLineNumber=0;
  my $OutputFile="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/db150_37_LiftoverBySameRSID_LociInfo.txt";
  open OUT, ">$OutputFile" or die "Can't open Output file:$OutputFile!";
  #liftover by RSID
   my $rsidSharedFile="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/db150_37_CommonSNVs.ucsc_subset.txt.gz";#db150_37_subset.txt.gz";
  open (INPUT, "gunzip -c $rsidSharedFile|") or die "gunzip  $rsidSharedFile: $!";;
  while (<INPUT>)
  {   
      chomp;
      if($iLineNumber==0)
      {
         $iLineNumber++;
         print OUT "$_\tTwoAlleles_Model\n";
         next;
      }
       my @cols=split(/\t/,$_);
       next if(scalar(@cols)==0);
       $cols[0] =~ s/chr//g;
       $cols[3] =~ s/^\s+|\s+$//g;
       next if(!looks_like_number($cols[0]));#judge if the chromosome is a number.
       next if(!defined($AllUsedSNVsInfo{$cols[3]}));
       $NumberofMatchRSID++; 

        # print "$cols[8];$AllUsedSNVsInfo{$cols[3]}\n";
        my @Alleles_GRch37=split(/\//,$cols[8]);
        my @Alleles_PredictedModel=split(/:/,$AllUsedSNVsInfo{$cols[3]});
        if($Alleles_GRch37[0] eq "-")#This for allele missing situation
        {
         my $Allelelength1_Model=length($Alleles_PredictedModel[0]);
         my $Allelelength2_Model=length($Alleles_PredictedModel[1]);
         if($Allelelength1_Model>$Allelelength2_Model) 
         {
           my $postfix= substr($Alleles_PredictedModel[0],0,$Allelelength2_Model); 
           if($postfix eq $Alleles_PredictedModel[1])
           {
              substr($Alleles_PredictedModel[0],0,$Allelelength2_Model,"");
              $Alleles_PredictedModel[1]="-";
           }
         } 
         else
         {
           my $postfix= substr($Alleles_PredictedModel[1],0,$Allelelength1_Model); 
           if($postfix eq $Alleles_PredictedModel[0])
           {       
              substr($Alleles_PredictedModel[1],0,$Allelelength1_Model,"");
              $Alleles_PredictedModel[0]="-";
           }  
         }
      }
       my $iMatchAllels=0;
       for(my $ii=0;$ii<@Alleles_PredictedModel;$ii++)
       {
          for(my $jj=0;$jj<@Alleles_GRch37;$jj++)
          {
           if($Alleles_GRch37[$jj] eq $Alleles_PredictedModel[$ii])
           {
              $iMatchAllels++;
            }
          }
        }
        my $iComplementaryMatch=0;
        if($Alleles_GRch37[0] ne "-")
        { 
          if($iMatchAllels==1 && scalar(@Alleles_GRch37)==2)
          {
           # print "one match: $AllUsedSNVsInfo{$cols[3]}:$cols[8]\n";
            # next;
          }
          elsif($iMatchAllels==0 || $iMatchAllels==1 && scalar(@Alleles_GRch37)>2)
          { 
          if(!defined($ComplementaryAlleles{$Alleles_PredictedModel[0]})||!defined($ComplementaryAlleles{$Alleles_PredictedModel[1]}))
          {
           # print "No match: $AllUsedSNVsInfo{$cols[3]}:$cols[8]\n";
            # next;
           }
          else
          {
           my @ComplementaryAlleles_Model=($ComplementaryAlleles{$Alleles_PredictedModel[0]},$ComplementaryAlleles{$Alleles_PredictedModel[1]});
           for(my $ii=0;$ii<@ComplementaryAlleles_Model;$ii++)
           {
               for(my $jj=0;$jj<@Alleles_GRch37;$jj++)
               {
                 if($Alleles_GRch37[$jj] eq $ComplementaryAlleles_Model[$ii])
                  {
                    $iComplementaryMatch++;
                  }
                }
            }
          }
          if($iComplementaryMatch==1||$iComplementaryMatch==0)
           {
       #     print "no complementary match: $AllUsedSNVsInfo{$cols[3]}:$cols[8]\n";
           # next;
           }
         }
        }
        else
         {
          if($iMatchAllels==1)
          {
           my $Allele_Reverse=reverse $Alleles_GRch37[1];
           my $AlleleLength=length($Alleles_GRch37[1]);
           my @ArrayAllele_Reverse = split //, $Allele_Reverse;           
           for(my $ii=0;$ii<@ArrayAllele_Reverse;$ii++)
           {
              $ArrayAllele_Reverse[$ii]=$ComplementaryAlleles{$ArrayAllele_Reverse[$ii]};
           }
           $Alleles_GRch37[1]= join('', @ArrayAllele_Reverse);
           for(my $ii=0;$ii<@Alleles_PredictedModel;$ii++)
           {
               for(my $jj=0;$jj<@Alleles_GRch37;$jj++)
               {
                 if($Alleles_GRch37[$jj] eq $Alleles_PredictedModel[$ii])
                  {
                    $iComplementaryMatch++;
                  }
                }
            }
          }
         }

      #  print "dbsnp $cols[8];model: $AllUsedSNVsInfo{$cols[3]};ma: $iMatchAllels;CM:$iComplementaryMatch;\n";
        if($iMatchAllels==2 || $iComplementaryMatch==2)
        {
        #   print "print $_\t$AllUsedSNVsInfo{$cols[3]}\n";
         print OUT "$_\t$AllUsedSNVsInfo{$cols[3]}\n";
         $iLineNumber++;
        }
        else
        { 
           print "NO output $cols[8];$AllUsedSNVsInfo{$cols[3]};$iMatchAllels;$iComplementaryMatch;\n";
        }
      # last if($iLineNumber>200);
   }
   close INPUT;
   close OUT;
print "Match rsid:$NumberofMatchRSID\n";

