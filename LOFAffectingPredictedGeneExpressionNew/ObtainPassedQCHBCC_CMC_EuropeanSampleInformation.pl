#!/usr/bin/perl -w
use Scalar::Util qw(looks_like_number);
 use strict;

 use lib "/home/hanl3/perl5/module/lib/site_perl/5.14.2";
 use Text::CSV_PP;
  use Tie::File; 
 use POSIX qw(WNOHANG);
 use IO::Uncompress::Gunzip qw($GunzipError);
 use warnings;
 use Scalar::Util qw(looks_like_number);


#     /data/ruderferlab/projects/cmc/files/CMC-WGSID_NoRNASeqDuplicateV2.map
#    /home/hanl3/cmc/scratch/Predicted_ActualGeneExpressionMerged/GTEXV8_Prediction/PredictedValues_Zscore/GTExV8BasedSKL_10073ut_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExCEUBasedINTZScore.txt
#    /home/hanl3/cmc/scratch/Predicted_ActualGeneExpressionMerged/GTEXV8_Prediction/PredictedValues_Zscore/GTExV8BasedSKL_11154ut_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExCEUBasedINTZScore.txt
#    /home/hanl3/cmc/scratch/Predicted_ActualGeneExpressionMerged/GTEXV8_Prediction/PredictedValues_Zscore/GTExV8BasedSKL_11694ut_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExCEUBasedINTZScore.txt



#Eli-s234  Eli-sw56  Eli-swe1

my @ChromosomeBasedDir=("SKL_10073","SKL_11154","SKL_11694");
 my @BatchNameList=("SKL_10073","SKL_11154","SKL_11694");

  my %AllPassedQCInds=();
  for(my $iBatch=0;$iBatch<@BatchNameList;$iBatch++)
  {
       my $IndInfoFile="/home/hanl3/cmc/scratch/Predicted_ActualGeneExpressionMerged/GTEXV8_Prediction/PredictedValues_Zscore/GTExV8Based$BatchNameList[$iBatch]ut_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExCEUBasedINTZScore.txt";
       open(INPUT,$IndInfoFile) || die "can’t open $IndInfoFile";
       while (<INPUT>)
       {
         chomp;
         $_ =~ s/^\s+|\s+$//g;      
         my @cols=split(/\s++/,$_);
          print "$cols[7]\t$cols[8]\n";
         for(my $iInd=8;$iInd<@cols;$iInd++)
         {
           $AllPassedQCInds{$cols[$iInd]}=1;
         }
         last;
      }
      close INPUT;
   }

   my $LineNum=0;
   my $SwedenPassedQCSampleInfoFile="/data/ruderferlab/projects/cmc/files/CMC-WGSID_NoRNASeqDuplicateV2_CEUSubset.map";
   open OUT, ">$SwedenPassedQCSampleInfoFile" or die "Can't open Output file:$SwedenPassedQCSampleInfoFile!"; 
   my $SwedenIndfoFile="/data/ruderferlab/projects/cmc/files/CMC-WGSID_NoRNASeqDuplicateV2.map";
   open(INPUT,$SwedenIndfoFile) || die "can’t open $SwedenIndfoFile";
   while (<INPUT>)
   {
      chomp;
       $_ =~ s/^\s+|\s+$//g;
       if($LineNum==0)
       {
        print OUT "$_\n";
        $LineNum++;
        next;
       }
       my @cols=split(/\s++/,$_);
       if(defined($AllPassedQCInds{$cols[9]}))
       {
        print OUT "$_\n";
       }
    }
    close INPUT;
    close OUT; 
