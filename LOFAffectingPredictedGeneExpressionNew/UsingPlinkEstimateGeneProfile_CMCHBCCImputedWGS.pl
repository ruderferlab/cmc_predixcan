#!/usr/bin/perl -w
 use strict;
  use 5.012;
 use lib "/home/hanl3/perl5/module/lib/site_perl/5.14.2";
 use Text::CSV_PP;
  use Tie::File; 
 use POSIX qw(WNOHANG);
 use IO::Uncompress::Gunzip qw($GunzipError);
 use warnings;
use Scalar::Util qw(looks_like_number);

#     /data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/SKL10073ShareV8MultiplyModel.CommonSNVs_ImputedWGS_FullGenotype
#     /data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/SKL11154ShareV8MultiplyModel.CommonSNVs_ImputedWGS_FullGenotype
#     /data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/SKL11694ShareV8MultiplyModel.CommonSNVs_ImputedWGS_FullGenotype
#     /data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/SKL10073ShareV8MultiplyModel.CommonSNVs_ImputedWGS_Haplotype1
#     /data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/SKL11154ShareV8MultiplyModel.CommonSNVs_ImputedWGS_Haplotype1
#     /data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/SKL11694ShareV8MultiplyModel.CommonSNVs_ImputedWGS_Haplotype1
#    /data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/SKL10073ShareV8MultiplyModel.CommonSNVs_ImputedWGS_Haplotype2
#     /data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/SKL11154ShareV8MultiplyModel.CommonSNVs_ImputedWGS_Haplotype2
#    /data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/SKL11694ShareV8MultiplyModel.CommonSNVs_ImputedWGS_Haplotype2
# /data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTExV8_Weight/*_*/$gene.wgt.score

  my @WGSBatches=("SKL10073","SKL11154","SKL11694");
  my @GenoTypes=("FullGenotype","Haplotype1","Haplotype2");
  my @PredictionMethods=("st","ut","xt");
  my @BrainsTissues=("Brain_Frontal_Cortex_BA9","Brain_Anterior_cingulate_cortex_BA24");
  my @BrainsTissuesNames=("DLPFC_BA9","ACC_BA24");

 my %Chr_GeneID=();#Chr_ geneid
 my %AllUsedSNVsInfo=(); 

 my $iSpecificBatch=$ARGV[0];#0,1,2
 my $iSpecificMethod=$ARGV[1];#0,1,2
 my $iSpecificTussue=$ARGV[2];#0,1
  
 for(my $iType=0;$iType<@GenoTypes;$iType++)
 {
  for(my $iBatch=$iSpecificBatch;$iBatch<$iSpecificBatch+1;$iBatch++)
  {
   for(my $iM=$iSpecificMethod;$iM<$iSpecificMethod+1;$iM++)
   {
      for(my $iT=$iSpecificTussue;$iT<$iSpecificTussue+1;$iT++)
      {
         my $iNumberofFile=1;

my $mydir="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTExV8_WeightUpdated/$PredictionMethods[$iM]_$BrainsTissuesNames[$iT]";
#   my $mydir="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTExV8_Weight/$PredictionMethods[$iM]_$BrainsTissuesNames[$iT]";
#          my $mydir="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTExV8_Weight/$PredictionMethods[$iM]_$BrainsTissuesNames[$iT]";
          opendir(DIR, $mydir) || die "Can't open $mydir: $!";
           while (my $weightFile=readdir (DIR)) {
           next if ($weightFile =~ m/^\./);
           my $geneID=$weightFile;
           $geneID =~ s/\.wgt\.score//ig;
           my $scoreWeightFile="$mydir/$weightFile";
           my $plinkFilePrefix="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/$WGSBatches[$iBatch]ShareV8MultiplyModel.CommonSNVs_ImputedWGS_$GenoTypes[$iType]";
           my $outfilePrefix="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTExV8_ProfileUpdated/$PredictionMethods[$iM]_$BrainsTissuesNames[$iT]/$WGSBatches[$iBatch]_$GenoTypes[$iType]_$geneID";
 
         # print "$mydir/$weightFile\n";
         # print "$plinkFilePrefix\n";
         # print "$outfilePrefix\n";
          my $cmd="plink --bfile $plinkFilePrefix --score $scoreWeightFile 1 3 4 sum  double-dosage --out $outfilePrefix";
          system($cmd); 
          $iNumberofFile++;
         # last if($iNumberofFile>2);
         }
         closedir (DIR);       
     }
   }
  }
}
exit 0;

