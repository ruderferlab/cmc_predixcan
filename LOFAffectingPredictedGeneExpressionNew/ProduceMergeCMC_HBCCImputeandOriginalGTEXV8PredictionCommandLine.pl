#!/usr/bin/perl -w
 use strict;
  use 5.012;
 use lib "/home/hanl3/perl5/module/lib/site_perl/5.14.2";
 use Text::CSV_PP;
  use Tie::File; 
 use POSIX qw(WNOHANG);
 use IO::Uncompress::Gunzip qw($GunzipError);
 use warnings;
use Scalar::Util qw(looks_like_number);

  my @WGSBatches=("SKL10073","SKL11154","SKL11694");
  my @PredictionMethods=("st","ut","xt");
  my @BrainsTissues=("Brain_Frontal_Cortex_BA9","Brain_Anterior_cingulate_cortex_BA24");
  my @BrainsTissuesNames=("DLPFC_BA9","ACC_BA24");
   my @WGSBatchNames=("SKL_10073","SKL_11154","SKL_11694");
 
 my @GenoTypes=("FullGenotype","Haplotype1","Haplotype2");
 my $OutZFile="/gpfs23/data/ruderferlab/projects/cmc/scripts/LOFAffectingPredictedGeneExpressionNew/GTEXV8_PredicatedExpression_Imputed_Original.sh";
 open OUT, ">$OutZFile" or die "Can't open Output file:$OutZFile!";
 for(my $iBatch=0;$iBatch<@WGSBatchNames;$iBatch++)
   {
     for(my $iType=0;$iType<@GenoTypes;$iType++)
     {  
      for(my $iM=0;$iM<@PredictionMethods;$iM++)
      {
       for(my $iT=0;$iT<@BrainsTissues;$iT++)
       {
        for(my $iType=0;$iType<@GenoTypes;$iType++)
        {
          print OUT "perl /gpfs23/data/ruderferlab/projects/cmc/scripts/LOFAffectingPredictedGeneExpressionNew/MergeCMC_HBCCImputedWGSGTEXV8PredictedProfile.pl\t$iBatch\t$iM\t$iT\t$iType\n"; 
        }
         print OUT "perl /gpfs23/data/ruderferlab/projects/cmc/scripts/LOFAffectingPredictedGeneExpressionNew/MergeCMC_HBCCOrginalWGSGTEXV8PredictedProfile.pl\t$iBatch\t$iM\t$iT\n"; 
      }
     }
   }
  }
close OUT;

