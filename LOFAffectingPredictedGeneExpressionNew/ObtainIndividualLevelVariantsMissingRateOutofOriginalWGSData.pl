#!/usr/bin/perl -w
 use strict;

 use lib "/home/hanl3/perl5/module/lib/site_perl/5.14.2";
 use Text::CSV_PP;
  use Tie::File; 
 use POSIX qw(WNOHANG);
 use IO::Uncompress::Gunzip qw($GunzipError);
 use warnings;
 use Scalar::Util qw(looks_like_number);


my $iSpecificBatch=$ARGV[0];
# /data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8AllSNVsInfoforPrediction.txt

my @NumberofInds=(337,335,128);
my @BatchList=("SKL_10073_B01_GRM_WGS_2016-02-25","SKL_11154_B01_GRM_WGS_2016-03-17","SKL_11694_B01_GRM_WGS_2017-08-18");
#     /home/hanl3/cmc/data/wgs/vcf/SKL_10073_B01_GRM_WGS_2016-02-25.recalibrated_variants_PASS_WholeGenome.vcf.gz[328 samples]
#          /home/hanl3/cmc/data/wgs/vcf/SKL_11154_B01_GRM_WGS_2016-03-17.recalibrated_variants_PASS_WholeGenome.vcf.gz[326 samples]
#               /home/hanl3/cmc/data/wgs/vcf/SKL_11694_B01_GRM_WGS_2017-08-18.recalibrated_variants_PASS_WholeGenome.vcf.gz[119 samples]
#
my @iAllowableMissingRate=(3,3,1);

  my %ComplementaryAlleles=();
  $ComplementaryAlleles{"A"}="T";
  $ComplementaryAlleles{"T"}="A";
  $ComplementaryAlleles{"C"}="G";
  $ComplementaryAlleles{"G"}="C";
  my @BatchNameList=("SKL_10073","SKL_11154","SKL_11694");for(my $iBatch=$iSpecificBatch;$iBatch<$iSpecificBatch+1;$iBatch++)
 {
     my @NumberofMissing_SNV=(0) x $NumberofInds[$iBatch];#Heterozygote
      my @NumberofMissing_INDEL=(0) x $NumberofInds[$iBatch];#Heterozygote
    my @SelectedIndInOneBatch=();
    my @SelectedIndIndexInOneBatch=();
    my $V8DifferentModelShareWGSFile="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/$BatchNameList[$iBatch]IndividualBasedVariansSummaryMissingness.txt";
    open OUT, ">$V8DifferentModelShareWGSFile" or die "Can't open Output file:$V8DifferentModelShareWGSFile!";
  
   my $WGSFile="/home/hanl3/cmc/data/wgs/vcf/$BatchList[$iBatch].recalibrated_variants_PASS_WholeGenome.vcf.gz";#snp150_37.txt.gz"; and   snp150.txt.gz
   open (INPUT, "gunzip -c $WGSFile|") or die "gunzip  $WGSFile: $!";;
  while (<INPUT>)
  {
     chomp;
      next if($_ =~ /^X|Y|MT/);
     if($_ =~ /^##/)
     {
      next if($_ =~ /^##GATKCommandLine/);
      next if($_ =~ /^##GVCFBlock/);
      next if($_ =~/^##contig=<ID=GL/); 
      next if($_ =~/^##contig=<ID=X/);
       next;
     }
      elsif($_ =~ /^#/)    
     {
        print OUT"$_\n";
        next;
        my @cols=split(/\s+/,$_);
        for(my $ii=9;$ii<@cols;$ii++)
        {
           push @SelectedIndInOneBatch,$cols[$ii];
            push @SelectedIndIndexInOneBatch,$ii;
        }
       my $iLength=scalar(@SelectedIndIndexInOneBatch);
       print "Number ofInds:\t$iLength\n";
     }
    else
     {  
          my @cols=split(/\s+/,$_,8);
          my @iAlleles=split(/,/,$cols[4]);#for the situations such as CTTT,CTTTT,C
           next  if(scalar(@iAlleles)>=2);#Remove alternative alleles number larger than 2
           @cols=split(/\s+/,$_);
           if(length($cols[3])==1 && length($cols[4])==1)
            {
                for(my $iCol=9;$iCol<@cols;$iCol++)
                {
                   if($cols[$iCol] =~/^\./)
                  {
                   $NumberofMissing_SNV[$iCol]++;
                  }
                }
            }
            elsif(length($cols[3])!=1 || length($cols[4])!=1)
            {
                for(my $iCol=9;$iCol<@cols;$iCol++)
                {
                   if($cols[$iCol] =~/^\./)
                  {
                   $NumberofMissing_INDEL[$iCol]++;
                  }
                }
             }
        }
   }
   close INPUT;
   print OUT "MissingSNVs";
   for(my $ii=1;$ii<@NumberofMissing_SNV;$ii++)
   {
     print OUT "\t$NumberofMissing_SNV[$ii]";
    }
   print OUT "\n";
   print OUT "MissingINDELs";
   for(my $ii=1;$ii<@NumberofMissing_INDEL;$ii++)
   {
     print OUT "\t$NumberofMissing_INDEL[$ii]";
    }
   print OUT "\n";
   close OUT;
}



