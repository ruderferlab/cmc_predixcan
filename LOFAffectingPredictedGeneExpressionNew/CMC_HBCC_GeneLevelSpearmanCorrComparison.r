#Convert imputed WGS based prediction into corresponing ZSCORE and int zscore

 library(magrittr);
 library(RNOmni);
 library(dplyr)
 library(stringr)

#Input file list
#/home/hanl3/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMCHBCC_DLPFC_GeneLevelSpearmanCorrforActualPredictedExpression/GTExV8FullGenotype_Imputed_st_GeneLevelSpearmanCorr.txt
#/home/hanl3/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMCHBCC_DLPFC_GeneLevelSpearmanCorrforActualPredictedExpression/GTExV8FullGenotype_Imputed_ut_GeneLevelSpearmanCorr.txt
#/home/hanl3/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMCHBCC_DLPFC_GeneLevelSpearmanCorrforActualPredictedExpression/GTExV8FullGenotype_Imputed_xt_GeneLevelSpearmanCorr.txt
#/home/hanl3/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMCHBCC_DLPFC_GeneLevelSpearmanCorrforActualPredictedExpression/GTExV8Original_st_GeneLevelSpearmanCorr.txt
#/home/hanl3/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMCHBCC_DLPFC_GeneLevelSpearmanCorrforActualPredictedExpression/GTExV8Original_ut_GeneLevelSpearmanCorr.txt
#/home/hanl3/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMCHBCC_DLPFC_GeneLevelSpearmanCorrforActualPredictedExpression/GTExV8Original_xt_GeneLevelSpearmanCorr.txt
#/home/hanl3/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMCHBCC_DLPFC_GeneLevelSpearmanCorrforActualPredictedExpression/PsychEncodeFullGenotype_Imputed_INT_GeneLevelSpearmanCorr.txt
#/home/hanl3/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMCHBCC_DLPFC_GeneLevelSpearmanCorrforActualPredictedExpression/PsychEncodeFullGenotype_Imputed_Standard_GeneLevelSpearmanCorr.txt
#/home/hanl3/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMCHBCC_DLPFC_GeneLevelSpearmanCorrforActualPredictedExpression/PsychEncodeOriginal_INT_GeneLevelSpearmanCorr.txt
#/home/hanl3/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMCHBCC_DLPFC_GeneLevelSpearmanCorrforActualPredictedExpression/PsychEncodeOriginal_Standard_GeneLevelSpearmanCorr.txt


#Imputed st ut,xt psyencode,standard
#Original st ut,xt psyencode,standard


#Impute and original

 mydir="/home/hanl3/cmc/results/LOFAffectingPredictedGeneExpression/WGSCMCHBCC_DLPFC_GeneLevelSpearmanCorrforActualPredictedExpression/";
  Outputfile=paste(mydir,"/CMC_HBCCDifferentMethodBasedCorrComparisonScatterPlot.pdf",sep="");
   pdf(Outputfile, 10, 10);
    par(mar=c(5,7,1.5,0.25), mgp=c(3,1,0), mfrow=c(3,2),oma=c(0,0,0,0));
    linewide=2;
    axixwide=0.5;
    tickwide=0.5;
   cex_size=0.62;
   leg_cex_size=0.8;
   axes=F;
   xaxs="s";
   cex_size1=0.65;

 
 DataTypes=c("FullGenotype","Original");
 for(iType in seq(1,2))
  {
    WholeDataTypes=NULL;
   MethodTypes=NULL;
   if(iType==1) 
   {
     WholeDataTypes=c("GTExV8FullGenotype_Imputed_st","GTExV8FullGenotype_Imputed_ut","GTExV8FullGenotype_Imputed_xt","PsychEncodeFullGenotype_Imputed_INT","PsychEncodeFullGenotype_Imputed_Standard");
     MethodTypes=c("Imputed_GTExV8_st","Imputed_GTExV8_ut","Imputed_GTExV8_xt","Imputed_PsychEncode_INT","Imputed_PsychEncode_Standard");
   }   
   else
   {
     WholeDataTypes=c("GTExV8Original_st","GTExV8Original_ut","GTExV8Original_xt","PsychEncodeOriginal_INT","PsychEncodeOriginal_Standard");
    MethodTypes=c("Original_GTExV8_st","Original_GTExV8_ut","Original_GTExV8_xt","Original_PsychEncode_INT","Original_PsychEncode_Standard");
   }
    for(ii in seq(1,4))
    {
      for(jj in seq((ii+1),5))
      {
        CorrFile1=paste(mydir,WholeDataTypes[ii],"_GeneLevelSpearmanCorr.txt",sep="");
        CorrMatrix1=read.table(CorrFile1, sep="",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE,row.names=NULL);#Obtain column names from File By check.names   
        CorrMatrix1=as.data.frame(CorrMatrix1);
        CorrFile2=paste(mydir,WholeDataTypes[jj],"_GeneLevelSpearmanCorr.txt",sep="");
        CorrMatrix2=read.table(CorrFile2, sep="",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE,row.names=NULL);#Obtain column names from File By check.names   
        CorrMatrix2=as.data.frame(CorrMatrix2);
        MergedCorr=inner_join(x=CorrMatrix1,y=CorrMatrix2,by =c("GeneID"),suffixes = c(".x",".y"));
        MergedCorr=as.data.frame(MergedCorr);
        colnames(MergedCorr)=c("GeneID","Corr1","Corr2");
        MergedCorr[,2]=as.numeric(MergedCorr[,2]);
        MergedCorr[,3]=as.numeric(MergedCorr[,3]);
        MergedCorr_Dim=dim(MergedCorr);
        corr=signif(cor(MergedCorr[,2],MergedCorr[,3],use="complete.obs"),4);; 
        plot(MergedCorr$Corr1, MergedCorr$Corr2, main=paste("Size",MergedCorr_Dim[1],"_Corr",corr,sep=""),xlab=MethodTypes[ii], ylab=MethodTypes[jj], pch=19,cex.lab=0.8);
        abline(a=0,b=1,lty=2,col=4);
      
      }
    }
  }

    WholeDataTypes1=c("GTExV8FullGenotype_Imputed_st","GTExV8FullGenotype_Imputed_ut","GTExV8FullGenotype_Imputed_xt","PsychEncodeFullGenotype_Imputed_INT","PsychEncodeFullGenotype_Imputed_Standard");
    MethodTypes1=c("Imputed_GTExV8_st","Imputed_GTExV8_ut","Imputed_GTExV8_xt","Imputed_PsychEncode_INT","Imputed_PsychEncode_Standard");

    WholeDataTypes2=c("GTExV8Original_st","GTExV8Original_ut","GTExV8Original_xt","PsychEncodeOriginal_INT","PsychEncodeOriginal_Standard");
    MethodTypes2=c("Original_GTExV8_st","Original_GTExV8_ut","Original_GTExV8_xt","Original_PsychEncode_INT","Original_PsychEncode_Standard");
     for(ii in seq(1,5))
      {
        CorrFile1=paste(mydir,WholeDataTypes1[ii],"_GeneLevelSpearmanCorr.txt",sep="");
        CorrMatrix1=read.table(CorrFile1, sep="",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE,row.names=NULL);#Obtain column names from File By check.names   
        CorrMatrix1=as.data.frame(CorrMatrix1);
        CorrFile2=paste(mydir,WholeDataTypes2[ii],"_GeneLevelSpearmanCorr.txt",sep="");
        CorrMatrix2=read.table(CorrFile2, sep="",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE,row.names=NULL);#Obtain column names from File By check.names   
        CorrMatrix2=as.data.frame(CorrMatrix2);
        MergedCorr=inner_join(x=CorrMatrix1,y=CorrMatrix2,by =c("GeneID"),suffixes = c(".x",".y"));
        MergedCorr=as.data.frame(MergedCorr);
        colnames(MergedCorr)=c("GeneID","Corr1","Corr2");
        MergedCorr[,2]=as.numeric(MergedCorr[,2]);
        MergedCorr[,3]=as.numeric(MergedCorr[,3]);
        MergedCorr_Dim=dim(MergedCorr);
        corr=signif(cor(MergedCorr[,2],MergedCorr[,3],use="complete.obs"),4);;
        plot(MergedCorr$Corr1, MergedCorr$Corr2, main=paste("Size",MergedCorr_Dim[1],"_Corr",corr,sep=""),xlab=MethodTypes1[ii], ylab=MethodTypes2[ii], pch=19,cex.lab=0.8);
        abline(a=0,b=1,lty=2,col=4);

      }



 

  dev.off(); 


b=0;
if(b)
{

  ConsiseBatchNamelist=c("SKL_10073","SKL_11154","SKL_11694");
  DataTypes=c("FullGenotype_Imputed","Original");


  Haplotypes=c("Haplotype1","Haplotype2"); 
  AllSampleIDInfoFile="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/WGSData_VariantSummary_InferredAncestry.txt";
  AllSampleIDMatrix=read.table(AllSampleIDInfoFile, sep="\t",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE);#Obtain column names from File By check.names  
  InferCEUSampleSubset=AllSampleIDMatrix[which(AllSampleIDMatrix$InferredAncestry=="CEU"),]
  CEUWGSID=InferCEUSampleSubset$WGSID;
  print(length(CEUWGSID));
  print(CEUWGSID[1:5]);

  for(iBatch in seq(1,3))#3
  {
    MergedPredictedGeneExpression=NULL;
    TotalGeneExpression=NULL;
    for(iType in seq(1,2))#2
    {
     PredictedExpressionFile=paste("/home/hanl3/cmc/scratch/Predicted_ActualGeneExpressionMerged/PsychEncodePredictionEx/PredictedValues/",ConsiseBatchNamelist[iBatch],"PsychEncode_PredictedGeneExpression_",Haplotypes[iType],"_Imputed.txt.gz",sep="");
     con<-file(PredictedExpressionFile,'rt');
     PredictedGeneExpression=read.table(con, sep="",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE,row.names=NULL);#Obtain column names from File By check.names   
      print(PredictedExpressionFile); 
     PredictedGeneExpression =as.data.frame(PredictedGeneExpression);
     GeneIDColNames=colnames(PredictedGeneExpression);
     GeneIDColNames=str_replace_all(GeneIDColNames, "[.]", "-");#replace "." into "-"
     colnames(PredictedGeneExpression)=GeneIDColNames; 
     GeneIDColNames=as.vector(GeneIDColNames);
     print(length(GeneIDColNames));
     OverlapID=intersect(GeneIDColNames,DuplicateOutliers);
     print(GeneIDColNames[c(1:10)]);
      print(length(OverlapID));
     print(dim(PredictedGeneExpression));
     GeneIDRowMatrix=PredictedGeneExpression[,c(1:8)];#Obtain Gene Name column information
     GeneIDRowMatrixColNames=colnames(GeneIDRowMatrix);
     print(GeneIDRowMatrixColNames);
       PredictedGeneExpression=PredictedGeneExpression[,(names(PredictedGeneExpression)%in%CEUWGSID)];#Keep CEU samples
       print(dim(PredictedGeneExpression));
       NormalPredictedGeneExpression=PredictedGeneExpression[,!(names(PredictedGeneExpression)%in%DuplicateOutliers)];#Remove duplicate outlier individuals
       print(dim(NormalPredictedGeneExpression));
       PredictedGeneExpressionWithoutGeneID=NormalPredictedGeneExpression[,!(names(NormalPredictedGeneExpression)%in%MissingOutlierCEUs)];#Remove Individuals with missing rate >=0.01 
       print(dim(PredictedGeneExpressionWithoutGeneID));
        PredictedGeneExpressionWithoutGeneID_Dim=dim(PredictedGeneExpressionWithoutGeneID);

        GeneExpressionWithoutGeneIDNames=colnames(PredictedGeneExpressionWithoutGeneID);
 
        GeneExpressionWithID=as.data.frame(cbind(GeneIDRowMatrix,PredictedGeneExpressionWithoutGeneID));
        colnames(GeneExpressionWithID)=c(GeneIDRowMatrixColNames,GeneExpressionWithoutGeneIDNames);
        print(colnames(GeneExpressionWithID)[1:10]);
        if(iType==1)
        {
           HyploType1PredictedGeneExpression=GeneExpressionWithID;
        }
        else#Chr     WGSBatch        PredictionMethods       BrainsTissues   Source  GeneID  NumberofLociInWeightModel       NumberofLociInActualModel
        {
            MergedPredictedGeneExpression=inner_join(x= HyploType1PredictedGeneExpression,y=GeneExpressionWithID,by =c("Chr","WGSBatch","PredictionMethods","BrainsTissues","GeneID"),suffixes = c(".x",".y"));
            print(dim(MergedPredictedGeneExpression));
         }
          close(con);
         MergedPredictedGeneExpression=as.data.frame(MergedPredictedGeneExpression);
         MergedPredictedGeneExpression_Subset=MergedPredictedGeneExpression[, !(colnames(MergedPredictedGeneExpression) %in% c("Source.x","NumberofLociInWeightModel.x","NumberofLociInActualModel.x","Source.y","NumberofLociInWeightModel.y","NumberofLociInActualModel.y"))]
       }
       print(dim(MergedPredictedGeneExpression_Subset));
      GeneIDRowMatrix=MergedPredictedGeneExpression_Subset[,c(1:5)];#Obtain Gene Name column information
      GeneIDRowMatrixColNames=colnames(GeneIDRowMatrix);
      TotalGeneExpression=MergedPredictedGeneExpression_Subset[,!(colnames(MergedPredictedGeneExpression_Subset) %in% c("Chr","WGSBatch","PredictionMethods","BrainsTissues","GeneID"))];
       print("Combined Matrix");
      print(dim(TotalGeneExpression));
       FullColNames=colnames(TotalGeneExpression);
       GeneExpressionDim=dim(TotalGeneExpression);

         TotalGeneExpression[,1:GeneExpressionDim[2]] <- sapply(TotalGeneExpression[,1:GeneExpressionDim[2]], as.numeric);
         TotalGeneExpressionINT= t(apply(TotalGeneExpression, 1, rankNorm));# The result from the apply() had to be transposed using t() to get the same layout as the input matrix A  
         TotalGeneExpressionZScore=t(apply(TotalGeneExpression, 1,scale));
         TotalGeneExpressionINTDim=dim(TotalGeneExpressionINT);
         HalfColumn=TotalGeneExpressionINTDim[2]/2;
         print(HalfColumn);

         A1Names=FullColNames[1:HalfColumn];
         A2Names=FullColNames[(HalfColumn+1):TotalGeneExpressionINTDim[2]];
         A1Names=gsub(".x", "",A1Names);
         A2Names=gsub(".y", "",A2Names);
         iUnequal=0;
         for(iNum in seq(1:HalfColumn))
         {
           if(A1Names[iNum] !=A2Names[iNum])
           {
             iUnequal= iUnequal+1;
           }
         }
         print(iUnequal);
         for(iType in seq(1,2))
         {
           StartPos=(iType-1)*HalfColumn;
           EndPos=iType*HalfColumn;
           GeneExpressionINT=TotalGeneExpressionINT[,c((StartPos+1):EndPos)];
           GeneExpressionZScore=TotalGeneExpressionZScore[,c((StartPos+1):EndPos)];
            GeneExpressionINTMerged=as.data.frame(cbind(GeneIDRowMatrix, GeneExpressionINT));#INT score merged
           colnames(GeneExpressionINTMerged)=c(GeneIDRowMatrixColNames,A1Names);;
           print(dim(GeneExpressionINTMerged));
           PredictedExpressionFile==paste("/home/hanl3/cmc/scratch/Predicted_ActualGeneExpressionMerged/PsychEncodePredictionEx/PredictedValues_Zscore/PsychEncodeWeightBased",ConsiseBatchNamelist[iBatch],"_",Haplotypes[iType],"_GenePredictedExCEUBasedINTZScore.txt",sep="");
           write.table(GeneExpressionINTMerged, file=PredictedExpressionFile, append = FALSE, quote =FALSE, sep = "\t", eol = "\n",col.names=TRUE,row.names=FALSE);

           GeneExpressionIZScoreMerged=as.data.frame(cbind(GeneIDRowMatrix,GeneExpressionZScore)); #Standard score merged
            colnames(GeneExpressionIZScoreMerged)=c(GeneIDRowMatrixColNames,A1Names);
            PredictedExpressionFile=paste("/home/hanl3/cmc/scratch/Predicted_ActualGeneExpressionMerged/PsychEncodePredictionEx/PredictedValues_Zscore/PsychEncodeWeightBased",ConsiseBatchNamelist[iBatch],"_",Haplotypes[iType],"_GenePredictedExCEUBasedStandardZScore.txt",sep="");
           write.table(GeneExpressionIZScoreMerged, file=PredictedExpressionFile, append = FALSE, quote =FALSE, sep = "\t", eol = "\n",col.names=TRUE,row.names=FALSE);
        }
  } 
}


