#! /usr/local/R/3.2.0/x86_64/intel14/nonet/bin/Rscript  --vanilla
view = 2;
  remove_outliers <- function(x, na.rm = TRUE, ...) {
  qnt <- quantile(x, probs=c(.25, .75), na.rm = na.rm, ...)
   H <- 3 * IQR(x, na.rm = na.rm)
   y <- x
   Lowerbound=qnt[1] - H;
   UpperBound=qnt[2] + H;
   Bound=c(Lowerbound[[1]],UpperBound[[1]]);
   return(Bound);
   #y[x < (qnt[1] - H)] <- NA
  # y[x > (qnt[2] + H)] <- NA
  # y
  }


  #  /data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/SKL_10073IndividualBasedVariansSummary.txt
  # /data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/SKL_11694IndividualBasedVariansSummary.txt
  # /data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/SKL_11154IndividualBasedVariansSummary.txt

  myDir="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/";
  Batches=c("SKL_10073","SKL_11694","SKL_11154");

    Outputfile=paste(myDir,"/CMC_HBCCThreeBatchesWGSBasedVariants_SummaryHistogramPlot.pdf",sep="");
    pdf(Outputfile, 7, 5);
   # jpeg(Outputfile, width = 480, height = 480, units = "px", pointsize = 12);
    par(mar=c(5,7,1.5,0.25), mgp=c(3,1,0), mfrow=c(3,1),oma=c(0,0,0,0));
    linewide=2;
    axixwide=0.5;
    tickwide=0.5;
   cex_size=0.62;
   leg_cex_size=0.8;
   axes=F;
   xaxs="s";
   cex_size1=0.65;
   for(iBatch in seq(1,3))
   {

     WGSVariantSummaryFile=paste(myDir,Batches[iBatch],"IndividualBasedVariansSummary.txt",sep="");
     print(WGSVariantSummaryFile);
     for(iType in seq(4,4))
     {
     VariantSummaryInfo=scan(WGSVariantSummaryFile,skip=iType,what="character",nlines=1);
     variantType=VariantSummaryInfo[1];
     VariantSummaryInfo=VariantSummaryInfo[10:length(VariantSummaryInfo)];
     VariantSummaryInfo=as.numeric(VariantSummaryInfo);
     Freq_Variants=hist(VariantSummaryInfo,breaks=20,plot=F);
     hist(VariantSummaryInfo,breaks=20,main=Batches[iBatch], xlab=variantType, border="blue", 
     col="green",las=1);
      OutVals=remove_outliers(VariantSummaryInfo); 
       print(OutVals);
      abline(v=OutVals[1],lty=2,col=2); 
      abline(v=OutVals[2],lty=2,col=4); 
    }
  }
   dev.off(); 
