 #!/usr/bin/perl -w
 use strict;
use warnings;


sub uniq {
    [ keys { map { $_ => 1 } @{$_[0]} } ]
};

#matrix transpose
sub pivot {
    my @src = @_;

    my $max_col = 0;
    $max_col < $#$_ and $max_col = $#$_ for @src;

    my @dest;
    for my $col (0..$max_col) {
        push @dest, [map {$src[$_][$col] // ''} (0..$#src)];
    }

    return @dest;
}
#    18,842 genes* 251 individuals /home/hanl3/cmc/scratch/Predicted_ActualGeneExpressionMerged/CMC_HBCCActualGeneExpression/CMC_DLPFC_ActualGeneExInferredCEUBasedStandardZScore.txt
#     17,251genes * 144 individuals  /home/hanl3/cmc/scratch/Predicted_ActualGeneExpressionMerged/CMC_HBCCActualGeneExpression/HBCC_DLPFC_ActualGeneExInferredCEUBasedStandardZScore.txt  
  my %ActualGenes=();
  my %ActualBatchSharedGenes=();
  my %ActualInds=();
  my @BatchTypes=("CMC_DLPFC","HBCC_DLPFC");
  my @ActualGeneList=();
  for(my $iBatch=0;$iBatch<@BatchTypes;$iBatch++)
  {
       my $iLineNum=0;
       my $ActualGeneExpressionFile="/home/hanl3/cmc/scratch/Predicted_ActualGeneExpressionMerged/CMC_HBCCActualGeneExpression/$BatchTypes[$iBatch]_ActualGeneExInferredCEUBasedStandardZScore.txt";
       print "$ActualGeneExpressionFile\n";
       open(INPUT,$ActualGeneExpressionFile) || die "can’t open $ActualGeneExpressionFile";
       while (<INPUT>)
       {
        chomp;
        if($iLineNum==0) 
        {
          $iLineNum++; 
          next;
        }
        my @cols=split(/\s++/,$_,2);
         if(!defined($ActualGenes{$cols[0]}))
         {
           $ActualGenes{$cols[0]}=1;
         }
         else
         {
          $ActualGenes{$cols[0]}++;
           $ActualBatchSharedGenes{$cols[0]}=1;
           push @ActualGeneList,$cols[0];
         } # last if($iLineNum>200);
          $iLineNum++; 
       }
       close INPUT;
  }

   my $NumberofAllActualGenes=keys %ActualGenes;
   my  $NumberofAllActualSharedGenes=keys %ActualBatchSharedGenes;
   print "Actual Genes:$NumberofAllActualGenes\t$NumberofAllActualSharedGenes\n";
 
   my @BatchList=("SKL_10073","SKL_11154","SKL_11694");
   my @PredictionMethods=("Standard","INT");  
   my @ImputedDataTypes=("FullGenotype_Imputed","Original");
   for(my $iIp=0;$iIp<2;$iIp++)
  {
    my %PredictedGenes=();
    for(my $iM=0;$iM<@PredictionMethods;$iM++)
    {
      my %BatchBasedGenes=();
     for(my $iB=0;$iB<@BatchList;$iB++)
     {
     my $NumberofUniqueValues=0;
      my $iLineNum=0;
#PsychEncodeWeightBasedSKL_11154_FullGenotype_Imputed_GenePredictedExCEUBasedStandardZScore.txt  
      my $PredictedGeneExpressionFile="/home/hanl3/cmc/scratch/Predicted_ActualGeneExpressionMerged/PsychEncodePredictionEx/PredictedValues_Zscore/PsychEncodeWeightBased$BatchList[$iB]_$ImputedDataTypes[$iIp]_GenePredictedExCEUBased$PredictionMethods[$iM]ZScore.txt";
      print "$PredictedGeneExpressionFile\n";
       open(INPUT,$PredictedGeneExpressionFile) || die "can’t open $PredictedGeneExpressionFile";
      while (<INPUT>)
      {
        chomp;
        if($iLineNum==0) 
        {
          $iLineNum++; 
          next;
        }
        my @cols=split(/\s++/,$_);
        my $GeneID=$cols[5];
        splice @cols,0,8;
        my $unique_cols = uniq(\@cols);
        my $NumberofElements=scalar(@{$unique_cols});
        if($NumberofElements==1)
        {
          $NumberofUniqueValues++;
          next;
        }
       if(!defined($PredictedGenes{$GeneID}))
        {
           $PredictedGenes{$GeneID}=1;
        }
         else
         {
          $PredictedGenes{$GeneID}++;
         }
        $BatchBasedGenes{$GeneID}=1;
         $iLineNum++;
      }
      close INPUT;
     print "NumberofUniqueValues:$NumberofUniqueValues\n";
     }
     my $NumberofGenes=keys %BatchBasedGenes;
    print "$PredictionMethods[$iM]$ImputedDataTypes[$iIp]:$NumberofGenes\n";
   }
     my %SharedPredictedGenes=();
     my %SharedPredictedActualGenes=();
     foreach my $GeneID (keys %PredictedGenes)
     { 
      if($PredictedGenes{$GeneID}>=1)
      {
        $SharedPredictedGenes{$GeneID}=1;
         if(defined($ActualBatchSharedGenes{$GeneID}))
          {
           $SharedPredictedActualGenes{$GeneID}=1;
          }
      }

    }
    my $NumberofAllPredictedGenes=keys %PredictedGenes;
    my  $NumberofAllPredictedSharedGenes=keys %SharedPredictedGenes;
    my $NumberofPredictedActualSharedGenes=keys %SharedPredictedActualGenes; 
    print "Predicted Genes:$NumberofAllPredictedGenes\t$NumberofAllPredictedSharedGenes\t$NumberofPredictedActualSharedGenes\n";
    

    my @SelectedGenelist=();
    for(my $ii=0;$ii<@ActualGeneList;$ii++)
    { 
       if(defined($SharedPredictedActualGenes{$ActualGeneList[$ii]}))
       {
         push @SelectedGenelist,$ActualGeneList[$ii];
       }
    }
     my @MergedGeneExpression=();
     my @MergedIndIDs=();
     my %GeneIDIND_GeneExpression=();
     my %WGSID_ActualBatch=();
     for(my $iBatch=0;$iBatch<@BatchTypes;$iBatch++)
     {
       my @IndIDs_InBatch=();
       my $iLineNum=0;
       my $ActualGeneExpressionFile="/home/hanl3/cmc/scratch/Predicted_ActualGeneExpressionMerged/CMC_HBCCActualGeneExpression/$BatchTypes[$iBatch]_ActualGeneExInferredCEUBasedStandardZScore.txt";
       print "$ActualGeneExpressionFile\n";
       open(INPUT,$ActualGeneExpressionFile) || die "can’t open $ActualGeneExpressionFile";
       while (<INPUT>)
       {
          chomp;
          if($iLineNum==0)
          {
            $iLineNum++;
            my @cols=split(/\s++/,$_);
            shift(@cols);#remove the first element for the array
            push  @MergedIndIDs,@cols;
            @IndIDs_InBatch=@cols;
             for(my $ii=0;$ii<@IndIDs_InBatch;$ii++)
             {
               $WGSID_ActualBatch{$IndIDs_InBatch[$ii]}=$BatchTypes[$iBatch];
             }
            next;
           }
           my @cols=split(/\s++/,$_,2);
           next if(!defined($SharedPredictedActualGenes{$cols[0]}));
           my @AllGeneExpression=split(/\s++/,$cols[1]); 
           for(my $ii=0;$ii<@IndIDs_InBatch;$ii++)
           {
             $GeneIDIND_GeneExpression{$cols[0]}{$IndIDs_InBatch[$ii]}=$AllGeneExpression[$ii];
            } 
           $iLineNum++;
        }
        close INPUT;
      }

       my $OutFile="/home/hanl3/cmc/scratch/Predicted_ActualGeneExpressionMerged/CMC_HBCCActualPredictedMergedGeneExpression/PsychEncode$ImputedDataTypes[$iIp]_MergeddActualPredictedExpression.txt";
      open OUT, ">$OutFile" or die "Can't open Output file:$OutFile!";
      print OUT "WGSID\tDataType\tMethod\tBatch";
      for(my $iG=0;$iG<@SelectedGenelist;$iG++)
       {
        print OUT "\t$SelectedGenelist[$iG]";
       } 
      print OUT "\n";
      my %AllSelectedWGSID=();
      for(my $iInd=0;$iInd<@MergedIndIDs;$iInd++)
      {
        $AllSelectedWGSID{$MergedIndIDs[$iInd]}=1;
         print OUT "$MergedIndIDs[$iInd]\tActual\tNA\t$WGSID_ActualBatch{$MergedIndIDs[$iInd]}";
         for(my $iG=0;$iG<@SelectedGenelist;$iG++)
         {
           print OUT "\t$GeneIDIND_GeneExpression{$SelectedGenelist[$iG]}{$MergedIndIDs[$iInd]}";
         }
        print OUT "\n";
      }
    for(my $iM=0;$iM<@PredictionMethods;$iM++)
    {
      my %GeneIDIND_PredictedGeneExpression=();
      my %WGSID_PredictedBatch=();
        for(my $iB=0;$iB<@BatchList;$iB++)
        {
         my @SelectedWGSIDs=(); 
         my @SelectedColumns=();
         my $iLineNum=0;
         my $PredictedGeneExpressionFile="/home/hanl3/cmc/scratch/Predicted_ActualGeneExpressionMerged/PsychEncodePredictionEx/PredictedValues_Zscore/PsychEncodeWeightBased$BatchList[$iB]_$ImputedDataTypes[$iIp]_GenePredictedExCEUBased$PredictionMethods[$iM]ZScore.txt";
    #      print "hh  $PredictedGeneExpressionFile\n";
          open(INPUT,$PredictedGeneExpressionFile) || die "can’t open $PredictedGeneExpressionFile";
          while (<INPUT>)
          {
            chomp;
            if($iLineNum==0)
             {
               $iLineNum++;
               my @cols=split(/\s++/,$_);
               for(my $iCol=0;$iCol<@cols;$iCol++)
               {
                  if(defined($AllSelectedWGSID{$cols[$iCol]}))
                 {
                   $WGSID_PredictedBatch{$cols[$iCol]}=$BatchList[$iB];
                   push @SelectedColumns,$iCol;
                   push @SelectedWGSIDs,$cols[$iCol];
                  }
                }
               next;
             }
             my @cols=split(/\s++/,$_,8);
             next if(!defined($SharedPredictedActualGenes{$cols[5]}));
             @cols=split(/\s++/,$_);
             for(my $jj=0;$jj<@SelectedColumns;$jj++)           
            {
              $GeneIDIND_PredictedGeneExpression{$cols[5]}{$SelectedWGSIDs[$jj]}=$cols[$SelectedColumns[$jj]];
            #  print "$jj\t$SelectedWGSIDs[$jj]\t$SelectedColumns[$jj]\t$cols[$SelectedColumns[$jj]]\n";
               my $iLength1=scalar(@SelectedWGSIDs);
               my $iLength2=scalar(@SelectedColumns);   
               my $iLength3=scalar(@cols); 
               if(!defined($cols[$SelectedColumns[$jj]]))
                {
                 print "$iLength1\t$iLength2\t$iLength3\t$SelectedColumns[$jj]\t$cols[$SelectedColumns[$jj]]\tThere\n";
                 last;
                 }
            }
           # last if($iLineNum>2);
            $iLineNum++;
           }
          close INPUT;
        }
        for(my $iInd=0;$iInd<@MergedIndIDs;$iInd++)
        {
           print OUT "$MergedIndIDs[$iInd]\t$ImputedDataTypes[$iIp]\t$PredictionMethods[$iM]\t$WGSID_PredictedBatch{$MergedIndIDs[$iInd]}";
           for(my $iG=0;$iG<@SelectedGenelist;$iG++)
           {
              if(!defined($GeneIDIND_PredictedGeneExpression{$SelectedGenelist[$iG]}{$MergedIndIDs[$iInd]}))
               {        
                 print OUT "\tNA";     
               }
              else
              {
                print OUT "\t$GeneIDIND_PredictedGeneExpression{$SelectedGenelist[$iG]}{$MergedIndIDs[$iInd]}";
              }
            }
           print OUT "\n";
        }
      }
  close OUT;
}

