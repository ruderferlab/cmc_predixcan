#! /usr/local/R/3.2.0/x86_64/intel14/nonet/bin/Rscript  --vanilla
library("dplyr");
library("tidyverse");

view = 2;
  remove_outliers <- function(x, na.rm = TRUE, ...) {
  qnt <- quantile(x, probs=c(.25, .75), na.rm = na.rm, ...)
   H <- 3 * IQR(x, na.rm = na.rm)
   y <- x
   Lowerbound=qnt[1] - H;
   UpperBound=qnt[2] + H;
   Bound=c(Lowerbound[[1]],UpperBound[[1]]);
   return(Bound);
   #y[x < (qnt[1] - H)] <- NA
  # y[x > (qnt[2] + H)] <- NA
  # y
  }

   Outliers=c("MSSM-DNA-PFC-375","MSSM-DNA-PFC-269","CMC-HBCC-DNA-ACC-6052","CMC-HBCC-DNA-ACC-5669","CMC-HBCC-DNA-ACC-4237","CMC-HBCC-DNA-ACC-5646",
    "CMC-HBCC-DNA-ACC-5777","CMC-HBCC-DNA-ACC-5682","CMC-HBCC-DNA-ACC-4284","CMC-HBCC-DNA-ACC-4137",
    "CMC-HBCC-DNA-ACC-6009","CMC-HBCC-DNA-ACC-6056","CMC-HBCC-DNA-ACC-4029","CMC-HBCC-DNA-ACC-4200",
    "CMC-HBCC-DNA-ACC-4074","CMC-HBCC-DNA-ACC-4204","CMC-HBCC-DNA-ACC-5654","CMC-HBCC-ACC-DNA-4235");
#Input file:
# /data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/WGSData_VariantSummary_Ancestry.txt

  myDir="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs/";
  Batches=c("SKL_10073","SKL_11694","SKL_11154");

    Outputfile=paste(myDir,"/PCAnalysisBasedonWGS_Scatterplot_OnlyCEUs.pdf",sep="");
    pdf(Outputfile, 7, 9);
   # jpeg(Outputfile, width = 480, height =700, units = "px", pointsize = 12);
    par(mar=c(5,7,1.5,0.25), mgp=c(3,1,0), mfrow=c(1,1),oma=c(0,0,0,0));
    linewide=2;
    axixwide=0.5;
    tickwide=0.5;
   cex_size=0.62;
   leg_cex_size=0.8;
   axes=F;
   xaxs="s";
   cex_size1=0.65;
  
   #WGSID   Sex     Batch   NumbofHyterrozyogotes_SNV       NumHyterrozyogotes_INDEL        Disease Ancestry  PC1     PC2
    WGSVariantSummaryFile=paste(myDir,"WGSData_VariantSummary_Ancestry.txt",sep="");
    WGSVariantSummaryMatrix=read.table(WGSVariantSummaryFile, sep="\t",colClasses="character",na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE);
    WGSVariantSummaryMatrix[,4]=as.numeric(WGSVariantSummaryMatrix[,4]);
    WGSVariantSummaryMatrix[,5]=as.numeric(WGSVariantSummaryMatrix[,5]);
    WGSVariantSummaryMatrix[,8]=as.numeric(WGSVariantSummaryMatrix[,8]);
    WGSVariantSummaryMatrix[,9]=as.numeric(WGSVariantSummaryMatrix[,9]);  
    WGSVariantSummaryMatrix$AncestryValue=NA;
    WGSVariantSummaryMatrix$AncestryValue[WGSVariantSummaryMatrix$Ancestry=="African-American"]=1;
    WGSVariantSummaryMatrix$AncestryValue[WGSVariantSummaryMatrix$Ancestry=="Asian"]=2;
    WGSVariantSummaryMatrix$AncestryValue[WGSVariantSummaryMatrix$Ancestry=="Caucasian"]=3;
    WGSVariantSummaryMatrix$AncestryValue[WGSVariantSummaryMatrix$Ancestry=="Hispanic"]=4;
    WGSVariantSummaryMatrix$AncestryValue[WGSVariantSummaryMatrix$Ancestry=="(Multiracial)"]=5;
    WGSVariantSummaryMatrix$AncestryValue=as.numeric(WGSVariantSummaryMatrix$AncestryValue); 
    #WGSVariantSummaryMatrix=WGSVariantSummaryMatrix[WGSVariantSummaryMatrix$Ancestry=="Caucasian",];#limit self-reported CEUs

 #   WGSVariantSummaryMatrix=WGSVariantSummaryMatrix[!(WGSVariantSummaryMatrix$WGSID%in%Outliers),];#Remove outliers judged by Ancestry.

    #  recode(WGSVariantSummaryMatrix$Ancestry,"African-American"=1,"Asian"=2,"Caucasian"=3,"Hispanic"=4,"(Multiracial)"=5);
    # WGSVariantSummaryMatrix$Ancestry=as.numeric(WGSVariantSummaryMatrix$Ancestry); 
   # print(WGSVariantSummaryMatrix$Ancestry);
     plot(WGSVariantSummaryMatrix$PC1,WGSVariantSummaryMatrix$PC2,main=NULL,cex.lab=0.8,ylab="PC2", xlab="PC1", pch=WGSVariantSummaryMatrix$AncestryValue-1, col =WGSVariantSummaryMatrix$AncestryValue);
     legend(x=0.04,y=0.3,c("African-American","Asian","Caucasian","Hispanic","(Multiracial)"),cex=.8,col=c(1:5),pch=c(0:4),bty = "n");
     #abline(v=-0.017,lty=2,col=6);
     # abline(h=0.035,lty=2,col=6);
    segments(-0.017,-0.05,-0.017,0.035,lty=2,col=6);
    segments(-0.05,0.035,-0.017,0.035,lty=2,col=6);
     
      WGSVariantSummaryMatrix$InferredAncestry="Non-CEU";
      WGSVariantSummaryMatrix$InferredAncestry[which(WGSVariantSummaryMatrix$PC1<=-0.017 & WGSVariantSummaryMatrix$PC2<=0.035)]="CEU";  
      InferredAncestryFile=paste(myDir,"WGSData_VariantSummary_InferredAncestry.txt",sep="");
      write.table(WGSVariantSummaryMatrix, file=InferredAncestryFile, append = FALSE, quote =FALSE, sep = "\t",
            eol = "\n",col.names=TRUE,row.names=FALSE);



   dev.off();
