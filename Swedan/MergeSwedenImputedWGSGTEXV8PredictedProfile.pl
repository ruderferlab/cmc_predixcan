#!/usr/bin/perl -w
 use strict;
  use 5.012;
 use lib "/home/hanl3/perl5/module/lib/site_perl/5.14.2";
 use Text::CSV_PP;
  use Tie::File; 
 use POSIX qw(WNOHANG);
 use warnings;
use Scalar::Util qw(looks_like_number);

# UTMOST, XT_Scan,PrediXcan two brain tissues' preciction model which based on GRch38 assembly
# /data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/gtex_v8_st_Brain_Anterior_cingulate_cortex_BA24.txt
#       gtex_v8_st_Brain_Frontal_Cortex_BA9.txt  gtex_v8_ut_Brain_Anterior_cingulate_cortex_BA24.txt
#       gtex_v8_ut_Brain_Frontal_Cortex_BA9.txt  gtex_v8_xt_Brain_Anterior_cingulate_cortex_BA24.txt  gtex_v8_xt_Brain_Frontal_Cortex_BA9.txt
#/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTEXV8_ACC_DLPFCPredictedExSNVs
#SKL_10073ShareV8MultiplyModel.CommonSNVs_PassWGS
#SKL_11154ShareV8MultiplyModel.CommonSNVs_PassWGS
#SKL_11694ShareV8MultiplyModel.CommonSNVs_PassWGS

  my @WGSBatches=("Eli-s234","Eli-sw56","Eli-swe1");
   my @WGSBatchNames=("Eli-s234","Eli-sw56","Eli-swe1");

  my @PredictionMethods=("st","ut","xt");
  my @BrainsTissues=("Brain_Frontal_Cortex_BA9","Brain_Anterior_cingulate_cortex_BA24");
  my @BrainsTissuesNames=("DLPFC_BA9","ACC_BA24");
 
 my %Chr_GeneID=();#Chr_ geneid
 my %AllUsedSNVsInfo=(); 

my $iSpecificBatch=$ARGV[0];#0,1,2
my $iSpecificMethod=$ARGV[1];#0,1,2
my $iSpecificTussue=$ARGV[2];#0,1
my $iSpecificGType=$ARGV[3];#0,1,2

#Obtain Gene list
#/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTExV8_Weight/*_*/$gene.wgt.score

   my @GenoTypes=("FullGenotype","Haplotype1","Haplotype2"); 

  for(my $iBatch=$iSpecificBatch;$iBatch<$iSpecificBatch+1;$iBatch++)
   {
     for(my $iType=$iSpecificGType;$iType<$iSpecificGType+1;$iType++)
     {  
      for(my $iM=$iSpecificMethod;$iM<$iSpecificMethod+1;$iM++)
      {
       for(my $iT=$iSpecificTussue;$iT<$iSpecificTussue+1;$iT++)
       {
       my $iNumberofFile=0;
        my %FullChr_GeneID=();
        my $mydir="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTExV8_Weight/$PredictionMethods[$iM]_$BrainsTissuesNames[$iT]";
            opendir(DIR, $mydir) || die "Can't open $mydir: $!";
            while (my $weightFile=readdir (DIR)) {
            next if ($weightFile =~ m/^\./);
            my $geneID=$weightFile;
            $geneID =~ s/\.wgt\.score//ig;
            my $scoreWeightFile="$mydir/$weightFile"; # print "$mydir/$weightFile\n";
           my $iLineNumber=0;
            my $ScoreFile="$mydir/$weightFile";
            open(INPUT,$ScoreFile) || die "can’t open $ScoreFile";
            while (<INPUT>)
            {   
              chomp;
              if($iLineNumber==0)
              {   
               my @cols=split(/\t/,$_,2);
               my @ChrPos=split(/:/,$cols[0]);
               if(!defined($FullChr_GeneID{$ChrPos[0]}))
               {   
                 $FullChr_GeneID{$ChrPos[0]}=$geneID;
               }   
              else
               {   
                 $FullChr_GeneID{$ChrPos[0]}.="\t$geneID";
               }   
             }   
              $iLineNumber++;
            }   
           close INPUT;
           $iNumberofFile++;
        #    last if($iNumberofFile>10);
         }   
         closedir (DIR);

             my $OutZFile="/data/ruderferlab/projects/cmc/scratch/sweden/MergedTranscriptPrediction/GTExV8PredictionEx/$WGSBatchNames[$iBatch]GTEXV8_PredictedGeneExpression$PredictionMethods[$iM]_$BrainsTissuesNames[$iT]_$GenoTypes[$iType]_Imputed.txt";
             open OUTZ, ">$OutZFile" or die "Can't open Output file:$OutZFile!";   
            print "$OutZFile\n";
      
            my @AllIndIDList=(); 
            for(my $iChr=1;$iChr<=22;$iChr++)
           {
             next if(!defined($FullChr_GeneID{$iChr}));
             my @ChrbasedGeneIDs=split(/\t/,$FullChr_GeneID{$iChr});
            my %SpecificChrBasedGeneIDs=();
              for(my $ii=0;$ii<@ChrbasedGeneIDs;$ii++)
             {
               $SpecificChrBasedGeneIDs{$ChrbasedGeneIDs[$ii]}=1;
             }

             my $iNumberofFile=0;
            my %Chr_GeneID=();
            my %GeneID_NumberofLociInModel=();
            my $mydir="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTExV8_Weight/$PredictionMethods[$iM]_$BrainsTissuesNames[$iT]";
            opendir(DIR, $mydir) || die "Can't open $mydir: $!";
            while (my $weightFile=readdir (DIR)) {
            next if ($weightFile =~ m/^\./);
            my $geneID=$weightFile;
            $geneID =~ s/\.wgt\.score//ig;
            next if(!defined($SpecificChrBasedGeneIDs{$geneID}));#if not defined the gene in the Chromosome,ignore  
          my $scoreWeightFile="$mydir/$weightFile"; # print "$mydir/$weightFile\n";
           my $iLineNumber=0;
            my $ScoreFile="$mydir/$weightFile";
            open(INPUT,$ScoreFile) || die "can’t open $ScoreFile";
            while (<INPUT>)
            {
              chomp;
              if($iLineNumber==0)
              {
               my @cols=split(/\t/,$_,2);
               my @ChrPos=split(/:/,$cols[0]);
               if(!defined($Chr_GeneID{$ChrPos[0]}))
               {
                 $Chr_GeneID{$ChrPos[0]}=$geneID;
               } 
              else
               {
                 $Chr_GeneID{$ChrPos[0]}.="\t$geneID";
               }
             }
              $iLineNumber++; 
            }         
           close INPUT;
           $GeneID_NumberofLociInModel{$geneID}=$iLineNumber;
           $iNumberofFile++;
         }
         closedir (DIR);
            my %GeneID_AllInfo=();
            my $NumberofGenes=keys %GeneID_NumberofLociInModel;
            print "$iChr\tNumber of Genes:$NumberofGenes\n";
            $iNumberofFile=0;         
            $mydir="/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction/GTExV8_ProfileV2/$PredictionMethods[$iM]_$BrainsTissuesNames[$iT]";
            opendir(DIR, $mydir) || die "Can't open $mydir: $!";
            while (my $profileFile=readdir (DIR)) {
            next if ($profileFile =~ m/^\./);
            next if($profileFile !~ m/profile$/);            
           #/$PredictionMethods[$iM]_$BrainsTissuesNames[$iT]/$WGSBatches[$iBatch]_$GenoTypes[$iType]_$geneID
           if ($profileFile =~ m/profile$/ && $profileFile =~ m/^$WGSBatches[$iBatch]\_$GenoTypes[$iType]\_/)
           {
               my $GeneIDFull=$profileFile;
               $GeneIDFull =~ s/.profile//ig;#SKL_11694_OriginalWGS_ENSG00000283538
               $GeneIDFull=~ s/^$WGSBatches[$iBatch]\_$GenoTypes[$iType]\_//ig;              
             next if(!defined($GeneID_NumberofLociInModel{$GeneIDFull}));
             my $iLineNumber=0; 
             my $iEffectiveNumber=0;
            my %WGSID_Expression=();
             my $iDependentLociFullNumber=-1;
             my $iDependentLociNumber=-1;
             my $profileFileName="$mydir/$profileFile";
#              print "$profileFileName\n";
             open(INPUT,$profileFileName) || die "can’t open $profileFileName";
             while (<INPUT>)
             {
               chomp;
               if($iLineNumber==0)
               {
                 $iLineNumber++;
                 next;
               }
              $_ =~ s/^\s+|\s+$//g;#Remove space around the string 
                my @cols=split(/\s+/,$_);
 
             if($iEffectiveNumber==0)
               {
               $iDependentLociFullNumber=$cols[3]/2;
               $iDependentLociNumber=$cols[4];
               }
              if($iNumberofFile==0 && $iChr==1)
              {
               push  @AllIndIDList,$cols[1];
              }
               $WGSID_Expression{$cols[1]}=$cols[5];
               $iEffectiveNumber++; 
             }
            close INPUT;     
            $iNumberofFile++;
            if($iChr==1 && $iNumberofFile==1)#make sure to only satisfy one condition
           {  
              print OUTZ "Chr\tWGSBatch\tPredictionMethods\tBrainsTissues\tSource\tGeneID\tNumberofLociInWeightModel\tFullNumber\tNumberofLociInActualModel";
              for(my $iInd=0;$iInd<@AllIndIDList;$iInd++)
              {
                print OUTZ "\t$AllIndIDList[$iInd]";
               }
                print OUTZ "\n";
            }
        print OUTZ "$iChr\t$WGSBatches[$iBatch]\tNA\tNA\tImputed$GenoTypes[$iType]\t$GeneIDFull\t$GeneID_NumberofLociInModel{$GeneIDFull}\t$iDependentLociFullNumber\t$iDependentLociNumber";
            for(my $iInd=0;$iInd<@AllIndIDList;$iInd++)
             {
                print OUTZ "\t$WGSID_Expression{$AllIndIDList[$iInd]}";
             }
             print OUTZ "\n";
           }       
          }
        closedir (DIR);
       }
      close OUTZ;
      print "Finished\n";
     }
    }
  }
}
exit 0;

