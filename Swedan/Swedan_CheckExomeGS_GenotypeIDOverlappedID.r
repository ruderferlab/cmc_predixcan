#Convert imputed WGS based prediction into corresponing ZSCORE and int zscore

 options(width=300)
 library(magrittr);
 library(RNOmni);
 library(dplyr)
 library(stringr)

  BatchList=c("Eli-s234","Eli-sw56","Eli-swe1");
  ConsiseBatchNamelist=c("Eli-s234","Eli-sw56","Eli-swe1");
  
  AllExomeSampleIDInfoFile="/data/ruderferlab/projects/sweden/data/exome/SwedanExomeSamples.txt";
  AllExomeSampleIDMatrix=read.table(AllExomeSampleIDInfoFile, sep="\t",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE);#Obtain column names from File By check.names  
  ExomeGSID=AllExomeSampleIDMatrix$ExomeSampleID;
  ExomeGSID=str_replace_all(ExomeGSID, "[.]", "-");

  print(length(ExomeGSID));

  RemovedIDInfoFile="/data/ruderferlab/projects/sweden/files/swexm.paper.rm";
  RemovedSampleIDMatrix=read.table(RemovedIDInfoFile, sep="\t",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE);#Obtain column names from File By check.names
  RemovedExGSID=RemovedSampleIDMatrix$RemovedID;
  print(length(RemovedExGSID));

#Eli-s234PsychEncode_PredictedGeneExpression_FullGenotype_Imputed.txt
  print(ExomeGSID[1:5]);
 
  AllGenotypedID=NULL;
  for(iBatch in seq(1,3))#3
  {
     PredictedExpressionFile=paste("/data/ruderferlab/projects/cmc/scratch/sweden/MergedTranscriptPrediction/PsychEncodePredictionEx/",ConsiseBatchNamelist[iBatch],"PsychEncode_PredictedGeneExpression_FullGenotype_Imputed.txt.gz",sep="");
     con<-file(PredictedExpressionFile,'rt');
     PredictedGeneExpression=read.table(con, sep="\t",colClasses="character",check.names=TRUE,nrows=1, na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE,row.names=NULL);#Obtain column names from File By check.names   
     print(PredictedExpressionFile); 
     PredictedGeneExpression =as.data.frame(PredictedGeneExpression);
     GeneIDColNames=as.vector(names(PredictedGeneExpression)); 
    print(attributes(GeneIDColNames));
     GeneIDColNames=str_replace_all(GeneIDColNames, "[.]", "-");#replace "." into "-"
     print(str(GeneIDColNames));
     GeneID=as.vector(GeneIDColNames[-1:-9]);
     print(length(GeneID));
    if(iBatch==1)
    {
       AllGenotypedID=GeneID;
    }
    else
    {
       AllGenotypedID=c(AllGenotypedID,GeneID);
    }
    print (length(AllGenotypedID));
     #colnames(PredictedGeneExpression)=GeneIDColNames; 

   # GeneIDColNames=as.vector(GeneIDColNames);
   # print(length(GeneIDColNames));
     OverlapID=intersect(GeneID,ExomeGSID);
     print(length(OverlapID));
   #  print(dim(PredictedGeneExpression));
   # GeneIDRowMatrix=PredictedGeneExpression[,c(1:9)];#Obtain Gene Name column information
   #  GeneIDRowMatrixColNames=colnames(GeneIDRowMatrix);
 
    # PredictedGeneExpression=PredictedGeneExpression[,(names(PredictedGeneExpression)%in%ExomeGSID)]; 
    # print(dim(PredictedGeneExpression));
      
     # GeneExpression=PredictedGeneExpression[,!(names(PredictedGeneExpression)%in%RemovedExGSID)];
    close(con);
}

    DuplicateElements=AllGenotypedID[duplicated(AllGenotypedID)]; 
   print(DuplicateElements);
    UniqElements=unique(AllGenotypedID);
   print(length(UniqElements));
   # print(DuplicateElements);
   OverlapID=intersect(AllGenotypedID,ExomeGSID);
   
 #  RemainedValues=setdiff(ExomeGSID, OverlapID);
   #print(RemainedValues); 
   print(length(OverlapID));
 #  print(length(RemainedValues)); 
  #  RemainedValues1=setdiff(AllGenotypedID, OverlapID);   
  #print(RemainedValues1[1:500]);
