#Convert imputed WGS based prediction into corresponing ZSCORE and int zscore

 library(magrittr);
 library(RNOmni);
 library(dplyr)
 library(stringr)

BatchList=c("Eli-s234","Eli-sw56","Eli-swe1");
ConsiseBatchNamelist=c("Eli-s234","Eli-sw56","Eli-swe1");


#MSSM-DNA-PFC-56	SKL_10073	0.0043	0.0103	CEU


  EffectiveSampleIDInfoFile="/data/ruderferlab/projects/sweden/data/geno/sweden_Exome_GenotypedShasredIndList.txt";
  EffectiveSampleIDMatrix=read.table(EffectiveSampleIDInfoFile, sep="\t",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE);#Obtain column names from File By check.names  
  
   RemovedIDInfoFile="/data/ruderferlab/projects/sweden/files/swexm.paper.rm";
  RemovedSampleIDMatrix=read.table(RemovedIDInfoFile, sep="\t",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE);#Obtain column names from File By check.names
  RemovedExGSID=RemovedSampleIDMatrix$RemovedID;
  print(length(RemovedExGSID));

  Tissues=c("DLPFC_BA9","ACC_BA24");
  PredictionMethods=c("st","ut","xt");
#Eli-s234GTEXV8_PredictedGeneExpressionst_ACC_BA24_FullGenotype_Imputed1.txt

  for(iBatch in seq(1,3))#3
  {
    for(iMethod in seq(1,3))#3
    {
    for(iTissue in seq(1,2))#2
    {   
        IndID_Batch=as.vector(EffectiveSampleIDMatrix[which(EffectiveSampleIDMatrix$Batch==BatchList[iBatch]),1]);
        print(length(IndID_Batch));
        PredictedExpressionFile=paste("/data/ruderferlab/projects/cmc/scratch/sweden/MergedTranscriptPrediction/GTExV8PredictionEx/",ConsiseBatchNamelist[iBatch],"GTEXV8_PredictedGeneExpression",PredictionMethods[iMethod],"_",Tissues[iTissue],"_FullGenotype_Imputed.txt.gz",sep="");
   #   iFileSize=file.info(PredictedExpressionFile)$size;
   #   if(iFileSize==0)
   #    {
   #      print ("PredictedExpressionFile\n"); 
   #      next;
   #    }
      con<-file(PredictedExpressionFile,'rt');
      PredictedGeneExpression=read.table(con, sep="\t",colClasses="character",check.names=TRUE,na.strings = "NA",nrows=-1, header=TRUE,fill=FALSE,strip.white=TRUE,row.names=NULL);#Obtain column names from File By check.names   
      print(PredictedExpressionFile); 
      PredictedGeneExpression =as.data.frame(PredictedGeneExpression);
      GeneIDColNames=colnames(PredictedGeneExpression);
      GeneIDColNames=str_replace_all(GeneIDColNames, "[.]", "-");#replace "." into "-"
      colnames(PredictedGeneExpression)=GeneIDColNames; 
      GeneIDColNames=as.vector(GeneIDColNames);
      print(length(GeneIDColNames));
      OverlapID=intersect(GeneIDColNames,IndID_Batch);
      print(length(OverlapID));
      print(dim(PredictedGeneExpression));
      GeneIDRowMatrix=PredictedGeneExpression[,c(1:9)];#Obtain Gene Name column information
      GeneIDRowMatrixColNames=colnames(GeneIDRowMatrix);
      PredictedGeneExpression=PredictedGeneExpression[,(names(PredictedGeneExpression)%in%IndID_Batch)]; 
      print(dim(PredictedGeneExpression));
      GeneExpression=PredictedGeneExpression[,!(names(PredictedGeneExpression)%in%RemovedExGSID)];
      print(dim(GeneExpression));
      GeneExpressionNames=colnames(GeneExpression);

      GeneExpressionDim=dim(GeneExpression);
      GeneExpression[,1:GeneExpressionDim[2]] <- sapply(GeneExpression[,1:GeneExpressionDim[2]], as.numeric);

      GeneExpressionINT= t(apply(GeneExpression, 1, rankNorm));# The result from the apply() had to be transposed using t() to get the same layout as the input matrix A  
      GeneExpressionZScore=t(apply(GeneExpression, 1,scale));
     print( "GeneExpressionZScore"); 
     GeneExpressionINTMerged=as.data.frame(cbind(GeneIDRowMatrix, GeneExpressionINT));
      colnames(GeneExpressionINTMerged)=c(GeneIDRowMatrixColNames,GeneExpressionNames);;
      print(dim(GeneExpressionINTMerged));#ConsiseBatchNamelist[iBatch],"GTEXV8_PredictedGeneExpression",PredictionMethods[iMethod],"_",Tissues[iTissue],"_",DataTypes[iType],".txt",sep=""); 
      PredictedExpressionFile=paste("/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8Based",ConsiseBatchNamelist[iBatch],PredictionMethods[iMethod],"_",Tissues[iTissue],"_FullGenotype_Imputed_GenePredictedExExGSINTZScore.txt",sep="");   
      write.table(GeneExpressionINTMerged, file=PredictedExpressionFile, append = FALSE, quote =FALSE, sep = "\t", eol = "\n",col.names=TRUE,row.names=FALSE);
     GeneExpressionIZScoreMerged=as.data.frame(cbind(GeneIDRowMatrix,GeneExpressionZScore)); 
     colnames(GeneExpressionIZScoreMerged)=c(GeneIDRowMatrixColNames,GeneExpressionNames); 
     PredictedExpressionFile=paste("/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8Based",ConsiseBatchNamelist[iBatch],PredictionMethods[iMethod],"_",Tissues[iTissue],"_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt",sep="");     
     write.table(GeneExpressionIZScoreMerged, file=PredictedExpressionFile, append = FALSE, quote =FALSE, sep = "\t", eol = "\n",col.names=TRUE,row.names=FALSE);
     close(con);
     }
   } 
  }

