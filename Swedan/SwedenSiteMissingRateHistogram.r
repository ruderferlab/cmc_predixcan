#! /usr/local/R/3.2.0/x86_64/intel14/nonet/bin/Rscript  --vanilla
view = 2;
  remove_outliers <- function(x, na.rm = TRUE, ...) {
  qnt <- quantile(x, probs=c(.25, .75), na.rm = na.rm, ...)
   H <- 3 * IQR(x, na.rm = na.rm)
   y <- x
   Lowerbound=qnt[1] - H;
   UpperBound=qnt[2] + H;
   Bound=c(Lowerbound[[1]],UpperBound[[1]]);
   return(Bound);
   #y[x < (qnt[1] - H)] <- NA
  # y[x > (qnt[2] + H)] <- NA
  # y
  }

 # /data/ruderferlab/projects/cmc/scratch/sweden/WESSiteMissingRateInfo.txt

  myDir="/data/ruderferlab/projects/cmc/scratch/sweden/";

    Outputfile=paste(myDir,"SwedenWESBasedVariants_MissingRate_AltAFSummaryHistogramPlot.pdf",sep="");
    pdf(Outputfile, 7, 5);
   # jpeg(Outputfile, width = 480, height = 480, units = "px", pointsize = 12);
    par(mar=c(5,7,1.5,0.25), mgp=c(3,1,0), mfrow=c(2,1),oma=c(0,0,0,0));
    linewide=2;
    axixwide=0.5;
    tickwide=0.5;
   cex_size=0.62;
   leg_cex_size=0.8;
   axes=F;
   xaxs="s";
   cex_size1=0.65;
 
     MissingInfoFile=paste(myDir,"WESSiteMissingRateInfo2.txt",sep="");
     print(MissingInfoFile);
     iSelectedColumns=c(7,9);
     MissingInfoDataFrame=read.table(MissingInfoFile, sep="\t",colClasses="character",check.names=TRUE,nrows=-1, na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE,row.names=NULL);
     MissingInfoDataFrame=as.data.frame(MissingInfoDataFrame); 
     MissingInfoDataFrame[,7]=as.numeric(MissingInfoDataFrame[,7]);
     MissingInfoDataFrame[,9]=as.numeric(MissingInfoDataFrame[,9]);
    VariantTypes=c("Missing Rate","Alt Allele Freq");
    for(iType in seq(1,2))
     {
       Freq_Variants=hist(MissingInfoDataFrame[,iSelectedColumns[iType]],freq=TRUE,breaks=c(seq(0,0.01,0.001),seq(0.02,0.1,0.01),seq(0.2,1.0,0.1)),plot=F);
      print(Freq_Variants$count);
      print(log10(Freq_Variants$count));
      ThresholdList=c(seq(0.001,0.01,0.001),seq(0.02,0.1,0.01),seq(0.2,1.0,0.1));
      barplot(log10(Freq_Variants$count), main=NULL, xlab=VariantTypes[iType],names.arg=ThresholdList,las=2,yaxt="n",ylab="Freq of variants");
    abline(h=4,lty=2,col=2);   
     axis(2, at=seq(0,6), labels=c(0,10,100,1000,10000,100000,1000000),las=2, cex.axis=0.7, tck=-.01);
    # hist(MissingInfoDataFrame[,iSelectedColumns[iType]],freq=FALSE,breaks=c(seq(0,0.1,0.01),seq(0.2,1.0,0.1)),main=NULL, xlab=VariantTypes[iType], border="blue", 
     # col="green",las=1);
     
#hist(MissingInfoDataFrame[,iSelectedColumns[iType]],freq=TRUE, breaks =quantile(MissingInfoDataFrame[,iSelectedColumns[iType]], 0:50/50),main=NULL,ylim=c(0,35000),xlab=VariantTypes[iType], border="blue",col="green",las=1);
    }
   dev.off(); 
   ThresholdList=c(seq(0.001,0.01,0.001),seq(0.02,0.1,0.01),seq(0.2,1.0,0.1));
   for(ii in seq(1,27))
   {
    MissingInfoDataFrameSubset=MissingInfoDataFrame[which(MissingInfoDataFrame$iMissingRate<ThresholdList[ii]),];
    MissingInfoDataFrameSubsetDim=dim(MissingInfoDataFrameSubset);
   NumbofVariants=round(MissingInfoDataFrameSubsetDim[[1]],0)
   Result=c(ThresholdList[ii],NumbofVariants);
    print(Result);
  } 

