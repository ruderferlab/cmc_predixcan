#! /usr/local/R/3.2.0/x86_64/intel14/nonet/bin/Rscript  --vanilla
library("dplyr");
library("tidyverse");

view = 2;
  remove_outliers <- function(x, na.rm = TRUE, ...) {
  qnt <- quantile(x, probs=c(.25, .75), na.rm = na.rm, ...)
   H <- 3 * IQR(x, na.rm = na.rm)
   y <- x
   Lowerbound=qnt[1] - H;
   UpperBound=qnt[2] + H;
   Bound=c(Lowerbound[[1]],UpperBound[[1]]);
   return(Bound);
   #y[x < (qnt[1] - H)] <- NA
  # y[x > (qnt[2] + H)] <- NA
  # y
  }


  myDir="/data/ruderferlab/projects/cmc/scratch/sweden/";
  Batches=c("Eli-s234","Eli-sw56","Eli-swe1");

    Outputfile=paste(myDir,"/PCAnalysisBasedonSwedenArray_Scatter_MissingHeterogousRate.pdf",sep="");
    pdf(Outputfile, 7, 9);
   # jpeg(Outputfile, width = 480, height =700, units = "px", pointsize = 12);
    par(mar=c(5,7,1.5,0.25), mgp=c(3,1,0), mfrow=c(3,1),oma=c(0,0,0,0));
    linewide=2;
    axixwide=0.5;
    tickwide=0.5;
   cex_size=0.62;
   leg_cex_size=0.8;
   axes=F;
   xaxs="s";
   cex_size1=0.65;
  
   #WGSID   Sex     Batch   NumbofHyterrozyogotes_SNV       NumHyterrozyogotes_INDEL        Disease Ancestry  PC1     PC2
    for(iBatch in seq(1,3))
    {

     WGSVariantSummaryFile=paste("/data/ruderferlab/projects/sweden/data/geno/sweden_merged_",Batches[iBatch],".eigenvec", sep="");
    WGSVariantSummaryMatrix=read.table(WGSVariantSummaryFile, sep=" ",colClasses="character",na.strings = "NA", header=FALSE,fill=FALSE,strip.white=TRUE);
    WGSVariantSummaryMatrix[,3]=as.numeric(WGSVariantSummaryMatrix[,3]);
    WGSVariantSummaryMatrix[,4]=as.numeric(WGSVariantSummaryMatrix[,4]);
      plot(WGSVariantSummaryMatrix[,3],WGSVariantSummaryMatrix[,4],main=Batches[iBatch],cex.lab=0.8,ylab="PC2", xlab="PC1");
#sweden_merged_Eli-s234MissingHeterogenotypeRate.txt

     MissingHeterogoteRateFile=paste("/data/ruderferlab/projects/sweden/data/geno/sweden_merged_",Batches[iBatch],"MissingHeterogenotypeRate.txt", sep="");
     SummaryMatrix=read.table(MissingHeterogoteRateFile, sep="\t",colClasses="character",na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE);
      SummaryMatrix[,2]=as.numeric(SummaryMatrix[,2]);
      SummaryMatrix[,3]=as.numeric(SummaryMatrix[,3]);
      hist(SummaryMatrix[,2],xlab="Heterogeneous Rate",main=Batches[iBatch]);
      print(summary(SummaryMatrix[,2]));
       hist(SummaryMatrix[,3],xlab="Missing Rate",main=Batches[iBatch]);
     print(summary(SummaryMatrix[,3]));
#     legend(x=0.04,y=0.3,c("African-American","Asian","Caucasian","Hispanic","(Multiracial)"),cex=.8,col=c(1:5),pch=c(0:4),bty = "n");
     #abline(v=-0.017,lty=2,col=6);
     # abline(h=0.035,lty=2,col=6);
 #   segments(-0.017,-0.05,-0.017,0.035,lty=2,col=6);
 #   segments(-0.05,0.035,-0.017,0.035,lty=2,col=6);
     
 #     WGSVariantSummaryMatrix$InferredAncestry="Non-CEU";
 #     WGSVariantSummaryMatrix$InferredAncestry[which(WGSVariantSummaryMatrix$PC1<=-0.017 & WGSVariantSummaryMatrix$PC2<=0.035)]="CEU";  
 #     InferredAncestryFile=paste(myDir,"WGSData_VariantSummary_InferredAncestry.txt",sep="");
 #     write.table(WGSVariantSummaryMatrix, file=InferredAncestryFile, append = FALSE, quote =FALSE, sep = "\t",
 #           eol = "\n",col.names=TRUE,row.names=FALSE);
  }


   dev.off();
