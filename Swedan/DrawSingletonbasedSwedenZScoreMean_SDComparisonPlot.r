#! /usr/local/R/3.2.0/x86_64/intel14/nonet/bin/Rscript  --vanilla
library("dplyr");
library("tidyverse");
library(ggplot2)
library(plyr);
 library(gridExtra)
 library(grid)
 library(lattice)
 library(devtools);
 library(magrittr);

view = 2;
  remove_outliers <- function(x, na.rm = TRUE, ...) {
  qnt <- quantile(x, probs=c(.25, .75), na.rm = na.rm, ...)
   H <- 3 * IQR(x, na.rm = na.rm)
   y <- x
   Lowerbound=qnt[1] - H;
   UpperBound=qnt[2] + H;
   Bound=c(Lowerbound[[1]],UpperBound[[1]]);
   return(Bound);
   #y[x < (qnt[1] - H)] <- NA
  # y[x > (qnt[2] + H)] <- NA
  # y
  }
#+++++++++++++++++++++++++
# Function to calculate the mean and the standard deviation
  # for each group
#+++++++++++++++++++++++++
# data : a data frame
# varname : the name of a column containing the variable
  #to be summariezed
# groupnames : vector of column names to be used as
  # grouping variables
data_summary <- function(data, varname, groupnames){
  require(plyr)
  summary_func <- function(x, col){
    c(mean = mean(x[[col]], na.rm=TRUE),
      sd = sd(x[[col]], na.rm=TRUE))
  }
  data_sum<-ddply(data, groupnames, .fun=summary_func,
                  varname)
  data_sum <- rename(data_sum, c("mean" = varname))
 return(data_sum)
}



   myDir="/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/Sweden";
    Outputfile=paste(myDir,"/SwedenSingletonBasedPredictionExpressionComparisonErrorBarplot.pdf",sep="");
    pdf(Outputfile, 9, 36);
   # jpeg(Outputfile, width = 480, height =700, units = "px", pointsize = 12);
    par(mar=c(5,7,1.5,0.25), mgp=c(3,1,0), mfrow=c(3,1),oma=c(0,0,0,0));
    linewide=2;
    axixwide=0.5;
    tickwide=0.5;
   cex_size=0.62;
   leg_cex_size=0.8;
   axes=F;
   xaxs="s";
   cex_size1=0.65;
  
    SingletonbasedZScoreFile="/data/ruderferlab/projects/cmc/scratch/sweden/SwedenVariantAnotaiton_GeneInfo_RareGtypeZscore_Singleton.txt";
    SingletonbasedZScoreMatrix=read.table(SingletonbasedZScoreFile, sep="\t",colClasses="character",na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE);
    DiseaseTypes=c("Control","Bipolar","Schizophrenia");
    SingletonbasedZScoreMatrix=as.data.frame(SingletonbasedZScoreMatrix);
    SingletonbasedZScoreColNames=colnames(SingletonbasedZScoreMatrix);

    print(colnames(SingletonbasedZScoreMatrix));
   print(dim(SingletonbasedZScoreMatrix));
    #Remove 4 types of variants
   print(SingletonbasedZScoreMatrix[c(1:10),"ExonicFunc_ref"]); 
   SingletonbasedZScoreMatrix=SingletonbasedZScoreMatrix[which(SingletonbasedZScoreMatrix$ExonicFunc_ref != "nonframeshift deletion"),];
    SingletonbasedZScoreMatrix=SingletonbasedZScoreMatrix[which(SingletonbasedZScoreMatrix$ExonicFunc_ref != "nonframeshift insertion"),];
    SingletonbasedZScoreMatrix=SingletonbasedZScoreMatrix[which(SingletonbasedZScoreMatrix$ExonicFunc_ref != "stoploss"),];
    SingletonbasedZScoreMatrix=SingletonbasedZScoreMatrix[which(SingletonbasedZScoreMatrix$ExonicFunc_ref != "unknown"),];
    SingletonbasedZScoreMatrix=SingletonbasedZScoreMatrix[which(SingletonbasedZScoreMatrix$PHE != "Bipolar"),];
    SingletonbasedZScoreMatrix$VariantClass=NULL;
    SingletonbasedZScoreMatrix$VariantClass[SingletonbasedZScoreMatrix$ExonicFunc_ref=="frameshift insertion"]="LOF";
    SingletonbasedZScoreMatrix$VariantClass[SingletonbasedZScoreMatrix$ExonicFunc_ref=="frameshift deletion"]="LOF";
    SingletonbasedZScoreMatrix$VariantClass[SingletonbasedZScoreMatrix$ExonicFunc_ref=="stopgain"]="LOF";
    SingletonbasedZScoreMatrix$VariantClass[SingletonbasedZScoreMatrix$ExonicFunc_ref=="nonsynonymous SNV"]="nonsynonymous";
    SingletonbasedZScoreMatrix$VariantClass[SingletonbasedZScoreMatrix$ExonicFunc_ref=="synonymous SNV"]="synonymous";
     myPlot<-list();
    iPlotNum=1; 
    print(dim(SingletonbasedZScoreMatrix));  
   for(iCol in seq(1,24,1))
    {
        iSelectedScoreColumnNum=29+iCol;
        SingletonbasedZScoreSubset=SingletonbasedZScoreMatrix[,c(27,54,iSelectedScoreColumnNum)];
        SingletonbasedZScoreSubset=as.data.frame(SingletonbasedZScoreSubset);
        colnames(SingletonbasedZScoreSubset)=c("Disease","VariantClass","Zscore");
        SingletonbasedZScoreSubset=SingletonbasedZScoreSubset[which(SingletonbasedZScoreSubset$Zscore!="NA"),];
        SingletonbasedZScoreSubset[,3]=as.numeric(SingletonbasedZScoreSubset[,3]);


        #SingletonbasedZScoreSummary<- data_summary(SingletonbasedZScoreSubset, varname="Zscore", groupnames=c("Disease","VariantClass"));
      myTitle=SingletonbasedZScoreColNames[29+iCol];
      print(SingletonbasedZScoreColNames[29+iCol]);
    
       ZScoreMeanSE=aggregate(Zscore ~Disease+VariantClass,SingletonbasedZScoreSubset, function(x) c(M = mean(x), SE = sd(x)/sqrt(length(x))));
       print(ZScoreMeanSE); 
      A=ZScoreMeanSE[,"Zscore"];
      A=as.data.frame(A);
      colnames(A)=c("Mean","SE");
      MergedData=cbind(ZScoreMeanSE[,c("Disease","VariantClass")],A);
       MergedData=as.data.frame(MergedData);
      colnames(MergedData)=c("Disease","ExonicFun","Mean","SE");   
      print(MergedData);
      myPlot[[iPlotNum]]<- ggplot(MergedData,aes(x=ExonicFun,y=Mean,fill=Disease))+          
        geom_bar(stat="identity", color="black",
           position=position_dodge()) +
          geom_errorbar(aes(ymin=Mean-SE, ymax=Mean+SE), width=.2,position=position_dodge(.9))+theme(axis.title.x = element_blank())+ggtitle(myTitle);


 
    # boxplot(Zscore ~ Disease +VariantClass, data = SingletonbasedZScoreSubset);
    # cols <- rainbow(3, s = 0.5)
    #   boxplot(Zscore ~ Disease +VariantClass, data = SingletonbasedZScoreSubset,
     #   at = c(1:3, 5:7,9:11), col = cols,
     #   names = c("", "LOF", "", "", "nonsynonymous", "","", "synonymous", ""), xaxs = FALSE,main=myTitle,ylim=c(-3,3));
     # legend("topleft", fill = cols, legend = c("BP","Control","SCZ"), horiz = T,bty = "n");
     #  abline(h =0,col="red", lwd=1, lty=2)
#      myPlot[[iPlotNum]]<- ggplot(SingletonbasedZScoreSubset, aes(VariantClass, Zscore, fill=factor(Disease)))+geom_boxplot();
      #  myPlot[[iPlotNum]]<- ggplot(SingletonbasedZScoreSummary, aes(x=VariantClass, y=Zscore, fill=Disease)) + geom_bar(stat="identity", color="black", 
      #     position=position_dodge()) +geom_errorbar(aes(ymin=Zscore-sd, ymax=Zscore+sd), width=.2,position=position_dodge(.9)); 
        iPlotNum=iPlotNum+1; 
    }

   pushViewport(viewport(layout=grid.layout(12,2)))#defined how to arrange the plots, there are 30 
   vplayout<-function(x,y){viewport(layout.pos.row=x,layout.pos.col=y)}
   iNum=1;
   for(i in myPlot)
   {
     print(iNum);
    if(iNum %% 2 == 1)
    {
        print(myPlot[[iNum]],vp=vplayout((iNum-1)%/%2+1,1));
    }
    else
    {
       print(myPlot[[iNum]],vp=vplayout((iNum-1)%/%2+1,2));
    }
   iNum=iNum+1;
  }
   dev.off();
