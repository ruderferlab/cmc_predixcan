#!/usr/bin/perl -w
use Scalar::Util qw(looks_like_number);
 use strict;

 use lib "/home/hanl3/perl5/module/lib/site_perl/5.14.2";
 use Text::CSV_PP;
  use Tie::File; 
 use POSIX qw(WNOHANG);
 use IO::Uncompress::Gunzip qw($GunzipError);
 use warnings;
 use Scalar::Util qw(looks_like_number);

  my %InferredEuropeans=();
  my $AllEuropeanSampleIDFile="/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/Sweden/SwedenPassedQCSampleInfo.txt";
  open(INPUT,$AllEuropeanSampleIDFile) || die "can’t open $AllEuropeanSampleIDFile";
  while (<INPUT>)
    {
       chomp;
       my @cols=split(/\s+/,$_);
       $InferredEuropeans{$cols[0]}=1;
    }
   close INPUT;
   my @AllInferredEuropeans=();
   my @AllSelectedIndex=();
  my %FilterSites=(); 
 my @VariantTypes=("SNP","INDEL");
 # /data/ruderferlab/projects/sweden/data/exome/swexm_INDEL.vcf
 #  /data/ruderferlab/projects/sweden/data/exome/swexm_SNP.vcf
  for(my $iType=0; $iType<@VariantTypes;$iType++)
  {
     my $iLineNumber=0;
     my $iFilterFile="/data/ruderferlab/projects/sweden/data/exome/swexm_$VariantTypes[$iType].vcf";
     open(INPUT,$iFilterFile) || die "can’t open $iFilterFile";
     while (<INPUT>)
     {   
       chomp;
       if($_ =~ m/^#/)
       {
         next;
       }
       my @cols=split(/\s+/,$_);
       next if($cols[0] eq "X" ||$cols[0] eq "Y"||$cols[0] eq "MT");
    #     next if($cols[0]<14);
       $FilterSites{$cols[2]}=1;
       $iLineNumber++;
    # last if($iLineNumber>200);
     }   
     close INPUT;
   }

    my $MissingRateFile1="/data/ruderferlab/projects/cmc/scratch/sweden/WESSiteMissingRateInfo_Prob.txt";
     open OUT1,">$MissingRateFile1" or die "Can't open Output file:$MissingRateFile1!";

    my $MissingRateFile="/data/ruderferlab/projects/cmc/scratch/sweden/WESSiteMissingRateInfo2.txt";  
     open OUT, ">$MissingRateFile" or die "Can't open Output file:$MissingRateFile!"; 
 print OUT  "SiteID\tiMissing\tiRefRef\tiRefAlt\tiAltAlt\tiTotal\tiMissingRate\tiOthers\tfAlleleFreq\n";
    my $WESFile="/data/ruderferlab/projects/sweden/data/exome/swexm.vcf.gz";
   open (INPUT, "gunzip -c $WESFile|") or die "gunzip  $WESFile: $!";;
   while (<INPUT>)
    {  
       chomp;
        if($_ =~ m/^##/)
        {  
           next;
        }
        if($_ =~ m/^#/)
        {
           my @cols=split(/\s+/,$_); 
           for(my $iInd=9;$iInd<@cols;$iInd++)
           {
             if(defined($InferredEuropeans{$cols[$iInd]}))
             {
              push @AllInferredEuropeans,$cols[$iInd];
              push @AllSelectedIndex,$iInd;
             } 
            else
             {
             print "$iInd\t";
             }
           }
           print "\n";
           next;
        }
        my @cols=split(/\s+/,$_,10);
        next if($cols[6] ne "PASS");
        next if(!defined($FilterSites{$cols[2]}));
     #   next if($cols[0]<14);
        @cols=split(/\s+/,$_);
        my %Gtypes_Number=(); 
          $Gtypes_Number{"./."}=0;
          $Gtypes_Number{"0/0"}=0; 
          $Gtypes_Number{"0/1"}=0; 
          $Gtypes_Number{"1/0"}=0;
          $Gtypes_Number{"1/1"}=0;
         my $iTotal=scalar(@AllSelectedIndex);
       #  my $iM=0; 
       # my $iAA=0;
       #  my $iAa=0;
       # my $iaa=0;
        for(my $iCol=0;$iCol<@AllSelectedIndex;$iCol++)
        {
          my $Gtype=substr($cols[$AllSelectedIndex[$iCol]], 0,3);
=cut;
          if($Gtype eq "./.")
           {
             $iM++;
           }
          elsif($Gtype eq "0/0")
           {
             $iAA++;
           }
          elsif($Gtype eq "0/1" || $Gtype eq "1/0"  )
           {
             $iAa++;
           }
          elsif($Gtype eq "1/1")
           {
             $iaa++;
           }
=cut;
          $Gtypes_Number{$Gtype}++;
        }
       
        my $iMissing=$Gtypes_Number{"./."};
        my $iRefRef=$Gtypes_Number{"0/0"};
        my $iRefAlt=$Gtypes_Number{"0/1"}+$Gtypes_Number{"1/0"};
        my $iAltAlt=$Gtypes_Number{"1/1"};
       
    #  print "$iM\t$iAA\t$iAa\t$iaa\t$iMissing\t$iRefRef\t$iRefAlt\t$iAltAlt\n";
     my $iMissingRate=sprintf("%.8f",$iMissing/$iTotal); 
        my $iOthers=$iTotal-$iMissing-$iRefRef-$iRefAlt-$iAltAlt;
     
         my $fAlleleFreq=0;
      if(($iRefRef+$iRefAlt+$iAltAlt)!=0)
       {
        $fAlleleFreq=sprintf("%.8f",(0.5*$iRefAlt+$iAltAlt)/($iRefRef+$iRefAlt+$iAltAlt));
       } 
     else
       {
        print OUT1 "$_\n";
       #last; 
      } 

      print OUT  "$cols[2]\t$iMissing\t$iRefRef\t$iRefAlt\t$iAltAlt\t$iTotal\t$iMissingRate\t$iOthers\t$fAlleleFreq\n";             
        if($iOthers!=0)
        {
         delete $Gtypes_Number{"./."};
         delete $Gtypes_Number{"0/0"};
         delete $Gtypes_Number{"0/1"};
          delete $Gtypes_Number{"1/0"};  
         delete $Gtypes_Number{"1/1"};
         foreach my $gType (keys %Gtypes_Number)
         {
           print "$cols[2]\t$gType\n";
           last;
          }
       }
   #  last;
    }
  close INPUT;
  close OUT;
 close OUT1;
