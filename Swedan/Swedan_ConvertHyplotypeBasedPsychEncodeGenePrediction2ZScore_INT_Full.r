#Convert imputed WGS based prediction into corresponing ZSCORE and int zscore

 library(magrittr);
 library(RNOmni);
 library(dplyr)
 library(stringr)

 options(width=1500)
  BatchList=c("Eli-s234","Eli-sw56","Eli-swe1");
  ConsiseBatchNamelist=c("Eli-s234","Eli-sw56","Eli-swe1");
  
  EffectiveSampleIDInfoFile="/data/ruderferlab/projects/sweden/data/geno/sweden_Exome_GenotypedShasredIndList.txt";
  EffectiveSampleIDMatrix=read.table(EffectiveSampleIDInfoFile, sep="\t",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE);#Obtain column names from File By check.names  

  RemovedIDInfoFile="/data/ruderferlab/projects/sweden/files/swexm.paper.rm";
  RemovedSampleIDMatrix=read.table(RemovedIDInfoFile, sep="\t",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE);#Obtain column names from File By check.names
  RemovedExGSID=RemovedSampleIDMatrix$RemovedID;
  print(length(RemovedExGSID));
    Haplotypes=c("Haplotype1","Haplotype2");


  for(iBatch in seq(1,3))#3
  {
     IndID_Batch=as.vector(EffectiveSampleIDMatrix[which(EffectiveSampleIDMatrix$Batch==BatchList[iBatch]),1]);
     print(length(IndID_Batch));
     
      TotalGeneExpression=NULL;
 
     MergedPredictedGeneExpression=NULL; 
     for(iType in seq(1,2))
     {
        PredictedExpressionFile=paste("/data/ruderferlab/projects/cmc/scratch/sweden/MergedTranscriptPrediction/PsychEncodePredictionEx/",ConsiseBatchNamelist[iBatch],"PsychEncode_PredictedGeneExpression_",Haplotypes[iType],"_Imputed.txt.gz",sep="");
       con<-file(PredictedExpressionFile,'rt');
       PredictedGeneExpression=read.table(con, sep="\t",colClasses="character",check.names=TRUE,nrows=-1, na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE,row.names=NULL);#Obtain column names from File By check.names   
     print(PredictedExpressionFile); 
     PredictedGeneExpression =as.data.frame(PredictedGeneExpression);
     GeneIDColNames=colnames(PredictedGeneExpression);
     GeneIDColNames=str_replace_all(GeneIDColNames, "[.]", "-");#replace "." into "-"
    colnames(PredictedGeneExpression)=GeneIDColNames; 
    GeneIDColNames=as.vector(GeneIDColNames);
     OverlapID=intersect(GeneIDColNames,IndID_Batch);
    print(length(OverlapID));
     print(dim(PredictedGeneExpression));
      
      GeneIDRowMatrix=PredictedGeneExpression[,c(1:9)];#Obtain Gene Name column information
      GeneIDRowMatrixColNames=colnames(GeneIDRowMatrix);
 
      PredictedGeneExpressionWithoutGeneID=PredictedGeneExpression[,(names(PredictedGeneExpression)%in%IndID_Batch)];#Use ID_match to remove some individuals
      print(dim(PredictedGeneExpressionWithoutGeneID));

      PredictedGeneExpressionWithoutGeneID=PredictedGeneExpressionWithoutGeneID[,!(names(PredictedGeneExpressionWithoutGeneID)%in%RemovedExGSID)]; #Remove the individuals noticed in the paper
      print(dim(PredictedGeneExpressionWithoutGeneID));
     
      GeneExpressionWithoutGeneIDNames=colnames(PredictedGeneExpressionWithoutGeneID);
      GeneExpressionWithID=as.data.frame(cbind(GeneIDRowMatrix,PredictedGeneExpressionWithoutGeneID));
      colnames(GeneExpressionWithID)=c(GeneIDRowMatrixColNames,GeneExpressionWithoutGeneIDNames);
      
    
     if(iType==1) 
      {
       HyploType1PredictedGeneExpression=GeneExpressionWithID;  
      }
      else
      {
       MergedPredictedGeneExpression=merge(x= HyploType1PredictedGeneExpression,y=GeneExpressionWithID,by =c("Chr","WGSBatch","PredictionMethods","BrainsTissues","GeneID"),suffixes = c("",".y"),);
       print(dim(MergedPredictedGeneExpression));
      }
      close(con);
      MergedPredictedGeneExpression=as.data.frame(MergedPredictedGeneExpression);
      MergedPredictedGeneExpression_Subset=MergedPredictedGeneExpression[, !(colnames(MergedPredictedGeneExpression) %in% c("Source","NumberofLociInWeightModel","FullNumber","NumberofLociInActualModel","Source.y","NumberofLociInWeightModel.y","FullNumber.y","NumberofLociInActualModel.y"))]
    }
     print(dim(MergedPredictedGeneExpression_Subset));
      GeneIDRowMatrix=MergedPredictedGeneExpression_Subset[,c(1:5)];#Obtain Gene Name column information
      GeneIDRowMatrixColNames=colnames(GeneIDRowMatrix);
      TotalGeneExpression=MergedPredictedGeneExpression_Subset[,!(colnames(MergedPredictedGeneExpression_Subset) %in% c("Chr","WGSBatch","PredictionMethods","BrainsTissues","GeneID"))];
       print("Combined Matrix");
      print(dim(TotalGeneExpression));
       FullColNames=colnames(TotalGeneExpression);    
       GeneExpressionDim=dim(TotalGeneExpression);

         TotalGeneExpression[,1:GeneExpressionDim[2]] <- sapply(TotalGeneExpression[,1:GeneExpressionDim[2]], as.numeric);
         TotalGeneExpressionINT= t(apply(TotalGeneExpression, 1, rankNorm));# The result from the apply() had to be transposed using t() to get the same layout as the input matrix A  
         TotalGeneExpressionZScore=t(apply(TotalGeneExpression, 1,scale));
         TotalGeneExpressionINTDim=dim(TotalGeneExpressionINT);
         HalfColumn=TotalGeneExpressionINTDim[2]/2;
         print(HalfColumn);

         A1Names=FullColNames[1:HalfColumn]; 
         A2Names=FullColNames[(HalfColumn+1):TotalGeneExpressionINTDim[2]];
         A2Names=gsub(".y", "",A2Names);
         iUnequal=0;
         for(iNum in seq(1:HalfColumn))
         { 
           if(A1Names[iNum] !=A2Names[iNum])
           {
             iUnequal= iUnequal+1;
           }
         }        
         print(iUnequal);
         for(iType in seq(1,2))
         {
           StartPos=(iType-1)*HalfColumn;
           EndPos=iType*HalfColumn;
           GeneExpressionINT=TotalGeneExpressionINT[,c((StartPos+1):EndPos)];
           GeneExpressionZScore=TotalGeneExpressionZScore[,c((StartPos+1):EndPos)];                        

            GeneExpressionINTMerged=as.data.frame(cbind(GeneIDRowMatrix, GeneExpressionINT));#INT score merged
           colnames(GeneExpressionINTMerged)=c(GeneIDRowMatrixColNames,A1Names);;
           print(dim(GeneExpressionINTMerged)); 
           PredictedExpressionFile=paste("/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/PsychEncodePredictionEx/PsychEncodeWeightBased",ConsiseBatchNamelist[iBatch],"_",Haplotypes[iType],"_Imputed_GenePredictedExExGSINTZScore.txt",sep="");   
           write.table(GeneExpressionINTMerged, file=PredictedExpressionFile, append = FALSE, quote =FALSE, sep = "\t", eol = "\n",col.names=TRUE,row.names=FALSE);
 
           GeneExpressionIZScoreMerged=as.data.frame(cbind(GeneIDRowMatrix,GeneExpressionZScore)); #Standard score merged
           colnames(GeneExpressionIZScoreMerged)=c(GeneIDRowMatrixColNames,A1Names); 
           PredictedExpressionFile=paste("/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/PsychEncodePredictionEx/PsychEncodeWeightBased",ConsiseBatchNamelist[iBatch],"_",Haplotypes[iType],"_Imputed_GenePredictedExExGSStandardZScore.txt",sep="");     
           write.table(GeneExpressionIZScoreMerged, file=PredictedExpressionFile, append = FALSE, quote =FALSE, sep = "\t", eol = "\n",col.names=TRUE,row.names=FALSE);
        }
} 



