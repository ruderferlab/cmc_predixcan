#!/bin/bash
#This is for original WGS data

 InputFileDir="/home/hanl3/cmc/scratch/sweden/FilterForPrediction/PsychEncodeWeight";
declare -a batches=("Eli-s234" "Eli-sw56" "Eli-swe1")
declare -a gtypes=("FullGenotype" "Haplotype1" "Haplotype2")

#Eli-s234SharePsychEncodeWeight.CommonSNVs_ImputedWGS_FullGenotype.vcf.gz  
#This is for imputation WGS data

for (( i =0; i <=2; i++ ))  
do   
 for (( j =0; j <=2; j++)) 
  do
      if(($i==0 || $i==2))
       then 
	continue
      fi
   for((k=1;k<=22;k++))
     do
     sFullpathPrefix="$InputFileDir/${batches[$i]}SharePsychEncodeWeight.CommonSNVs_ImputedWGS_${gtypes[$j]}";
     ResultPathPrefix="$InputFileDir/ChromosomeSplitted/${batches[$i]}SharePsychEncodeWeight.CommonSNVs_ImputedWGS_${gtypes[$j]}_Chr$k";
     plink --vcf "$sFullpathPrefix.vcf.gz" --set-missing-var-ids @:#   --chr "$k"  --make-bed --out "$ResultPathPrefix"
     done
   done 
done 
