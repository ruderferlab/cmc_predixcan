#Convert imputed WGS based prediction into corresponing ZSCORE and int zscore

 library(magrittr);
 library(RNOmni);
 library(dplyr)
 library(stringr)

  BatchList=c("Eli-s234","Eli-sw56","Eli-swe1");
  ConsiseBatchNamelist=c("Eli-s234","Eli-sw56","Eli-swe1");
  
  EffectiveSampleIDInfoFile="/data/ruderferlab/projects/sweden/data/geno/sweden_Exome_GenotypedShasredIndList.txt";
  EffectiveSampleIDMatrix=read.table(EffectiveSampleIDInfoFile, sep="\t",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE);#Obtain column names from File By check.names  

  RemovedIDInfoFile="/data/ruderferlab/projects/sweden/files/swexm.paper.rm";
  RemovedSampleIDMatrix=read.table(RemovedIDInfoFile, sep="\t",colClasses="character",check.names=TRUE,na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE);#Obtain column names from File By check.names
  RemovedExGSID=RemovedSampleIDMatrix$RemovedID;
  print(length(RemovedExGSID));

  for(iBatch in seq(1,3))#3
  {
     IndID_Batch=as.vector(EffectiveSampleIDMatrix[which(EffectiveSampleIDMatrix$Batch==BatchList[iBatch]),1]);
     print(length(IndID_Batch));
     print(IndID_Batch[1:10]);
     PredictedExpressionFile=paste("/data/ruderferlab/projects/cmc/scratch/sweden/MergedTranscriptPrediction/PsychEncodePredictionEx/",ConsiseBatchNamelist[iBatch],"PsychEncode_PredictedGeneExpression_FullGenotype_Imputed.txt.gz",sep="");
     print(PredictedExpressionFile);
     con<-file(PredictedExpressionFile,'rt');
     PredictedGeneExpression=read.table(con, sep="\t",colClasses="character",check.names=TRUE,nrows=-1, na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE,row.names=NULL);#Obtain column names from File By check.names   
     print(PredictedExpressionFile); 
     PredictedGeneExpression =as.data.frame(PredictedGeneExpression);
     GeneIDColNames=colnames(PredictedGeneExpression);
     GeneIDColNames=str_replace_all(GeneIDColNames, "[.]", "-");#replace "." into "-"
    colnames(PredictedGeneExpression)=GeneIDColNames; 
    GeneIDColNames=as.vector(GeneIDColNames);
    print(length(GeneIDColNames));
     OverlapID=intersect(GeneIDColNames,IndID_Batch);
    print(length(OverlapID));
     print(dim(PredictedGeneExpression));
    GeneIDRowMatrix=PredictedGeneExpression[,c(1:9)];#Obtain Gene Name column information
     GeneIDRowMatrixColNames=colnames(GeneIDRowMatrix);
 
     PredictedGeneExpression=PredictedGeneExpression[,(names(PredictedGeneExpression)%in%IndID_Batch)]; 
     print(dim(PredictedGeneExpression));
      
      GeneExpression=PredictedGeneExpression[,!(names(PredictedGeneExpression)%in%RemovedExGSID)];
      print(dim(GeneExpression));
      GeneExpressionNames=colnames(GeneExpression);     

      GeneExpressionDim=dim(GeneExpression);
     GeneExpression[,1:GeneExpressionDim[2]] <- sapply(GeneExpression[,1:GeneExpressionDim[2]], as.numeric);

      GeneExpressionINT= t(apply(GeneExpression, 1, rankNorm));# The result from the apply() had to be transposed using t() to get the same layout as the input matrix A  
     GeneExpressionZScore=t(apply(GeneExpression, 1,scale));
     print( "GeneExpressionZScore");
  
     GeneExpressionINTMerged=as.data.frame(cbind(GeneIDRowMatrix, GeneExpressionINT));
      colnames(GeneExpressionINTMerged)=c(GeneIDRowMatrixColNames,GeneExpressionNames);;
      print(dim(GeneExpressionINTMerged)); 
     PredictedExpressionFile=paste("/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/PsychEncodePredictionEx/PsychEncodeWeightBased",ConsiseBatchNamelist[iBatch],"_FullGenotype_Imputed_GenePredictedExExGSINTZScore.txt",sep="");   
      write.table(GeneExpressionINTMerged, file=PredictedExpressionFile, append = FALSE, quote =FALSE, sep = "\t", eol = "\n",col.names=TRUE,row.names=FALSE);
     GeneExpressionIZScoreMerged=as.data.frame(cbind(GeneIDRowMatrix,GeneExpressionZScore)); 
     colnames(GeneExpressionIZScoreMerged)=c(GeneIDRowMatrixColNames,GeneExpressionNames); 
     PredictedExpressionFile=paste("/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/PsychEncodePredictionEx/PsychEncodeWeightBased",ConsiseBatchNamelist[iBatch],"_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt",sep="");     
     write.table(GeneExpressionIZScoreMerged, file=PredictedExpressionFile, append = FALSE, quote =FALSE, sep = "\t", eol = "\n",col.names=TRUE,row.names=FALSE);
    close(con);
} 



