#!/usr/bin/perl -w
use Scalar::Util qw(looks_like_number);
 use strict;

 use lib "/home/hanl3/perl5/module/lib/site_perl/5.14.2";
 use Text::CSV_PP;
  use Tie::File; 
 use POSIX qw(WNOHANG);
 use IO::Uncompress::Gunzip qw($GunzipError);
 use warnings;
 use Scalar::Util qw(looks_like_number);

  my %InferredEuropeans=();
  my $AllEuropeanSampleIDFile="/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/Sweden/SwedenPassedQCSampleInfo.txt";
  open(INPUT,$AllEuropeanSampleIDFile) || die "can’t open $AllEuropeanSampleIDFile";
  while (<INPUT>)
    {
       chomp;
       my @cols=split(/\s+/,$_);
       $InferredEuropeans{$cols[0]}=1;
    }
   close INPUT;
   my @AllInferredEuropeans=();
   my @AllSelectedIndex=();
  my %FilterSites=(); 
 my @VariantTypes=("SNP","INDEL");
 # /data/ruderferlab/projects/sweden/data/exome/swexm_INDEL.vcf
 #  /data/ruderferlab/projects/sweden/data/exome/swexm_SNP.vcf
  for(my $iType=0; $iType<@VariantTypes;$iType++)
  {
     my $iLineNumber=0;
     my $iFilterFile="/data/ruderferlab/projects/sweden/data/exome/swexm_$VariantTypes[$iType].vcf";
     open(INPUT,$iFilterFile) || die "can’t open $iFilterFile";
     while (<INPUT>)
     {   
       chomp;
       if($_ =~ m/^#/)
       {
         next;
       }
       my @cols=split(/\s+/,$_);
       next if($cols[0] eq "X" ||$cols[0] eq "Y"||$cols[0] eq "MT");
       $FilterSites{$cols[2]}=1;
       $iLineNumber++;
       #last if($iLineNumber>200);
     }   
     close INPUT;
   }


    my $InferredEuropeansWESFile="/data/ruderferlab/projects/sweden/data/exome/swexm_InferredEuropeans.vcf";  
     open OUT, ">$InferredEuropeansWESFile" or die "Can't open Output file:$InferredEuropeansWESFile!"; 
     my $WESFile="/data/ruderferlab/projects/sweden/data/exome/swexm.vcf.gz";
     open (INPUT, "gunzip -c $WESFile|") or die "gunzip  $WESFile: $!";;
     while (<INPUT>)
     {  
       chomp;
        if($_ =~ m/^##/)
        {  
         next if($_=~/^##GATKCommandLine/);
         next if($_ =~/^##contig=<ID=GL/); 
         next if($_ =~/^##FILTER=<ID=VQSR/);
        print OUT "$_\n";
           next;
        }
        if($_ =~ m/^#/)
        {
           my @cols=split(/\s+/,$_); 
           for(my $ii=0;$ii<8;$ii++)
           {
             print OUT "$cols[$ii]\t";
           }
            print OUT "$cols[8]";
           for(my $iInd=9;$iInd<@cols;$iInd++)
           {
             if(defined($InferredEuropeans{$cols[$iInd]}))
             {
               print OUT "\t$cols[$iInd]";
             push @AllInferredEuropeans,$cols[$iInd];
              push @AllSelectedIndex,$iInd;
             } 
           }
           print OUT "\n";
           next;
        }
        my @cols=split(/\s+/,$_,10);
        next if($cols[6] ne "PASS");
        next if(!defined($FilterSites{$cols[2]}));
        @cols=split(/\s+/,$_);
        my %Gtypes_Number=(); 
          $Gtypes_Number{"./."}=0;
          $Gtypes_Number{"0/0"}=0; 
          $Gtypes_Number{"0/1"}=0; 
          $Gtypes_Number{"1/0"}=0;
          $Gtypes_Number{"1/1"}=0;
         my $iTotal=scalar(@AllSelectedIndex);
        my @AllSelectedSampleGtypes=();
        for(my $iCol=0;$iCol<@AllSelectedIndex;$iCol++)
        {
            my $Gtype=substr($cols[$AllSelectedIndex[$iCol]], 0,3);
            push @AllSelectedSampleGtypes,$cols[$AllSelectedIndex[$iCol]];
            $Gtypes_Number{$Gtype}++;
        }   
        my $iMissing=$Gtypes_Number{"./."};
        my $iRefRef=$Gtypes_Number{"0/0"};
        my $iRefAlt=$Gtypes_Number{"0/1"}+$Gtypes_Number{"1/0"};
        my $iAltAlt=$Gtypes_Number{"1/1"};
        my $iMissingRate=sprintf("%.4f",$iMissing/$iTotal); 
        next if($iMissingRate>=0.05);        
        for(my $ii=0;$ii<8;$ii++)
        {
          print OUT "$cols[$ii]\t";
        }
        print OUT "$cols[8]";
        for(my $iInd=0;$iInd<@AllSelectedSampleGtypes;$iInd++)
        {
         print OUT "\t$AllSelectedSampleGtypes[$iInd]";
         }
        print OUT "\n";
      # last;
    }
  close INPUT;
  close OUT;
