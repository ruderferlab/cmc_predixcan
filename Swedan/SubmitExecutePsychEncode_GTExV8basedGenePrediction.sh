#!/bin/bash
#SBATCH --mail-user=lide.han@vumc.org
#SBATCH --mail-type=FAIL
#SBATCH --ntasks=1
#SBATCH --time=9:00:00
#SBATCH --mem=15G
#SBATCH --array=1-442
#SBATCH --output=tmp_array_job_slurm_%A_%a.out
#SBATCH --account=vgi

echo "SLURM_JOBID: " $SLURM_JOBID
echo "SLURM_ARRAY_TASK_ID: " $SLURM_ARRAY_TASK_ID
echo "SLURM_ARRAY_JOB_ID: " $SLURM_ARRAY_JOB_ID

module load PLINK/1.9b_5.2
# prog44190,240501
#awk 'int((NR-1)/2406)+1=='$SLURM_ARRAY_TASK_ID /gpfs23/data/ruderferlab/projects/cmc/scripts/Swedan/SwedanPredictGeneExpressionCommandlineGTExV8_Eli-sw56.sh|sh

awk 'int((NR-1)/100)+1=='$SLURM_ARRAY_TASK_ID /gpfs23/data/ruderferlab/projects/cmc/scripts/Swedan/SwedanPredictGeneExpressionCommandlinePsychEncodeWeight_Eli-sw56.sh|sh
