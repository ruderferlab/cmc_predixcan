#!/usr/bin/perl -w
use Scalar::Util qw(looks_like_number);
 use strict;

 use lib "/home/hanl3/perl5/module/lib/site_perl/5.14.2";
 use Text::CSV_PP;
  use Tie::File; 
 use POSIX qw(WNOHANG);
 use IO::Uncompress::Gunzip qw($GunzipError);
 use warnings;
 use Scalar::Util qw(looks_like_number);


#my $iSpecificBatch=1;

my $iSpecificChr=$ARGV[0];
my $iSpecificBatch=$ARGV[1];
# /home/hanl3/cmc/scratch/sweden/MichiganImputaionInputdataResult/*/chr$iChr.dose.vcf.gz
# /home/hanl3/cmc/scratch/sweden/MichiganImputaionInputdataResult/*/chr*.info.gz

#Eli-s234  Eli-sw56  Eli-swe1
 my @BatchNameList=("Eli-swe1","Eli-sw56","Eli-s234");


  for(my $iBatch=$iSpecificBatch;$iBatch<($iSpecificBatch+1);$iBatch++)
  {

      my @Indlist_Chr1=();
      my @Indlist_OtherChrs=();
      my $iDuplicateLoci=0;
     my %UniqLoci=();
     for(my $iChr=$iSpecificChr;$iChr<($iSpecificChr+1);$iChr++)
    {
       my $FullVCFFile="/fs0/hanld/Eli_56/$BatchNameList[$iBatch]ImputedSNVs_1000GPV3Rsq03_Chr$iChr.vcf";
       my $WGSFile="/home/hanl3/cmc/scratch/sweden/MichiganImputaionInputdataResult/$BatchNameList[$iBatch]/chr$iChr.dose.vcf.gz";

     if($iBatch==0)
     {
        $FullVCFFile="/fs0/hanld/Eli_sw1/$BatchNameList[$iBatch]ImputedSNVs_1000GPV3Rsq03_Chr$iChr.dose.vcf";
        $WGSFile="/fs0/hanld/Eli_1/chr$iChr.dose.vcf.gz";
     }
     elsif($iBatch==2)
     {
         $FullVCFFile="/fs0/hanld/Eli_s234/$BatchNameList[$iBatch]ImputedSNVs_1000GPV3Rsq03_Chr$iChr.dose.vcf";
         $WGSFile="/fs0/hanld/Eli_234/chr$iChr.dose.vcf.gz";
     }
  
    open OUT, ">$FullVCFFile" or die "Can't open Output file:$FullVCFFile!"; 
     open (INPUT, "gunzip -c $WGSFile|") or die "gunzip  $WGSFile: $!";;
      while (<INPUT>)
      {  
       chomp;
        if($_ =~ m/^##/)
        {  
           if($iChr==1)
           {
            print OUT "$_\n";
           }
           next;
        }
         if($_ =~ m/^#/)
         { 
            print OUT "$_\n"; 
            next;
         }
         my @cols=split(/\s+/,$_);
        # $cols[2]="$cols[0]:$cols[1]"; 
         $cols[8]="GT";
         if(!defined($UniqLoci{$cols[2]}))
         {
          $UniqLoci{$cols[2]}=1;
          for(my $iCol=9;$iCol<@cols;$iCol++)
          {
           $cols[$iCol]=substr($cols[$iCol],0,3);
          }
          my $OneRowStr=join("\t",@cols);
          print OUT "$OneRowStr\n";
          }
         else
          {
             $iDuplicateLoci++;
  #         print "$cols[0]\t$cols[1]\t$cols[3]\t$cols[4]\n";
          }
        }
         close INPUT; 
         print "Duplicate loci:$iChr\t$iDuplicateLoci\n";
            close OUT;
         print "RemoveDuplicate Loci:$iDuplicateLoci\n";
     }
 }
 
