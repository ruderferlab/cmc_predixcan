#!/usr/bin/perl -w
use Scalar::Util qw(looks_like_number);
 use strict;

 use lib "/home/hanl3/perl5/module/lib/site_perl/5.14.2";
 use Text::CSV_PP;
  use Tie::File; 
 use POSIX qw(WNOHANG);
 use IO::Uncompress::Gunzip qw($GunzipError);
 use warnings;
 use Scalar::Util qw(looks_like_number);

  my $iLineNumber=0;
  my %AllSelectedSites_MissingRate_AltFreq=();
  my $SwedenMissingIndInfoFile="/data/ruderferlab/projects/cmc/scratch/sweden/WESSiteMissingRateInfo.txt";
  open(INPUT,$SwedenMissingIndInfoFile) || die "can’t open $SwedenMissingIndInfoFile";
  while (<INPUT>)
    {  
       if($iLineNumber==0)
       {
         $iLineNumber++;
         next;
       }
       chomp;
       my @cols=split(/\s+/,$_);
       next if($cols[8]==0);#remove single alleles
      if($cols[6]<0.01 && $cols[8]<0.01)#missing rate:cols[6], AltAF_cols[8]
      {
       $AllSelectedSites_MissingRate_AltFreq{$cols[0]}="$cols[6]\t$cols[8]";;
      }
     # last if($iLineNumber>10);
    $iLineNumber++; 
    }
    close INPUT;
    my $Size=keys %AllSelectedSites_MissingRate_AltFreq;
    print "Sites Size:$Size\n";

      my %EnsembleST_SG=();#ENST to ENSG
       $iLineNumber=0;#ENST to ENSG
       my $EnsembleIDFile="/home/hanl3/cmc/database/EnsembleGeneID.txt";
      open(INPUT,$EnsembleIDFile) || die "can’t open $EnsembleIDFile";
        while (<INPUT>)
        {
           chomp;
             my @cols=split(/\t/,$_);
           if($iLineNumber==0)
           {
             $iLineNumber++;
              next;
           }
          $EnsembleST_SG{$cols[1]}=$cols[12];
        }
      close INPUT;
    $iLineNumber=0;
    my %GeneName_PLI_OE_Score=();
    my @RepeatGeneList=();
    my $GenePli_OEscoreFile="/home/hanl3/cmc/files/constraint.txt";
    open(INPUT,$GenePli_OEscoreFile) || die "can’t open $GenePli_OEscoreFile";
    while (<INPUT>)
    {  
       if($iLineNumber==0)
       {       
         $iLineNumber++;
         # gene	transcript	canonical	obs_lof	exp_lof(5)	oe_lof	oe_lof_lower	oe_lof_upper	obs_mis	exp_mis(10)	oe_mis	oe_mis_lower	oe_mis_upper	obs_syn	exp_syn(15)	oe_syn	oe_syn_lower	oe_syn_upper	lof_z	mis_z(20)	syn_z	pLI	pRec	pNull	gene_issues(25)
          my @cols=split(/\s+/,$_);
         print "$cols[5]\t$cols[21]\n";# oe_lof[5], pLI[21]
         next;   
       }       
        chomp;  
        my @cols=split(/\s+/,$_);
        
        if($cols[2] eq "true" && $cols[5] ne "NA")
        {
          my $ENSG=$EnsembleST_SG{$cols[1]};
            $GeneName_PLI_OE_Score{$ENSG}="$cols[5]\t$cols[21]";
         }
      }  
    close INPUT;
     $Size=keys %GeneName_PLI_OE_Score;
    print "ENSG Sites Size:$Size\n";
  
 #Update CADD information
# /fs0/hanld/CADD/CADDv1.4_hg19_InDels_inclAnno_subset.tsv.gz
#/fs0/hanld/CADD/CADDv1.4_hg19_SNVs_inclAnno_subset.tsv.gz
   my %VariantTypes_CADD=();
    my @VariantTypes=("InDels","SNVs");
    for(my $iType=0;$iType<2;$iType++)
    {
       my  $iLineNumber=0;
       my $CADDFile="/fs0/hanld/CADD/CADDv1.4_hg19_$VariantTypes[$iType]_inclAnno_subset.tsv.gz";
       print "$CADDFile\n"; 
       open (INPUT, "gunzip -c $CADDFile|") or die "gunzip  $CADDFile: $!";;
        while (<INPUT>)
        { 
           next if($_=~/^##/);      
           chomp;  
           if($iLineNumber==0)
           { 
               $iLineNumber++;
               my @cols=split(/\s++/,$_);
               next;
            }
           my @cols=split(/\s++/,$_);
           my $CombinedSiteStr="$cols[0]:$cols[1]:$cols[2]:$cols[3]";
           next if(!defined($AllSelectedSites_MissingRate_AltFreq{$CombinedSiteStr}));#Make sure the variant have good quality
           $VariantTypes_CADD{$CombinedSiteStr}=$cols[16];  
           #print "$_\n";        
          # last;
         }
        close INPUT;
     }
     $Size=keys %VariantTypes_CADD;
    print "CADD file Sites Size:$Size\n";

  my $SwedenAnnovarInfoFile="/data/ruderferlab/projects/cmc/scratch/sweden/SwedenVariantAnotaiton_GeneInfo.txt";
  open OUT, ">$SwedenAnnovarInfoFile" or die "Can't open Output file:$SwedenAnnovarInfoFile!";
     $iLineNumber=0;
     my @SelectedItems=();
     my @SelectedIndex=();  
      my $AnnotationFile="/data/ruderferlab/projects/cmc/scratch/sweden/Annovar/swex/vcf/annovar/allchroms.genome_annovar.vcf.gz";
     open (INPUT, "gunzip -c $AnnotationFile|") or die "gunzip  $AnnotationFile: $!";;
     while (<INPUT>)
     {       
       chomp;  
        my @cols=split(/\t/,$_);
        if($iLineNumber==0) 
        {  
          $iLineNumber++;
          for(my $ii=0;$ii<17;$ii++)
          {
           push  @SelectedItems,$cols[$ii];
            push @SelectedIndex,$ii;
          }
           push @SelectedIndex,53;
           push @SelectedIndex,83;
           push @SelectedIndex,89;
           push @SelectedIndex,104;
           push @SelectedIndex,116;
           push  @SelectedItems, $cols[53];#     CADD_phred
           push  @SelectedItems,$cols[83];#gnomAD_genome_AL
           push  @SelectedItems,$cols[89];#gnomAD_genome_NFE
           push  @SelectedItems,$cols[104];
           push  @SelectedItems,$cols[116];
           for(my $ii=0;$ii<@SelectedItems;$ii++)
           {
            print OUT  "$SelectedItems[$ii]\t";
           }
           print OUT "MissingRate\tAltFreq";
           print OUT "\tOE_ZScore\tPLI\n";
           next;   
        }
         next if($cols[89] ne "."&& $cols[89]>=0.01);#Remove common variants
         my $CombinedSiteStr="$cols[0]:$cols[1]:$cols[2]:$cols[3]";
         next if(!defined($AllSelectedSites_MissingRate_AltFreq{$CombinedSiteStr}));#Make sure the variant have good quality
         next if($cols[8] ne $cols[12]);  #make sure two annotation same 
         next if($cols[8] !~/^exonic/);#Only using coding variants
         my @GeneList=split(/[;,]/,$cols[13]);
         my %AllGeneID=(); #ENSGID_cols13,WGSID_cols32
         for(my $iG=0;$iG<@GeneList;$iG++)
         {
            my @ENSGList=split(/[(]/,$GeneList[$iG]);
            if($ENSGList[0]=~/^ENSG/)
            {
             $AllGeneID{$ENSGList[0]}=1;
            }
         }
        foreach my $GeneID (sort keys %AllGeneID)#split gene annotation information by ENSG_ID
        {
           $cols[13]=$GeneID;
           for(my $iCol=0;$iCol<17;$iCol++)
           {       
              print OUT "$cols[$SelectedIndex[$iCol]]\t";
           } 
           if(defined($VariantTypes_CADD{$CombinedSiteStr}))
           {
              print OUT "$VariantTypes_CADD{$CombinedSiteStr}\t";
           }
           else
           {
              print OUT "$cols[17]\t";
           }
           for(my $iCol=18;$iCol<@SelectedIndex;$iCol++)
           {      
              print OUT "$cols[$SelectedIndex[$iCol]]\t";
           }             
             print OUT "$AllSelectedSites_MissingRate_AltFreq{$CombinedSiteStr}";     
           if(defined($GeneName_PLI_OE_Score{$cols[13]}))
           {
              print OUT "\t$GeneName_PLI_OE_Score{$cols[13]}"; #="$cols[5]\t$cols[21]";
           } 
           else
           {
              print OUT"\tNA\tNA";
           }
            print OUT "\n";
         }
          $iLineNumber++;
     # last if($iLineNumber>10);
     }
   close INPUT;
    print "number of variants:$iLineNumber\n";
   close OUT;

