#!/usr/bin/perl -w
use Scalar::Util qw(looks_like_number);
 use strict;

 use lib "/home/hanl3/perl5/module/lib/site_perl/5.14.2";
 use Text::CSV_PP;
  use Tie::File; 
 use POSIX qw(WNOHANG);
 use IO::Uncompress::Gunzip qw($GunzipError);
 use warnings;
 use Scalar::Util qw(looks_like_number);


#       /data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-s234st_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt
#       /data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-sw56st_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt
#       /data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-swe1st_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt


#Eli-s234  Eli-sw56  Eli-swe1

my @ChromosomeBasedDir=("Eli_sw1","Eli_sw56","Eli_s234");
 my @BatchNameList=("Eli-swe1","Eli-sw56","Eli-s234");

  my %AllPassedQCInds=();
  for(my $iBatch=0;$iBatch<@BatchNameList;$iBatch++)
  {
       my $IndInfoFile="/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8Based$BatchNameList[$iBatch]st_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt";
       open(INPUT,$IndInfoFile) || die "can’t open $IndInfoFile";
       while (<INPUT>)
       {
         chomp;
         $_ =~ s/^\s+|\s+$//g;      
         my @cols=split(/\s++/,$_);
          print "$cols[8]\t$cols[9]\n";
         for(my $iInd=9;$iInd<@cols;$iInd++)
         {
           $AllPassedQCInds{$cols[$iInd]}=1;
         }
         last;
      }
      close INPUT;
   }

   my $LineNum=0;
   my $SwedenPassedQCSampleInfoFile="/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/Sweden/SwedenPassedQCSampleInfo.txt";
   open OUT, ">$SwedenPassedQCSampleInfoFile" or die "Can't open Output file:$SwedenPassedQCSampleInfoFile!"; 
   my $SwedenIndfoFile="/data/ruderferlab/projects/sweden/files/swexm.cov.tsv";
   open(INPUT,$SwedenIndfoFile) || die "can’t open $SwedenIndfoFile";
   while (<INPUT>)
   {
      chomp;
       $_ =~ s/^\s+|\s+$//g;
       if($LineNum==0)
       {
        print OUT "$_\n";
        $LineNum++;
        next;
       }
       my @cols=split(/\s++/,$_);
       if(defined($AllPassedQCInds{$cols[0]}))
       {
        print OUT "$_\n";
       }
    }
    close INPUT;
    close OUT; 
