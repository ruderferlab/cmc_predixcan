#!/usr/bin/perl -w
use Scalar::Util qw(looks_like_number);
 use strict;

 use lib "/home/hanl3/perl5/module/lib/site_perl/5.14.2";
 use Text::CSV_PP;
  use Tie::File; 
 use POSIX qw(WNOHANG);
 use IO::Uncompress::Gunzip qw($GunzipError);
 use warnings;
 use Scalar::Util qw(looks_like_number);

#Input file List:
#Annotated files
#    /data/ruderferlab/projects/cmc/scratch/sweden/SwedenVariantAnotaiton_GeneInfo.txt# 895024 lines
#     /data/ruderferlab/projects/sweden/data/exome/swexm_InferredEuropeans.vcf.gz
#     /data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/Sweden/SwedenPassedQCSampleInfo.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/PsychEncodePredictionEx
#PsychEncodeWeightBasedEli-s234_FullGenotype_Imputed_GenePredictedExExGSINTZScore.txt       PsychEncodeWeightBasedEli-sw56_FullGenotype_Imputed_GenePredictedExExGSINTZScore.txt       PsychEncodeWeightBasedEli-swe1_FullGenotype_Imputed_GenePredictedExExGSINTZScore.txt
#PsychEncodeWeightBasedEli-s234_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt  PsychEncodeWeightBasedEli-sw56_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt  PsychEncodeWeightBasedEli-swe1_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt
#PsychEncodeWeightBasedEli-s234_Haplotype1_Imputed_GenePredictedExExGSINTZScore.txt         PsychEncodeWeightBasedEli-sw56_Haplotype1_Imputed_GenePredictedExExGSINTZScore.txt         PsychEncodeWeightBasedEli-swe1_Haplotype1_Imputed_GenePredictedExExGSINTZScore.txt
#PsychEncodeWeightBasedEli-s234_Haplotype1_Imputed_GenePredictedExExGSStandardZScore.txt    PsychEncodeWeightBasedEli-sw56_Haplotype1_Imputed_GenePredictedExExGSStandardZScore.txt    PsychEncodeWeightBasedEli-swe1_Haplotype1_Imputed_GenePredictedExExGSStandardZScore.txt
#PsychEncodeWeightBasedEli-s234_Haplotype2_Imputed_GenePredictedExExGSINTZScore.txt         PsychEncodeWeightBasedEli-sw56_Haplotype2_Imputed_GenePredictedExExGSINTZScore.txt         PsychEncodeWeightBasedEli-swe1_Haplotype2_Imputed_GenePredictedExExGSINTZScore.txt
#PsychEncodeWeightBasedEli-s234_Haplotype2_Imputed_GenePredictedExExGSStandardZScore.txt    PsychEncodeWeightBasedEli-sw56_Haplotype2_Imputed_GenePredictedExExGSStandardZScore.txt    PsychEncodeWeightBasedEli-swe1_Haplotype2_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-s234st_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-s234st_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-s234st_DLPFC_BA9_Haplotype1_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-s234st_DLPFC_BA9_Haplotype1_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-s234st_DLPFC_BA9_Haplotype2_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-s234st_DLPFC_BA9_Haplotype2_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-s234ut_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-s234ut_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-s234ut_DLPFC_BA9_Haplotype1_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-s234ut_DLPFC_BA9_Haplotype1_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-s234ut_DLPFC_BA9_Haplotype2_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-s234ut_DLPFC_BA9_Haplotype2_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-s234xt_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-s234xt_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-sw56st_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-sw56st_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-sw56ut_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-sw56ut_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-sw56ut_DLPFC_BA9_Haplotype1_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-sw56ut_DLPFC_BA9_Haplotype1_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-sw56ut_DLPFC_BA9_Haplotype2_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-sw56ut_DLPFC_BA9_Haplotype2_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-sw56xt_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-sw56xt_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-swe1st_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-swe1st_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-swe1st_DLPFC_BA9_Haplotype1_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-swe1st_DLPFC_BA9_Haplotype1_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-swe1st_DLPFC_BA9_Haplotype2_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-swe1st_DLPFC_BA9_Haplotype2_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-swe1ut_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-swe1ut_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-swe1ut_DLPFC_BA9_Haplotype1_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-swe1ut_DLPFC_BA9_Haplotype1_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-swe1ut_DLPFC_BA9_Haplotype2_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-swe1ut_DLPFC_BA9_Haplotype2_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-swe1xt_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-swe1xt_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-swe1xt_DLPFC_BA9_Haplotype1_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-swe1xt_DLPFC_BA9_Haplotype1_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-swe1xt_DLPFC_BA9_Haplotype2_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-swe1xt_DLPFC_BA9_Haplotype2_Imputed_GenePredictedExExGSStandardZScore.txt

#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/PsychEncodePredictionEx/
#PsychEncodeWeightBasedEli-s234_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt 
#PsychEncodeWeightBasedEli-sw56_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt
#PsychEncodeWeightBasedEli-swe1_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-s234st_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSINTZScore.txt


 sub PredictedZScoreCombined
    {
         my ($hGeneIDListStr,$PredictedZscoreFile,$hGene_WGSID,$iGeneIDCol)=@_;;
         my %GeneID_WGSDID_ZScore=%{$hGene_WGSID};
         my %GeneIDList=%{$hGeneIDListStr};
         my %ExistGeneID_WGSIDZscore=();
          my  $iLineNumber=0;
          my @WGSID_ZScore=();
          open(INPUT, $PredictedZscoreFile) || die "can’t open $PredictedZscoreFile";  
          while (<INPUT>)
          {   
           chomp;
           if($iLineNumber==0)
           {
              my @cols=split(/\t/,$_);
              $iLineNumber++;
              @WGSID_ZScore=@cols;
              next;
           }
           my  @cols=split(/\t/,$_,8);
           next if(!defined($GeneIDList{$cols[$iGeneIDCol]}));
           @cols=split(/\t/,$_);
           for(my $ii=0;$ii<@WGSID_ZScore;$ii++)
           {
            if(defined($GeneID_WGSDID_ZScore{$cols[$iGeneIDCol]}{$WGSID_ZScore[$ii]}))
            { 
             $ExistGeneID_WGSIDZscore{$cols[$iGeneIDCol]}{$WGSID_ZScore[$ii]}=$cols[$ii];
            } 
          }  
        }  
       close INPUT;
        my $iNonMissingValues=0;
        my $iMissingValues=0;
        foreach my $GeneID (sort keys %GeneID_WGSDID_ZScore) {
           foreach my $WGSID (keys %{ $GeneID_WGSDID_ZScore{$GeneID} }) {
              if(defined($ExistGeneID_WGSIDZscore{$GeneID}{$WGSID}))
             {
               $GeneID_WGSDID_ZScore{$GeneID}{$WGSID}.="\t$ExistGeneID_WGSIDZscore{$GeneID}{$WGSID}";
               $iNonMissingValues++;
            }      
           else
          {  
            $GeneID_WGSDID_ZScore{$GeneID}{$WGSID}.="\tNA";
            $iMissingValues++;
          }
        }
       } 
       print "$PredictedZscoreFile\t$iNonMissingValues\t$iMissingValues\n";
       return %GeneID_WGSDID_ZScore; 
    }


#PsychEncodeWeightBasedEli-s234_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt
#PsychEncodeWeightBasedEli-sw56_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt
#PsychEncodeWeightBasedEli-swe1_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt

  my @BatchLongNameList=("s234","sw56","swe1");
  my @BatchList=("s234","sw56","swe1");
  my @Methods=("st","ut","xt");
  my %AllGeneWithZScores=();
  for(my $iBatch=0;$iBatch<3;$iBatch++)
  {
   my $iLineNumber=0;
    my $sPsychencodeZScoreFile="/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/PsychEncodePredictionEx/PsychEncodeWeightBasedEli-$BatchList[$iBatch]_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt";
    open(INPUT, $sPsychencodeZScoreFile) || die "can’t open $sPsychencodeZScoreFile";
    while (<INPUT>)
    {   
        chomp;
        if($iLineNumber==0)
        {
          my @cols=split(/\s++/,$_);
           $iLineNumber++;
          next;
         }
       my @cols=split(/\s++/,$_,7);
       $AllGeneWithZScores{$cols[5]}=1;#ENSG geneid cols[5]
      # last;
     }
     close INPUT;
  }
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-swe1xt_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSINTZScore.txt
  my $SizeofGenes=keys %AllGeneWithZScores;
  print "SizeofGenes:$SizeofGenes\n";
  for(my $iBatch=0;$iBatch<3;$iBatch++)
  {
   for(my $iM=0;$iM<3;$iM++)
   {
   my $iLineNumber=0;
    my $sGTExV8ZScoreFile="/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-$BatchList[$iBatch]$Methods[$iM]_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt";
    open(INPUT, $sGTExV8ZScoreFile) || die "can’t open $sGTExV8ZScoreFile";
    while (<INPUT>)
    {   
        chomp;
        if($iLineNumber==0)
        {
          my @cols=split(/\s++/,$_);
           $iLineNumber++;
          next;
         }
       my @cols=split(/\s++/,$_,7);
       $AllGeneWithZScores{$cols[5]}=1;#ENSG geneid cols[5]
       #last;
     }
     close INPUT;
   } 
 }
   $SizeofGenes=keys %AllGeneWithZScores;
   print "SizeofGenes:$SizeofGenes\n";  
 
   my %RareSites=();
   my $iDuplicateSites=0;
  my $iLineNumber=0;
  my $HeadingFile="";
   my $AnnotationFile="/data/ruderferlab/projects/cmc/scratch/sweden/SwedenVariantAnotaiton_GeneInfo.txt";
   open(INPUT, $AnnotationFile) || die "can’t open $AnnotationFile";
    while (<INPUT>)
    {   
        chomp;
        if($iLineNumber==0)
        {
         $HeadingFile=$_;
           $iLineNumber++;
            next;
         }
       my @cols=split(/\t/,$_);#Gene_ens 14th col
       next if($cols[23]==0);#remove Alt AF==0
     #  next if(!defined($AllGeneWithZScores{$cols[13]})); #remove the Variants without corresponding Gene expression
       my $CombinedSites="$cols[0]:$cols[1]:$cols[2]:$cols[3]";
       if(!defined($RareSites{$CombinedSites}))
       {
         $RareSites{$CombinedSites}=$_; 
       }
      else
       {
        $RareSites{$CombinedSites}.="?$_";
     #  print "$CombinedSites\n"; 
       $iDuplicateSites++;
    #  last;
      }
    }
   close INPUT;
   my $SizeofRareSites=keys %RareSites;
   print "SizeofRareSites:$SizeofRareSites\tDuplicates:$iDuplicateSites\n";

    my $OutputFile="/data/ruderferlab/projects/cmc/scratch/sweden/SwedenVariantAnotaiton_GeneInfo_RareGtype1.txt";
    open OUT, ">$OutputFile" or die "Can't open Output file:$OutputFile!";
    print OUT "$HeadingFile\tWESID_Rare\tGT_DP_GQ_Rare\n";
    my @WESID=();
    my @WESIDIndex=();
     my $WESDataFile="/data/ruderferlab/projects/sweden/data/exome/swexm_InferredEuropeans.vcf.gz";
     open (INPUT, "gunzip -c $WESDataFile|") or die "gunzip  $WESDataFile: $!";;
      while (<INPUT>)
      {
       chomp;  
       next  if($_ =~ m/^##/);
       if($_ =~ m/^#/)
       {       
          @WESID=split(/\s++/,$_);         
          next;   
       }       
       my @cols=split(/\s++/,$_,10);   
      next if(!defined($RareSites{$cols[2]}));#Filter the variants by position
      next if($cols[6] ne "PASS");
      @cols=split(/\s++/,$_);
      my @SelectedRareGTypesStr=();
      my @SelectedRareInds=();
      for(my $iCol=9;$iCol<@cols;$iCol++)
      {
         my $GTypeStr=substr($cols[$iCol],0,3); 
         if(index($GTypeStr,'1') != -1)
         { 
           my @GtypeList=split(/:/,$cols[$iCol]);
          $cols[$iCol]="$GtypeList[0]:$GtypeList[2]:$GtypeList[3]";    
          push @SelectedRareGTypesStr,$cols[$iCol];
          push @SelectedRareInds,$WESID[$iCol];  
         }
      }     
      my $AllSelectedGtypeStr=join("?",@SelectedRareGTypesStr);
      my $AllSelectedIndIDs=join("?",@SelectedRareInds);
      my @AnnotationList=split(/\?/,$RareSites{$cols[2]});
      for(my $ii=0;$ii<@AnnotationList;$ii++)
      {
       print OUT "$AnnotationList[$ii]\t$AllSelectedIndIDs\t$AllSelectedGtypeStr\n";
     }
     #last;
    }
   close INPUT;
   close OUT;

