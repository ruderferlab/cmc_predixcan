#!/usr/bin/perl -w
use Scalar::Util qw(looks_like_number);
 use strict;

 use lib "/home/hanl3/perl5/module/lib/site_perl/5.14.2";
 use Text::CSV_PP;
  use Tie::File; 
 use POSIX qw(WNOHANG);
 use IO::Uncompress::Gunzip qw($GunzipError);
 use warnings;
 use Scalar::Util qw(looks_like_number);

#Input file List:
#Annotated files
#    /data/ruderferlab/projects/cmc/scratch/sweden/SwedenVariantAnotaiton_GeneInfo.txt# 895024 lines
#     /data/ruderferlab/projects/sweden/data/exome/swexm_InferredEuropeans.vcf.gz
#     /data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/Sweden/SwedenPassedQCSampleInfo.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/PsychEncodePredictionEx
#PsychEncodeWeightBasedEli-s234_FullGenotype_Imputed_GenePredictedExExGSINTZScore.txt       PsychEncodeWeightBasedEli-sw56_FullGenotype_Imputed_GenePredictedExExGSINTZScore.txt       PsychEncodeWeightBasedEli-swe1_FullGenotype_Imputed_GenePredictedExExGSINTZScore.txt
#PsychEncodeWeightBasedEli-s234_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt  PsychEncodeWeightBasedEli-sw56_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt  PsychEncodeWeightBasedEli-swe1_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt
#PsychEncodeWeightBasedEli-s234_Haplotype1_Imputed_GenePredictedExExGSINTZScore.txt         PsychEncodeWeightBasedEli-sw56_Haplotype1_Imputed_GenePredictedExExGSINTZScore.txt         PsychEncodeWeightBasedEli-swe1_Haplotype1_Imputed_GenePredictedExExGSINTZScore.txt
#PsychEncodeWeightBasedEli-s234_Haplotype1_Imputed_GenePredictedExExGSStandardZScore.txt    PsychEncodeWeightBasedEli-sw56_Haplotype1_Imputed_GenePredictedExExGSStandardZScore.txt    PsychEncodeWeightBasedEli-swe1_Haplotype1_Imputed_GenePredictedExExGSStandardZScore.txt
#PsychEncodeWeightBasedEli-s234_Haplotype2_Imputed_GenePredictedExExGSINTZScore.txt         PsychEncodeWeightBasedEli-sw56_Haplotype2_Imputed_GenePredictedExExGSINTZScore.txt         PsychEncodeWeightBasedEli-swe1_Haplotype2_Imputed_GenePredictedExExGSINTZScore.txt
#PsychEncodeWeightBasedEli-s234_Haplotype2_Imputed_GenePredictedExExGSStandardZScore.txt    PsychEncodeWeightBasedEli-sw56_Haplotype2_Imputed_GenePredictedExExGSStandardZScore.txt    PsychEncodeWeightBasedEli-swe1_Haplotype2_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-s234st_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-s234st_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-s234st_DLPFC_BA9_Haplotype1_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-s234st_DLPFC_BA9_Haplotype1_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-s234st_DLPFC_BA9_Haplotype2_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-s234st_DLPFC_BA9_Haplotype2_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-s234ut_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-s234ut_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-s234ut_DLPFC_BA9_Haplotype1_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-s234ut_DLPFC_BA9_Haplotype1_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-s234ut_DLPFC_BA9_Haplotype2_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-s234ut_DLPFC_BA9_Haplotype2_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-s234xt_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-s234xt_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-sw56st_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-sw56st_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-sw56ut_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-sw56ut_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-sw56ut_DLPFC_BA9_Haplotype1_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-sw56ut_DLPFC_BA9_Haplotype1_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-sw56ut_DLPFC_BA9_Haplotype2_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-sw56ut_DLPFC_BA9_Haplotype2_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-sw56xt_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-sw56xt_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-swe1st_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-swe1st_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-swe1st_DLPFC_BA9_Haplotype1_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-swe1st_DLPFC_BA9_Haplotype1_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-swe1st_DLPFC_BA9_Haplotype2_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-swe1st_DLPFC_BA9_Haplotype2_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-swe1ut_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-swe1ut_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-swe1ut_DLPFC_BA9_Haplotype1_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-swe1ut_DLPFC_BA9_Haplotype1_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-swe1ut_DLPFC_BA9_Haplotype2_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-swe1ut_DLPFC_BA9_Haplotype2_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-swe1xt_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-swe1xt_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-swe1xt_DLPFC_BA9_Haplotype1_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-swe1xt_DLPFC_BA9_Haplotype1_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-swe1xt_DLPFC_BA9_Haplotype2_Imputed_GenePredictedExExGSINTZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-swe1xt_DLPFC_BA9_Haplotype2_Imputed_GenePredictedExExGSStandardZScore.txt

#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/PsychEncodePredictionEx/
#PsychEncodeWeightBasedEli-s234_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt 
#PsychEncodeWeightBasedEli-sw56_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt
#PsychEncodeWeightBasedEli-swe1_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt
#/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-s234st_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSINTZScore.txt

=cut;
 sub PredictedZScoreCombined
    {
         my ($hGeneIDListStr,$PredictedZscoreFile,$hGene_WGSID,$iGeneIDCol)=@_;;
         my %GeneID_WGSDID_ZScore=%{$hGene_WGSID};
         my %GeneIDList=%{$hGeneIDListStr};
         my %ExistGeneID_WGSIDZscore=();
          my  $iLineNumber=0;
          my @WGSID_ZScore=();
          open(INPUT, $PredictedZscoreFile) || die "can’t open $PredictedZscoreFile";  
          while (<INPUT>)
          {   
           chomp;
           if($iLineNumber==0)
           {
              my @cols=split(/\t/,$_);
              $iLineNumber++;
              @WGSID_ZScore=@cols;
              next;
           }
           my  @cols=split(/\t/,$_,8);
           next if(!defined($GeneIDList{$cols[$iGeneIDCol]}));
           @cols=split(/\t/,$_);
           for(my $ii=0;$ii<@WGSID_ZScore;$ii++)
           {
            if(defined($GeneID_WGSDID_ZScore{$cols[$iGeneIDCol]}{$WGSID_ZScore[$ii]}))
            { 
             $ExistGeneID_WGSIDZscore{$cols[$iGeneIDCol]}{$WGSID_ZScore[$ii]}=$cols[$ii];
            } 
          }  
        }  
       close INPUT;
        my $iNonMissingValues=0;
        my $iMissingValues=0;
        foreach my $GeneID (sort keys %GeneID_WGSDID_ZScore) {
           foreach my $WGSID (keys %{ $GeneID_WGSDID_ZScore{$GeneID} }) {
              if(defined($ExistGeneID_WGSIDZscore{$GeneID}{$WGSID}))
             {
               $GeneID_WGSDID_ZScore{$GeneID}{$WGSID}.="\t$ExistGeneID_WGSIDZscore{$GeneID}{$WGSID}";
               $iNonMissingValues++;
            }      
           else
          {  
            $GeneID_WGSDID_ZScore{$GeneID}{$WGSID}.="\tNA";
            $iMissingValues++;
          }
        }
       } 
       print "$PredictedZscoreFile\t$iNonMissingValues\t$iMissingValues\n";
       return %GeneID_WGSDID_ZScore; 
    }

  my @BatchLongNameList=("s234","sw56","swe1");
  my @BatchList=("s234","sw56","swe1");
  my @Methods=("st","ut","xt");
  
my @GTypes=("FullGenotype","Haplotype1","Haplotype2");
 
 my @ZScroeTypes=("Standard","INT");
 
  my @GTypesConcise=("fg","ht1","ht2");
  my @ZScroeTypesConcise=("std","int");

    my %AllPsychEncodeGeneList=();#Obtain all genes'ENSGID from Psychencode weight
    my %AllPsychEncodeIndList_Batch=(); #Obtain all individuals' batch information from Psychencode 
  for(my $iBatch=0;$iBatch<3;$iBatch++)#3
    {
      my $iLineNumber=0;
      my $sPsychencodeZScoreFile="/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/PsychEncodePredictionEx/PsychEncodeWeightBasedEli-$BatchList[$iBatch]_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt";
      open(INPUT, $sPsychencodeZScoreFile) || die "can’t open $sPsychencodeZScoreFile";
      while (<INPUT>)
      {
         chomp;
         if($iLineNumber==0)
         {
           my @cols=split(/\s++/,$_);
           for(my $ii=9;$ii<@cols;$ii++)
           {
             $AllPsychEncodeIndList_Batch{$cols[$ii]}=$iBatch;
            }
           $iLineNumber++;
            next;
          }
         my @cols=split(/\s++/,$_,7);
         $AllPsychEncodeGeneList{$cols[5]}=1;#ENSG geneid cols[5]
          $iLineNumber++;
       }
       close INPUT;
     }
my $SizeofGenes=keys %AllPsychEncodeGeneList;
 print "Number of Psychode Genes:$SizeofGenes\n ";

     my %AllGTExV8GeneList=();#Obtain all genes'ENSGID from GTExV8 weight
  my %AllGTExV8IndList_Batch=(); #Obtain all individuals' batch information from GTExV8
  for(my $iBatch=0;$iBatch<3;$iBatch++)#3
  {
   for(my $iM=0;$iM<3;$iM++)#3
   {
     my $iLineNumber=0;
     my $sGTExV8ZScoreFile="/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-$BatchList[$iBatch]$Methods[$iM]_DLPFC_BA9_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt";
     open(INPUT, $sGTExV8ZScoreFile) || die "can’t open $sGTExV8ZScoreFile";
     while (<INPUT>)
     {
        chomp;
        if($iLineNumber==0)
        {
          my @cols=split(/\s++/,$_);
          for(my $ii=9;$ii<@cols;$ii++)
          {
             $AllGTExV8IndList_Batch{$cols[$ii]}=$iBatch;
          }  
           $iLineNumber++;
           next;
         }
       my @cols=split(/\s++/,$_,7);
       $AllGTExV8GeneList{$cols[5]}=1;#ENSG geneid cols[5]
     }
     close INPUT;
  }
 }

$SizeofGenes=keys %AllGTExV8GeneList;
 print "Number of GTExV8 Genes:$SizeofGenes\n ";

    my $iLineNumber=0;
   my %IndID_DiseaseSex=();
    my $IndInfoFile="/data/ruderferlab/projects/cmc/results/LOFAffectingPredictedGeneExpression/Sweden/SwedenPassedQCSampleInfo.txt";
    open(INPUT, $IndInfoFile) || die "can’t open $IndInfoFile";
    while (<INPUT>)
    {
        chomp;
        if($iLineNumber==0)
        {
          my @cols=split(/\s++/,$_);#13 PT-BSRW 27
          $iLineNumber++;
          next;
        }
       my @cols=split(/\s++/,$_);
       $IndID_DiseaseSex{$cols[0]}="$cols[4]\t$cols[5]\t$cols[6]";
     }
     close INPUT;
 my $SizeofInds=keys %IndID_DiseaseSex;
 print "Number of GTExV8 Indss:$SizeofInds\n";

    $iLineNumber=0;
    my %PsychEncodeENSGID_Ind_Zscore=();
    my %GTExV8ENSGID_Ind_Zscore=();

    my $GTypeInformationFile="/data/ruderferlab/projects/cmc/scratch/sweden/SwedenVariantAnotaiton_GeneInfo_RareGtype.txt";
    open(INPUT, $GTypeInformationFile) || die "can’t open $GTypeInformationFile";
    while (<INPUT>)
    {
        chomp;
        if($iLineNumber==0)
        {
          my @cols=split(/\t/,$_);#13 PT-BSRW 27
          $iLineNumber++;
          next;
        }
       my @cols=split(/\t/,$_);
       my $iLength=scalar(@cols); next if($iLength<=27);
       my @AllInds=split(/\?/, $cols[26]);
      
       for(my $ii=0;$ii<@AllInds;$ii++)
       {
        if(defined($AllPsychEncodeGeneList{$cols[13]}))
        {
         $PsychEncodeENSGID_Ind_Zscore{$cols[13]}{$AllInds[$ii]}="";
         }
        if(defined($AllGTExV8GeneList{$cols[13]}))
        {
         $GTExV8ENSGID_Ind_Zscore{$cols[13]}{$AllInds[$ii]}="";
        }
       }     
       $iLineNumber++; 
        #Test code
       #if($iLineNumber>10)
      # {
      #  last;
      # }
    }
   close INPUT;

  my @MissingScore_PsychEncode=();
  my @Heading_PsychEncode=();
 
  my %WESID_Batch=();
  for(my $iBatch=0;$iBatch<3;$iBatch++)
  {
      my $sPsychencodeZScoreFile="/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/PsychEncodePredictionEx/PsychEncodeWeightBasedEli-$BatchList[$iBatch]_FullGenotype_Imputed_GenePredictedExExGSStandardZScore.txt";
      my @AllAvailableWESID=();
       print "$sPsychencodeZScoreFile\n";
      open(INPUT, $sPsychencodeZScoreFile) || die "can’t open $sPsychencodeZScoreFile";
       while (<INPUT>)
       {
             chomp;
             my @cols=split(/\t/,$_);#13 PT-BSRW 27
             @AllAvailableWESID=@cols;
             last;
        }
       close INPUT;
       for(my $ii=9;$ii<@AllAvailableWESID;$ii++)
       {
         $WESID_Batch{$AllAvailableWESID[$ii]}=$BatchList[$iBatch];
       }
   }





  my $iPsychEncodeScoreNumber=0;
  for(my $iS=0;$iS<@ZScroeTypes;$iS++)
   {
    for(my $iG=0;$iG<@GTypes;$iG++)
     {
         $iPsychEncodeScoreNumber++;
      my $HeadingStr="$ZScroeTypesConcise[$iS]_$GTypesConcise[$iG]";
       push @Heading_PsychEncode,$HeadingStr;
       push @MissingScore_PsychEncode,"NA";
       my %PsychEncodeENSGID_Ind_Zscore_Batch=();
       for(my $iBatch=0;$iBatch<3;$iBatch++)
        {
           my $GeneIDIndex=5;
           if($iG!=0)
           {
             $GeneIDIndex=4;
           }

          my $sPsychencodeZScoreFile="/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/PsychEncodePredictionEx/PsychEncodeWeightBasedEli-$BatchList[$iBatch]_$GTypes[$iG]_Imputed_GenePredictedExExGS$ZScroeTypes[$iS]ZScore.txt";
         my @AllAvailableWESID=();       
         my  $iLineNumber=0;
         open(INPUT, $sPsychencodeZScoreFile) || die "can’t open $sPsychencodeZScoreFile";
         while (<INPUT>)
         {
            chomp;
            if($iLineNumber==0)
            {
              my @cols=split(/\t/,$_);#13 PT-BSRW 27
              @AllAvailableWESID=@cols;  
              $iLineNumber++;
              next;
            }
            my @cols=split(/\t/,$_);#13 PT-BSRW 27
            for(my $ii=$GeneIDIndex+1;$ii<@cols;$ii++)
            {
               if(defined($PsychEncodeENSGID_Ind_Zscore{$cols[$GeneIDIndex]}{$AllAvailableWESID[$ii]}))
               {
                 $PsychEncodeENSGID_Ind_Zscore_Batch{$cols[$GeneIDIndex]}{$AllAvailableWESID[$ii]}=$cols[$ii];
               }  
            }
          }
          close INPUT;
       }
      my $iNonMissingValues=0; 
      foreach my $GeneID (sort keys %PsychEncodeENSGID_Ind_Zscore)
       {
         foreach my $WGSID (keys %{ $PsychEncodeENSGID_Ind_Zscore{$GeneID} })
         {
             if($iPsychEncodeScoreNumber==1)
             {
               if(defined($PsychEncodeENSGID_Ind_Zscore_Batch{$GeneID}{$WGSID}))
               {
                 $iNonMissingValues++;
                 $PsychEncodeENSGID_Ind_Zscore{$GeneID}{$WGSID}=$PsychEncodeENSGID_Ind_Zscore_Batch{$GeneID}{$WGSID};
                }
               else
               {
                 $PsychEncodeENSGID_Ind_Zscore{$GeneID}{$WGSID}="NA";
               }
             }
             else
             {
               if(defined($PsychEncodeENSGID_Ind_Zscore_Batch{$GeneID}{$WGSID}))
               {
                $iNonMissingValues++;
                 $PsychEncodeENSGID_Ind_Zscore{$GeneID}{$WGSID}.="\t$PsychEncodeENSGID_Ind_Zscore_Batch{$GeneID}{$WGSID}";
                }  
               else
               {
                 $PsychEncodeENSGID_Ind_Zscore{$GeneID}{$WGSID}.="\tNA";
               }
             }
         }
       }
        print "$ZScroeTypesConcise[$iS]_$GTypesConcise[$iG]\t$iNonMissingValues\n";
     }
   }

   my $MissingScore_PsychEncodeStr=join("\t",@MissingScore_PsychEncode);
   my $Heading_PsychEncodeStr=join("\t",@Heading_PsychEncode);

   my @MissingScore_GTExV8=();
  my @Heading_GTExV8=();
  my $iGTExV8ScoreNumber=0;
   for(my $iM=0;$iM<3;$iM++)
  {
    for(my $iS=0;$iS<@ZScroeTypes;$iS++)
     {
      for(my $iG=0;$iG<@GTypes;$iG++)
      {
        $iGTExV8ScoreNumber++;
        my $HeadingStr="$Methods[$iM]_$ZScroeTypesConcise[$iS]_$GTypesConcise[$iG]";
        push @Heading_GTExV8,$HeadingStr;
        push @MissingScore_GTExV8,"NA";
         my %GTExV8ENSGID_Ind_Zscore_Batch=();

        for(my $iBatch=0;$iBatch<3;$iBatch++)
        {      
           my $sGTExV8ZScoreFile="/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction_Zscore/GTExV8PredictionEx/GTExV8BasedEli-$BatchList[$iBatch]$Methods[$iM]_DLPFC_BA9_$GTypes[$iG]_Imputed_GenePredictedExExGS$ZScroeTypes[$iS]ZScore.txt";
           my $GeneIDIndex=5;
           if($iG!=0)
           {
             $GeneIDIndex=4;
           }
         my @AllAvailableWESID=();
         my  $iLineNumber=0;
         open(INPUT, $sGTExV8ZScoreFile) || die "can’t open $sGTExV8ZScoreFile";
         while (<INPUT>)
         {
            chomp;
            if($iLineNumber==0)
            {
              my @cols=split(/\t/,$_);#13 PT-BSRW 27
              @AllAvailableWESID=@cols;
              $iLineNumber++;
              next;
            }   
            my @cols=split(/\t/,$_);#13 PT-BSRW 27
            for(my $ii=$GeneIDIndex+1;$ii<@cols;$ii++)
            {
               if(defined($GTExV8ENSGID_Ind_Zscore{$cols[$GeneIDIndex]}{$AllAvailableWESID[$ii]}))
               {
                 $GTExV8ENSGID_Ind_Zscore_Batch{$cols[$GeneIDIndex]}{$AllAvailableWESID[$ii]}=$cols[$ii];
               }
            }
          }  
          close INPUT;
        }
      my $iNonMissingValues=0;
       foreach my $GeneID (sort keys %GTExV8ENSGID_Ind_Zscore)
       {  
         foreach my $WGSID (keys %{ $GTExV8ENSGID_Ind_Zscore{$GeneID} })
         {
             if($iGTExV8ScoreNumber==1)
             {
               if(defined($GTExV8ENSGID_Ind_Zscore_Batch{$GeneID}{$WGSID}))
               {
                  $iNonMissingValues++;
                 $GTExV8ENSGID_Ind_Zscore{$GeneID}{$WGSID}=$GTExV8ENSGID_Ind_Zscore_Batch{$GeneID}{$WGSID};
                }
               else
               {
                 $GTExV8ENSGID_Ind_Zscore{$GeneID}{$WGSID}="NA";
               }
             }
             else
             {
               if(defined($GTExV8ENSGID_Ind_Zscore_Batch{$GeneID}{$WGSID}))
               {
                 $iNonMissingValues++;
                 $GTExV8ENSGID_Ind_Zscore{$GeneID}{$WGSID}.="\t$GTExV8ENSGID_Ind_Zscore_Batch{$GeneID}{$WGSID}";
                }
               else
               {
                 $GTExV8ENSGID_Ind_Zscore{$GeneID}{$WGSID}.="\tNA";
               }
             }
         }
       }  
        print "$ZScroeTypesConcise[$iS]_$GTypesConcise[$iG]\t$iNonMissingValues\n";       
     }
   }
  }

  my $MissingScore_GTExV8Str=join("\t",@MissingScore_GTExV8);
  my $Heading_GTExV8Str=join("\t",@Heading_GTExV8);

   #Remove $cols[11],$cols[15]
   
    my $OutputZScoreFile="/data/ruderferlab/projects/cmc/scratch/sweden/SwedenVariantAnotaiton_GeneInfo_RareGtypeZscore.txt";
    open OUT, ">$OutputZScoreFile" or die "Can't open Output file:$OutputZScoreFile!";
    $iLineNumber=0;
    $GTypeInformationFile="/data/ruderferlab/projects/cmc/scratch/sweden/SwedenVariantAnotaiton_GeneInfo_RareGtype.txt";
    open(INPUT, $GTypeInformationFile) || die "can’t open $GTypeInformationFile";
    while (<INPUT>)
    {
        chomp;
          if($iLineNumber==0)
          {
            my @cols=split(/\t/,$_);#13 PT-BSRW 27
             splice(@cols, 11, 1);
             splice(@cols, 14, 2);
            for(my $iCol=0;$iCol<@cols;$iCol++)
            {   
               print OUT "$cols[$iCol]\t";
            }   
            print OUT "BATCH\tPHE\tSEX\tBatch2\t";
             print OUT "$Heading_PsychEncodeStr\t";
            print OUT "$Heading_GTExV8Str";
             print OUT "\n";
            $iLineNumber++;
            next;
          }
         my @cols=split(/\t/,$_);
         $iLineNumber++;
       # if($iLineNumber>100)
       # {
        # last;
       # }
          my $iLength=scalar(@cols);next if($iLength<=27);
         splice(@cols, 11, 1);
         splice(@cols, 14, 2);
         my @AllInds=split(/\?/, $cols[23]);
         my @AllGtypes=split(/\?/, $cols[24]);
         splice(@cols,23,2);     
         my $IndInfoStr=join("\t",@cols);
         for(my $ii=0;$ii<@AllInds;$ii++)
         {
           next if(!defined($PsychEncodeENSGID_Ind_Zscore{$cols[12]}{$AllInds[$ii]}) && !defined($GTExV8ENSGID_Ind_Zscore{$cols[12]}{$AllInds[$ii]}));#Remove psychencode and GTExV8 none of Z-Scores
            print OUT "$IndInfoStr\t$AllInds[$ii]\t$AllGtypes[$ii]\t$IndID_DiseaseSex{$AllInds[$ii]}\t$WESID_Batch{$AllInds[$ii]}\t";
           if(defined($PsychEncodeENSGID_Ind_Zscore{$cols[12]}{$AllInds[$ii]}))
            {
              print OUT "\t$PsychEncodeENSGID_Ind_Zscore{$cols[12]}{$AllInds[$ii]}";
            }
            else
            {
              print OUT "\t$MissingScore_PsychEncodeStr";
            }
            if(defined($GTExV8ENSGID_Ind_Zscore{$cols[12]}{$AllInds[$ii]}))
            {
             print OUT "\t$GTExV8ENSGID_Ind_Zscore{$cols[12]}{$AllInds[$ii]}";
            }
            else
            {
             print OUT "\t$MissingScore_GTExV8Str";
            }
            print OUT "\n";
         }

    }
   close INPUT;
   close OUT;

    my  $OutputFile="/data/ruderferlab/projects/cmc/scratch/sweden/SwedenVariantAnotaiton_GeneInfo_RareGtypeZscore_Singleton.txt";
     open OUT, ">$OutputFile" or die "Can't open Output file:$OutputFile!";
      $iLineNumber=0;
    # my  $OutputZScoreFile="/data/ruderferlab/projects/cmc/scratch/sweden/SwedenVariantAnotaiton_GeneInfo_RareGtypeZscore.txt";
     open(INPUT, $OutputZScoreFile) || die "can’t open $OutputZScoreFile";
     while (<INPUT>)
     {
        chomp;
        if($iLineNumber==0)
         {
            my @cols=split(/\t/,$_);#13 PT-BSRW 27
            $iLineNumber++;
    print "$cols[20]\n";  
            print OUT "$_\n";
            next;
         }
         my @cols=split(/\t/,$_);#13 PT-BSRW 27
         if($cols[20]<0.000043)
         {
          print OUT  "$_\n";
         }
     }
     close INPUT;
     close OUT;
=cut;
my $iLineNumber=0;
   my @HeadLineList=();
    my  $OutputZScoreFile="/data/ruderferlab/projects/cmc/scratch/sweden/SwedenVariantAnotaiton_GeneInfo_RareGtypeZscore.txt";
     open(INPUT, $OutputZScoreFile) || die "can’t open $OutputZScoreFile";
     while (<INPUT>)
     {  
        chomp;
        if($iLineNumber==0)
         {  
            my @cols=split(/\t/,$_);#13 PT-BSRW 27
             @HeadLineList= @cols; 
            $iLineNumber++;
            next;
         }
        my @cols=split(/\t/,$_);
        for(my $ii=0;$ii<@cols;$ii++)
        {
         print "$ii\t$HeadLineList[$ii]\t$cols[$ii]\n";
        }
     #   last if($iLineNumber>=2);
       $iLineNumber++;
     }
     close INPUT;
=cut;
