#!/usr/bin/perl -w
use Scalar::Util qw(looks_like_number);
 use strict;

 use lib "/home/hanl3/perl5/module/lib/site_perl/5.14.2";
 use Text::CSV_PP;
  use Tie::File; 
 use POSIX qw(WNOHANG);
  use IO::Compress::Gzip qw(gzip $GzipError) ;
 use warnings;
 use Scalar::Util qw(looks_like_number);


my $iSpecificBatch=$ARGV[0];

# /home/hanl3/cmc/scratch/sweden/MichiganImputaionInputdataResult/*/chr$iChr.dose.vcf.gz
# /home/hanl3/cmc/scratch/sweden/MichiganImputaionInputdataResult/*/chr*.info.gz

#Eli-s234  Eli-sw56  Eli-swe1
 my @BatchNameList=("Eli-swe1","Eli-sw56","Eli-s234");


  for(my $iBatch=$iSpecificBatch;$iBatch<($iSpecificBatch+1);$iBatch++)
  {
      my $FullVCFFile="/home/hanl3/cmc/scratch/sweden/MichiganImputaionInputdataResult/$BatchNameList[$iBatch]ImputedSNVs_1000GPV3Rsq03.vcf";
      open OUT, ">$FullVCFFile" or die "Can't open Output file:$FullVCFFile!";

      my @Indlist_Chr1=();
      my @Indlist_OtherChrs=();
    my $iDuplicateLoci=0;
     my %UniqLoci=();
     for(my $iChr=1;$iChr<=22;$iChr++)
    {
      
      my $WGSFile="/home/hanl3/cmc/scratch/sweden/MichiganImputaionInputdataResult/$BatchNameList[$iBatch]/chr$iChr.dose.vcf.gz";
      if($iBatch==2) 
      {
       $WGSFile="/fs0/hanld/Eli_234/chr$iChr.dose.vcf.gz";
      }      
      elsif($iBatch==0)
      {
       $WGSFile="/fs0/hanld/Eli_1/chr$iChr.dose.vcf.gz";
      }
     open (INPUT, "gunzip -c $WGSFile|") or die "gunzip  $WGSFile: $!";;
      while (<INPUT>)
      {  
       chomp;
        if($_ =~ m/^##/)
        {  
           if($iChr==1)
           {
            print OUT "$_\n";
           }
           next;
        }
        if($_ =~ m/^#/)
        { 
          my @cols=split(/\s+/,$_);
          if($iChr==1)
          {
           @Indlist_Chr1=@cols;
            print OUT "$_\n"; 
         }
         else
         {
           @Indlist_OtherChrs=@cols;
           for(my $iCol=9;$iCol<@cols;$iCol++)
           {
             if($Indlist_OtherChrs[$iCol] ne $Indlist_Chr1[$iCol])
              {
                print " Error$iChr\t$Indlist_OtherChrs[$iCol]\t $Indlist_Chr1[$iCol]\n";
                last;
              }
           }
          }
          next;
       }
         my @cols=split(/\s+/,$_);
        # $cols[2]="$cols[0]:$cols[1]"; 
         $cols[8]="GT";
         if(!defined($UniqLoci{$cols[2]}))
         {
          $UniqLoci{$cols[2]}=1;
          for(my $iCol=9;$iCol<@cols;$iCol++)
          {
           $cols[$iCol]=substr($cols[$iCol],0,3);
          }
          my $OneRowStr=join("\t",@cols);
          print OUT "$OneRowStr\n";
          }
         else
          {
             $iDuplicateLoci++;
  #         print "$cols[0]\t$cols[1]\t$cols[3]\t$cols[4]\n";
          }
        }
         close INPUT; 
         print "Duplicate loci:$iChr\t$iDuplicateLoci\n";
     }
     close OUT;
      print "RemoveDuplicate Loci:$iDuplicateLoci\n";
   
     gzip $FullVCFFile => "$FullVCFFile.gz"
           or die "gzip failed: $GzipError\n";

 }
 
