#! /usr/local/R/3.2.0/x86_64/intel14/nonet/bin/Rscript  --vanilla
library("dplyr");
library("tidyverse");

view = 2;
  remove_outliers <- function(x, na.rm = TRUE, ...) {
  qnt <- quantile(x, probs=c(.25, .75), na.rm = na.rm, ...)
   H <- 3 * IQR(x, na.rm = na.rm)
   y <- x
   Lowerbound=qnt[1] - H;
   UpperBound=qnt[2] + H;
   Bound=c(Lowerbound[[1]],UpperBound[[1]]);
   return(Bound);
   #y[x < (qnt[1] - H)] <- NA
  # y[x > (qnt[2] + H)] <- NA
  # y
  }

   myDir="/data/ruderferlab/projects/cmc/scratch/sweden/SwedenPCAAnalysis";
    Outputfile=paste(myDir,"/Sweden_Hapmapphase3basedPCAanalysisScatterPlot.pdf",sep="");
    pdf(Outputfile, 7, 9);
   # jpeg(Outputfile, width = 480, height =700, units = "px", pointsize = 12);
    par(mar=c(5,7,1.5,0.25), mgp=c(3,1,0), mfrow=c(1,1),oma=c(0,0,0,0));
    linewide=2;
    axixwide=0.5;
    tickwide=0.5;
   cex_size=0.62;
   leg_cex_size=0.8;
   axes=F;
   xaxs="s";
   cex_size1=0.65;
  
  #SampleID	PC1	PCR2	Source	Code
    WGSVariantSummaryFile="/data/ruderferlab/projects/cmc/scratch/sweden/SwedenPCAAnalysis/SwedenFull_YRI_CHB_CEU.eigenvec_Ancestry";
    WGSVariantSummaryMatrix=read.table(WGSVariantSummaryFile, sep="\t",colClasses="character",na.strings = "NA", header=TRUE,fill=FALSE,strip.white=TRUE);
   WGSVariantSummaryMatrix=as.data.frame(WGSVariantSummaryMatrix);
    WGSVariantSummaryMatrix$PC1=as.numeric(WGSVariantSummaryMatrix$PC1);
    WGSVariantSummaryMatrix$PC2=as.numeric(WGSVariantSummaryMatrix$PC2); 
    WGSVariantSummaryMatrix$Code=as.numeric(WGSVariantSummaryMatrix$Code)

   #WGSVariantSummaryMatrix[,2]=as.numeric(WGSVariantSummaryMatrix[,2]);
   # WGSVariantSummaryMatrix[,3]=as.numeric(WGSVariantSummaryMatrix[,3]);
   # WGSVariantSummaryMatrix[,5]=as.numeric(WGSVariantSummaryMatrix[,5]);
    
 plot(WGSVariantSummaryMatrix$PC1,WGSVariantSummaryMatrix$PC2,main=NULL,cex.lab=0.8,ylab="PC2", xlab="PC1", pch=WGSVariantSummaryMatrix$code-1, col =WGSVariantSummaryMatrix$Code);
     legend(x=0.06,y=-0.04,c("swe1","sw56","s234","CEU","CHB","YRI"),cex=.8,col=c(1:6),pch=c(0:5),bty = "n");
    segments(-0.017,-0.012,0.003,-0.012,lty=1,col=6);
    segments(-0.017,0.0055,0.003,0.0055,lty=1,col=6);
    segments(0.003,-0.012,0.003,0.0055,lty=1,col=6);
     
      WGSVariantSummaryMatrix$InferredAncestry="Non-CEU";
      WGSVariantSummaryMatrix$InferredAncestry[which(WGSVariantSummaryMatrix$PC1<=0.0035 & WGSVariantSummaryMatrix$PC2>=-0.012  & WGSVariantSummaryMatrix$PC2<=0.0055)]="CEU";  
      print(WGSVariantSummaryMatrix$InferredAncestry[1:10]);

      InferredAncestryFile="/data/ruderferlab/projects/cmc/scratch/sweden/SwedenPCAAnalysis/SwedenFull_YRI_CHB_CEU.eigenvec_InferredAncestry";
      write.table(WGSVariantSummaryMatrix, file=InferredAncestryFile, append = FALSE, quote =FALSE, sep = "\t",
           eol = "\n",col.names=TRUE,row.names=FALSE);



   dev.off();
