#!/usr/bin/perl -w
 use strict;
  use 5.012;
 use lib "/home/hanl3/perl5/module/lib/site_perl/5.14.2";
 use Text::CSV_PP;
  use Tie::File; 
 use POSIX qw(WNOHANG);
 use IO::Uncompress::Gunzip qw($GunzipError);
 use warnings;
use Scalar::Util qw(looks_like_number);

  my @WGSBatches=("Eli-s234","Eli-sw56","Eli-swe1");
   my @WGSBatchNames=("Eli-s234","Eli-sw56","Eli-swe1");
 
 my %Chr_GeneID=();#Chr_ geneid
 my %AllUsedSNVsInfo=(); 

my $iSpecificBatch=$ARGV[0];#0,1,2
my $iSpecificGType=$ARGV[1];#0,1,2

#Obtain Gene list
#/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTExV8_Weight/*_*/$gene.wgt.score

   my @GenoTypes=("FullGenotype","Haplotype1","Haplotype2");
 

        my $iNumberofFile=0;
        my %FullChr_GeneID=();
            my $mydir="/data/ruderferlab/resources/psychencode/PEC_TWAS_weights_Score";
            opendir(DIR, $mydir) || die "Can't open $mydir: $!";
            while (my $weightFile=readdir (DIR)) {
            next if ($weightFile =~ m/^\./);
            my $geneID=$weightFile;
            $geneID =~ s/\.wgt\.score//ig;
            my $scoreWeightFile="$mydir/$weightFile";
            next if(-z $scoreWeightFile);#if the file is empty, ignore
             my $iLineNumber=0;
            my $ScoreFile="$mydir/$weightFile";
            open(INPUT,$ScoreFile) || die "can’t open $ScoreFile";
            while (<INPUT>)
            {
              if($iLineNumber==0)
              {
                chomp;
                my @cols=split(/\t/,$_,2);
                my @ChrPos=split(/:/,$cols[0]);
                if(!defined($FullChr_GeneID{$ChrPos[0]}))
                {
                 $FullChr_GeneID{$ChrPos[0]}=$geneID;
               }
              else
               {
                 $FullChr_GeneID{$ChrPos[0]}.="\t$geneID";
               }
              }
              $iLineNumber++;
            }
           close INPUT;
             $iNumberofFile++;
            #  last if($iNumberofFile>10);
         }
         closedir (DIR);





  for(my $iBatch=$iSpecificBatch;$iBatch<$iSpecificBatch+1;$iBatch++)
   {
     for(my $iType=$iSpecificGType;$iType<$iSpecificGType+1;$iType++)
     {  
         my $OutZFile="/data/ruderferlab/projects/cmc/scratch/sweden/MergedTranscriptPrediction/PsychEncodePredictionEx/$WGSBatchNames[$iBatch]PsychEncode_PredictedGeneExpression_$GenoTypes[$iType]_Imputed.txt";
         open OUTZ, ">$OutZFile" or die "Can't open Output file:$OutZFile!";   
         my @AllIndIDList=(); 
        for(my $iChr=1;$iChr<=22;$iChr++)
         {
         
            next if(!defined($FullChr_GeneID{$iChr}));#remove the chromosome if the chromosome contain no genes
             my @ChrbasedGeneIDs=split(/\t/,$FullChr_GeneID{$iChr});
            my %SpecificChrBasedGeneIDs=();
              for(my $ii=0;$ii<@ChrbasedGeneIDs;$ii++)
             {
               $SpecificChrBasedGeneIDs{$ChrbasedGeneIDs[$ii]}=1;
             }

            my $iNumberofFile=0;
            my %Chr_GeneID=();
            my %GeneID_NumberofLociInModel=();
            my $mydir="/data/ruderferlab/resources/psychencode/PEC_TWAS_weights_Score";
            opendir(DIR, $mydir) || die "Can't open $mydir: $!";
            while (my $weightFile=readdir (DIR)) {
            next if ($weightFile =~ m/^\./);
            my $geneID=$weightFile;
            
            $geneID =~ s/\.wgt\.score//ig;
            next if(!defined($SpecificChrBasedGeneIDs{$geneID}));#if not defined the gene in the Chromosome,ignore
            my $scoreWeightFile="$mydir/$weightFile";
            next if(-z $scoreWeightFile);#if the file is empty, ignore
             my $iLineNumber=0;
            my $ScoreFile="$mydir/$weightFile";
            open(INPUT,$ScoreFile) || die "can’t open $ScoreFile";
            while (<INPUT>)
            {
              if($iLineNumber==0)
              {
               chomp;
               my @cols=split(/\t/,$_,2);
               my @ChrPos=split(/:/,$cols[0]);
               if(!defined($Chr_GeneID{$ChrPos[0]}))
               {
                 $Chr_GeneID{$ChrPos[0]}=$geneID;
               } 
              else
               {
                 $Chr_GeneID{$ChrPos[0]}.="\t$geneID";
               }
             }
              $iLineNumber++; 
            }         
           close INPUT;
           $GeneID_NumberofLociInModel{$geneID}=$iLineNumber;
           $iNumberofFile++;
         }
         closedir (DIR);
            my %GeneID_AllInfo=();
            my $NumberofGenes=keys %GeneID_NumberofLociInModel;
            print "$iChr\tNumber of Genes:$NumberofGenes\n";
            $iNumberofFile=0;         
             $mydir="/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction/PsychEncode_Profile";           
            opendir(DIR, $mydir) || die "Can't open $mydir: $!";
            while (my $profileFile=readdir (DIR)) {
            next if ($profileFile =~ m/^\./);
            next if($profileFile !~ m/profile$/);            
           #/$PredictionMethods[$iM]_$BrainsTissuesNames[$iT]/$WGSBatches[$iBatch]_$GenoTypes[$iType]_$geneID
           if ($profileFile =~ m/profile$/ && $profileFile =~ m/^$WGSBatches[$iBatch]\_$GenoTypes[$iType]\_/)
           {
               my $GeneIDFull=$profileFile;
               $GeneIDFull =~ s/.profile//ig;#Eli-swe1_Haplotype1_ENSG00000166923
               $GeneIDFull=~ s/^$WGSBatches[$iBatch]\_$GenoTypes[$iType]\_//ig;              
             next if(!defined($GeneID_NumberofLociInModel{$GeneIDFull}));
             my $iLineNumber=0; 
             my $iEffectiveNumber=0;
             my $iDependentLociNumber=-1;
              my $iDependentLociFullNumber=-1;
             my $profileFileName="$mydir/$profileFile";
            #  print "$profileFileName\n";
             my %WGSID_Expression=(); 
            open(INPUT,$profileFileName) || die "can’t open $profileFileName";
             while (<INPUT>)
             {
               chomp;
               if($iLineNumber==0)
               {
                 $iLineNumber++;
                 next;
               }
             $_ =~ s/^\s+|\s+$//g;#Remove space around the string      
           my @cols=split(/\s+/,$_);
  
             my $iLength=scalar(@cols); 
             if($iEffectiveNumber==0)
               {
                 $iDependentLociFullNumber=$cols[3]/2;
                 $iDependentLociNumber=$cols[4];
               }
              if($iNumberofFile==0 && $iChr==1) 
              {
               push  @AllIndIDList,$cols[1];
              }
               $WGSID_Expression{$cols[1]}=$cols[5];
               my $NumberofColumns=scalar(@cols);
               $iEffectiveNumber++;
             }
            close INPUT;     
              $iNumberofFile++;
            if($iChr==1 && $iNumberofFile==1)#Only the 1st information given title
           {
              print OUTZ "Chr\tWGSBatch\tPredictionMethods\tBrainsTissues\tSource\tGeneID\tNumberofLociInWeightModel\tFullNumber\tNumberofLociInActualModel";
              for(my $iInd=0;$iInd<@AllIndIDList;$iInd++)
              {
                print OUTZ "\t$AllIndIDList[$iInd]";
               }
                print OUTZ "\n";
            }
            
            print OUTZ "$iChr\t$WGSBatches[$iBatch]\tNA\tNA\tImputed$GenoTypes[$iType]\t$GeneIDFull\t$GeneID_NumberofLociInModel{$GeneIDFull}\t$iDependentLociFullNumber\t$iDependentLociNumber";
            for(my $iInd=0;$iInd<@AllIndIDList;$iInd++)
             {
                print OUTZ "\t$WGSID_Expression{$AllIndIDList[$iInd]}";
             }
             print OUTZ "\n";
           }       
          }
        closedir (DIR);
      }#$iChrloop
      close OUTZ;
  }
}
exit 0;

