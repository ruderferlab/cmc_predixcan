#!/usr/bin/perl -w
use Scalar::Util qw(looks_like_number);
 use strict;

 use lib "/home/hanl3/perl5/module/lib/site_perl/5.14.2";
 use Text::CSV_PP;
  use Tie::File; 
 use POSIX qw(WNOHANG);
 use IO::Uncompress::Gunzip qw($GunzipError);
 use warnings;
 use Scalar::Util qw(looks_like_number);

#/data/ruderferlab/projects/sweden/data/geno/sweden_merged_Eli-s234.fam  sweden_merged_Eli-sw56.fam  sweden_merged_Eli-swe1.fam


#Eli-s234  Eli-sw56  Eli-swe1

my @ChromosomeBasedDir=("Eli_sw1","Eli_sw56","Eli_s234");
 my @BatchNameList=("Eli-swe1","Eli-sw56","Eli-s234");

 my %AllSNPsAcrossSweden=();
  for(my $iBatch=0;$iBatch<@BatchNameList;$iBatch++)
  {
       my $IndInfoFile="/data/ruderferlab/projects/sweden/data/geno/sweden_merged_$BatchNameList[$iBatch].bim";
       open(INPUT,$IndInfoFile) || die "can’t open $IndInfoFile";
       while (<INPUT>)
       {
        chomp;
        $_ =~ s/^\s+|\s+$//g;      
          my @cols=split(/\s++/,$_);
         if(!defined($AllSNPsAcrossSweden{$cols[1]}))
         {
         $AllSNPsAcrossSweden{$cols[1]}=1;
         }
        else
         {
          $AllSNPsAcrossSweden{$cols[1]}++;
         }
      }
      close INPUT;
   }

    my $size=keys %AllSNPsAcrossSweden;
   print "Number of All SNVs: $size\n";
  my %SharedSNVs=();
    my $iNumberofFilterSNVs=0;
   foreach my $SNPID (keys %AllSNPsAcrossSweden)
   {
     if($AllSNPsAcrossSweden{$SNPID}>=3)
     {
        $SharedSNVs{$SNPID}=1;
       $iNumberofFilterSNVs++;
     }
   }
  print "$iNumberofFilterSNVs\n";

  my $SharedSNVListFile="/data/ruderferlab/projects/cmc/scratch/sweden/SwedenPCAAnalysis/sweden_HapmapSharedSNVlist.txt";
       open OUT, ">$SharedSNVListFile" or die "Can't open Output file:$SharedSNVListFile!"; 
  my $Icount=0;
  my $Hapmap3RefencemarkerFile="/fs0/1000_Phase3/CEU_CHB_YRI_subsets/ALL.chr1-22.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes_CEU.bim";
     open(INPUT,$Hapmap3RefencemarkerFile) || die "can’t open $Hapmap3RefencemarkerFile";
       while (<INPUT>)
       {
        chomp;
        $_ =~ s/^\s+|\s+$//g;
       my @cols=split(/\s++/,$_);
       next if(!defined($SharedSNVs{$cols[1]}));
       print OUT "$cols[1]\n";   
      $Icount++;
      }
    close INPUT;
   print "Share SNVs:$Icount\n";
    
