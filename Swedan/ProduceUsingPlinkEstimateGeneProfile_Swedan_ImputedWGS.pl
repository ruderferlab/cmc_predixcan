#!/usr/bin/perl -w
 use strict;
  use 5.012;
 use lib "/home/hanl3/perl5/module/lib/site_perl/5.14.2";
 use Text::CSV_PP;
  use Tie::File; 
 use POSIX qw(WNOHANG);
 use IO::Uncompress::Gunzip qw($GunzipError);
 use warnings;
use Scalar::Util qw(looks_like_number);
#Input model type



  my @PredictedTypes=("GTExV8","PsychEncodeWeight");
  my  @WGSBatches=("Eli-s234","Eli-sw56","Eli-swe1");

  my @PredictionMethods=("st","ut","xt");
  my @BrainsTissues=("Brain_Frontal_Cortex_BA9","Brain_Anterior_cingulate_cortex_BA24");
  my @BrainsTissuesNames=("DLPFC_BA9","ACC_BA24");
  my %GeneID_Chr=();

    my $iPredictedType=$ARGV[0]; #[0,1] 
    my $iSpecificBatch=$ARGV[1];  #0,1,2


   if($iPredictedType==0)
     {
        for(my $iM=0;$iM<@PredictionMethods;$iM++)
       {
         for(my $iT=0;$iT<@BrainsTissues;$iT++)
         {
          my $iNumberofFile=1;
          my $mydir="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTExV8_Weight/$PredictionMethods[$iM]_$BrainsTissuesNames[$iT]";
          opendir(DIR, $mydir) || die "Can't open $mydir: $!";
           while (my $weightFile=readdir (DIR)) {
           next if ($weightFile =~ m/^\./);
           my $InputFile="$mydir/$weightFile";  
           my $geneID=$weightFile;
           $geneID =~ s/\.wgt\.score//ig;   
           open(INPUT,$InputFile) || die "can’t open $InputFile";
           while (<INPUT>)
           {
             chomp;
             my @cols=split(/\s+/,$_);
             my @ChrPos=split(/:/,$cols[0]); 
             $GeneID_Chr{$geneID}=$ChrPos[0];
             last;
           }
            close INPUT;
           $iNumberofFile++;
         }
         closedir (DIR);
        }
      }
    }
    else
    {
         my $iNumberofFile=1;
          my $mydir="/data/ruderferlab/resources/psychencode/PEC_TWAS_weights_Score";
          opendir(DIR, $mydir) || die "Can't open $mydir: $!";
           while (my $weightFile=readdir (DIR)) {
           next if ($weightFile =~ m/^\./);
           next if($weightFile !~ /\.wgt\.score$/);
            my $InputFile="$mydir/$weightFile";  
          
           next if(-z $InputFile);#if the file is empty, ignore

           my $geneID=$weightFile;
           $geneID =~ s/\.wgt\.score//ig;
           open(INPUT, $InputFile) || die "can’t open $InputFile";
           while (<INPUT>)
           {
             chomp;
             my @cols=split(/\s+/,$_);
             my @ChrPos=split(/:/,$cols[0]);
             $GeneID_Chr{$geneID}=$ChrPos[0];
             last;
           }
            close INPUT;
           $iNumberofFile++;
         }
         closedir (DIR);
     }
     my   $NumberofGenes=keys %GeneID_Chr;
     print "NumberofGenes2 :$NumberofGenes\n";

  my $SwedanPredictExpressionCommandlineFile="/gpfs23/data/ruderferlab/projects/cmc/scripts/Swedan/SwedanPredictGeneExpressionCommandline$PredictedTypes[$iPredictedType]_$WGSBatches[$iSpecificBatch].sh";
  open OUT, "> $SwedanPredictExpressionCommandlineFile" or die "Can't open Output file:$SwedanPredictExpressionCommandlineFile!";
  
  my @GenoTypes=("FullGenotype","Haplotype1","Haplotype2");
   for(my $iBatch=$iSpecificBatch;$iBatch<$iSpecificBatch+1;$iBatch++)
   {
      if($iPredictedType==0)
      {  
       for(my $iType=0;$iType<@GenoTypes;$iType++)
       {
        for(my $iM=0;$iM<@PredictionMethods;$iM++)
        {
         for(my $iT=0;$iT<@BrainsTissues;$iT++)
         {
          my $iNumberofFile=1;
          my $mydir="/data/ruderferlab/projects/cmc/scratch/CMC_Transcriptome_Prediction/GTExV8_Weight/$PredictionMethods[$iM]_$BrainsTissuesNames[$iT]";
          opendir(DIR, $mydir) || die "Can't open $mydir: $!";
           while (my $weightFile=readdir (DIR)) {
           next if ($weightFile =~ m/^\./);
           my $geneID=$weightFile;
           $geneID =~ s/\.wgt\.score//ig;
           my $scoreWeightFile="$mydir/$weightFile";#/home/hanl3/cmc/scratch/sweden/FilterForPrediction/GTExV8/ChromosomeSplitted/${batches[$i]}ShareGTEXV8.CommonSNVs_ImputedWGS_${gtypes[$j]}_Chr$k";
           my $iSpecificChr=$GeneID_Chr{$geneID};#/home/hanl3/cmc/scratch/sweden/FilterForPrediction/GTExV8/ChromosomeSplitted/Eli-swe1ShareGTEXV8.CommonSNVs_ImputedWGS_FullGenotype_Chr3
           my $plinkFilePrefix="/home/hanl3/cmc/scratch/sweden/FilterForPrediction/GTExV8/ChromosomeSplitted/$WGSBatches[$iBatch]ShareGTEXV8.CommonSNVs_ImputedWGS_$GenoTypes[$iType]_Chr$iSpecificChr";
           my $outfilePrefix="/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction/GTExV8_ProfileV2/$PredictionMethods[$iM]_$BrainsTissuesNames[$iT]/$WGSBatches[$iBatch]_$GenoTypes[$iType]_$geneID";
           my $cmd="plink --bfile $plinkFilePrefix --score $scoreWeightFile 1 3 4 sum  double-dosage --out $outfilePrefix";# system($cmd);
           print OUT "$cmd\n";
           $iNumberofFile++;
         }
         closedir (DIR);
        }
        }
      }
     }
    else
    {
        for(my $iType=0;$iType<@GenoTypes;$iType++)
         {
          my $iNumberofFile=1;
          my $mydir="/data/ruderferlab/resources/psychencode/PEC_TWAS_weights_Score";
          opendir(DIR, $mydir) || die "Can't open $mydir: $!";
           while (my $weightFile=readdir (DIR)) {
           next if ($weightFile =~ m/^\./);
           my $geneID=$weightFile;
           next if($weightFile !~ /\.wgt\.score$/);
            $geneID =~ s/\.wgt\.score//ig;
           my $scoreWeightFile="$mydir/$weightFile";
            next if(-z $scoreWeightFile);#if the file is empty, ignore

           if(!defined($GeneID_Chr{$geneID}))
           {
              print "$weightFile\t"; 
              print "$geneID\n";
            }
           my $iSpecificChr=$GeneID_Chr{$geneID};         
           my $plinkFilePrefix="/home/hanl3/cmc/scratch/sweden/FilterForPrediction/PsychEncodeWeight/ChromosomeSplitted/$WGSBatches[$iBatch]SharePsychEncodeWeight.CommonSNVs_ImputedWGS_$GenoTypes[$iType]_Chr$iSpecificChr";
           my $outfilePrefix="/data/ruderferlab/projects/cmc/scratch/sweden/Transcriptome_Prediction/PsychEncode_Profile/$WGSBatches[$iBatch]_$GenoTypes[$iType]_$geneID";
           my $cmd="plink --bfile $plinkFilePrefix --score $scoreWeightFile 1 2 4 --out $outfilePrefix";# system($cmd);
           print OUT "$cmd\n";
           $iNumberofFile++;
         }
         closedir (DIR);
       } 
     } 
   }

 close  OUT;
