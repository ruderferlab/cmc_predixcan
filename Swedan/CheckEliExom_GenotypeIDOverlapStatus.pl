#!/usr/bin/perl -w
use Scalar::Util qw(looks_like_number);
 use strict;

 use lib "/home/hanl3/perl5/module/lib/site_perl/5.14.2";
 use Text::CSV_PP;
  use Tie::File; 
 use POSIX qw(WNOHANG);
 use IO::Uncompress::Gunzip qw($GunzipError);
 use warnings;
 use Scalar::Util qw(looks_like_number);

#/data/ruderferlab/projects/sweden/data/geno/sweden_merged_Eli-s234.fam  sweden_merged_Eli-sw56.fam  sweden_merged_Eli-swe1.fam


#Eli-s234  Eli-sw56  Eli-swe1

 my %SwedenSamples_EUR=();#Sweden samples are judged as European population
  my $InferredSampleAncestryFile="/data/ruderferlab/projects/cmc/scratch/sweden/SwedenPCAAnalysis/SwedenFull_YRI_CHB_CEU.eigenvec_InferredAncestry";
  open(INPUT,$InferredSampleAncestryFile) || die "can’t open $InferredSampleAncestryFile";
   while (<INPUT>)
    {
       chomp;
       my @cols=split(/\s++/,$_);
       if($cols[3] ne "CEU" && $cols[5] eq "CEU") 
       {
         $SwedenSamples_EUR{$cols[0]}=1;
        }
     }
   close INPUT;
 my $SwedenSampleSize=keys %SwedenSamples_EUR;
  my %DuplicatedInds=();
  my @DuplicateInds=("PT-8W4V","PT-8VWD","PT-8VWF","PT-8VWE","PT-CDG8","PT-8VGR","PT-CDGY","PT-CDHH","PT-CDHG","PT-CDH8","PT-CDFO","PT-CDGG","PT-CDFW", "PT-CDFL", "PT-CDGH","PT-CDGO");
   for(my $iInd=0;$iInd<@DuplicateInds;$iInd++)
   {
     $DuplicatedInds{$DuplicateInds[$iInd]}=1;
   }

  my @BatchNameList=("Eli-swe1","Eli-sw56","Eli-s234");
    my @Batches=("Eli-swe1","Eli-sw56","El234");
   my %DuplicateSampleMissingRate=();#16 duplicate sample all selected the batch of Eli-sw56
  for(my $iBatch=0;$iBatch<@BatchNameList;$iBatch++)
  { #sweden_merged_El234new.het
     my $iLineNumber=0;
     my $BatchBasedMissingRate_HetorateFile="/data/ruderferlab/projects/sweden/data/geno/sweden_merged_$BatchNameList[$iBatch]MissingHeterogenotypeRate.txt";
     open(INPUT,$BatchBasedMissingRate_HetorateFile) || die "can’t open $BatchBasedMissingRate_HetorateFile";
     while (<INPUT>)
     {
        if($iLineNumber==0)
        {
          $iLineNumber++;
          next;
         }
        chomp;
        my @cols=split(/\s++/,$_);
        if(defined($DuplicatedInds{$cols[0]}))
        {
         $DuplicateSampleMissingRate{$cols[0]}{$BatchNameList[$iBatch]}=$cols[2];
        } 
      }
      close INPUT;
   }
 my %DuplicateSample_BatchSelected=();
  for(my $iInd=0;$iInd<@DuplicateInds;$iInd++)
  {
    my @UsedBatches=();
    my @IndividualsMissingRates=();
    for(my $iBatch=0;$iBatch<@BatchNameList;$iBatch++)
     {
      if(defined($DuplicateSampleMissingRate{$DuplicateInds[$iInd]}{$BatchNameList[$iBatch]}))
      {
       push @UsedBatches,$BatchNameList[$iBatch];
       push @IndividualsMissingRates,$DuplicateSampleMissingRate{$DuplicateInds[$iInd]}{$BatchNameList[$iBatch]};
      }
     }
     #print "@UsedBatches\n";
    # print "@IndividualsMissingRates\n";
     if($IndividualsMissingRates[0]>=$IndividualsMissingRates[1])
     {
       $DuplicateSample_BatchSelected{$DuplicateInds[$iInd]}=$UsedBatches[1];
     }
    else
    {
     $DuplicateSample_BatchSelected{$DuplicateInds[$iInd]}=$UsedBatches[0];
    }
   print "$DuplicateInds[$iInd]\t$DuplicateSample_BatchSelected{$DuplicateInds[$iInd]}\n";#All dulicate sample select Eli-sw56, because the batch make them produce the variants with smaller missing rate 
 }

  print "Sample Size:$SwedenSampleSize\n";

  my %AllInd_Geno=();
  my %AllInd_Batch=();
  for(my $iBatch=0;$iBatch<@BatchNameList;$iBatch++)
  {
       my %EveryIndMissingrate=();
     my $iLineNumber=0;
     my $BatchBasedMissingRate_HetorateFile="/data/ruderferlab/projects/sweden/data/geno/sweden_merged_$BatchNameList[$iBatch]MissingHeterogenotypeRate.txt";
     open(INPUT,$BatchBasedMissingRate_HetorateFile) || die "can’t open $BatchBasedMissingRate_HetorateFile";
     while (<INPUT>)
     {
        if($iLineNumber==0)
        {
          $iLineNumber++;
          next;
         }
         chomp;
         my @cols=split(/\s++/,$_);
         $EveryIndMissingrate{$cols[0]}=$cols[2];
         $AllInd_Batch{$cols[0]}=$BatchNameList[$iBatch];
      }  
      close INPUT;

     my %RemovedInds=();
        $iLineNumber=0;
#    73 /data/ruderferlab/projects/sweden/data/geno/sweden_merged_Eli-s234.genome
#  122 /data/ruderferlab/projects/sweden/data/geno/sweden_merged_Eli-sw56.genome
#    8 /data/ruderferlab/projects/sweden/data/geno/sweden_merged_Eli-swe1.genome
       my $GenomeIBDFile="/data/ruderferlab/projects/sweden/data/geno/sweden_merged_$BatchNameList[$iBatch].genome";
       open(INPUT,$GenomeIBDFile) || die "can’t open $GenomeIBDFile";
       while (<INPUT>)
       {       
        chomp;  
         if($iLineNumber==0)
         {       
          $iLineNumber++;
          next;   
         }       
         $_ =~ s/^\s+|\s+$//g;
         my @cols=split(/\s++/,$_);
         if($EveryIndMissingrate{$cols[1]}>=$EveryIndMissingrate{$cols[3]})
         {
            $RemovedInds{$cols[1]}=$EveryIndMissingrate{$cols[1]};
         }
        else
          {
            $RemovedInds{$cols[3]}=$EveryIndMissingrate{$cols[3]};
          }
        $iLineNumber++;
      }       
      close INPUT; 
      my $iRemovedSampleSize=keys %RemovedInds;
      print "$iLineNumber\tRemoved samples size:$iRemovedSampleSize\n";
     
        my $IndInfoFile="/data/ruderferlab/projects/sweden/data/geno/sweden_merged_$BatchNameList[$iBatch].fam";
       open(INPUT,$IndInfoFile) || die "can’t open $IndInfoFile";
       while (<INPUT>)
       {
        chomp;
        my @cols=split(/\s++/,$_);
          $cols[1] =~ s/^\s+|\s+$//g;
        
         next if(!defined($SwedenSamples_EUR{$cols[1]})); #Remove non-CEU population samples
         next if(defined($RemovedInds{$cols[1]}));#Remove high correlated Individuals
         next if(defined($DuplicatedInds{$cols[1]}) && $iBatch!=1);#remove low quality duplicate individuals
         if(!defined($AllInd_Geno{$cols[1]}))
         {
         $AllInd_Geno{$cols[1]}=1;
         }
        else
         {
          $AllInd_Geno{$cols[1]}++;
         }
      }
      close INPUT;
   }


       my $Genotyping_ExomeSeqSharedIndsListFile="/data/ruderferlab/projects/sweden/data/geno/sweden_Exome_GenotypedShasredIndList.txt";
       open OUT, ">$Genotyping_ExomeSeqSharedIndsListFile" or die "Can't open Output file:$Genotyping_ExomeSeqSharedIndsListFile!";
       my $size=keys %AllInd_Geno;
       print "Genotyping inds: $size\n";
       my %OverlappInd=();
       my $iLineNumber=0;
       my $iDuplicatedInds=0;
       my $IndInfoFromExomFile="/data/ruderferlab/projects/sweden/data/exome/SwedanExomeSamples.txt";
       open(INPUT,$IndInfoFromExomFile) || die "can’t open $IndInfoFromExomFile";
       while (<INPUT>)
       {
       if($iLineNumber==0)
       {
        $iLineNumber++;
        next;
       }
        chomp;
       $_=~ s/^\s+|\s+$//g;
        my @cols=split(/\s++/,$_);
         if(defined($AllInd_Geno{$cols[0]}))
         {
          if($AllInd_Geno{$cols[0]}==2)
          {
            $iDuplicatedInds++;
           }  
         if(!defined($OverlappInd{$cols[0]}))
           {
             $OverlappInd{$cols[0]}=1;
           }
           else
           {
            $OverlappInd{$cols[0]}++;
            print "$cols[0]\n";
            }
         }
       $iLineNumber++; 
     }
      close INPUT;
     $size=keys %OverlappInd;
   print  OUT "SampleID\tBatch\tDuplicateStatus\n";
   print "SwedanExomeSamples size:$iLineNumber\t$iDuplicatedInds\t  Overlapped inds: $size\n";
   foreach my $Ind (keys %OverlappInd)
   {
    if(defined($DuplicateSample_BatchSelected{$Ind}))
     {
       print OUT "$Ind\tEli-sw56\t1\n";
     }
     else
     {
       print OUT "$Ind\t$AllInd_Batch{$Ind}\t0\n";
      }
    }
  close OUT;
=cut;
      my $HighClosedPairIndsFile="/data/ruderferlab/projects/sweden/data/geno/sweden_merged_HighClosedPairInds.txt";
       open OUT1, ">$HighClosedPairIndsFile" or die "Can't open Output file:$HighClosedPairIndsFile!";
 
 my @Batches=("Eli-swe1","Eli-sw56","El234");
  for(my $iBatch=0;$iBatch<@BatchNameList;$iBatch++)
  { #sweden_merged_El234new.het
 
       my $BatchBasedMissingRate_Hetorate="/data/ruderferlab/projects/sweden/data/geno/sweden_merged_$BatchNameList[$iBatch]MissingHeterogenotypeRate.txt";
       open OUT, ">$BatchBasedMissingRate_Hetorate" or die "Can't open Output file:$BatchBasedMissingRate_Hetorate!";
      
        my $iLineNumber=0;
        my %AllIndHerozygoteRate=();
       my $IndInfoFile="/data/ruderferlab/projects/sweden/data/geno/sweden_merged_$Batches[$iBatch]new.het";
       open(INPUT,$IndInfoFile) || die "can’t open $IndInfoFile";
       while (<INPUT>)
       {
        chomp;
         if($iLineNumber==0)
         {
          $iLineNumber++;      
          next;
         }
         $_ =~ s/^\s+|\s+$//g;
         my @cols=split(/\s++/,$_);
         my $fHetRate=sprintf("%.4f",($cols[4]-$cols[2])/$cols[4]);
         $AllIndHerozygoteRate{$cols[1]}=$fHetRate;
      }
      close INPUT;
    
        $iLineNumber=0;
        my %AllIndMissingRate=();
       my $IndMissingFile="/data/ruderferlab/projects/sweden/data/geno/sweden_merged_$Batches[$iBatch]new.imiss";
       open(INPUT,$IndMissingFile) || die "can’t open $IndMissingFile";
       while (<INPUT>)
       {
        chomp;
         if($iLineNumber==0)
         {
          print OUT "SampleID\tHerozygoteRate\tMissingRate\n";
          $iLineNumber++;
          next;
         }
         $_ =~ s/^\s+|\s+$//g;
         my @cols=split(/\s++/,$_);
         my $fMissingRate=$cols[5];
         $AllIndMissingRate{$cols[1]}=$cols[5];
        print OUT "$cols[1]\t$AllIndHerozygoteRate{$cols[1]}\t$AllIndMissingRate{$cols[1]}\n"
      }
      close INPUT; 
      close  OUT;
        

       $iLineNumber=0;
       my $GenomeIBDFile="/data/ruderferlab/projects/sweden/data/geno/sweden_merged_$BatchNameList[$iBatch].genome";
       open(INPUT,$GenomeIBDFile) || die "can’t open $GenomeIBDFile";
       while (<INPUT>)
       {   
        chomp;
         if($iLineNumber==0)
         {  
          if($iBatch==0)
          { 
            print OUT1 "SampleID1\tSampleID2\t$BatchNameList[$iBatch]\tPI_HAT\n";
          }
          $iLineNumber++;
          next;
         }   
         $_ =~ s/^\s+|\s+$//g;
         my @cols=split(/\s++/,$_);
        if($cols[9]>=0.8)
        {
        print OUT1 "$cols[1]\t$cols[3]\t$BatchNameList[$iBatch]\t$cols[9]\n";
        }
      }   
      close INPUT; 
   }
       close  OUT1;
=cut; 

