#!/usr/bin/perl -w
use Scalar::Util qw(looks_like_number);
 use strict;

 use lib "/home/hanl3/perl5/module/lib/site_perl/5.14.2";
 use Text::CSV_PP;
  use Tie::File; 
 use POSIX qw(WNOHANG);
 use IO::Uncompress::Gunzip qw($GunzipError);
 use warnings;
 use Scalar::Util qw(looks_like_number);

#/data/ruderferlab/projects/sweden/data/geno/sweden_merged_Eli-s234.fam  sweden_merged_Eli-sw56.fam  sweden_merged_Eli-swe1.fam


#Eli-s234  Eli-sw56  Eli-swe1

my @ChromosomeBasedDir=("Eli_sw1","Eli_sw56","Eli_s234");
 my @BatchNameList=("Eli-swe1","Eli-sw56","Eli-s234");

  my %AllInd_Source=();
  my %AllInd_Code=();
  for(my $iBatch=0;$iBatch<@BatchNameList;$iBatch++)
  {
       my $IndInfoFile="/data/ruderferlab/projects/sweden/data/geno/sweden_merged_$BatchNameList[$iBatch].fam";
       open(INPUT,$IndInfoFile) || die "can’t open $IndInfoFile";
       while (<INPUT>)
       {
         chomp;
         $_ =~ s/^\s+|\s+$//g;      
         my @cols=split(/\s++/,$_);
         $AllInd_Source{$cols[1]}=$BatchNameList[$iBatch];
         $AllInd_Code{$cols[1]}=$iBatch+1;
      }
      close INPUT;
   }

  my @Ancestries=("CEU","CHB","YRI");
  for(my $iAncestry=0;$iAncestry<@Ancestries;$iAncestry++) 
  {
     my $IndfoFile="/fs0/1000_Phase3/CEU_CHB_YRI_subsets/ALL.chr1-22.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes_$Ancestries[$iAncestry].fam";
     open(INPUT,$IndfoFile) || die "can’t open $IndfoFile";
       while (<INPUT>)
       {
         chomp;
         $_ =~ s/^\s+|\s+$//g;
         my @cols=split(/\s++/,$_);
         $AllInd_Source{$cols[1]}=$Ancestries[$iAncestry];
         $AllInd_Code{$cols[1]}=$iAncestry+4;
        }
      close INPUT;
   }


  my $PCAanalysis_AncestryFile="/data/ruderferlab/projects/cmc/scratch/sweden/SwedenPCAAnalysis/SwedenFull_YRI_CHB_CEU.eigenvec_Ancestry";
  open OUT, ">$PCAanalysis_AncestryFile" or die "Can't open Output file:$PCAanalysis_AncestryFile!"; 
   print OUT "SampleID\tPC1\tPCR2\tSource\tCode\n";
   my $PCAAnalysisFile="/data/ruderferlab/projects/cmc/scratch/sweden/SwedenPCAAnalysis/SwedenFull_YRI_CHB_CEU.eigenvec";
   open(INPUT,$PCAAnalysisFile) || die "can’t open $PCAAnalysisFile";
   while (<INPUT>)
   {
      chomp;
       $_ =~ s/^\s+|\s+$//g;
       my @cols=split(/\s++/,$_);
      print OUT "$cols[1]\t$cols[2]\t$cols[3]\t$AllInd_Source{$cols[1]}\t$AllInd_Code{$cols[1]}\n";   
    }
    close INPUT;
    close OUT; 
